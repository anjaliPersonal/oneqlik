import { Injectable } from '@angular/core';
import { delay, share } from 'rxjs/operators';
import { Country, Port, VehicleList } from '../types';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URLs } from 'src/app/app.model';
import { AuthService } from 'src/app/auth/auth.service';

@Injectable()
export class PortService {
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  userData: any = {};
  private vehicleLists: VehicleList[] = [];
  constructor(
    private http: HttpClient,
    private constUrl: URLs,
    private authService: AuthService, ) {
    this.getToken();
  }
  
  getToken() {
    this.authService.getTokenData().subscribe(data => {
      this.userData = data;
      console.log("dashboard data=> ", this.userData);

      this.getVehicleList(this.userData).subscribe(respData => {
        // console.log("respData: ",respData);
        this.vehicleLists = JSON.parse(JSON.stringify(respData)).devices;
        console.log("vehicleLists: ", this.vehicleLists);
      })
    })
  }

  getVehicleList(data) {
    return this.http.get(this.constUrl.mainUrl + 'devices/getDeviceByUserDropdown?id=' + data._id + '&email=' + data.email, { headers: this.headers })
      .pipe();
  }

  private vehicleListsObservable: Observable<VehicleList[]>;

  getVehicleLists(page?: number, size?: number): VehicleList[] {
    let vehicleLists = [];

    this.vehicleLists.forEach(vehicle => {
      vehicleLists.push(vehicle);
    });

    if (page && size) {
      vehicleLists = vehicleLists.slice((page - 1) * size, ((page - 1) * size) + size);
    }
    return vehicleLists;
  }

  getVehiclesAsync(page?: number, size?: number, timeout = 1000): Observable<VehicleList[]> {
    if (this.vehicleListsObservable) {
      return this.vehicleListsObservable;
    }

    this.vehicleListsObservable = new Observable<VehicleList[]>(observer => {
      observer.next(this.getVehicleLists(page, size));
      observer.complete();
    }).pipe(
      delay(timeout),
      share()
    );
  }

  filterVehicleLists(vehicleLists: VehicleList[], text: string): VehicleList[] {
    return vehicleLists.filter(veh => {
      return veh.Device_Name.toLowerCase().indexOf(text) !== -1;
    });
  }

}

import { Component, OnInit } from '@angular/core';
// import { AppService } from '../app.service';
import { AuthService } from '../auth/auth.service';
import { URLs } from '../app.model';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: [
    './styles/notifications.page.scss',
    './styles/notifications.shell.scss'
  ],
})
export class NotificationPage implements OnInit {
  notifications: any = {};
  userData: any;

  constructor(
    // private appService: AppService,
    // private route: ActivatedRoute,
    private authService: AuthService,
    private constUrl: URLs,
    private loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.getToken();
  }

  getToken() {
    this.authService.getTokenData().subscribe(data => {
      this.userData = data;
      console.log("dashboard data=> ", this.userData);
      this.getNotifications();
    })
  }

  getNotifications() {
    this.notifications.today = [];
    this.notifications.yesterday = [];
    this.notifications.other = [];

    var todayDate = new Date();
    // subtract one day from current time                           
    var yesterdayDate = todayDate.setDate(todayDate.getDate() - 1);
    var todaysDate = new Date();

    var url = this.constUrl.mainUrl + "notifs/getNotifiLimit?user=" + this.userData._id + "&pageNo=" + 1 + "&size=" + 500;
    this.loadingController.create({
      message: 'Loading notifications...'
    }).then(loadEl => {
      loadEl.present();
      this.authService.getMethod(url)
        .subscribe(respData => {
          loadEl.dismiss();
          let variable = JSON.parse(JSON.stringify(respData));
          for (var a = 0; a < variable.length; a++) {
            if (new Date(variable[a].timestamp).setHours(0, 0, 0, 0) === todaysDate.setHours(0, 0, 0, 0)) {
              this.notifications.today.push(variable[a]);
            } else if (new Date(variable[a].timestamp).setHours(0, 0, 0, 0) === new Date(yesterdayDate).setHours(0, 0, 0, 0)) {
              this.notifications.yesterday.push(variable[a]);
            }
            else {
              this.notifications.other.push(variable[a]);
            }
          }
          // this.notifications = respData;
          console.log("notifs: ", this.notifications)
        },
          err => {
            loadEl.dismiss();
          });
    });
  }

}

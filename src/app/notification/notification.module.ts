import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NotificationPage } from './notification.page';
import { ComponentsModule } from '../tabs/components/components.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild([
      {
        path: '',
        component: NotificationPage,
        // resolve: {
        //   data: NotificationsResolver
        // }
      }
    ])
  ],
  declarations: [NotificationPage],
})
export class NotificationPageModule {}

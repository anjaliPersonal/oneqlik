import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-notif-detail',
  templateUrl: './notif-detail.page.html',
  styleUrls: ['./notif-detail.page.scss'],
})
export class NotifDetailPage implements OnInit {

  notifType: string;

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('notifId')) {
        this.navCtrl.navigateBack('/notification');
        return;
      }
      this.notifType = paramMap.get('notifId');
      console.log('notifType: ' + this.notifType);
    });
  }

}

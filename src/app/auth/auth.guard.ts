import { AuthService } from './auth.service';
import { Router, Route, UrlSegment, CanLoad } from '@angular/router';
import { Observable, of } from 'rxjs';
import { take, tap, switchMap } from 'rxjs/operators';

export class AuthGuard implements CanLoad {
    constructor(
        private authService: AuthService,
        private route: Router
    ) {

    }

    canLoad(
        route: Route,
        segments: UrlSegment[]
    ): Observable<boolean> | Promise<boolean> | boolean {
        return this.authService.userIsAuthenticated.pipe(
            take(1), 
            switchMap(isAuthenticated => {
                if (!isAuthenticated) {
                    return this.authService.autoLogin();
                } else {
                    return of(isAuthenticated);
                }
            }),
            tap(isAuthenticated => {
            if (!isAuthenticated) {
                this.route.navigateByUrl('/auth');
            }
        }));
    }
}
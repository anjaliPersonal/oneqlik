import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, ToastController, ModalController, AlertController, MenuController } from '@ionic/angular';
import { AuthService, AuthResponseData } from './auth.service';
import { VerifyOTPComponent } from './verify-otp/verify-otp.component';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  isLogin = true;
  isLoading = false;
  constructor(
    private router: Router,
    private loadingCtrl: LoadingController,
    private authService: AuthService,
    private toastController: ToastController,
    private modalController: ModalController,
    private alertController: AlertController,
    private menuCtrl: MenuController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }
  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const pass = form.value.password;
    const fname = form.value.fname;
    const mobno = form.value.mobno;
    const userid = form.value.userid;
    
    console.log(email, pass);
    this.authenticate(fname, mobno, email, pass, userid);
    form.reset();
  }

  showTostMsg(msg) {
    this.toastController.create({
      message: msg,
      duration: 1800,
      position: 'bottom'
    }).then(toastEl => {
      toastEl.present();
    });
  }

  onSwitchAuthMode() {
    this.isLogin = !this.isLogin;
  }

  authenticate(fname: string, mobno: number, email: any, pass: string, userid: string) {
    this.isLoading = true;
    ////////
    function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }

    var isEmail = validateEmail(email);
    var isName = isNaN(email);
    var strNum;


    if (isName == false) {
      strNum = email.trim();
    }
    var data;
    if (isEmail == false && isName == false && strNum.length == 10) {
      data = {
        "psd": pass,
        "ph_num": email
      };
    }
    else if (isEmail) {
      data = {
        "psd": pass,
        "emailid": email
      };
    }
    else {
      data = {
        "psd": pass,
        "user_id": email
      };
    }
    ////////
    this.loadingCtrl.create({
      keyboardClose: true, message: 'Loging in...'
    }).then(loadingEl => {
      loadingEl.present();
      let authObs: Observable<AuthResponseData>;
      if (this.isLogin) {
        authObs = this.authService.login(data);
      } else {
        authObs = this.authService.signup(fname, mobno, email, pass, userid);
      }
      authObs.subscribe(respData => {
        loadingEl.dismiss();
        this.isLoading = false;
        if (respData.message === 'Email ID or Mobile Number already exists') {
          this.showTostMsg(respData.message);
          return;
        }
        if (respData.message === 'OTP sent successfully') {
          this.modalController.create({
            component: VerifyOTPComponent,
            componentProps: {
              phonenumber: mobno
            }
          }).then(modelEl => {
            modelEl.present();
          });
          return;
        }
        this.showTostMsg('Welcome, you have logged in successfully!');
        this.router.navigateByUrl('/maintabs/tabs/dashboard');
      },
        error => {
          loadingEl.dismiss();
          console.log(error.error.message);
          let msg = "Could not sign you up. Please try again.";
          if(error.error.message) {
            msg = error.error.message;
          }
          this.showAlert(msg);
        });
    });
  }

  private showAlert(message: string) {
    this.alertController.create({
      header: 'Authentication failed',
      message: message,
      buttons: [{
        text: 'Okay',
        cssClass: 'secondary',
        handler: () => {
          console.log('Confirm Okay');
        }
      }]
    }).then(alertEl => alertEl.present());
  }

  handleFirstNameValue(event) {
    let firstName = event.target.value;
  }


}

export class User {
    constructor(
        public _token: string
        // _id: string,
        // email: string,
        // phn: string,
        // orgName: string,
        // fn: string,
        // ln: string,
        // isDealer: boolean,
        // isOrganisation: boolean,
        // role: string,
        // exp: number,
        // status: boolean,
        // isSuperAdmin: boolean,
        // isOperator: boolean,
        // fuel_unit: string,
        // language_code: string,
    ) { }

    get token() {
        return this._token;
    }
}
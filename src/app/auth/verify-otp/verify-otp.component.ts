import { Component, OnInit } from '@angular/core';
import { NavParams, ToastController, ModalController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-verify-otp',
  templateUrl: './verify-otp.component.html',
  styleUrls: ['./verify-otp.component.scss'],
})
export class VerifyOTPComponent implements OnInit {
  phoneNumber: string;

  constructor(
    private navParams: NavParams,
    private authService: AuthService,
    private toastController: ToastController,
    private router: Router,
    private modalController: ModalController
  ) {
    console.log(navParams.get('phonenumber'))
    this.phoneNumber = navParams.get('phonenumber');
  }

  ngOnInit() { }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const otp = form.value.otp;
    this.authService.sendOtp(this.phoneNumber, otp).subscribe(resData => {
      console.log(resData);
      if (resData.message === "Didn't match otp") {
        this.showTostMsg(resData.message);
        return;
      }
      this.router.navigateByUrl('auth');
    },
    err => {
      console.log(err);
      this.showTostMsg(err.message);
      this.modalController.dismiss();
    })
  }

  showTostMsg(msg) {
    this.toastController.create({
      message: msg,
      duration: 1800,
      position: 'bottom'
    }).then(toastEl => {
      toastEl.present();
    });
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, from } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { User } from './user.model';
import { Plugins } from '@capacitor/core';
import { URLs } from '../app.model';
import { Router } from '@angular/router';

export interface AuthResponseData {
  message?: string;
  token?: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _user = new BehaviorSubject<User>(null);

  constructor(
    private http: HttpClient,
    private constURL: URLs,
    private router: Router
  ) { }

  autoLogin() {
    return from(Plugins.Storage.get({ key: 'authData' })).pipe(map(storedData => {
      if (!storedData || !storedData.value) {
        return null;
      }
      const parsedData = JSON.parse(storedData.value) as { token: string; };
      const user = new User(parsedData.token);
      return user;
    }),
      tap(user => {
        if (user) {
          this._user.next(user);
        }
      }),
      map(user => {
        return !!user;
      })
    );
  }

  get userIsAuthenticated() {
    return this._user.asObservable().pipe(map(user => {
      if (user) {
        return !!user.token;
      } else {
        return false;
      }
    }))
  };

  private storeAuthData(token: string) {
    const tokenS = JSON.stringify(token);
    Plugins.Storage.set({ key: 'authData', value: tokenS })
  };

  private setUserData(userData: AuthResponseData) {
    this._user.next(
      new User(userData.token)
    );
    this.storeAuthData(userData.token);
  }

  login(data) {
    return this.http
      .post<AuthResponseData>(
        this.constURL.mainUrl + 'users/LoginWithOtp',
        data
      )
      .pipe(tap(this.setUserData.bind(this)));
  }

  logout() {
    this._user.next(null);
    Plugins.Storage.clear();
    this.router.navigateByUrl('/auth');
  }

  signup(fname: string, mobno: number, email: string, pass: string, userid: string) {
    return this.http
      .post<AuthResponseData>(
        this.constURL.mainUrl + 'users/signUp',
        {
          "first_name": fname,
          "last_name": '',
          "org_name": null,
          "email": email,
          "password": pass,
          "user_id": userid,
          "phone": String(mobno),
          "isDealer": false,
          "custumer": true,
          "supAdmin": "59cbbdbe508f164aa2fef3d8",
          "imageDoc": []
        })
      .pipe(tap(this.setUserData.bind(this)));
  }

  sendOtp(mobno: string, otp: number) {
    return this.http.post<AuthResponseData>(this.constURL.mainUrl + 'users/signUp', {
      phone: mobno,
      otp: otp
    })
  }

  getTokenData() {
    return from(Plugins.Storage.get({ key: 'authData' })).pipe(map(storedToken => {
      if (!storedToken || !storedToken.value) {
        return null;
      }
      const obj = JSON.parse(
        window.atob(
          JSON.parse(JSON.stringify(storedToken)).value.split(".")[1]
        )
      );
      return obj;
    }));
  }

  postMethod(url: string, payload: any = {}) {
    return this.http.post(url, payload).pipe(tap(resData => {
      console.log(resData);
    }));
  }

  getMethod(url: string) {
    return this.http.get(url).pipe(tap(resData => {
      console.log(resData);
    }));
  }
}

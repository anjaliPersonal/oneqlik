import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URLs } from './app.model';
import { Observable } from 'rxjs';
import { finalize, tap, delay } from 'rxjs/operators';
import { Report } from './report/report.model';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  userData: any;
  constructor(
    private http: HttpClient,
    private constUrl: URLs,
  
  ) {
  }
  get reports() {
    return [...this._reports];
  }
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  private _reports: Report[] = [
    new Report('Daily', 'daily'),
    new Report('Distance', 'dist'),
    new Report('Summary', 'summ'),
    new Report('Overspeed', 'ospeed'),
    new Report('AC', 'ac'),
    new Report('Route Violation', 'routevio'),
    new Report('Speed Variation', 'speedvar'),
    new Report('Trip', 'trip'),
    new Report('Geofence', 'geo'),
    new Report('Idle', 'idle'),
    new Report('Daywise', 'daywise'),
    new Report('Ignition', 'ign'),
    new Report('SOS', 'sos'),
    new Report('Stoppage', 'stop'),
    new Report('POI', 'poi'),
    new Report('Value', 'value')
  ];

  getData(): Observable<any> {
    console.log("appservice data=> ", this.userData);
    const dataObservable = this.http.get<any>(this.constUrl.mainUrl + 'users/getCustomer?uid=59cbbdbe508f164aa2fef3d8&pageNo=1&size=10')
      .pipe(
        tap(val => {
          console.log('getData STARTED');
        }),
        delay(1000),
        finalize(() => {
          console.log('getData COMPLETED');
        })
      );
    return dataObservable;
  }

  getNotifTypes() {
    return this.http.get(this.constUrl.mainUrl + 'notifs/getTypes', { headers: this.headers })
      .pipe();
  }

  getReportTypes(key: string) {
    // return this.http.get('../assets/json/reports-data.json').pipe();
    return { ...this._reports.find(r => r.key === key) };
  }

  getAddress(cord) {
    return this.http.post(this.constUrl.mainUrl + "googleAddress/getGoogleAddress", cord, { headers: this.headers })
      .pipe();
  }

  // getData() {
  //   // var baseURLp = 'https://www.oneqlik.in/users/getCust?uid=' + this.islogin._id + '&pageNo=' + this.page + '&size=' + this.limit;
  //   this.http.get(this.constUrl.mainUrl + '/users/getCustomer?uid=59cbbdbe508f164aa2fef3d8&pageNo=1&size=10', {headers: this.headers})

  // }
}

import { Injectable } from '@angular/core';
import { LanguageModel } from './language.model';

@Injectable()
export class LanguageService {
  languages: Array<LanguageModel> = new Array<LanguageModel>();

   constructor() {
     this.languages.push(
      { name: 'English', code: 'en' },
      { name: 'Spanish', code: 'es' },
      { name: 'Hindi', code: 'hi' },
      { name: 'Marathi', code: 'mr' },
      { name: 'Telugu', code: 'te' },
      { name: 'Tamil', code: 'ta' },
      { name: 'Persian', code: 'fa' },
      { name: 'Kannada', code: 'kn' },
      { name: 'Bangali', code: 'bn' },
      { name: 'Malayalam', code: 'ml' },
      { name: 'Gujarati', code: 'gu' },
     );
   }

   getLanguages() {
     return this.languages;
   }

 }

import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Report } from './report.model';

@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {
  loadedReports: Report[];
  constructor(
    private appService: AppService
  ) { }

  ngOnInit() {
    this.loadedReports = [...this.appService.reports];
  }

  // getRepType() {
  //   this.appService.getReportTypes().subscribe((data) => {
  //     console.log("report types ", data);
  //   });
  // }

}

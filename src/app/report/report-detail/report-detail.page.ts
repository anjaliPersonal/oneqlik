import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { AppService } from 'src/app/app.service';
import { Report } from '../report.model';
import { AuthService } from 'src/app/auth/auth.service';
import { URLs } from 'src/app/app.model';
import * as moment from 'moment';

@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.page.html',
  styleUrls: ['./report-detail.page.scss'],
})
export class ReportDetailPage implements OnInit {
  report: Report;
  userData: any = {};
  pageNo: number = 0;
  from: any;
  to: any;
  vehicleData: any;
  limit: number = 10;
  reportData: any = [];
  datetimeFrom: string;
  datetimeTo: string;
  vehObj: any = [];
  msgString: string;
  messageToSendP: any;
  showString: string;
  locationEndAddress: any;
  minTime: number = 5;
  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private appService: AppService,
    private authService: AuthService,
    private constUrl: URLs,
    private loadingController: LoadingController,
    private toastCtrl: ToastController,
  ) {
    console.log("vehonj", this.vehObj);
    this.getToken();
    this.from = moment({ hours: 0 }).format();
    this.to = moment().format();//new Date(a).toISOString();

  }

  ngOnInit() {
    console.log("vehonj", this.vehObj);
    this.route.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('reportId')) {
        this.navCtrl.navigateBack('/report');
        return;
      }
      this.report = this.appService.getReportTypes(paramMap.get('reportId'));
      this.messageToSendP = this.report.key;
      if (this.report.key === 'summ' || this.report.key === 'dist') {
        this.datetimeFrom = moment({ hours: 0 }).subtract(1, 'days').format(); // yesterday date with 12:00 am
        this.datetimeTo = moment({ hours: 0 }).format(); // today date and time with 12:00am
      } else {
        this.datetimeFrom = moment({ hours: 0 }).format();
        this.datetimeTo = moment().format();//new Date(a).toISOString();
      }
    });
  }

  exampleMethodParent($event) {
    this.datetimeFrom = $event;
    //this.getGlobalreportData();
  }

  exampleMethodParent1($event) {
    this.datetimeTo = $event;
    //this.getGlobalreportData();
  }

  getVehObj($event) {
    debugger
    this.vehObj = $event;
    console.log("vecobj", this.vehObj);
    this.getGlobalreportData();
    // this.getIdleReportData();
    this.getDailyReportData();
    //console.log("vehobj", this.vehObj);
  }


  getToken() {
    this.authService.getTokenData().subscribe(data => {
      this.userData = data;
      if (this.report.key === 'daily') {
        this.msgString = "Loading daily report...";
        this.getDailyReportData();
      }
      else if(this.report.key === 'value') {
        this.msgString = "Loading value screen...";
        this.getDailyReportData();
      }
      else if (this.report.key === 'idle') {
        this.msgString === 'Loading Idle Report...',
        this.getIdleReportData();
      } else if (this.report.key === 'summ') {
        this.msgString = "Loading summary report...";
        this.getGlobalreportData();
      } else if (this.report.key === 'ospeed') {
        this.msgString = "Loading overspeed report...";
        this.getGlobalreportData();
      } else if (this.report.key === 'daywise') {
        this.msgString = 'Loading Overspeed Report...';
        //this.getGlobalreportData();
      } else if (this.report.key === 'dist') {
        this.msgString = "Loading distance report..";
        this.getGlobalreportData();
      } else if (this.report.key === 'trip') {
        this.msgString = "Loading trip report..";
        this.getGlobalreportData();
      } else if (this.report.key === 'ign') {
        this.msgString = "Loading ignition report..";
        this.getGlobalreportData();
      } else if (this.report.key === 'sos') {
        this.msgString = "Loading sos report..";
        this.getGlobalreportData();
      } else if (this.report.key === 'stop') {
        this.msgString = 'Loading stoppage report';
        this.getGlobalreportData();
      } else if (this.report.key === 'poi') {
        this.msgString = 'Loading Poi Report'
        this.getGlobalreportData();
      } else if (this.report.key === 'ac') {
        this.msgString = 'Loading AC Report'
        this.getGlobalreportData();
      }
    })
  }

  changeDate(key) {
    this.datetimeFrom = undefined;
    if (key === 'today') {
      this.datetimeFrom = moment({ hours: 0 }).format();
    } else if (key === 'yest') {
      this.datetimeFrom = moment().subtract(1, 'days').format();
    } else if (key === 'week') {
      this.datetimeFrom = moment().subtract(1, 'weeks').endOf('isoWeek').format();
    } else if (key === 'month') {
      this.datetimeFrom = moment().startOf('month').format();
    }
  }

  getIdleReportData() {
  console.log("entered");
  debugger;
  var vehId = [];
  if (this.report.key === 'idle') {
    for (var t = 0; t < this.vehObj.length; t++) {
    vehId.push(this.vehObj[t]._id);
    }
  }
  console.log("id", vehId)
  // if (vehId.length === 0) {
  //   this.toastCtrl.create({
  //     message: 'Please select vehicle and try again..',
  //     duration: 1500,
  //     position: 'bottom'
  //   })
  //   return;
  // }
  // console.log("id", vehId);
  var burl;
  burl = this.constUrl.mainUrl + 'stoppage/idleReportdatatable';
  var payload = {
    "draw": 3,
    "columns": [
      {
        "data": "_id"
      },
      {
        "data": "device"
      },
      {
        "data": "device.Device_Name"
      },
      {
        "data": "start_time"
      },
      {
        "data": "end_time"
      },
      {
        "data": "lat"
      },
      {
        "data": "long"
      },
      {
        "data": "ac_status"
      },
      {
        "data": "idle_time"
      },
      {
        "data": "address"
      },
      {
        "data": null,
        "defaultContent": ""
      }
    ],
    "order": [
      {
        "column": 0,
        "dir": "asc"
      }
    ],
    "start": 0,
    "length": 10,
    "search": {
      "value": "",
      "regex": false
    },
    "op": {},
    "select": [],
    "find": {
      "device": {
        "$in": vehId

      },
      "start_time": {
        "$gte": new Date(this.datetimeFrom).toISOString()
      },
      "end_time": {
        "$lte": new Date(this.datetimeTo).toISOString()
      },
      "idle_time": {
        "$gte": (this.minTime * 60000)
      }
    }
  };
  this.reportData = [];

  this.loadingController
    .create({
      message: this.msgString
    })
    .then(loadEl => {
      loadEl.present();
      this.authService.postMethod(burl, payload).subscribe(respData => {
        loadEl.dismiss();
        console.log("idleReportData", respData);
        debugger;
        var data = JSON.parse(JSON.stringify(respData))
        let outerthis = this;
        var i = 0, howManyTimes = data.length;
        function f() {
          for( var i = 0; i < data.data.length; i++ ) {
          outerthis.reportData.push(
            {
              'Device_Name': data.data[i].device.Device_Name,
              'start_location': {
                'lat': data.data[i].lat,
                'long': data.data[i].long
              },
              'ac_status': (data.data[i].ac_status ? data.data[i].ac_status : 'NA'),
              'end_time': data.data[i].end_time,
              'duration': outerthis.parseMillisecondsIntoReadableTime(data.data[i].idle_time),
              'start_time': data.data[i].start_time
            });
            outerthis.start_address(outerthis.reportData[i], i);
        }
                  i++;
          if (i < howManyTimes) {
            setTimeout(f, 100);
          }
        }
        f();
        console.log("Final idleReportData", outerthis.reportData);
      });
    });
  }

  parseMillisecondsIntoReadableTime(milliseconds) {
    //Get hours from milliseconds
    var hours = milliseconds / (1000 * 60 * 60);
    var absoluteHours = Math.floor(hours);
    var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    // return h + ':' + m;
    return h + ':' + m + ':' + s;
  }

  getDailyReportData() {
    console.log("entered");
    var vehId = [];
    if (this.vehObj.length === 0) {
      vehId = [];
    } else {
      if (this.report.key === 'value') {
        for(var t = 0; t < this.vehObj.length; t++) {
          vehId.push(this.vehObj[t].Device_ID);
        }
      }
    }
    this.pageNo = 0;
    var baseUrl;
    baseUrl = this.constUrl.mainUrl + "devices/daily_report";
    let that = this;
    var currDay = new Date().getDay();
    var currMonth = new Date().getMonth();
    var currYear = new Date().getFullYear();
    var selectedDay = new Date(that.to).getDay();
    var selectedMonth = new Date(that.to).getMonth();
    var selectedYear = new Date(that.to).getFullYear();
    var devname, devid, today_odo, today_running, today_stopped, t_idling, t_ofr, today_trips, maxSpeed, mileage;
    if ((currDay == selectedDay) && (currMonth == selectedMonth) && (currYear == selectedYear)) {
      devname = "Device_Name";
      devid = "Device_ID";
      today_odo = "today_odo";
      today_running = "today_running";
      today_stopped = "today_stopped";
      t_idling = "t_idling"
      t_ofr = "t_ofr";
      today_trips = "today_trips";
      maxSpeed = "maxSpeed";
      mileage = "Mileage"
    } else {
      console.log("else block called");
      devid = "imei";
      devname = "ID.Device_Name";
      today_odo = "today_odo";
      today_running = "today_running";
      today_stopped = "today_stopped";
      t_idling = "t_idling"
      t_ofr = "t_ofr";
      today_trips = "today_trips";
      maxSpeed = "ID.maxSpeed";
      mileage = "Mileage"
    }
    // debugger
    var payload = {};
    debugger;
    if (this.vehObj == undefined) {
      payload = {
        "draw": 2,
        "columns": [
          {
            "data": devname
          },
          {
            "data": devid
          },
          {
            "data": today_odo
          },
          {
            "data": today_running
          },
          {
            "data": today_stopped
          },
          {
            "data": t_idling
          },
          {
            "data": t_ofr
          },
          {
            "data": today_trips
          },
          {
            "data": maxSpeed
          },
          {
            "data": mileage
          },
          { "data": "t_running" },
          { "data": "t_stopped" },
          { "data": "t_idling" },
          { "data": "t_ofr" },
          { "data": "t_noGps" },
          {
            "data": null,
            "defaultContent": ""
          }
        ],
        "order": [
          {
            "column": 0,
            "dir": "asc"
          }
        ],
        "start": 0,
        "length": this.limit,
        "search": {
          "value": "",
          "regex": false
        },
        "op": {},
        "select": [],
        "find": {
          "user_id": this.userData._id,
          "date": new Date(this.datetimeTo).toISOString()
        }
      }
    } else {
      payload = {
        "draw": 2,
        "columns": [
          {
            "data": devname
          },
          {
            "data": devid
          },
          {
            "data": today_odo
          },
          {
            "data": today_running
          },
          {
            "data": today_stopped
          },
          {
            "data": t_idling
          },
          {
            "data": t_ofr
          },
          {
            "data": today_trips
          },
          {
            "data": mileage
          },
          {
            "data": maxSpeed
          },
          { "data": "t_running" },
          { "data": "t_stopped" },
          { "data": "t_idling" },
          { "data": "t_ofr" },
          { "data": "t_noGps" },
          {
            "data": null,
            "defaultContent": ""
          }
        ],
        "order": [
          {
            "column": 0,
            "dir": "asc"
          }
        ],
        "start": 0,
        "length": this.limit,
        "search": {
          "value": "",
          "regex": false
        },
        "op": {},
        "select": [],
        "find": {
          "user_id": this.userData._id,
          "devId": vehId,
          "date": new Date(this.datetimeTo).toISOString()
        }
      }
    }
    this.reportData = [];

    this.loadingController.create({
      message: this.msgString
    }).then(loadEl => {
      loadEl.present();
      this.authService.postMethod(baseUrl, payload)
        .subscribe(respData => {
          loadEl.dismiss();
          var data = JSON.parse(JSON.stringify(respData))

          for (var i = 0; i < data.data.length; i++) {
            // var ignOff = 86400000 - parseInt(data.data[i].today_running);
            this.reportData.push({
              _id: data.data[i]._id,
              Device_ID: data.data[i].Device_ID ? data.data[i].Device_ID : data.data[i].imei,
              Device_Name: data.data[i].Device_Name ? data.data[i].Device_Name : (data.data[i].ID ? data.data[i].ID.Device_Name : 'N/A'),
              maxSpeed: data.data[i].maxSpeed ? data.data[i].maxSpeed : (data.data[i].ID ? data.data[i].ID.maxSpeed : '0'),
              today_odo: (data.data[i].today_odo).toFixed(2),
              today_running: this.millisToMinutesAndSeconds(data.data[i].today_running),
              today_stopped: this.millisToMinutesAndSeconds(data.data[i].today_stopped),
              t_idling: this.millisToMinutesAndSeconds(data.data[i].t_idling),
              t_ofr: this.millisToMinutesAndSeconds(data.data[i].t_ofr),
              today_trips: data.data[i].today_trips,
              mileage: data.data[i].Mileage ? ((data.data[i].today_odo) / Number(data.data[i].Mileage)).toFixed(2) : 'N/A'
              // avgSpeed: this.calcAvgSpeed(odo1[0], data.data[i].today_running)
            })
          }

          console.log("daily report data: ", respData)

        }, error => {
          loadEl.dismiss();
          console.log("error in service=> " + error);
        })
    })


  }

  millisToMinutesAndSeconds(millis) {
    var ms = millis;
    ms = 1000 * Math.round(ms / 1000); // round to nearest second
    var d = new Date(ms);
    // debugger
    var min1;
    var min = d.getUTCMinutes();
    if ((min).toString().length == 1) {
      min1 = '0' + (d.getUTCMinutes()).toString();
    } else {
      min1 = min;
    }

    return d.getUTCHours() + ':' + min1;
  }
  
  getGlobalreportData() {
    debugger
    this.reportData = [];
    var vehId = [];
    console.log("vehid", vehId);
    console.log("vehobj", this.vehObj);
    if (this.vehObj.length === 0) {
      vehId = [];
    } else {
      // debugger
      if (this.report.key === 'dist') {
        vehId = this.vehObj._id;
      } else {
        if (this.report.key === 'sos') {
          vehId = this.vehObj.Device_ID;
        } else {
          if(this.report.key === 'stop') {
           for(var t = 0; t < this.vehObj.length; t++) {
             vehId.push(this.vehObj[t]._id);
           }
          }
        else {
        if (this.report.key === 'ospeed') {
          for(var t = 0; t < this.vehObj.length; t++) {
            vehId.push(this.vehObj[t].Device_ID);
          }
        } else {
          if (this.report.key === 'ign') {
            for(var t = 0; t < this.vehObj.length; t++) {
              vehId.push(this.vehObj[t].Device_ID);
            }
          } else {
            if(this.report.key === 'ac') {
              for(var t = 0; t < this.vehObj.length; t++) {
                vehId.push(this.vehObj[t].Device_ID)
              }
            }
        else {
          if (this.report.key === 'daywise') {
            for (var t = 0; t < this.vehObj.length; t++) {
              vehId.push(this.vehObj[t].Device_ID);
            }
          } else {
            for (var t = 0; t < this.vehObj.length; t++) {
              vehId.push(this.vehObj[t]._id);
            }
          }
        }
      }
      }
      }
    }
    }
    }

    this.loadingController.create({
      message: this.msgString
    }).then(loadEl => {
      loadEl.present();
      let url;
      if (this.report.key === 'summ') {
        url = this.constUrl.mainUrl + "summary/summaryReport?from=" + 
        new Date(this.datetimeFrom).toISOString() + '&to=' + new Date(this.datetimeTo).toISOString() + '&user=' + this.userData._id + '&device=' + vehId;
      } else if (this.report.key === 'ospeed') {
        url = this.constUrl.mainUrl + "notifs/overSpeedReport?from_date=" + 
        new Date(this.datetimeFrom).toISOString() + '&to_date=' + new Date(this.datetimeTo).toISOString() + '&_u=' + this.userData._id + '&device=' + vehId;
      } else if(this.report.key === 'daywise') {
        url = this.constUrl.mainUrl + 'summary/getDayWiseReport?from=' +
        new Date(this.datetimeFrom).toString() + '&to=' + new Date(this.datetimeTo).toString() + '&user=' + this.userData._id + '&device=' + vehId;
      } else if (this.report.key === 'dist') {
        url = this.constUrl.mainUrl + "summary/distance?from=" + new Date(this.datetimeFrom).toISOString() + '&to=' + new Date(this.datetimeTo).toISOString() + '&user=' + this.userData._id + '&device=' + vehId;
      } else if (this.report.key === 'trip') {
        url = this.constUrl.mainUrl + "user_trip/trip_detail?from_date=" + new Date(this.datetimeFrom).toISOString() + "&to_date=" + new Date(this.datetimeTo).toISOString() + "&uId=" + this.userData._id + "&device=" + vehId;
      } else if (this.report.key === 'ign') {
        url = this.constUrl.mainUrl + "notifs/ignitionReport?from_date=" + new Date(this.datetimeFrom).toISOString() + "&to_date=" + new Date(this.datetimeTo).toISOString() + "&_u=" + this.userData._id + "&device=" + vehId;
      } else if (this.report.key === 'sos') {
        url = this.constUrl.mainUrl + "notifs/statusReport";
      } else if (this.report.key === 'stop') {
        url = this.constUrl.mainUrl + 'stoppage/stoppageReport?from_date=' +
        new Date(this.datetimeFrom).toISOString() + '&to_date=' + new Date(this.datetimeTo).toISOString() +
        '&_u=' + this.userData._id + '&vname=' + vehId;
      } else if (this.report.key === 'ac') {
        url = this.constUrl.mainUrl + 'notifs/ACSwitchReport?from_date=' +
        new Date(this.datetimeFrom).toISOString() + '&to_date=' + new Date(this.datetimeTo).toISOString() +
        '&user=' + this.userData._id + '&device=' + vehId;
      }

      if (this.report.key !== 'sos') {
        this.func1(loadEl, url);
      } else {
        this.func2(loadEl, url, vehId);
      }

    })
  }

  func1(loadEl, url) {
    this.authService.getMethod(url)
      .subscribe(respData => {
        loadEl.dismiss();
        console.log(" report data: ", respData);
        var data = JSON.parse(JSON.stringify(respData));
        if (data.length > 0) {
          if (this.showString) {
            this.showString = undefined;
          }
          // debugger
          if (this.report.key === 'summ') {
            this.summ(data);
          } else if (this.report.key === 'ospeed') {
            this.ovspeed(data);
          } else if (this.report.key === 'daywise') {
            this.daywise(data);
          } else if (this.report.key === 'dist') {
            this.distanceRep(data);
          } else if (this.report.key === 'trip') {
            this.tripRep(data);
          } else if (this.report.key === 'ign') {
            this.ignRep(data);
          } else if (this.report.key === 'sos') {
            this.sosRep(data);
          } else if (this.report.key === 'stop') {
            this.stoppage(data);
          } else if (this.report.key == 'ac') {
            this.acRep(data);
          }
        } else {
          this.showString = "Oops.. report data not found..!";
        }
      },
        err => {
          loadEl.dismiss();
        });
  }

  func2(loadEl, url, vehId) {
    // debugger
    var payload = {};

    if (this.vehObj.length === 0) {
      payload = {
        "draw": 3,
        "columns": [
          {
            "data": "_id"
          },
          {
            "data": "device"
          },
          {
            "data": "vehicleName"
          },
          {
            "data": "timestamp"
          },
          {
            "data": "address"
          }
        ],
        "order": [
          {
            "column": 0,
            "dir": "asc"
          }
        ],
        "start": this.pageNo,
        "length": 10,
        "search": {
          "value": "",
          "regex": false
        },
        "find": {
          "user": this.userData._id,
          "type": "SOS",
          "timestamp": {
            "$gte": new Date(this.datetimeFrom).toISOString(),
            "$lte": new Date(this.datetimeTo).toISOString()
          }
        }
      }
    } else {
      payload = {
        "draw": 3,
        "columns": [
          {
            "data": "_id"
          },
          {
            "data": "device"
          },
          {
            "data": "vehicleName"
          },
          {
            "data": "timestamp"
          },
          {
            "data": "address"
          }
        ],
        "order": [
          {
            "column": 0,
            "dir": "asc"
          }
        ],
        "start": this.pageNo,
        "length": 10,
        "search": {
          "value": "",
          "regex": false
        },
        "find": {
          "user": this.userData._id,
          "type": "SOS",
          "device": vehId,
          "timestamp": {
            "$gte": new Date(this.datetimeFrom).toISOString(),
            "$lte": new Date(this.datetimeTo).toISOString()
          }
        }
      }
    }
    this.authService.postMethod(url, payload)
      .subscribe((respData) => {
        loadEl.dismiss();
        console.log(" report data: ", respData);
        var data = JSON.parse(JSON.stringify(respData));
        if (data.data.length > 0) {
          if (this.showString) {
            this.showString = undefined;
          }
          // debugger
          if (this.report.key === 'sos') {
            this.sosRep(data.data);
          }
        } else {
          this.showString = "Oops.. report data not found..!";
        }

      },
        err => {
          loadEl.dismiss();
        });
  }


  acRep(data) {
  console.log("AcReportData", data);
  let that = this;
  var i = 0, howManyTimes = data.length;
  function f() {
    that.reportData.push({
      'Device_Name': data[i].VehicleName,
      'Time_First': that.timeConvert(data[i]['1stTime']),
      'Time_Second': that.timeConvert(data[i]['2stTime']),
      'Action_First': data[i]['1stAction'],
      'Action_Second': data[i]['2stAction']
    });
    console.log("see push data", that.reportData);
    i++;
    if (i < howManyTimes) {
      setTimeout(f, 100);
    }
  }
  f();
  }
   timeConvert(min) {
    var num = min;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + " hour " + rminutes + " minute";
    }
  ovspeed(data) {
    let that = this;
    var i = 0, howManyTimes = data.length;
    function f() {
      that.reportData.push({
        'Device_Name': data[i].vehicleName,
        'overSpeed': data[i].overSpeed,
        'start_location': {
          'lat': data[i].lat,
          'long': data[i].long
        },
        'timestamp': data[i].timestamp
      });
      that.start_address(that.reportData[i], i);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  daywise(data) {
    let outerthis = this;
    var i = 0, howManyTimes = data.length;
    function f() {
      outerthis.reportData.push(
        {
          // 'Date': new Date(daywiseReport[i].Date),
          'Date': moment(data[i].Date).format('ddd MMM DD YYYY'),
          'VRN': data[i].VRN,
          'end_location': data[i].end_location,
          'start_location': data[i].start_location,
          'Distance(Kms)': data[i]["Distance(Kms)"],
          'Moving Time': data[i]['Moving Time'],
          'Stoppage Time': data[i]['Stoppage Time'],
          'Idle Time': data[i]['Idle Time']
        });

        outerthis.start_address(outerthis.reportData[i], i);
        outerthis.end_address(outerthis.reportData[i], i);
        console.log("see data here: ", outerthis.reportData);
        console.log("address", outerthis.reportData.start_location)
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  stoppage(data) {
    let outerthis = this;
    outerthis.locationEndAddress = undefined;
    var i = 0, howManyTimes = data.length;
    function f() {
      var fd = new Date(data[i].arrival_time).getTime();
      var td = new Date(data[i].departure_time).getTime();
      var time_difference = td - fd;
      var total_min = time_difference / 60000;
      var hours = total_min / 60
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      var Durations = rhours + 'hrs : ' + rminutes + 'mins';

      outerthis.reportData.push({
        'arrival_time': data[i].arrival_time,
        'departure_time': data[i].departure_time,
        'Durations': Durations,
        'device': data[i].device ? data[i].device.Device_Name : 'N/A',
        'end_location': {
          'lat': data[i].lat,
          'long': data[i].long
        }
      });

      outerthis.end_address(outerthis.reportData[i], i);
      console.log("see StopageData here: ", outerthis.reportData);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  summ(data) {
    let that = this;
    var i = 0, howManyTimes = data.length;
    function f() {

      that.reportData.push(
        {
          'Device_Name': data[i].devObj[0].Device_Name,
          'routeViolations': data[i].today_routeViolations,
          'overspeeds': that.millisToMinutesAndSeconds(data[i].today_overspeeds),
          'ignOn': that.millisToMinutesAndSeconds(data[i].today_running),
          'ignOff': that.millisToMinutesAndSeconds(data[i].today_stopped),
          'distance': (data[i].today_odo).toFixed(2),
          'tripCount': data[i].today_trips,
          'mileage': ((data[i].devObj[0].Mileage == null) || (data[i].devObj[0].Mileage == undefined)) ? "NA" : (data[i].today_odo / parseFloat(data[i].devObj[0].Mileage)).toFixed(2),
          'end_location': data[i].end_location,
          'start_location': data[i].start_location,
          't_ofr': that.millisToMinutesAndSeconds(data[i].t_ofr),
          't_idling': that.millisToMinutesAndSeconds(data[i].t_idling)
        });

      that.start_address(that.reportData[i], i);
      that.end_address(that.reportData[i], i);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  distanceRep(data) {
    let outerthis = this;
    var i = 0, howManyTimes = data.length;
    function f() {

      outerthis.reportData.push({
        'distance': data[i].distance,
        'Device_Name': data[i].device.Device_Name,
        'end_location': {
          'lat': data[i].endLat,
          'long': data[i].endLng
        },
        'start_location': {
          'lat': data[i].startLat,
          'long': data[i].startLng
        }
      });

      outerthis.start_address(outerthis.reportData[i], i);
      outerthis.end_address(outerthis.reportData[i], i);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
  }

  tripRep(data) {
    let that = this;
    var i = 0, howManyTimes = data.length;
    function f() {
      // var deviceId = data[i]._id;
      var distanceBt = data[i].distance / 1000;

      var gmtDateTime = moment.utc(JSON.stringify(data[i].start_time).split('T')[1].split('.')[0], "HH:mm:ss");
      var gmtDate = moment.utc(JSON.stringify(data[i].start_time).slice(0, -1).split('T'), "YYYY-MM-DD");
      var Startetime = gmtDateTime.local().format(' h:mm a');
      var Startdate = gmtDate.format('ll');
      var gmtDateTime1 = moment.utc(JSON.stringify(data[i].end_time).split('T')[1].split('.')[0], "HH:mm:ss");
      var gmtDate1 = moment.utc(JSON.stringify(data[i].end_time).slice(0, -1).split('T'), "YYYY-MM-DD");
      var Endtime = gmtDateTime1.local().format(' h:mm a');
      var Enddate = gmtDate1.format('ll');

      var startDate = new Date(data[i].start_time).toLocaleString();
      var endDate = new Date(data[i].end_time).toLocaleString();

      var fd = new Date(startDate).getTime();
      var td = new Date(endDate).getTime();
      var time_difference = td - fd;
      var total_min = time_difference / 60000;
      var hours = total_min / 60
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      var Durations = rhours + 'hrs' + ' ' + rminutes + 'mins';
      that.reportData.push(
        {
          'Device_Name': data[i].device.Device_Name,
          'Device_ID': data[i].device.Device_ID,
          'Startetime': Startetime,
          'Startdate': Startdate,
          'Endtime': Endtime,
          'Enddate': Enddate,
          'distance': distanceBt,
          '_id': data[i]._id,
          'start_time': data[i].start_time,
          'end_time': data[i].end_time,
          'duration': Durations,
          'end_location': {
            'lat': data[i].end_lat,
            'long': data[i].end_long
          },
          'start_location': {
            'lat': data[i].start_lat,
            'long': data[i].start_long
          }
        });

      that.start_address(that.reportData[i], i);
      that.end_address(that.reportData[i], i);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    } f();
  }

  ignRep(data) {
    let outerthis = this;
    var i = 0, howManyTimes = data.length;
    function f() {

      outerthis.reportData.push({
        'Device_Name': data[i].vehicleName,
        'switch': data[i].switch,
        'timestamp': data[i].timestamp,
        'start_location': {
          'lat': data[i].lat,
          'long': data[i].long
        }
      });
      outerthis.start_address(outerthis.reportData[i], i);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
  }

  sosRep(data) {
    for (var i = 0; i < data.length; i++) {
      this.reportData.push(data[i]);
    }
    // this.reportData.push(data)
  }

  start_address(item, index) {
    // debugger
    let that = this;
    if (!item.start_location) {
      that.reportData[index].StartLocation = "N/A";
      return;
    }
    let tempcord = {
      "lat": item.start_location.lat,
      "long": item.start_location.long
    }
    this.appService.getAddress(tempcord)
      .subscribe(resp => {
        // console.log("test");
        // console.log("result", resp);
        // if(resp !== undefined) {

        // }
        var res = JSON.parse(JSON.stringify(resp));
        console.log("result " + res.address);
        // that.allDevices[index].address = res.address;
        if (res.message == "Address not found in databse") {
          that.reportData[index].StartLocation = 'N/A';
          // this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.long)
          //   .then(res => {
          //     var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          //     that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
          //     that.distanceReportData[index].StartLocation = str;
          //     // console.log("inside", that.address);
          //   })
        } else {
          if (res.address !== undefined) {
            that.reportData[index].StartLocation = res.address;
          } else {
            that.reportData[index].StartLocation = 'N/A';
          }
          // that.reportData[index].StartLocation = res.address;
        }
      })
  }

  end_address(item, index) {
    let that = this;
    if (!item.end_location) {
      that.reportData[index].EndLocation = "N/A";
      return;
    }
    let tempcord = {
      "lat": item.end_location.lat,
      "long": item.end_location.long
    }
    this.appService.getAddress(tempcord)
      .subscribe(resp => {
        // console.log("test");
        var res = JSON.parse(JSON.stringify(resp));

        console.log("endlocation result", res.address);
        // console.log(res.address);
        // that.allDevices[index].address = res.address;
        if (res.message == "Address not found in databse") {
          that.reportData[index].EndLocation = 'N/A';
          // this.geocoderApi.reverseGeocode(item.end_location.lat, item.end_location.long)
          //   .then(res => {
          //     var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          //     that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
          //     that.distanceReportData[index].EndLocation = str;
          //     // console.log("inside", that.address);
          //   })
        } else {
          if (res.address !== undefined) {
            that.reportData[index].EndLocation = res.address;
          } else {
            that.reportData[index].EndLocation = 'N/A';
          }
        }
      })
  }
}

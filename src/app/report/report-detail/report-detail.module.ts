import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ReportDetailPage } from './report-detail.page';
import { ComponentsModule } from 'src/app/tabs/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ReportDetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule
  ],
  declarations: [ReportDetailPage]
})
export class ReportDetailPageModule { }

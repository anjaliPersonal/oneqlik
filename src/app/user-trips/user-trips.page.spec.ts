import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTripsPage } from './user-trips.page';

describe('UserTripsPage', () => {
  let component: UserTripsPage;
  let fixture: ComponentFixture<UserTripsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTripsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTripsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

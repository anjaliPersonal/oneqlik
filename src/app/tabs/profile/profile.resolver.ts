import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from './user.service';
import { Observable } from 'rxjs';
import { DataStore } from '../../shell/data-store';
import { ProfileModel } from './profile.model';

@Injectable()
export class UserProfileResolver implements Resolve<any> {

  constructor(private userService: UserService) { }

  resolve() {
    const dataSource: Observable<ProfileModel> = this.userService.getProfileDataSource();
    const dataStore: DataStore<ProfileModel> = this.userService.getProfileStore(dataSource);

    return dataStore;
  }
}
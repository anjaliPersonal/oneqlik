import { Component, OnInit, HostBinding } from '@angular/core';
import { ProfileModel } from './profile.model';
import { ActivatedRoute } from '@angular/router';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { LanguageService } from '../../language/language.service';
import { TranslateService } from '@ngx-translate/core';
import { DrawerState } from 'ion-bottom-drawer';
import { AuthService } from 'src/app/auth/auth.service';
import { URLs } from 'src/app/app.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: [
    './styles/user-profile.page.scss',
    './styles/user-profile.shell.scss',
    './styles/user-profile.ios.scss',
    './styles/user-profile.md.scss'
  ],
  // styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  profile: ProfileModel;
  available_languages = [];
  translations;
  opass: any; newpass: any; cpass: any;
  showProvider: boolean = false;
  showPassword: boolean = false;
  showSettings: boolean = false;

  show = false; show1 = false; show2 = false;

  dockedHeight: number = 50; // - Height of the drawer in docked position.Default value:
  shouldBounce: boolean = true; // - Determines whether the drawer should automatically bounce between docked, closed and top positions.Default value:
  disableDrag: boolean = false; // - Disables drawer drag.Default value:
  distanceTop: number = 0; // - Distance from top of fully opened drawer.Default value:
  transition: string = "0.25s ease -in -out"; // - Specify custom CSS transition for bounce movement.Default value:
  state: DrawerState = DrawerState.Docked; // - Current state of the drawer.Possible values: DrawerState.Bottom, DrawerState.Docked, DrawerState.Top.Default value:
  minimumHeight: number = 0; // - Height of the drawer when in closed state calculated from the bottom of the screen.Default value:
  userData: any;

  defaultScreen: string;
  defaultMapType: string;
  defaultFuelUnit: string;
  defaultlang: string;
  voiceNotif: any;
  mapNightMode: any;

  constructor(
    private route: ActivatedRoute,
    public alertController: AlertController,
    public languageService: LanguageService,
    public translate: TranslateService,
    private authService: AuthService,
    private toastController: ToastController,
    private loadingController: LoadingController,
    private constUrl: URLs
  ) {
    this.getToken();
  }
  @HostBinding('class.is-shell') get isShell() {
    return (this.profile && this.profile.isShell) ? true : false;
  }
  ngOnInit(): void {
    // debugger
    this.route.data.subscribe((resolvedRouteData) => {
      const profileDataStore = resolvedRouteData['data'];

      profileDataStore.state.subscribe(
        (state) => {
          this.profile = state;

          console.log("see profile here: ", this.profile)

          // get translations for this page to use in the Language Chooser Alert
          this.getTranslations();

          this.translate.onLangChange.subscribe(() => {
            this.getTranslations();
          });
        },
        (error) => { }
      );
    },
      (error) => { });
  }

  getToken() {
    this.authService.getTokenData().subscribe(data => {
      this.userData = data;
      console.log("dashboard data=> ", this.userData);

    })
  }
  sorted: any = [];
  showProviderInfo() {
    this.showProvider = true;
    if (this.userData.Dealer_ID != undefined) {
      this.sorted = this.userData.Dealer_ID;
    } else {
      this.sorted.first_name = this.userData.fn;
      this.sorted.last_name = this.userData.ln;
      this.sorted.phone = this.userData.phn;
      this.sorted.email = this.userData.email;
    }
  }

  getTranslations() {
    // get translations for this page to use in the Language Chooser Alert
    this.translate.getTranslation(this.translate.currentLang)
      .subscribe((translations) => {
        this.translations = translations;
      });
  }

  async openLanguageChooser() {
    this.available_languages = this.languageService.getLanguages()
      .map(item =>
        ({
          name: item.name,
          type: 'radio',
          label: item.name,
          value: item.code,
          checked: item.code === this.translate.currentLang
        })
      );

    const alert = await this.alertController.create({
      header: this.translations.SELECT_LANGUAGE,
      inputs: this.available_languages,
      cssClass: 'language-alert',
      buttons: [
        {
          text: this.translations.CANCEL,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => { }
        }, {
          text: this.translations.OK,
          handler: (data) => {
            if (data) {
              // this.translate.use(data);
            }
          }
        }
      ]
    });
    await alert.present();
  }

  onPassword() {
    this.showPassword = true;
  }

  onSettings() {
    this.showSettings = true;
  }

  onLogout() {
    this.alertController.create({
      message: 'Do you want to logout from the app?',
      buttons: [
        {
          text: 'Back'
        }, {
          text: 'Proceed',
          handler: () => {
            this.authService.logout();
          }
        }
      ]
    }).then((alertEl) => {
      alertEl.present();
    });
  }

  onPasswordSubmit() {
    if (this.newpass !== this.cpass) {
      this.showToast('New password and confirm password not matched..!');
      return;
    }

    let payload = {
      "ID": this.userData._id,
      "OLD_PASS": this.opass,
      "NEW_PASS": this.newpass
    }
    let url = this.constUrl.mainUrl + 'users/updatePassword';
    this.loadingController.create({
      message: 'Please wait.. we are updating password..'
    }).then((loadEl) => {
      loadEl.present();
      this.authService.postMethod(url, payload)
        .subscribe(respData => {
          loadEl.dismiss();
          if (respData) {
            var res = JSON.parse(JSON.stringify(respData));
            console.log(res);
            if (res) {
              this.showToast('Congrats! Your password has been updated.');
              this.authService.logout();
            }
          }
        },
          err => {
            loadEl.dismiss();
            if (err.error.message) {
              this.showToast(err.error.message);
            }
            console.log(err.error.message);
          })
    })
  }

  async onImmobPass() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Immobilize Password',
      subHeader: 'Enter password for engine cut',
      inputs: [
        {
          name: 'pass',
          type: 'text',
          placeholder: 'password'
        },
        {
          name: 'cpass',
          type: 'text',
          placeholder: 'confirm password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log('Confirm Ok: ', data);
            if (data.pass !== data.cpass) {
              this.showToast('password and confirm password not matched..!');
              return;
            }
            this.setImmobPassword(data);
          }
        }
      ]
    });

    await alert.present();
  }

  setImmobPassword(data) {
    let payload = {
      uid: this.userData._id,
      engine_cut_psd: data.pass
    }
    let url = this.constUrl.mainUrl + "users/set_user_setting";
    this.loadingController.create({
      message: 'Please wait.. we are setting password for engine cut.'
    }).then((loadEl) => {
      loadEl.present();
      this.authService.postMethod(url, payload)
        .subscribe(respData => {
          loadEl.dismiss();
          if (respData) {
            var res = JSON.parse(JSON.stringify(respData));
            if(res) {
              this.showToast('Congrats.. engine cut password has been saved!')
            }
          }
        },
          err => {
            loadEl.dismiss();
            console.log(err);
          });
    });
  }

  onApplyChnages() {
    
  }

  showToast(msg) {
    this.toastController.create({
      message: msg,
      position: 'middle',
      duration: 2000
    }).then((toastEl) => {
      toastEl.present();
    });
  }

}

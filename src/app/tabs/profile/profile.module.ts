import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProfilePage } from './profile.page';
import { UserProfileResolver } from './profile.resolver';
import { UserService } from './user.service';
import { LanguageService } from 'src/app/language/language.service';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../components/components.module';
import { IonBottomDrawerModule } from 'ion-bottom-drawer';

const routes: Routes = [
  {
    path: '',
    component: ProfilePage,
    resolve: {
      data: UserProfileResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    TranslateModule,
    IonBottomDrawerModule
  ],
  declarations: [ProfilePage],
  providers: [
    UserProfileResolver,
    UserService,
    LanguageService
  ]
})
export class ProfilePageModule {}

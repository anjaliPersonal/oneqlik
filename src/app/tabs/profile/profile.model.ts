import { ShellModel } from '../../shell/data-store';

export class ProfileModel extends ShellModel {
  GET_notif: boolean;
  email: string;
  email_verfi: boolean;
  exp: number;
  fn: string;
  fuel_unit: string;
  groups: Array<String> = [];
  iat: number;
  imageDoc: [];
  isDealer: boolean;
  isOperator: boolean;
  isOrganisation: boolean;
  isSuperAdmin: boolean;
  language_code: string;
  ln: string;
  phn: string;
  phone_verfi: boolean;
  report_preference: {};
  role: string;
  status: boolean;
  _id: string;
  _orgName: string;

  userImage: string;
  // name: string;
  // membership: string;
  // job: string;
  // likes: string;
  // followers: string;
  // following: string;
  // about: string;
  // friends: Array<{ image: string, name: string }> = [
  //   {
  //     image: '',
  //     name: ''
  //   },
  //   {
  //     image: '',
  //     name: ''
  //   },
  //   {
  //     image: '',
  //     name: ''
  //   },
  //   {
  //     image: '',
  //     name: ''
  //   }
  // ];
  // photos: Array<string> = [
  //   '',
  //   '',
  //   '',
  //   ''
  // ];

  constructor() {
    super();
  }
}

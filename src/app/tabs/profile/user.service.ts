import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { UserFriendsModel } from './friends/user-friends.model';
import { DataStore } from '../../shell/data-store';
import { ProfileModel } from './profile.model';
import { AuthService } from 'src/app/auth/auth.service';
import { Plugins} from '@capacitor/core';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class UserService {
  private profileDataStore: DataStore<ProfileModel>;
  // private friendsDataStore: DataStore<UserFriendsModel>;

  constructor(private http: HttpClient, private authService: AuthService,) { }

  public getProfileDataSource(): Observable<ProfileModel> {
    // this.authService.getTokenData().subscribe(data => {
    //   return data;
    //   console.log("dashboard data=> ", data);
    // })
    return from(Plugins.Storage.get({ key: 'authData' })).pipe(map(storedToken => {
      if (!storedToken || !storedToken.value) {
        return null;
      }
      const obj = JSON.parse(
        window.atob(
          JSON.parse(JSON.stringify(storedToken)).value.split(".")[1]
        )
      );
      // return this.http.get<ProfileModel>('./assets/sample-data/user/user-profile.json');
      return obj;
    }));
    // return this.http.get<ProfileModel>('./assets/sample-data/user/user-profile.json');
  }

  public getProfileStore(dataSource: Observable<ProfileModel>): DataStore<ProfileModel> {
    // Use cache if available
    if (!this.profileDataStore) {
      // Initialize the model specifying that it is a shell model
      const shellModel: ProfileModel = new ProfileModel();
      this.profileDataStore = new DataStore(shellModel);
      // Trigger the loading mechanism (with shell) in the dataStore
      this.profileDataStore.load(dataSource);
    }
    return this.profileDataStore;
  }

  // public getFriendsDataSource(): Observable<UserFriendsModel> {
  //   return this.http.get<UserFriendsModel>('./assets/sample-data/user/user-friends.json');
  // }

  // public getFriendsStore(dataSource: Observable<UserFriendsModel>): DataStore<UserFriendsModel> {
  //   // Use cache if available
  //   if (!this.friendsDataStore) {
  //     // Initialize the model specifying that it is a shell model
  //     const shellModel: UserFriendsModel = new UserFriendsModel();
  //     this.friendsDataStore = new DataStore(shellModel);
  //     // Trigger the loading mechanism (with shell) in the dataStore
  //     this.friendsDataStore.load(dataSource);
  //   }
  //   return this.friendsDataStore;
  // }

}

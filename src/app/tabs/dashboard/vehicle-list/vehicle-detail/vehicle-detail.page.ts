import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { URLs } from 'src/app/app.model';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-vehicle-detail',
  templateUrl: './vehicle-detail.page.html',
  styleUrls: ['./vehicle-detail.page.scss'],
})
export class VehicleDetailPage implements OnInit {
  userData: any = {};

  constructor(
    private authService: AuthService,
    private constURL: URLs,
    private loadingController: LoadingController
  ) {
    this.getToken
  }

  getToken() {
    this.authService.getTokenData().subscribe(data => {
      this.userData = data;
      console.log("dashboard data=> ", this.userData);
    })
  }

  ngOnInit() {
  }

  ionViewWillLoad() {

  }
}

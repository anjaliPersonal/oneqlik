import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { AuthService } from 'src/app/auth/auth.service';
import { URLs } from 'src/app/app.model';
import { LoadingController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.page.html',
  styleUrls: ['./vehicle-list.page.scss'],
})
export class VehicleListPage implements OnInit {
  @ViewChild(IonInfiniteScroll, {
    static: true
  }) infiniteScroll: IonInfiniteScroll;
  scrollEv: any;
  laodingEl: HTMLIonLoadingElement;
  userData: any = {};
  checkIfStat: string = 'RUNNING';
  pageNo: number = 0;
  limit: number = 6
  vehicleListData: any[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private constURL: URLs,
    private loadingController: LoadingController
  ) {
    this.getToken();
    // debugger
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        var data = JSON.parse(params.special);
        this.checkIfStat = data.label
        console.log(data);
      }
    });
  }

  ngOnInit() { }

  segmentChanged(ev: any) {
    console.log('Segment changed', ev.detail.value);
    this.checkIfStat = ev.detail.value;
    this.scrollEv = undefined;
    this.laodingEl = undefined;
    this.pageNo = 0;
    this.vehicleListData = [];
    this.getVehicleList();
  }

  getToken() {
    this.authService.getTokenData().subscribe(data => {
      this.userData = data;
      console.log("vehicle data=> ", this.userData);
      this.getVehicleList();
    })
  }

  ionViewWillLoad() {

  }

  getVehicleList() {
    if (this.scrollEv) {
      this.getData();
    } else {
      this.loadingController.create({
        message: "Loading vehciles..."
      }).then(loadEl => {
        loadEl.present();
        this.laodingEl = loadEl;
        this.pageNo = 0;
        this.vehicleListData = [];
        this.getData();
      });
    }
  }

  getData() {
    var baseURLp;
    if (this.checkIfStat) {
      baseURLp = this.constURL.mainUrl + 'devices/getDeviceByUser?id=' + this.userData._id + '&email=' + this.userData.email + '&statuss=' + this.checkIfStat + '&skip=' + this.pageNo + '&limit=' + this.limit;
    }
    else {
      baseURLp = this.constURL.mainUrl + 'devices/getDeviceByUser?id=' + this.userData._id + '&email=' + this.userData.email + '&skip=' + this.pageNo + '&limit=' + this.limit;
    }

    if (this.userData.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.userData._id;
    } else {
      if (this.userData.isDealer == true) {
        baseURLp += '&dealer=' + this.userData._id;
      }
    }
    this.authService.getMethod(baseURLp)
      .subscribe(resData => {
        if (this.laodingEl) {
          this.laodingEl.dismiss();
        }
        let parsedData;
        if (this.scrollEv) {
          this.scrollEv.target.complete();
          this.scrollEv.target.disable = true;
          parsedData = JSON.parse(JSON.stringify(resData));
          if (parsedData.devices.length === 0) {
            return;
          }
          for (let i = 0; i < parsedData.devices.length; i++) {
            this.vehicleListData.push(parsedData.devices[i]);
          }

        } else {
          if (this.laodingEl) {
            this.laodingEl.dismiss();
          }
          parsedData = JSON.parse(JSON.stringify(resData));
          if (parsedData.devices.length === 0) {
            return;
          }
          this.vehicleListData = parsedData.devices;
        }
      },
        err => {
          console.log("error=> ", err);
          if (this.laodingEl) {
            this.laodingEl.dismiss();
          }
          if (this.scrollEv) {
            this.scrollEv.target.complete();
            this.scrollEv.target.disable = true;
          }
        });
  }

  loadData(event) {
    this.scrollEv = undefined;
    this.laodingEl = undefined;
    this.scrollEv = event;
    this.pageNo = this.pageNo + 1;
    this.getVehicleList();
  }

  btnClicked(d, key) {
    debugger
    if (key === 'live') {
      this.router.navigateByUrl('/maintabs/tabs/dashboard/common/' + d.Device_ID + '/' + key);
    } else if (key === 'history') {
      // this.router.navigateByUrl('/maintabs/tabs/dashboard/common/' + d.Device_ID + '/' + key);
      this.router.navigateByUrl('/maintabs/tabs/dashboard/history/' + d.Device_ID + '/' + key);
    } else if (key === 'lock') {

    } else if (key === 'theft') {

    } else if (key === 'tow') {

    } else if (key === 'driver') {

    }
  }

}

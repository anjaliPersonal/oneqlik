import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Chart } from 'chart.js';
import 'chartjs-plugin-labels';
import { Plugins } from '@capacitor/core';
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';
import { AuthService } from 'src/app/auth/auth.service';
import { URLs } from 'src/app/app.model';
import { LoadingController, Events } from '@ionic/angular';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  @ViewChild("doughnutCanvas", {
    static: true
  }) doughnutCanvas: ElementRef;
  @ViewChild("barCanvas", {
    static: true
  }) barCanvas: ElementRef;

  // private barChart: Chart;
  doughnutChart: Chart;
  barChart: Chart;
  // private lineChart: Chart;
  userData: any = {};
  isDealer: boolean = false;
  isCustomer: boolean = false;
  slideOpts = {
    initialSlide: 0,
    speed: 400
  };
  public pieChart: GoogleChartInterface;

  constructor(
    private router: Router,
    private authService: AuthService,
    private constURL: URLs,
    private loadingController: LoadingController,
    private event: Events
  ) {
    this.getToken();
    this.checkIfDealerORCustomer();

  }

  ionSlideNextStart() {
    console.log("slide next starts..")
  }

  ionSlideDidChange() {
    console.log(" slide has been changed!!")
  }

  ionViewDidEnter() {
    // this.loadSimplePieChart();
    this.getChartData();
  }

  // loadSimplePieChart() {
  //   this.pieChart = {
  //     chartType: 'PieChart',
  //     dataTable: [
  //       ['Task', 'Hours per Day'],
  //       ['Work', 11],
  //       ['Eat', 2],
  //       ['Commute', 2],
  //       ['Watch TV', 2],
  //       ['Sleep', 7]
  //     ],
  //     //opt_firstRowIsData: true,
  //     options: {
  //       // 'title': 'Tasks',
  //       // legend: { position: 'bottom', alignment: 'start' },
  //       width: 620,
  //       height: 450,
  //       sliceVisibilityThreshold: 0,
  //       pieHole: 0.5,
  //       // 'chartArea': {'width': '80%', 'height': '80%'},
  //       'legend': { 'position': 'bottom' }
  //     },
  //   };
  // }


  onAdminClicked() {
    Plugins.Storage.get({ key: 'superAdminToken' }).then(storedToken => {
      if (!storedToken || !storedToken.value) {
        return;
      }
      Plugins.Storage.set({ key: 'authData', value: storedToken.value });
      localStorage.setItem("dealer_status", 'OFF')
      Plugins.Storage.remove({ key: 'superAdminToken' });
      this.isDealer = false;
      this.getToken();
    });
  }
  onDealerClicked() {
    Plugins.Storage.get({ key: 'superAdminToken' }).then(storedToken => {
      if (!storedToken || !storedToken.value) {
        return;
      }
      Plugins.Storage.get({ key: 'dealer' }).then(storedToken2 => {
        if (!storedToken2 || !storedToken2.value) {
          return;
        }
        Plugins.Storage.set({ key: 'authData', value: storedToken2.value });
        localStorage.setItem("dealer_status", 'ON')
        localStorage.setItem("custumer_status", 'OFF');
        Plugins.Storage.remove({ key: 'dealer' });
        this.isCustomer = false;
        this.isDealer = true;
        this.getToken();
      });
    });
  }

  checkIfDealerORCustomer() {
    var dealStat = localStorage.getItem('dealer_status');
    var custStat = localStorage.getItem('custumer_status');
    if (dealStat !== null && dealStat === 'ON' && custStat === 'OFF') {
      this.isDealer = true;
    } else if (custStat !== null && custStat === 'ON' && dealStat === 'OFF') {
      this.isCustomer = true;
    }
  }

  getToken() {
    this.authService.getTokenData().subscribe(data => {
      this.userData = data;
      console.log("dashboard data=> ", this.userData);
      this.event.publish('Auth:Data', this.userData);
      this.getChartData();
      this.getVehicleUtilizationChart();
    })
  }

  ngOnInit() {

  }
  //  getChartData() {
  //   var data = [
  //     {
  //         value: 300,
  //         color:"#F7464A",
  //         highlight: "#FF5A5E",
  //         label: "Red"
  //     },
  //     {
  //         value: 50,
  //         color: "#46BFBD",
  //         highlight: "#5AD3D1",
  //         label: "Green"
  //     },
  //     {
  //         value: 100,
  //         color: "#FDB45C",
  //         highlight: "#FFC870",
  //         label: "Yellow"
  //     }
  // ];

  // var canvas = new Chart(this.doughnutCanvas.nativeElement, {

  // })

  // // var canvas = document.getElementById("canvas");
  // var ctx = canvas.getContext("2d");
  // var midX = canvas.width/2;
  // var midY = canvas.height/2

  // // Create a pie chart
  // var myPieChart = new Chart(ctx).Pie(data, {
  //     showTooltips: false,
  //     onAnimationProgress: drawSegmentValues
  // });

  // var radius = myPieChart.outerRadius;

  // function drawSegmentValues()
  // {
  //     for(var i=0; i<myPieChart.segments.length; i++) 
  //     {
  //         ctx.fillStyle="white";
  //         var textSize = canvas.width/10;
  //         ctx.font= textSize+"px Verdana";
  //         // Get needed variables
  //         var value = myPieChart.segments[i].value;
  //         var startAngle = myPieChart.segments[i].startAngle;
  //         var endAngle = myPieChart.segments[i].endAngle;
  //         var middleAngle = startAngle + ((endAngle - startAngle)/2);

  //         // Compute text location
  //         var posX = (radius/2) * Math.cos(middleAngle) + midX;
  //         var posY = (radius/2) * Math.sin(middleAngle) + midY;

  //         // Text offside by middle
  //         var w_offset = ctx.measureText(value).width/2;
  //         var h_offset = textSize/4;

  //         ctx.fillText(value, posX - w_offset, posY + h_offset);
  //     }
  // }
  //  }


  getChartData() {
    let that = this;
    this.chartPlugin();
    var to = new Date().toISOString();
    var d = new Date();
    var a = d.setHours(0, 0, 0, 0)
    var from = new Date(a).toISOString();
    var _baseUrl = this.constURL.mainUrl + 'gps/getDashboard?email=' + this.userData.email + '&from=' + from + '&to=' + to + '&id=' + this.userData._id;
    if (this.userData.isSuperAdmin == true) {
      _baseUrl += '&supAdmin=' + this.userData._id;
    } else {
      if (this.userData.isDealer == true) {
        _baseUrl += '&dealer=' + this.userData._id;
      }
    }
    this.loadingController.create({
      message: 'Loading chart data...'
    }).then((loadEl) => {
      loadEl.present();
      this.authService.getMethod(_baseUrl)
        .subscribe(respData => {
          loadEl.dismiss();
          console.log("dashboard data: ", respData);
          var data = JSON.parse(JSON.stringify(respData))
          this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
            type: "doughnut",
            data: {
              labels: ["RUNNING", "IDLING", "STOPPED", "OUT OF REACH", "NO GPS", "EXPIRED", "NO DATA"],
              datasets: [
                {
                  label: "# of Votes",
                  data: [
                    data.running_devices,
                    data["Ideal Devices"],
                    data["OFF Devices"],
                    data["OutOfReach"],
                    data["no_gps_fix_devices"],
                    data["expire_status"],
                    data["no_data"]
                  ],
                  backgroundColor: [
                    '#11a46e', '#ffc13c', '#ff3f32', '#1bb6d4', '#dadada', '#454344'
                  ],
                  hoverBackgroundColor: ["rgba(255, 99, 132, 0.2)", "rgba(54, 162, 235, 0.2)", "rgba(255, 206, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(153, 102, 255, 0.2)", "rgba(255, 159, 64, 0.2)"]
                }
              ]
            },
            options: {
              responsive: true,
              maintainAspectRatio: false,
              plugins: {
                labels: {
                  render: 'value',
                  fontSize: 16,
                  fontColor: '#fff',
                  images: [
                    {
                      src: 'image.png',
                      width: 16,
                      height: 16
                    }
                  ],
                  outsidePadding: 2,
                  textMargin: 4
                }
              },
              elements: {
                center: {
                  text: data.Total_Vech,
                  color: '#d80622', // Default is #000000
                  fontStyle: 'Arial', // Default is Arial
                  sidePadding: 10 // Defualt is 20 (as a percentage)
                }
              },
              tooltips: {
                enabled: false
              },
              pieceLabel: {
                mode: 'value',
                fontColor: ['white', 'white', 'white', 'white', 'white']
              },
              legend: {
                display: true,
                labels: {
                  fontColor: 'rgb(255, 99, 132)',
                  fontSize: 10,
                  boxWidth: 10,
                  usePointStyle: true
                },
                position: 'bottom',
              },
              onClick: function (event, elementsAtEvent) {
                var activePoints = elementsAtEvent;
                if (activePoints[0]) {
                  var chartData = activePoints[0]['_chart'].config.data;
                  var idx = activePoints[0]['_index'];
                  var label = chartData.labels[idx];
                  var value = chartData.datasets[0].data[idx];
                  var obj = {
                    label: label,
                    value: value
                  }
                  // localStorage.setItem('status', label);
                  // that.router.navigateByUrl('maintabs/tabs/dashboard/vehicle-list/', obj);
                  let navigationExtras: NavigationExtras = {
                    queryParams: {
                      special: JSON.stringify(obj)
                    }
                  };
                  that.router.navigate(['maintabs', 'tabs', 'dashboard', 'vehicle-list'], navigationExtras);
                  // that.navCtrl.push('AddDevicesPage', {
                  //   label: label,
                  //   value: value
                  // });
                }
              }
            }
          });
          // this.doughnutCanvas.nativeElement.canvas.parentNode.style.height = '128px';
          // this.doughnutCanvas.nativeElement.canvas.parentNode.style.width = '128px';
        },
          err => {
            console.log(err);
            loadEl.dismiss();
          });
    })
  }

  getVehicleUtilizationChart() {
    var role;
    if (this.userData.isSuperAdmin === true) {
      role = 'supAdmin';
    } else if (this.userData.isDealer === true) {
      role = 'Dealer';
    } else {
      role = 'User';
    }
    var _burl = this.constURL.mainUrl + "gps/vehicleUtilizationPercentage?role=" + role + "&id=" + this.userData._id;
    this.authService.getMethod(_burl)
      .subscribe((respData) => {
        if (respData) {
          var res = JSON.parse(JSON.stringify(respData));
          if (res.message) {
            res.length = 0;
            this.manipulateData(res);
          } else {
            this.manipulateData(res);
          }

        }
      },
        err => {
          console.log(err)
        });

  }

  getBarChart(data, options) {
    debugger
    // this.barCanvas.nativeElement.canvas.parentNode.style.height = '128px';
    // this.barCanvas.nativeElement.canvas.parentNode.style.width = '128px';
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: 'bar',
      data: data,
      options: options

    });
  }

  manipulateData(bargraph) {
    var chartOptions = {
      maintainAspectRatio: false,
      responsive: true,
      title: {
        display: true,
        fontSize: 14,
        text: 'Vehicle Utilization(%)'
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            max: 100,
            min: 0
          }
        }]
      },
      // scales: {
      //   yAxes: [{
      //     stacked: true,
      //     gridLines: {
      //       display: true,
      //       color: "rgba(255,99,132,0.2)"
      //     }
      //   }],
      //   xAxes: [{
      //     gridLines: {
      //       display: false
      //     }
      //   }]
      // },
      plugins: {
        labels: {
          render: 'value',
          fontSize: 0,
          fontColor: '#fff',
          images: [
            {
              src: 'image.png',
              width: 16,
              height: 16
            }
          ],
          outsidePadding: 2,
          textMargin: 4
        }
      },

    }
    // var data = {
    //   labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
    //   datasets: [{
    //     label: "Dataset #1",
    //     backgroundColor: "rgba(255,99,132,0.2)",
    //     borderColor: "rgba(255,99,132,1)",
    //     borderWidth: 2,
    //     hoverBackgroundColor: "rgba(255,99,132,0.4)",
    //     hoverBorderColor: "rgba(255,99,132,1)",
    //     data: [65, 59, 20, 81, 56, 55, 40],
    //   }]
    // };
    // var chartOptions = {
    //   responsive: true,
    //   maintainAspectRatio: false,
    //   title: {
    //     display: true,
    //     fontSize: 14,
    //     text: 'Vehicle Utilization(%)'
    //   },
    //   plugins: {
    //     labels: {
    //       render: 'value',
    //       fontSize: 0,
    //       fontColor: '#fff',
    //       images: [
    //         {
    //           src: 'image.png',
    //           width: 16,
    //           height: 16
    //         }
    //       ],
    //       outsidePadding: 2,
    //       textMargin: 4
    //     }
    //   },

    //   scales: {
    //     yAxes: [{
    //       ticks: {
    //         beginAtZero: true,
    //         max: 100,
    //         min: 0
    //       }
    //     }]
    //   }
    // }
    if (bargraph.length != 0) {
      var varGraphObject = bargraph;

      for (let bg in varGraphObject) {
        var runningArr = [
          (bg[0]['Daily']['Running'] != undefined) ? (bg[0]['Daily']['Running']).toFixed(2) : 0,
          (bg[1]['Weekly']['Running'] != undefined) ? (bg[1]['Weekly']['Running']).toFixed(2) : 0,
          (bg[2]['Monthly']['Running'] != undefined) ? (bg[2]['Monthly']['Running']).toFixed(2) : 0
        ];

        var IdleArr = [
          (bg[0]['Daily']['Idle'] != undefined) ? (bg[0]['Daily']['Idle']).toFixed(2) : 0,
          (bg[1]['Weekly']['Idle'] != undefined) ? (bg[1]['Weekly']['Idle']).toFixed(2) : 0,
          (bg[2]['Monthly']['Idle'] != undefined) ? (bg[2]['Monthly']['Idle']).toFixed(2) : 0
        ];
        var OFRArr = [
          (bg[0]['Daily']['OFR'] != undefined) ? (bg[0]['Daily']['OFR']).toFixed(2) : 0,
          (bg[1]['Weekly']['OFR'] != undefined) ? (bg[1]['Weekly']['OFR']).toFixed(2) : 0,
          (bg[2]['Monthly']['OFR'] != undefined) ? (bg[2]['Monthly']['OFR']).toFixed(2) : 0
        ];
        var StopArr = [
          (bg[0]['Daily']['Stop'] != undefined) ? (bg[0]['Daily']['Stop']).toFixed(2) : 0,
          (bg[1]['Weekly']['Stop'] != undefined) ? (bg[1]['Weekly']['Stop']).toFixed(2) : 0,
          (bg[2]['Monthly']['Stop'] != undefined) ? (bg[2]['Monthly']['Stop']).toFixed(2) : 0
        ];
      }

      var barChartData = {
        labels: [
          "Today",
          "Week",
          "Month"
        ],
        datasets: [
          {
            label: "Running",
            backgroundColor: "#11a46e",
            borderColor: "white",
            borderWidth: 1,
            data: [runningArr[0], runningArr[1], runningArr[2]]
          },
          {
            label: "Out of Reach",
            backgroundColor: "#1bb6d4",
            borderColor: "white",
            borderWidth: 1,
            data: [OFRArr[0], OFRArr[1], OFRArr[2]]
          },
          {
            label: "Idle",
            backgroundColor: "#ffc13c",
            borderColor: "white",
            borderWidth: 1,
            data: [IdleArr[0], IdleArr[1], IdleArr[2]]
          },
          {
            label: "Stopped",
            backgroundColor: "#ff3f32",
            borderColor: "white",
            borderWidth: 1,
            data: [StopArr[0], StopArr[1], StopArr[2]]
          }
        ]
      };



      this.getBarChart(barChartData, chartOptions);
    } else {
      var data = {
        labels: [
          "Today",
          "Week",
          "Month"
        ],
        datasets: [
          {
            label: "Running",
            backgroundColor: "#11a46e",
            borderColor: "white",
            borderWidth: 1,
            data: [0, 0, 0]
          },
          {
            label: "Out of Reach",
            backgroundColor: "#1bb6d4",
            borderColor: "white",
            borderWidth: 1,
            data: [0, 0, 0]
          },
          {
            label: "Idle",
            backgroundColor: "#ffc13c",
            borderColor: "white",
            borderWidth: 1,
            data: [0, 0, 0]
          },
          {
            label: "Stopped",
            backgroundColor: "#ff3f32",
            borderColor: "white",
            borderWidth: 1,
            data: [0, 0, 0]
          }
        ]
      };
      this.getBarChart(data, chartOptions);
    }
  }

  chartPlugin() {
    Chart.pluginService.register({
      beforeDraw: function (chart) {
        if (chart.config.options.elements.center) {
          //Get ctx from string
          var ctx = chart.chart.ctx;

          //Get options from the center object in options
          var centerConfig = chart.config.options.elements.center;
          var fontStyle = centerConfig.fontStyle || 'Arial';
          var txt = centerConfig.text;
          var color = centerConfig.color || '#000';
          var sidePadding = centerConfig.sidePadding || 20;
          var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
          //Start with a base font of 30px
          ctx.font = "30px " + fontStyle;

          //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
          var stringWidth = ctx.measureText(txt).width;
          var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

          // Find out how much the font can grow in width.
          var widthRatio = elementWidth / stringWidth;
          var newFontSize = Math.floor(30 * widthRatio);
          var elementHeight = (chart.innerRadius * 2);

          // Pick a new font size so it will not be larger than the height of label.
          var fontSizeToUse = Math.min(newFontSize, elementHeight);

          //Set font settings to draw it correctly.
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';
          var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
          var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
          ctx.font = fontSizeToUse + "px " + fontStyle;
          ctx.fillStyle = color;

          //Draw text in center
          ctx.fillText(txt, centerX, centerY);
        }
      }
    });
  }

  onLiveTracking() {
    this.router.navigateByUrl('/maintabs/tabs/dashboard/live-tracking');
  }

}

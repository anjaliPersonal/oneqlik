
import { Component, OnInit, AfterViewInit } from '@angular/core';
import {
  ToastController,
  Platform,
  LoadingController,

} from '@ionic/angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation,
  GoogleMapsMapTypeId,
  LatLngBounds,
  LatLng,
  Spherical
} from '@ionic-native/google-maps';

import { AuthService } from 'src/app/auth/auth.service';
import { URLs } from 'src/app/app.model';
import * as moment from 'moment';
import * as io from 'socket.io-client';
import * as _ from "lodash";
// import { ExpiredPage } from '../expired/expired.page';

@Component({
  selector: 'app-live-tracking',
  templateUrl: './live-tracking.page.html',
  styleUrls: ['./live-tracking.page.scss'],
})
export class LiveTrackingPage implements OnInit, AfterViewInit {
  map: GoogleMap;
  loading: any;
  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};
  userdetails: any;
  socketSwitch: any = {};
  socketChnl: any = [];
  _io: any;
  allData: any = {};
  someKey: any = [];
  impkey: any;
  img: any;
  vehObj: any;
  messageToSendP: string = "multipleLive";
  showComponent: boolean = false;
  screenKey: string = "live";

  constructor(public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private authService: AuthService,
    private platform: Platform,
    private constUrl: URLs,
    private loadingController: LoadingController,
    // public modalController: ModalController,
  ) {
    this.getToken();
  }

  ngAfterViewInit() {
    console.log("live screen:  ngAfterViewInit()");
    localStorage.removeItem("live_FLAG");
    this.map = this.newMap();
  }

  async ngOnInit() {
    // Since ngOnInit() is executed before `deviceready` event,
    // you have to wait the event.
    console.log("Live screen:  ngOnInit()");
  }

  ionViewWillEnter() {
    console.log("lives creen:  ionViewWillEnter()");
    this._io = io.connect('https://www.oneqlik.in/gps', { transports: ["websocket"] });
    this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
  }

  ionViewWillLeave() {
    for (var e = 0; e < this.someKey.length; e++) {
      if (this.allData[this.someKey[e]].mark) {
        this.allData[this.someKey[e]].mark.remove();
      }
    }
  }

  ionViewDidLeave() {
    console.log("live screen:  ionViewDidLeave()");
    for (var i = 0; i < this.socketChnl.length; i++)
      this._io.removeAllListeners(this.socketChnl[i]);
    this._io.on('disconnect', (data) => {
      console.log('Ignition IO disconnected', data);
    })
  }

  ionViewDidEnter() {
    console.log("live screen:  ionViewDidEnter()");
    if (this.socketChnl.length > 0) {
      for (var i = 0; i < this.socketChnl.length; i++)
        this._io.removeAllListeners(this.socketChnl[i]);
      this._io.on('disconnect', () => {
        this._io.open();
      })
    }
    this.allData = {};
    // this.deviceDeatils = undefined;
    this.userDevices();
  }

  getToken() {
    this.authService.getTokenData().subscribe(data => {
      this.userdetails = data;
    })
  }

  clickedAll() {
    // this.vehObj.l;
  }

  newMap() {
    let mapOptions = {
      controls: {
        zoom: false,
        myLocation: true,
        myLocationButton: false,
      },
      mapTypeControlOptions: {
        mapTypeIds: [GoogleMapsMapTypeId.ROADMAP, 'map_style']
      },
      gestures: {
        rotate: false,
        tilt: false
      },
      // mapType: this.mapKey
    }
    let map = GoogleMaps.create('map_canvas', mapOptions);
    if (this.platform.is('android')) {
      // map.setPadding(300, 50, 100, 50);
      map.setPadding(50, 50, 50, 50);
    }
    return map;
  }

  getVehObj($event) {
    if ($event) {
      debugger
      var tempObj = $event;
      this.vehObj = undefined;
      this.showComponent = false;
      // this.vehObj = $event;
      this.getSingleDeviceObj(tempObj.Device_ID);
    } else {
      this.showComponent = false;
      this.vehObj = undefined;
    }
  }

  getSingleDeviceObj(id) {
    let that = this;
    var url = this.constUrl.mainUrl + "devices/getDevicebyId?deviceId=" + id;
    this.loadingController.create({
      message: 'please wait...'
    }).then((loadEl) => {
      loadEl.present();
      this.authService.getMethod(url)
        .subscribe(respData => {
          loadEl.dismiss();
          if (respData) {
            var res = JSON.parse(JSON.stringify(respData));
            console.log(res);
            that.vehObj = res;
            // that.vehObj = {
            //   key: 'live',
            //   deviceObj: res
            // };

            this.showComponent = true;
            // this.forSinglevehicle(res);
          }
        },
          err => {
            loadEl.dismiss();
            console.log(err);
          })
    })


  }

  // async openExpiredModal(data) {
  //   const modal = await this.modalController.create({
  //     component: ExpiredPage,
  //     componentProps: { param: data }
  //   });
  //   return await modal.present();
  // }

  // forSinglevehicle(data) {
  //   console.log(data);
  //   if (data.status == 'Expired') {
  //     this.openExpiredModal(data);
  //   } else {
  //     let that = this;
  //   }
  // }

  userDevices() {
    var baseURLp;
    let that = this;
    baseURLp = this.constUrl.mainUrl + 'devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;

    if (this.userdetails.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.userdetails._id;
      // this.isSuperAdmin = true;
    } else {
      if (this.userdetails.isDealer == true) {
        baseURLp += '&dealer=' + this.userdetails._id;
        // this.isDealer = true;
      }
    }
    if (localStorage.getItem("live_FLAG") == null) {
      that.loadingController.create({
        message: 'Please wait...'
      }).then((loadEl => {
        loadEl.present();
        that.accessFunc(loadEl, baseURLp);
      }))
    } else {
      that.accessFunc(null, baseURLp);
    }
  }
  mapData: any = [];
  accessFunc(loadEl, baseURLp) {
    let that = this;
    that.authService.getMethod(baseURLp)
      .subscribe(respData => {
        if (loadEl) {
          loadEl.dismiss();
        }

        if (localStorage.getItem("live_FLAG") === null) {
          localStorage.setItem("live_FLAG", "live_FLAG");
        }
        var resp = JSON.parse(JSON.stringify(respData));
        that.mapData = [];
        that.mapData = resp.devices.map(function (d) {
          if (d.last_location !== undefined) {
            return { lat: d.last_location['lat'], lng: d.last_location['long'] };
          } else {
            if (d.last_loc !== undefined) {
              if (d.last_loc.coordinates[1] !== 0 && d.last_loc.coordinates[0] !== 0) {
                return { lat: d.last_loc.coordinates[1], lng: d.last_loc.coordinates[0] }
              }
            }
          }
        });

        let bounds = new LatLngBounds(that.mapData);
        that.map.moveCamera({
          target: bounds,
          // zoom: 10
        })
        for (var i = 0; i < resp.devices.length; i++) {
          if (resp.devices[i].status != "Expired") {
            that.socketInit(resp.devices[i]);
          }
        }

        // that.callGeofence(); // drawing geofence

      },
        err => {
          if (loadEl) {
            loadEl.dismiss();
          }
          console.log(err);
        });
  }

  getIconUrl(data) {
    let that = this;
    var iconUrl;
    if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0) {
      if (that.platform.is('ios')) {
        iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
      } else if (that.platform.is('android')) {
        iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
      }
    } else {
      if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0) {
        if (that.platform.is('ios')) {
          iconUrl = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
        } else if (that.platform.is('android')) {
          iconUrl = './assets/imgs/vehicles/idling' + data.iconType + '.png';
        }
      } else {
        if (that.platform.is('ios')) {
          iconUrl = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
        } else if (that.platform.is('android')) {
          iconUrl = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
        }
      }
    }
    console.log("icon url: ", iconUrl);
    return iconUrl;
  }

  socketInit(pdata, center = false) {
    let that = this;
    // that.allData = {};
    that._io.emit('acc', pdata.Device_ID);
    that.socketChnl.push(pdata.Device_ID + 'acc');
    that.someKey.push(pdata._id);
    that._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {

      if (d4 != undefined)
        (function (data) {

          if (data == undefined) {
            return;
          }
          console.log("on ping: ", data)

          if (data._id != undefined && (data.last_location != undefined || data.last_loc != undefined)) {
            var key = data._id;
            // that.someKey.push(key);

            that.impkey = data._id;
            // that.deviceDeatils = data;
            // debugger
            var strStr;
            if (that.allData[key]) {
              strStr = ' Speed: ' + data.last_speed + ' kmph' + '\n Battery: ' + (data.battery ? data.battery : '0') + '%' + '\n Pedometer: ' + (data.steps ? data.steps : 'N/A') + '\n Heartbeat: ' + (data.tumbling_num ? data.tumbling_num : 'N/A') + '\n Last Updated Time: ' + moment(new Date(data.last_ping_on)).format('LLLL');
              // strStr = ' Speed: ' + data.last_speed + ' kmph' + '\n Battery: ' + data.batteryStatus + '%' + '\n Pedometer: ' + data.last_speed + '\n Heartbeat: ' + data.last_speed + '\n Last Updated Time: ' + moment(new Date(data.last_ping_on)).format('LLLL');
              that.socketSwitch[key] = data;
              if (that.allData[key].mark != undefined) {
                that.allData[key].mark.setSnippet(strStr);
                that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                var temp = _.cloneDeep(that.allData[key].poly[1]);
                that.allData[key].poly[0] = _.cloneDeep(temp);
                that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };

                var speed = data.status == "RUNNING" ? data.last_speed : 0;

                that.liveTrack(that.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
              } else {
                return;
              }
            }
            else {
              that.allData[key] = {};
              that.socketSwitch[key] = data;
              that.allData[key].poly = [];
              if (data.last_location) {
                that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });

              } else {
                if (data.last_loc) {
                  that.allData[key].poly.push({ lat: data.last_loc.coordinates[1], lng: data.last_loc.coordinates[0] });

                }
              }
              strStr = ' Speed: ' + data.last_speed + ' kmph' + '\n Battery: ' + (data.battery ? data.battery : '0') + '%' + '\n Pedometer: ' + (data.steps ? data.steps : 'N/A') + '\n Heartbeat: ' + (data.tumbling_num ? data.tumbling_num : 'N/A') + '\n Last Updated Time: ' + moment(new Date(data.last_ping_on)).format('LLLL');

              // that.getAddressTitle(that.allData[key].mark);
              // var fileUrl;
              // var imgUrl, str1;
              // if (data.deviceImage.length > 0) {
              //   imgUrl = data.deviceImage[0];
              //   str1 = imgUrl.split('public/');
              //   fileUrl = "http://13.126.36.205/" + str1[1];
              // }
              // console.log("file url on home page: ", fileUrl);

              if (data.last_location != undefined) {
                /////////////// merge images using canvas
                let canvas: any = document.createElement('canvas');
                canvas.width = 100;
                canvas.height = 100;
                var ctx = canvas.getContext('2d');
                var imageObj1 = new Image();
                imageObj1.crossOrigin = "anonymous";
                ////////
                that.getIconUrl(data);
                ///////
                imageObj1.src = that.getIconUrl(data);
                imageObj1.onload = function () {
                  ctx.drawImage(imageObj1, 0, 0, 60, 80);
                  that.img = canvas.toDataURL("image/png");
                  const image = {
                    url: that.img,
                    size: {
                      width: 37,
                      height: 50
                    }
                  };
                  that.map.addMarker({
                    title: data.Device_Name,
                    position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                    icon: image
                  }).then((marker: Marker) => {
                    that.allData[key].mark = marker;
                    marker.setSnippet(strStr);
                    marker.addEventListener(GoogleMapsEvent.MARKER_CLICK).subscribe(e => {

                      marker.showInfoWindow();

                      // that.getAddressTitle(marker, strStr);
                    });
                  });
                }


                // };
              }
            }
          }
        })(d4)
    })
  }

  liveTrack(map, mark, coords, speed, delay, center, id, that) {

    var target = 0;

    clearTimeout(that.ongoingGoToPoint[id]);
    clearTimeout(that.ongoingMoveMarker[id]);

    if (center) {
      map.setCameraTarget(coords[0]);
    }

    function _goToPoint() {
      if (target > coords.length)
        return;

      var lat = 0;
      var lng = 0;
      if (mark.getPosition().lat !== undefined || mark.getPosition().lng !== undefined) {
        lat = mark.getPosition().lat;
        lng = mark.getPosition().lng;
      }

      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
        // mark.setIcon(icons);
      }
      function _moveMarker() {

        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          // if (that.selectedVehicle != undefined) {
          //   map.setCameraTarget(new LatLng(lat, lng));
          // }
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          // if (that.selectedVehicle != undefined) {
          //   map.setCameraTarget(dest);
          // }
          that.latlngCenter = dest;
          mark.setPosition(dest);
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }

  // async onButtonClick() {
  //   this.map.clear();

  //   this.loading = await this.loadingCtrl.create({
  //     message: 'Please wait...'
  //   });
  //   await this.loading.present();

  //   // Get the location of you
  //   this.map.getMyLocation().then((location: MyLocation) => {
  //     this.loading.dismiss();
  //     console.log(JSON.stringify(location, null, 2));

  //     // Move the map camera to the location with animation
  //     this.map.animateCamera({
  //       target: location.latLng,
  //       zoom: 17,
  //       tilt: 30
  //     });

  //     // add a marker
  //     let marker: Marker = this.map.addMarkerSync({
  //       title: '@ionic-native/google-maps plugin!',
  //       snippet: 'This plugin is awesome!',
  //       position: location.latLng,
  //       animation: GoogleMapsAnimation.BOUNCE
  //     });

  //     // show the infoWindow
  //     marker.showInfoWindow();

  //     // If clicked it, display the alert
  //     marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
  //       this.showToast('clicked!');
  //     });
  //   })
  //     .catch(err => {
  //       this.loading.dismiss();
  //       this.showToast(err.error_message);
  //     });
  // }

  // async showToast(message: string) {
  //   let toast = await this.toastCtrl.create({
  //     message: message,
  //     duration: 2000,
  //     position: 'middle'
  //   });

  //   toast.present();
  // }
}

// import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';

// @Component({
//   selector: 'app-live-tracking',
//   templateUrl: './live-tracking.page.html',
//   styleUrls: ['./live-tracking.page.scss'],
// })
// export class LiveTrackingPage implements OnInit, AfterViewInit {
//   @ViewChild('map', { static: true }) mapElementRef: ElementRef;
//   constructor(private renderer: Renderer2) { }

//   ngOnInit() {}

//   ngAfterViewInit() {
//     this.getGoogleMap().then(googleMaps => {
//       const mapEl = this.mapElementRef.nativeElement;
//       const map = new googleMaps.Map(mapEl, {
//         center: { lat: -34.397, lng: 150.644 },
//         zoom: 16
//       });
//       googleMaps.event.addListenerOnce(map, 'idle', () => {
//         this.renderer.addClass(mapEl, 'visible');
//       });
//     }).catch(err => {
//       console.log(err);
//     });
//   }

//   private getGoogleMap(): Promise<any> {
//     const win = window as any;
//     const googleModule = win.google;
//     if (googleModule && googleModule.maps) {
//       return Promise.resolve(googleModule.maps);
//     }
//     return new Promise((resolve, reject) => {
//       const script = document.createElement('script');
//       script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8';
//       script.async = true;
//       script.defer = true;
//       document.body.appendChild(script);
//       script.onload = () => {
//         const loadedGoogleMpdule = win.google;
//         if (loadedGoogleMpdule && loadedGoogleMpdule.maps) {
//           resolve(loadedGoogleMpdule.maps);
//         } else {
//           reject('Google maps SDK not available.');
//         }
//       };
//     });
//   }
// }

import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation,
  LocationService,
  GoogleMapOptions,
  LatLngBounds,
  LatLng,
  Spherical,
  MarkerIcon
} from '@ionic-native/google-maps';
import { Platform, ToastController, LoadingController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/auth/auth.service';
import { URLs } from 'src/app/app.model';
import { ActivatedRoute } from '@angular/router';
// import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  datetimeFrom: string;
  datetimeTo: string;
  messageToSendP: string = 'history';
  vehObj: any = [];
  map: GoogleMap;
  userData: any;
  dataArrayCoords: any = [];
  mapData: any = [];
  playPause: boolean;
  mark: any;
  deviceObj: any;
  isSingle: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private constUrl: URLs,
    public toastCtrl: ToastController,
    private plt: Platform,
    private loadingController: LoadingController,
    private authService: AuthService
  ) {

    this.getData();
  }

  ngOnInit() {

    // this.route.paramMap.subscribe((paramMap) => {
    //   if(!paramMap.has('dev_id')){
    //     this.navCtrl.navigateBack('/maintabs/tabs/dashboard/vehicle-list');
    //     return;
    //   }

    // console.log("history data: ", paramMap.get('dev_id'), " key: ", paramMap.get('key'))

    // })
    localStorage.removeItem("markerTarget");
    this.datetimeFrom = moment({ hours: 0 }).format();
    this.datetimeTo = moment().format();//new Date(a).toISOString();

    // this.plt.ready().then(() => {
    //   this.loadMap();
    // });
  }
  getData() {
    this.route.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('dev_id') && !paramMap.has('key')) {
        this.getToken();
        this.isSingle = false;
        // this.navCtrl.navigateBack('/maintabs/tabs/dashboard/vehicle-list');
        // return;
        this.plt.ready().then(() => {
          this.loadMap();
        });
      } else {
        let url = this.constUrl.mainUrl + "devices/getDevicebyId?deviceId=" + paramMap.get("dev_id");
        this.loadingController.create({
          spinner: "crescent"
        }).then((loadEl) => {
          loadEl.present();
          this.authService.getMethod(url)
            .subscribe((respData) => {
              loadEl.dismiss();
              if (respData) {
                var res = JSON.parse(JSON.stringify(respData));
                this.deviceObj = res;
                this.messageToSendP = paramMap.get('key');
              }
            },
              err => {
                loadEl.dismiss();
                console.log(err);
              });
        })
      }

    })
  }

  getToken() {
    this.authService.getTokenData().subscribe(data => {
      this.userData = data;
    })
  }

  changeDate(key) {
    this.checkIfMapExist();     // check if map is already defined, if yes then first remove it
    this.datetimeFrom = undefined;
    if (key === 'today') {
      this.datetimeFrom = moment({ hours: 0 }).format();
    } else if (key === 'yest') {
      this.datetimeFrom = moment().subtract(1, 'days').format();
    } else if (key === 'week') {
      this.datetimeFrom = moment().subtract(1, 'weeks').endOf('isoWeek').format();
    } else if (key === 'month') {
      this.datetimeFrom = moment().startOf('month').format();
    }
  }

  checkIfMapExist() {
    if (this.map != undefined) {
      this.map.remove();
      this.loadMap();
    }
  }

  exampleMethodParent($event) {
    this.datetimeFrom = $event;
    this.getHistoryOfSelectedVehicle();
  }

  exampleMethodParent1($event) {
    this.datetimeTo = $event;
    this.getHistoryOfSelectedVehicle();
  }

  getVehObj($event) {
    this.checkIfMapExist();
    this.vehObj = $event;
    // this.vehObj = {
    //   key: 'history',
    //   deviceObj: $event
    // };
    this.getHistoryOfSelectedVehicle();
  }

  loadMap() {
    LocationService.getMyLocation().then((myLocation: MyLocation) => {
      let options: GoogleMapOptions = {
        camera: {
          target: myLocation.latLng
        },
        preferences: {
          zoom: {
            minZoom: 5,
            maxZoom: 50
          },

          padding: {
            left: 10,
            top: 10,
            bottom: 10,
            right: 10
          },

          building: true
        }
      };
      this.map = GoogleMaps.create('map_canvas', options);
    });
  }

  getHistoryOfSelectedVehicle() {
    if (new Date(this.datetimeTo).toISOString() <= new Date(this.datetimeFrom).toISOString()) {
      this.toastCtrl.create({
        message: 'Please select valid time and try again..(to time always greater than from Time).',
        duration: 3000,
        position: 'bottom'
      }).then((toastEl) => {
        toastEl.present();
      });
      return;
    }

    this.loadingController.create({
      message: 'Loading history data...',
    }).then((loadEl) => {
      loadEl.present();
      this.getHistoryData(loadEl);
    });
  }

  getDistanceAndSpeed(loadEl) {
    var url = this.constUrl.mainUrl + "gps/getDistanceSpeed?imei=" + this.vehObj.Device_ID + "&from=" + new Date(this.datetimeFrom).toISOString() + "&to=" + new Date(this.datetimeTo).toISOString();
    this.authService.getMethod(url)
      .subscribe(respData => {
        loadEl.dismiss();
        if (respData != {}) {
          var res = JSON.parse(JSON.stringify(respData));
          console.log("check history: " + res.Distance);
        }

      },
        err => {
          console.log(err);
          loadEl.dismiss();
        });
  }

  getHistoryData(loadEl) {
    var url1 = this.constUrl.mainUrl + "gps/v2?id=" + this.vehObj.Device_ID + "&from=" + new Date(this.datetimeFrom).toISOString() + "&to=" + new Date(this.datetimeTo).toISOString();
    this.authService.getMethod(url1)
      .subscribe(respData => {
        loadEl.dismiss();
        if (respData != undefined) {
          var res = JSON.parse(JSON.stringify(respData));
          console.log("res gps data: ", res);
          if (res.length > 1) {
            this.plotHstory(res.reverse());
          }
        }
      },
        err => {
          loadEl.dismiss();
          console.log(err);
        });
  }

  plotHstory(data) {
    let that = this;
    that.dataArrayCoords = [];
    for (var i = 0; i < data.length; i++) {
      if (data[i].lat && data[i].lng) {
        var arr = [];
        var cumulativeDistance = 0;
        var startdatetime = new Date(data[i].insertionTime);
        arr.push(data[i].lat);
        arr.push(data[i].lng);
        arr.push({ "time": startdatetime.toLocaleString() });
        arr.push({ "speed": data[i].speed });
        arr.push({ "imei": data[i].imei });

        if (data[i].isPastData != true) {
          if (i === 0) {
            cumulativeDistance += 0;
          } else {
            cumulativeDistance += data[i].distanceFromPrevious ? parseFloat(data[i].distanceFromPrevious) : 0;
          }
          data[i]['cummulative_distance'] = (cumulativeDistance).toFixed(2);
          arr.push({ "cumu_dist": data[i]['cummulative_distance'] });
        } else {
          data[i]['cummulative_distance'] = (cumulativeDistance).toFixed(2);
          arr.push({ "cumu_dist": data[i]['cummulative_distance'] });
        }
        that.dataArrayCoords.push(arr);
      }
    }
    that.mapData = [];
    that.mapData = data.map(function (d) {
      return { lat: d.lat, lng: d.lng };
    })
    that.mapData.reverse();

    this.drawOnMap();
  }

  drawOnMap() {
    let that = this;
    let bounds = new LatLngBounds(that.mapData);

    that.map.animateCamera({
      target: bounds
    });

    that.map.addMarker({
      title: 'D',
      position: that.mapData[0],
      icon: 'red',
      styles: {
        'text-align': 'center',
        'font-style': 'italic',
        'font-weight': 'bold',
        'color': 'red'
      },
    }).then((marker: Marker) => {
      marker.showInfoWindow();

      that.map.addMarker({
        title: 'S',
        position: that.mapData[that.mapData.length - 1],
        icon: 'green',
        styles: {
          'text-align': 'center',
          'font-style': 'italic',
          'font-weight': 'bold',
          'color': 'green'
        },
      }).then((marker: Marker) => {
        marker.showInfoWindow();
      });
    });

    that.map.addPolyline({
      points: that.mapData,
      color: '#635400',
      width: 3,
      geodesic: true
    })

    // let mapOptions = {
    //   gestures: {
    //     rotate: false,
    //     tilt: true
    //   }
    // }

  }
  target: number = 0;
  speed: number = 0;
  animateHistory() {
    let that = this;
    // that.showZoom = true;
    if (localStorage.getItem("markerTarget") != null) {
      that.target = JSON.parse(localStorage.getItem("markerTarget"));
    }
    that.playPause = !that.playPause; // This would alternate the state each time

    var coord = that.dataArrayCoords[that.target];
    // that.coordreplaydata = coord;
    var lat = coord[0];
    var lng = coord[1];

    var startPos = [lat, lng];
    that.speed = 200; // km/h

    if (that.playPause) {
      that.map.setCameraTarget({ lat: lat, lng: lng });
      if (that.mark == undefined) {
        debugger
        var icicon;
        // let icon: MarkerIcon = {
        //   url: 'http://icons.iconarchive.com/icons/iconarchive/red-orb-alphabet/24/Number-1-icon.png',
        //   // url: './assets/imgs/running' + that.vehObj.iconType + '.png',
        //   size: {
        //     width: 50,
        //     height: 50
        //   }
        // };
        // console.log("icon object: ", icon)
        if (that.plt.is('ios')) {
          icicon = 'www/assets/imgs/running' + that.vehObj.iconType + '.png';
        } else if (that.plt.is('android')) {
          icicon = './assets/imgs/running' + that.vehObj.iconType + '.png';
        }
        that.map.addMarker({
          icon: {
            url: icicon,
            size: {
              height: 40,
              width: 40
            }
          },
          // icon: icon,
          // styles: {
          //   'text-align': 'center',
          //   'font-style': 'italic',
          //   'font-weight': 'bold',
          //   'color': 'green'
          // },
          position: new LatLng(startPos[0], startPos[1]),
        }).then((marker: Marker) => {
          that.mark = marker;
          that.liveTrack(that.map, that.mark, that.dataArrayCoords, that.target, startPos, that.speed, 100);
        });
      } else {
        // debugger
        // that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
        that.liveTrack(that.map, that.mark, that.dataArrayCoords, that.target, startPos, that.speed, 100);
      }
    } else {
      that.mark.setPosition(new LatLng(startPos[0], startPos[1]));
    }
  }

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};
  liveTrack(map, mark, coords, target, startPos, speed, delay) {
    let that = this;
    // that.events.subscribe("SpeedValue:Updated", (sdata) => {
    //   speed = sdata;
    // })
    var target = target;
    clearTimeout(that.ongoingGoToPoint[coords[target][4].imei]);
    clearTimeout(that.ongoingMoveMarker[coords[target][4].imei]);
    console.log("check coord imei: ", coords[target][4].imei);
    if (!startPos.length)
      coords.push([startPos[0], startPos[1]]);

    function _gotoPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000;
      if (coords[target] == undefined)
        return;
      var dest = new LatLng(coords[target][0], coords[target][1]);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); //in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target][0] - lat) / numStep;
      var deltaLng = (coords[target][1] - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
      }

      function _moveMarker() {
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }

          mark.setPosition(new LatLng(lat, lng));
          map.setCameraTarget(new LatLng(lat, lng))
          that.ongoingMoveMarker[coords[target][4].imei] = setTimeout(_moveMarker, delay);
        } else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }

          mark.setPosition(dest);
          map.setCameraTarget(dest);
          target++;
          that.ongoingGoToPoint[coords[target][4].imei] = setTimeout(_gotoPoint, delay);
        }
      }
      a++
      if (a > coords.length) {

      } else {
        // that.speedMarker = coords[target][3].speed;
        // that.updatetimedate = coords[target][2].time;
        // that.cumu_distance = coords[target][5].cumu_dist;

        if (that.playPause) {
          _moveMarker();
          target = target;
          localStorage.setItem("markerTarget", target);

        } else { }
        // km_h = km_h;
      }
    }
    var a = 0;
    _gotoPoint();
  }

}

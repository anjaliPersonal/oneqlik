import { Component, ViewChild, ElementRef, OnInit, Input, NgZone } from '@angular/core';
import { Geolocation,  Plugins, CameraDirection } from '@capacitor/core';
import { Platform } from '@ionic/angular';
import { ILatLng, LatLng, CameraPosition, GoogleMap } from '@ionic-native/google-maps/ngx';
//import { NativeGeocoder, NativeGeocoderOptions, NativeGeocoderResult} from '@ionic-native/native-geocoder/ngx';


declare var google;


@Component({
  selector: 'app-add-geofence',
  templateUrl: './add-geofence.page.html',
  styleUrls: ['./add-geofence.page.scss'],
})
export class AddGeofencePage implements OnInit {
  @ViewChild('mapElement', {static: true}) mapElement: ElementRef;
  public search: string = '';
  private googleAutoComplete = new google.maps.places.AutocompleteService();
  public searchReasults = new Array<any>();
  map: GoogleMap;
  markers = [];
  lat: number;
  long: number;
  constructor(public platform: Platform, private ngZone: NgZone, ) { }

  ngOnInit() {
    this.getLocation();
  }

  async getLocation() {
    const position = await Geolocation.getCurrentPosition();
    console.log('details', position);
    this.lat = position.coords.latitude;
    this.long = position.coords.longitude;
    console.log(this.lat);
    const latLng = new google.maps.LatLng(this.lat, this.long);
    const mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
  }

  watchPosition() {
    const wait = Geolocation.watchPosition({}, (position, err) => {
    });
    console.log('detalis 2', wait);
  }

  // searchChanged() {
  //   if(!this.search.trim().length) return;
  //   this.googleAutoComplete.getPlacePredictions({ input: this.search}, data => {
  //     this.ngZone.run(() => {
  //       this.searchReasults = data;
  //     });
  //     //console.log("cheack", data);
  //   });
  //   console.log("result", this.searchReasults);
  // }

  // showPlace(item: any) {
  //   console.log(item);
  //   console.log("map2", this.map);
  //   let that = this;
  //   that.googleAutoComplete.query = item.description;
  //   console.log("console item=>" + JSON.stringify(item));
  //   let options: NativeGeocoderOptions = {
  //     useLocale: true,
  //     maxResults: 5
  //   };
  //   this.nativeGeocoder.forwardGeocode( item.description, options)
  //   .then((result: NativeGeocoderResult[]) => {
  //     console.log('The coordinates are latitude=' + result[0].latitude + ' and longitude=' + result[0].longitude);
  //     that.newLat = result[0].latitude;
  //     that.newLng = result[0].longitude;
  //   //   let pos: CameraPosition<ILatLng> = {
  //   //     target: new LatLng(that.newLat, that.newLng),
  //   //   zoom: 15,
  //   //   tilt: 30
  //   // };
  //     // this.map.animateCamera(pos);
  //     // that.map.animateCamera({
  //     //   target: { lat: that.newLat, lng: that.newLng },
  //     //   zoom: 15,
  //     //   duration: 3000,
  //     //   padding: 0 // default = 20px
  //     //   });
  //     this.map.addMarker({
  //     title: '',
  //     position: new LatLng(that.newLat, that.newLng)
  //   }).then((data) => {
  //     console.log("marker")
  //   });
  //   });
  // }

}

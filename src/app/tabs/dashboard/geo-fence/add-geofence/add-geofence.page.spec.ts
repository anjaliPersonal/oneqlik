import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGeofencePage } from './add-geofence.page';

describe('AddGeofencePage', () => {
  let component: AddGeofencePage;
  let fixture: ComponentFixture<AddGeofencePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddGeofencePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGeofencePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

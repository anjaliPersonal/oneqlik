import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeoFencePage } from './geo-fence.page';

describe('GeoFencePage', () => {
  let component: GeoFencePage;
  let fixture: ComponentFixture<GeoFencePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeoFencePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeoFencePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

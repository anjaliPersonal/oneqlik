import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';
import { AuthService } from 'src/app/auth/auth.service';
import { URLs } from 'src/app/app.model';

@Component({
  selector: 'app-common',
  templateUrl: './common.page.html',
  styleUrls: ['./common.page.scss'],
})
export class CommonPage implements OnInit {
  deviceObj: any;
  screenKey: string;

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private authService: AuthService,
    private constUrl: URLs,
    private loadingController: LoadingController
  ) {
    this.getData();
  }

  ngOnInit() {

  }

  getData() {
    this.route.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('Device_ID') && !paramMap.has('key')) {
        this.navCtrl.navigateBack('/maintabs/tabs/dashboard/vehicle-list');
        return;
      }
      let url = this.constUrl.mainUrl + "devices/getDevicebyId?deviceId=" + paramMap.get("Device_ID");
      this.loadingController.create({
        spinner: "crescent"
      }).then((loadEl) => {
        loadEl.present();
        this.authService.getMethod(url)
          .subscribe((respData) => {
            loadEl.dismiss();
            if (respData) {
              var res = JSON.parse(JSON.stringify(respData));
              this.deviceObj = res;
              this.screenKey = paramMap.get('key');
            }
          },
            err => {
              loadEl.dismiss();
              console.log(err);
            });
      })
    })
  }

}

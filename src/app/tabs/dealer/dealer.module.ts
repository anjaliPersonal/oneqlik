import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DealerPage } from './dealer.page';
import { AddPointsComponent } from './add-points/add-points';
import { AddDealerPage } from './add-dealer/add-dealer.page';

const routes: Routes = [
  {
    path: '',
    component: DealerPage
  }
];

@NgModule({
  entryComponents: [AddPointsComponent, AddDealerPage],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DealerPage, AddPointsComponent, AddDealerPage]
})
export class DealerPageModule { }

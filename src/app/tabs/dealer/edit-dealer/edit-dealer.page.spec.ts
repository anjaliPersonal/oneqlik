import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDealerPage } from './edit-dealer.page';

describe('EditDealerPage', () => {
  let component: EditDealerPage;
  let fixture: ComponentFixture<EditDealerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDealerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDealerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

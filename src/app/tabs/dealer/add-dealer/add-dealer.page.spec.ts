import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDealerPage } from './add-dealer.page';

describe('AddDealerPage', () => {
  let component: AddDealerPage;
  let fixture: ComponentFixture<AddDealerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDealerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDealerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

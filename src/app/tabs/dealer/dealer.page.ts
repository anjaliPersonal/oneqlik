import { LoadingController, IonInfiniteScroll, ModalController, Platform, ToastController, AlertController } from '@ionic/angular';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Plugins } from '@capacitor/core';

import { AuthService } from 'src/app/auth/auth.service';
import { URLs } from 'src/app/app.model';
import { Router } from '@angular/router';
import { AddPointsComponent } from './add-points/add-points';
import { AddDealerPage } from './add-dealer/add-dealer.page';

@Component({
  selector: 'app-dealer',
  templateUrl: './dealer.page.html',
  styleUrls: ['./dealer.page.scss'],
})
export class DealerPage implements OnInit {
  @ViewChild(IonInfiniteScroll, {
    static: true
  }) infiniteScroll: IonInfiniteScroll;
  userData: any = {};
  laodingEl: HTMLIonLoadingElement;
  pageNo: number = 1;
  limit: number = 6;
  dealersList: any[] = [];
  scrollEv: any;
  platformKey: string;
  searchBar: boolean = false;

  constructor(
    private authService: AuthService,
    private constURL: URLs,
    private loadingController: LoadingController,
    private route: Router,
    private modalCtrl: ModalController,
    private platform: Platform,
    private toastController: ToastController,
    private alertController: AlertController
  ) {
    this.getToken();
    if (this.platform.is('android')) {
      this.platformKey = 'md';
    } else if (this.platform.is('ios')) {
      this.platformKey = 'ios';
    }
  }

  ngOnInit() {
  }

  getToken() {
    this.authService.getTokenData().subscribe(data => {
      this.userData = data;
      console.log("vehicle data=> ", this.userData);
      this.getDealersList();
    })
  }

  getDealersList() {
    if (this.scrollEv) {
      this.getData();
    } else {
      this.loadingController.create({
        message: "Loading dealers..."
      }).then(loadEl => {
        loadEl.present();
        this.laodingEl = loadEl;
        this.pageNo = 1;
        this.dealersList = [];
        this.getData();
      });
    }
  }

  addPoints(item) {
    this.modalCtrl.create({
      component: AddPointsComponent,
      componentProps: {
        'params': this.userData,
        'dealerData': item
      },
      cssClass: 'my-custom-modal-css'
    }).then((modalEl) => {
      modalEl.present();
      return modalEl.onDidDismiss();
    }).then((resultData) => {
      console.log(resultData);
    });
  }

  getData() {
    const bUrl = this.constURL.mainUrl + 'users/getDealers?supAdmin=' + this.userData._id + '&pageNo=' + this.pageNo + '&size=' + this.limit;
    this.authService.getMethod(bUrl)
      .subscribe(resData => {
        if (this.laodingEl) {
          this.laodingEl.dismiss();
        }
        let parsedData;
        if (this.scrollEv) {
          this.scrollEv.target.complete();
          this.scrollEv.target.disable = true;
          parsedData = JSON.parse(JSON.stringify(resData));
          if (parsedData.length === 0) {
            return;
          }
          for (let i = 0; i < parsedData.length; i++) {
            this.dealersList.push(parsedData[i]);
          }
        } else {
          if (this.laodingEl) {
            this.laodingEl.dismiss();
          }
          parsedData = JSON.parse(JSON.stringify(resData));
          if (parsedData.length === 0) {
            return;
          }
          this.dealersList = parsedData;
        }
        // this.dealersList = JSON.parse(JSON.stringify(resData));
      },
        err => {
          if (this.laodingEl) {
            this.laodingEl.dismiss();
          }
          console.log("getting error from server=> ", err);
        });
  }

  loadData(event) {
    this.scrollEv = undefined;
    this.laodingEl = undefined;
    this.scrollEv = event;
    this.pageNo = this.pageNo + 1;
    this.getDealersList();
  }

  onDealerClicked(dealerObj) {
    Plugins.Storage.get({ key: 'authData' }).then(storedToken => {
      if (!storedToken || !storedToken.value) {
        return;
      }
      Plugins.Storage.set({ key: 'superAdminToken', value: storedToken.value });
      localStorage.setItem('custumer_status', 'OFF');
      localStorage.setItem('dealer_status', 'ON');
      var url = this.constURL.mainUrl + "users/getCustumerDetail?uid=" + dealerObj._id;
      this.authService.getMethod(url)
        .subscribe(respData => {
          debugger
          var data = JSON.parse(JSON.stringify(respData));
          const tokenS = JSON.stringify(data.custumer_token);
          Plugins.Storage.set({ key: 'authData', value: tokenS });
          this.route.navigateByUrl('/maintabs/tabs');
        });
    });
  }

  addDealer(key, dealerData) {
    if (key === 'add') {
      this.modalCtrl.create({
        component: AddDealerPage,
        componentProps: {
          'params': this.userData,
          'key': key
        }
      }).then((modalEl) => {
        modalEl.present();
        return modalEl.onDidDismiss();
      }).then((resultData) => {
        console.log(resultData);
      });
    } else if (key === 'edit') {
      this.modalCtrl.create({
        component: AddDealerPage,
        componentProps: {
          'params': this.userData,
          'dealerData': dealerData,
          'key': key
        }
      }).then((modalEl) => {
        modalEl.present();
        return modalEl.onDidDismiss();
      }).then((resultData) => {
        console.log(resultData);
      });
    }
  }
  onCancel() {
    this.searchBar = false;
    this.getDealersList();
  }

  callSearch(ev) {
    this.pageNo = 1;
    let searchKey = ev.target.value;
    let url = this.constURL.mainUrl + 'users/getDealers?supAdmin=' + this.userData._id + '&pageNo=' + this.pageNo + '&size=' + this.limit + '&search=' + searchKey;
    this.authService.getMethod(url)
      .subscribe(respData => {
        if (respData) {
          var res = JSON.parse(JSON.stringify(respData));
          if (res.length > 0) {
            this.dealersList = res;
          }
        }
      },
        err => {
          console.log("err", err)
        });
  }

  onCustStatus(custData) {
    var msg;
    if (custData.status) {
      msg = 'Are you sure, you want to InActivate this dealer?';
    } else {
      msg = 'Are you sure, you want to Activate this dealer?';
    }
    this.alertController.create({
      message: msg,
      buttons: [{
        text: 'Proceed',
        handler: () => {
          this.doAction(custData);
        }
      },
      {
        text: 'Back',
        handler: () => {
          // this.getcustomer();
        }
      }]
    }).then((alertEl) => {
      alertEl.present();
    })
  }

  showToast(msgString) {
    this.toastController.create({
      message: msgString,
      duration: 2000,
      position: 'middle'
    }).then((toastEl) => {
      toastEl.present();
    });
  }

  doAction(data) {
    let stat;
    if (data.status) {
      stat = false;
    } else {
      stat = true;
    }
    let payloadData = {
      "uId": data._id,
      "loggedIn_id": this.userData._id,
      "status": stat
    };
    let url = this.constURL.mainUrl + 'users/user_status';
    this.loadingController.create({
      message: "Please wait... we are updating dealer's status.."
    }).then((loadEl) => {
      loadEl.present();
      this.authService.postMethod(url, payloadData)
        .subscribe(respData => {
          loadEl.dismiss();
          if (respData) {
            // var res = JSON.parse(JSON.stringify(respData));
            this.showToast("Dealer's status updated successfully.");
            // this.pageNo = 1;
            // this.getCustomersList();
            this.dealersList = [];
            this.getData();
          }
        },
          err => {
            loadEl.dismiss();
            console.log(err);
            this.showToast("Dealer's status updated successfully.");
            // this.pageNo = 1;
            // this.getCustomersList();
            this.dealersList = [];
            this.getData();
          });
    });
  }

  async onDeleteClicked(custData) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: 'Are you sure? You want to delete this dealer?',
      buttons: [
        {
          text: 'Proceed',
          handler: () => {
            console.log('Favorite clicked');
            this.continueToDeleteCust(custData);
          }
        }, {
          text: 'Back',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }

  continueToDeleteCust(custData) {
    let url = this.constURL.mainUrl + "users/deleteUser";
    let payLoadData = {
      "userId": custData._id,
      'deleteuser': true
    }
    this.authService.postMethod(url, payLoadData).
      subscribe(respData => {
        if (respData) {
          var res = JSON.parse(JSON.stringify(respData));
          if (res) {
            this.alertController.create({
              message: 'Dealer deleted successfully. Reload dealer list to reflect changes?',
              buttons: [
                {
                  text: 'Proceed',
                  handler: () => {
                    console.log('Favorite clicked');
                    this.getDealersList();
                  }
                }, {
                  text: 'Back',
                  role: 'cancel',
                  handler: () => {
                    console.log('Cancel clicked');
                  }
                }
              ]
            }).then((alertEl) => {
              alertEl.present();
            });
          }
        }
      },
        err => {
          console.log(err);
        });
  }
}

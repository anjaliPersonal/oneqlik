import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealerPage } from './dealer.page';

describe('DealerPage', () => {
  let component: DealerPage;
  let fixture: ComponentFixture<DealerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-add-points',
    templateUrl: './add-points.html',
    styleUrls: ['./add-points.scss'],
})

export class AddPointsComponent implements OnInit {

    constructor(
        private modalCtrl: ModalController
    ) {

    }

    ngOnInit() { }

    onCancel() {
        this.modalCtrl.dismiss();
    }

}
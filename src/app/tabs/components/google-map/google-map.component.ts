import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment,
  GoogleMapsMapTypeId
} from '@ionic-native/google-maps';
import { ModalController } from '@ionic/angular';
import { ExpiredPage } from '../../dashboard/expired/expired.page';

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.scss'],
})
export class GoogleMapComponent implements OnInit, AfterViewInit {
  map: GoogleMap;
  @Input() mapOptions: GoogleMapOptions;
  @Input() passedObj: any;
  @Input() key: string;

  traffic: boolean = false;
  constructor(
    public modalController: ModalController,
  ) {

  }

  ngOnInit() { }

  ngAfterViewInit() {
    console.log("received object: ", this.passedObj);
    console.log("passed key is: ", this.key);
    if (this.key === 'live') {
      this.forSinglevehicle(this.passedObj);
    } else if (this.key === 'history') {

    }
  }

  initMap() {
    let that = this;
    if (that.passedObj.last_location) {
      let mapOptions: GoogleMapOptions = {
        camera: {
          target: {
            lat: that.passedObj.last_location['lat'],
            lng: that.passedObj.last_location['long']
          },
          zoom: 18,
          tilt: 30
        }
      };
      this.map = GoogleMaps.create('map', mapOptions);
      let marker: Marker = this.map.addMarkerSync({
        title: that.passedObj.Device_Name,
        icon: 'blue',
        animation: 'DROP',
        position: {
          lat: that.passedObj.last_location['lat'],
          lng: that.passedObj.last_location['long']
        }
      });
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        // alert('clicked');
      });
    }
  }

  async openExpiredModal(data) {
    const modal = await this.modalController.create({
      component: ExpiredPage,
      componentProps: { param: data }
    });
    return await modal.present();
  }

  forSinglevehicle(data) {
    console.log(data);
    if (data.status == 'Expired') {
      this.openExpiredModal(data);
    } else {
      this.initMap();
    }
  }

  onClickStack(key) {
    let that = this;
    if (key === 's') {
      that.map.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
    } else if (key === 'n') {
      that.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
    } else if (key === 't') {
      that.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
    }
  }

  onClickTraffic() {
    let that = this;
    that.traffic = !that.traffic;
    that.map.setTrafficEnabled(that.traffic);
  }

}

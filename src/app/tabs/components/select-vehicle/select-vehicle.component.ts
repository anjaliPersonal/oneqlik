import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { IonicSelectableComponent } from 'ionic-selectable';
import { Subscription } from 'rxjs';
import { VehicleList } from 'src/types';
import { PortService } from 'src/services';

@Component({
  selector: 'app-select-vehicle',
  templateUrl: './select-vehicle.component.html',
  styleUrls: ['./select-vehicle.component.scss'],
})
export class SelectVehicleComponent implements OnInit {
  page = 2;

  vehicleLists: VehicleList[];
  vehicleList: VehicleList;
  vehicleListsSubscription: Subscription;

  @Output() selectedVehObj = new EventEmitter<string>();
  @Input() receivedParentMessage: string;
  constructor(
    private portService: PortService
  ) { }

  ngOnInit() {

    setTimeout(() => {
      this.vehicleLists = this.portService.getVehicleLists();
      // console.log("in here=> ", this.vehicleLists);
    }, 3000);
  }

  filterVehicleLists(vehicleLists: VehicleList[], text: string) {
    return vehicleLists.filter(veh => {
      return veh.Device_Name.toLowerCase().indexOf(text) !== -1;
    });
  }

  searchVehicles(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    const text = event.text.trim().toLowerCase();
    event.component.startSearch();

    // Close any running subscription.
    if (this.vehicleListsSubscription) {
      this.vehicleListsSubscription.unsubscribe();
    }

    if (!text) {
      // Close any running subscription.
      if (this.vehicleListsSubscription) {
        this.vehicleListsSubscription.unsubscribe();
      }

      event.component.items = this.portService.getVehicleLists(1, 15);

      // Enable and start infinite scroll from the beginning.
      this.page = 2;
      event.component.endSearch();
      event.component.enableInfiniteScroll();
      return;
    }

    this.vehicleListsSubscription = this.portService.getVehiclesAsync().subscribe(vehicles => {
      // Subscription will be closed when unsubscribed manually.
      if (this.vehicleListsSubscription.closed) {
        return;
      }

      event.component.items = this.filterVehicleLists(vehicles, text);
      event.component.endSearch();
    });
  }

  getMoreVehicles(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    const text = (event.text || '').trim().toLowerCase();

    // There're no more vehicles - disable infinite scroll.
    if (this.page > 3) {
      event.component.disableInfiniteScroll();
      return;
    }

    this.portService.getVehiclesAsync(this.page, 15).subscribe(vehs => {
      vehs = event.component.items.concat(vehs);

      if (text) {
        vehs = this.filterVehicleLists(vehs, text);
      }

      event.component.items = vehs;
      event.component.endInfiniteScroll();
      this.page++;
    });
  }

  onChnage(vehObj) {
    this.selectedVehObj.emit(vehObj);
  }
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ModalController } from '@ionic/angular';
// import { Ionic4DatepickerModalComponent } from '@logisticinfotech/ionic4-datepicker';


@Component({
  selector: 'app-from-to',
  templateUrl: './from-to.component.html',
  styleUrls: ['./from-to.component.scss'],
})
export class FromToComponent implements OnInit {

  datePickerObjPtBr: any = {};
  datePickerObj: any = {};
  selectedDate;
  mydate = "";

  @Input() fromDate: string;
  @Output() fromDateOutput = new EventEmitter<string>();
  @Output() toDateOutput = new EventEmitter<string>();

  @Input() toDate: any;

  constructor(public modalCtrl: ModalController) { 
    console.log("parent to child: ", this.fromDate)
  
  }


  ngOnInit() {
   
  }

  onfrChangeFromDate(date) {
    this.fromDateOutput.emit(date);
  }

  onChangeToDate(date) {
    this.toDateOutput.emit(date);
  }

}

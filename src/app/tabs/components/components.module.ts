import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
// import { TextShellComponent } from './text-shell/text-shell.component';
// import { ImageShellComponent } from './image-shell/image-shell.component';
import { FromToComponent } from './from-to/from-to.component';
import { SelectVehicleComponent } from './select-vehicle/select-vehicle.component';
import { FormsModule } from '@angular/forms';
import { IonicSelectableModule } from 'ionic-selectable';
import { PortService } from 'src/services';
import { ShellModule } from 'src/app/shell/shell.module';
import { CheckboxWrapperComponent } from './checkbox-wrapper/checkbox-wrapper.component';
import { ShowHidePasswordComponent } from './show-hide-password/show-hide-password.component';
import { CounterInputComponent } from './counter-input/counter-input.component';
import { RatingInputComponent } from './rating-input/rating-input.component';
import { GoogleMapComponent } from './google-map/google-map.component';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    IonicSelectableModule,
    FormsModule,
    ShellModule,
  ],
  declarations: [
    FromToComponent,
    SelectVehicleComponent,
    CheckboxWrapperComponent,
    ShowHidePasswordComponent,
    CounterInputComponent,
    RatingInputComponent,
    GoogleMapComponent
  ],
  exports: [
    ShellModule,
    FromToComponent,
    SelectVehicleComponent,
    CheckboxWrapperComponent,
    ShowHidePasswordComponent,
    CounterInputComponent,
    RatingInputComponent,
    GoogleMapComponent
  ],
  entryComponents: [],
  providers: [PortService]
})

export class ComponentsModule { }

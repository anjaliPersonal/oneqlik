import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { TabsProvider } from './tabs.provider';
import { Observable } from 'rxjs';
import { Resolve } from '@angular/router';
import { CustShellListingModel } from './tabs.model';
// import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class TabsResolverService implements Resolve<any> {
  userData: any;

  constructor(
    private appService: AppService,
    // private authService: AuthService
  ) { }

  // getToken() {
  //   this.authService.getTokenData().subscribe(data => {
  //     this.userData = data;

  //   })
  // }

  // resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<never> {
  //   const url = this.constUrl.mainUrl + 'users/getCustomer?uid=59cbbdbe508f164aa2fef3d8&pageNo=1&size=10';
  //   return this._http.get(url).pipe(catchError(error => {
  //     return EMPTY;
  //   }),
  //     mergeMap(something => {
  //       let someArr = {};
  //       if (something) {
  //         someArr = {
  //           something
  //         };
  //         return of(someArr);
  //       } else {
  //         return EMPTY;
  //       }
  //     })
  //   );
  // }

  private getDataWithShell(): Observable<CustShellListingModel> {
    // private getDataWithShell(): Observable<CustShellModel> {
    // Initialize the model specifying that it is a shell model
    // debugger
    const CustModel: CustShellListingModel = new CustShellListingModel(true);
    // const CustModel: CustShellModel = new CustShellModel(true);
    const dataObservable = this.appService.getData();

    const tabsProvider = new TabsProvider(
      CustModel,
      dataObservable
    );
    return tabsProvider.observable;
  }

  resolve() {
    // debugger
    // this.getToken();
    // this.authService.getTokenData().subscribe(data => {
    //   this.userData = data;
    // Get the Shell Provider from the service
    const tabsProviderObservable = this.getDataWithShell();

    // Resolve with Shell Provider
    const observablePromise = new Promise((resolve, reject) => {
      resolve(tabsProviderObservable);
    });
    // console.log("observablePromise=> ", observablePromise)
    return observablePromise;
    // })

  }
}

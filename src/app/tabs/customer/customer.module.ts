import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { AddCustomerComponent } from 'src/app/tabs/customer/add-customer/add-customer.component';
import { CustomerPage } from './customer.page';
import { TabsResolverService } from '../tabs-resolver.service';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  {
    path: '',
    component: CustomerPage,
    resolve: {
      someKey: TabsResolverService
    }
  }
];

@NgModule({
  entryComponents: [AddCustomerComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustomerPage, AddCustomerComponent],
  providers: [TabsResolverService]
})
export class CustomerPageModule { }

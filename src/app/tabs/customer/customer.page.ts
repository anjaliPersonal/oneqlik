import { Component, OnInit } from '@angular/core';
// import { ActivatedRoute } from '@angular/router';
import { ModalController, LoadingController, Platform, ToastController, AlertController } from '@ionic/angular';

import { AddCustomerComponent } from './add-customer/add-customer.component';
import { CustShellListingModel } from '../tabs.model';
import { AuthService } from 'src/app/auth/auth.service';
import { URLs } from 'src/app/app.model';
import { Plugins } from '@capacitor/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.page.html',
  styleUrls: [
    './customer.page.scss',
    './shell-elements.scss'],
})
export class CustomerPage implements OnInit {
  // We will assign data coming from the Route Resolver to this property
  routeResolveData: CustShellListingModel;
  scrollEv: any;
  userData: any = {};
  pageNo: number = 1;
  limit: number = 6;
  laodingEl: HTMLIonLoadingElement;
  customerListData: any[];
  platformKey: string;
  searchBar: boolean = false;

  constructor(
    private modalCtrl: ModalController,
    private route: Router,
    private authService: AuthService,
    private constURL: URLs,
    private loadingController: LoadingController,
    private platform: Platform,
    private toastController: ToastController,
    private alertController: AlertController
  ) {
    this.getToken();
    if (this.platform.is('android')) {
      this.platformKey = 'md';
    } else if (this.platform.is('ios')) {
      this.platformKey = 'ios';
    }
  }

  getToken() {
    this.authService.getTokenData().subscribe(data => {
      this.userData = data;
      console.log("customer data=> ", this.userData);
      this.getCustomersList();
    })
  }

  onCancel() {
    this.searchBar = false;
    this.getCustomersList();
  }

  callSearch(ev) {
    this.pageNo = 1;
    let searchKey = ev.target.value;
    let url = this.constURL.mainUrl + 'users/getCust?uid=' + this.userData._id + '&pageNo=' + this.pageNo + '&size=' + this.limit + '&search=' + searchKey;
    this.authService.getMethod(url)
      .subscribe(respData => {
        if (respData) {
          var res = JSON.parse(JSON.stringify(respData));
          if (res.length > 0) {
            this.customerListData = res;
          }
        }
      },
        err => {
          console.log("err", err)
        });
  }

  async onDeleteClicked(custData) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: 'Are you sure? You want to delete this customer?',
      // position: 'middle',
      buttons: [
        {
          // side: 'start',
          // icon: 'trash',
          text: 'Proceed',
          handler: () => {
            console.log('Favorite clicked');
            this.continueToDeleteCust(custData);
          }
        }, {
          text: 'Back',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }

  continueToDeleteCust(custData) {
    let url = this.constURL.mainUrl + "users/deleteUser";
    let payLoadData = {
      "userId": custData._id,
      'deleteuser': true
    }
    this.authService.postMethod(url, payLoadData).
      subscribe(respData => {
        if (respData) {
          var res = JSON.parse(JSON.stringify(respData));
          if (res) {
            this.toastController.create({
              // header: 'Alert',
              message: 'Customer deleted successfully. Reload customer list to reflect changes?',
              position: 'middle',
              buttons: [
                {
                  // side: 'start',
                  icon: 'reload',
                  text: 'Proceed',
                  handler: () => {
                    console.log('Favorite clicked');
                    this.getCustomersList();
                  }
                }, {
                  text: 'Back',
                  role: 'cancel',
                  handler: () => {
                    console.log('Cancel clicked');
                  }
                }
              ]
            }).then((toastEl) => {
              toastEl.present();
            });
          }
        }
      },
        err => {
          console.log(err);
        });
  }

  // ngOnInit() {
  //   // debugger
  //   console.log('Progressive Shell Resovlers - ngOnInit()');
  //   if (this.route && this.route.data) {
  //     // We resolved a promise for the data Observable
  //     const promiseObservable = this.route.data;
  //     console.log('Progressive Shell Resovlers - Route Resolve Observable => promiseObservable: ', promiseObservable);

  //     if (promiseObservable) {
  //       promiseObservable.subscribe(promiseValue => {
  //         const dataObservable = promiseValue['someKey'];
  //         console.log('Progressive Shell Resovlers - Subscribe to promiseObservable => dataObservable: ', dataObservable);

  //         if (dataObservable) {
  //           dataObservable.subscribe(observableValue => {
  //             if (observableValue.length !== undefined) {
  //               const somArr = {
  //                 isShell: false,
  //                 something: observableValue
  //               };
  //               const pageData: CustShellListingModel = somArr;
  //               if (pageData) {
  //                 this.routeResolveData = pageData;
  //               }
  //               // tslint:disable-next-line:max-line-length
  //               console.log('Progressive Shell Resovlers - Subscribe to dataObservable (can emmit multiple values) => PageData (' + ((pageData && pageData.isShell) ? 'SHELL' : 'REAL') + '): ', pageData);
  //             } else {
  //               const pageData: CustShellListingModel = observableValue;
  //               if (pageData) {
  //                 this.routeResolveData = pageData;
  //               }
  //               // As we are implementing an App Shell architecture, pageData will be firstly an empty shell model,
  //               // and the real remote data once it gets fetched
  //               // tslint:disable-next-line:max-line-length
  //               console.log('Progressive Shell Resovlers - Subscribe to dataObservable (can emmit multiple values) => PageData (' + ((pageData && pageData.isShell) ? 'SHELL' : 'REAL') + '): ', pageData);
  //             }
  //           });
  //         } else {
  //           console.warn('No dataObservable coming from Route Resolver promiseObservable');
  //         }
  //       });
  //     } else {
  //       console.warn('No promiseObservable coming from Route Resolver data');
  //     }
  //   } else {
  //     console.warn('No data coming from Route Resolver');
  //   }
  // }

  ngOnInit() {

  }

  getCustomersList() {
    if (this.scrollEv) {
      this.getData();
    } else {
      this.loadingController.create({
        message: 'Loading customers...'
      }).then(loadEl => {
        loadEl.present();
        this.laodingEl = loadEl;
        this.pageNo = 1;
        this.customerListData = [];
        this.getData();
      });
    }
  }

  getData() {
    var baseURLp = this.constURL.mainUrl + 'users/getCust?uid=' + this.userData._id + '&pageNo=' + this.pageNo + '&size=' + this.limit;
    this.authService.getMethod(baseURLp)
      .subscribe(resData => {
        if (this.laodingEl) {
          this.laodingEl.dismiss();
        }
        let parsedData;
        if (this.scrollEv) {
          this.scrollEv.target.complete();
          this.scrollEv.target.disable = true;
          parsedData = JSON.parse(JSON.stringify(resData));
          if (parsedData.length === 0) {
            return;
          }
          for (let i = 0; i < parsedData.length; i++) {
            this.customerListData.push(parsedData[i]);
          }
        } else {
          if (this.laodingEl) {
            this.laodingEl.dismiss();
          }
          parsedData = JSON.parse(JSON.stringify(resData));
          if (parsedData.length === 0) {
            return;
          }
          this.customerListData = parsedData;
        }
      },
        err => {
          console.log("error=> ", err);
          if (this.laodingEl) {
            this.laodingEl.dismiss();
          }
          if (this.scrollEv) {
            this.scrollEv.target.complete();
            this.scrollEv.target.disable = true;
          }
        });
  }

  onAddCust(key, custData) {
    if (key === 'add') {
      this.modalCtrl.create({
        component: AddCustomerComponent,
        componentProps: {
          'params': this.userData,
          'key': key
        }
      }).then((modalEl) => {
        modalEl.present();
        return modalEl.onDidDismiss();
      }).then((resultData) => {
        console.log(resultData);
      });
    } else if (key === 'edit') {
      this.modalCtrl.create({
        component: AddCustomerComponent,
        componentProps: {
          'params': this.userData,
          'custData': custData,
          'key': key
        }
      }).then((modalEl) => {
        modalEl.present();
        return modalEl.onDidDismiss();
      }).then((resultData) => {
        console.log(resultData);
      });
    }

  }

  onCustStatus(custData) {
    var msg;
    if (custData.status) {
      msg = 'Are you sure, you want to InActivate this customer?';
    } else {
      msg = 'Are you sure, you want to Activate this customer?';
    }
    this.alertController.create({
      message: msg,
      buttons: [{
        text: 'Proceed',
        handler: () => {
          this.doAction(custData);
        }
      },
      {
        text: 'Back',
        handler: () => {
          // this.getcustomer();
        }
      }]
    }).then((alertEl) => {
      alertEl.present();
    })
  }

  showToast(msgString) {
    this.toastController.create({
      message: msgString,
      duration: 2000,
      position: 'middle'
    }).then((toastEl) => {
      toastEl.present();
    });
  }

  doAction(data) {
    let stat;
    if (data.status) {
      stat = false;
    } else {
      stat = true;
    }
    let payloadData = {
      "uId": data._id,
      "loggedIn_id": this.userData._id,
      "status": stat
    };
    let url = this.constURL.mainUrl + 'users/user_status';
    this.loadingController.create({
      message: "Please wait... we are updating customer's status.."
    }).then((loadEl) => {
      loadEl.present();
      this.authService.postMethod(url, payloadData)
        .subscribe(respData => {
          loadEl.dismiss();
          if (respData) {
            // var res = JSON.parse(JSON.stringify(respData));
            this.showToast("Customer's status updated successfully.");
            // this.pageNo = 1;
            // this.getCustomersList();
            this.customerListData = [];
            this.getData();
          }
        },
          err => {
            loadEl.dismiss();
            console.log(err);
            this.showToast("Customer's status updated successfully.");
            // this.pageNo = 1;
            // this.getCustomersList();
            this.customerListData = [];
            this.getData();
          });
    });
  }


  loadData(event) {
    this.scrollEv = undefined;
    this.scrollEv = event;
    this.pageNo = this.pageNo + 1;
    this.getCustomersList();
  }

  onCustomerClicked(custObj) {
    Plugins.Storage.get({ key: 'authData' }).then(storedToken => {
      if (!storedToken || !storedToken.value) {
        return;
      }
      Plugins.Storage.set({ key: 'dealer', value: storedToken.value });
      localStorage.setItem('custumer_status', 'ON');
      localStorage.setItem('dealer_status', 'OFF');
      var url = this.constURL.mainUrl + "users/getCustumerDetail?uid=" + custObj._id;
      this.authService.getMethod(url)
        .subscribe(respData => {
          var data = JSON.parse(JSON.stringify(respData));
          const tokenS = JSON.stringify(data.custumer_token);
          Plugins.Storage.set({ key: 'authData', value: tokenS });
          this.route.navigateByUrl('/maintabs/tabs');
        });
    });
  }

}

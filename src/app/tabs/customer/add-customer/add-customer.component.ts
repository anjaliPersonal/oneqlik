import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController, NavParams, ToastController } from '@ionic/angular';
import { URLs } from 'src/app/app.model';
import * as moment from 'moment';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss'],
})
export class AddCustomerComponent implements OnInit {
  docNumber: any;
  userId: any;
  docType: any;
  address: any;
  mobNumber: any;
  pass: any;
  eMail: any;
  lName: any;
  fName: any;
  cpass: any;
  userData: any;
  msgString: string = "";
  dealersData: any = [];
  show: boolean = false;
  show1: boolean = false;
  dateShow: boolean = false;
  selectedDealer: any;
  titleString: string;
  custData: any = {};
  screenKey: any;
  expDate: any;
  exDate: any;

  constructor(
    private modalCtrl: ModalController,
    private loadingController: LoadingController,
    private consURL: URLs,
    private navParams: NavParams,
    private toastController: ToastController,
    private authService: AuthService
  ) {

  }

  ngOnInit() {
    this.userData = this.navParams.get('params');
    this.screenKey = this.navParams.get('key');
    if (this.screenKey === 'add') {
      this.titleString = "Add Customer";
      console.log('Navparams get: ', this.navParams.get('params'));
      this.getDealerList();
    } else if (this.screenKey === 'edit') {
      this.custData = this.navParams.get('custData');
      this.titleString = "Edit " + this.custData.first_name + "'s info";
      console.log("customer's data: ", this.custData);
      console.log(moment(new Date(this.custData.expiration_date), 'DD-MM-YYYY').format('MM/DD/YYYY'))
      this.fillUpForm(this.custData);
    }
  }

  fillUpForm(data) {
    this.userId = (data.userid ? data.userid : null);
    this.fName = (data.first_name ? data.first_name : null);
    this.lName = (data.last_name ? data.last_name : null);
    this.pass = (data.pass ? data.pass : null);
    // this.cpass = (data.pass ? data.pass : null);
    this.mobNumber = (data.phone ? data.phone : null);
    this.eMail = (data.email ? data.email : null);
    // this.docType = ((data.docObject.length > 0) ? data.docObject[0].doctype : null);
    // this.docNumber = ((data.docObject.length > 0) ? data.docObject[0].phone : null);
    this.address = (data.address ? data.address : null);
    this.expDate = (data.expiration_date ? moment(new Date(data.expiration_date), 'DD-MM-YYYY').format('DD/MM/YYYY hh:mm a') : moment(new Date(this.getExpDate()), 'DD-MM-YYYY').format('DD/MM/YYYY hh:mm a'))
  }

  onCancle() {
    this.modalCtrl.dismiss();
  }

  onSubmit() {
    debugger
    if (this.screenKey === 'add') {
      this.loadingController.create({
        message: "Please wait... we are adding customer...",
      }).then((loadEl) => {
        loadEl.present();
        this.addCustomer(loadEl);
      })
    } else if (this.screenKey === 'edit') {
      this.editCustomer();
    }
  }

  editCustomer() {
    let payLoad: any = {};
    let url = this.consURL.mainUrl + "users/editUserDetails";
    if (this.exDate == undefined) {
      if (this.custData.expiration_date) {
        this.exDate = this.custData.expiration_date;
      }
    }
    payLoad = {
      "contactid": this.custData._id,
      "address": this.address,
      "expire_date": new Date(this.exDate).toISOString(),
      "first_name": this.fName,
      "last_name": this.lName,
      "status": this.custData.status,
      "user_id": this.userId,
      "email": this.eMail,
      "phone": this.mobNumber
    }
    this.loadingController.create({
      message: "Please wait... we are updating customer...",
    }).then((loadEl) => {
      loadEl.present();
      this.authService.postMethod(url, payLoad)
        .subscribe(respData => {
          loadEl.dismiss();
          if (respData) {
            var res = JSON.parse(JSON.stringify(respData));
            this.showToast('Customer updated successfully');
            this.onCancle();
          }
        },
          err => {
            loadEl.dismiss();
            console.log(err);
          })
    });
  }
  dealerOnChnage(dealerData) {
    console.log(dealerData);
  }

  showToast(msgString) {
    this.toastController.create({
      message: msgString,
      duration: 2000,
      position: 'middle'
    }).then((toastEl) => {
      toastEl.present();
    });
  }

  getDealerList() {
    let url = this.consURL.mainUrl + "users/getAllDealerVehicles?supAdmin=" + this.userData._id;
    this.msgString = "Please wait we are loading dealer's list...";
    this.authService.getMethod(url)
      .subscribe(respData => {
        this.msgString = "";
        if (respData) {
          var res = JSON.parse(JSON.stringify(respData));
          // console.log("dealers list: ", res);
          this.dealersData = res;
        }
      },
        err => {
          this.msgString = "";
          console.log(err);
        });
  }

  getExpDate() {
    var currentDate = new Date();
    var futureMonth = moment(currentDate).add('years', 1).format('L');
    // console.log("futureMonth: " + new Date(futureMonth).toISOString());
    return new Date(futureMonth).toISOString();
  }

  addCustomer(loadEl) {
    let payLoad: any = {};

    if (this.pass !== this.cpass) {
      loadEl.dismiss();
      this.showToast('Password mismatched!!!');
      return;
    }

    payLoad = {
      "first_name": this.fName,
      "last_name": this.lName,
      "email": this.eMail,
      "password": this.pass,
      "phone": this.mobNumber,
      "expdate": this.getExpDate(),
      "custumer": true,
      "user_id": this.userId,
      "address": this.address,
      "std_code": {
        "countryCode": "in",
        "dialcode": "91"
      },
      "timezone": "Asia/Kolkata",
      "imageDoc": [
        {
          "doctype": this.docType,
          "image": "",
          "phone": this.docNumber
        }
      ]
    }

    if (this.userData.isSuperAdmin) {
      payLoad.supAdmin = this.userData._id;
      payLoad.isDealer = false;
      if (this.selectedDealer !== undefined) {
        payLoad.Dealer = this.selectedDealer;
      } else {
        payLoad.Dealer = this.userData._id;
      }
    } else if (this.userData.isDealer) {
      payLoad.supAdmin = this.userData.supAdmin;
      payLoad.isDealer = this.userData.isDealer;
      if (this.selectedDealer !== undefined) {
        payLoad.Dealer = this.selectedDealer;
      } else {
        payLoad.Dealer = this.userData._id;
      }
    }

    let url = this.consURL.mainUrl + "users/signUp";
    this.authService.postMethod(url, payLoad)
      .subscribe(respData => {
        loadEl.dismiss();
        if (respData) {
          var res = JSON.parse(JSON.stringify(respData));
          console.log("added customer check: ", res);
          if (res.message) {
            this.showToast(res.message);
            if (res.message === 'Registered') {
              this.onCancle();
            }
          }
        }
      },
        err => {
          console.log(err);
          loadEl.dismiss();
        });
  }
}

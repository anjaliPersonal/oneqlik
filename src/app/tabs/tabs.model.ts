export class CustShellModel {
  first_name: string;
  last_name: string;
  created_on: string;
  email: string;
  phone: number;
  pass: string;
  total_vehicle: number;
}

export class CustShellListingModel {
  something?: Array<CustShellModel> = [
    new CustShellModel(),
    new CustShellModel(),
    new CustShellModel(),
    new CustShellModel(),
    new CustShellModel(),
    new CustShellModel()
  ];

  constructor(readonly isShell: boolean) { }
}



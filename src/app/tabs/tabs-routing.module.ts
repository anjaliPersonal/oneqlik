import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { TabsResolverService } from './tabs-resolver.service';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'dashboard',
        children: [
          {
            path: '',
            loadChildren: './dashboard/dashboard.module#DashboardPageModule'
          },
          {
            path: 'live-tracking',
            loadChildren: './dashboard/live-tracking/live-tracking.module#LiveTrackingPageModule'
          },
          {
            path: 'vehicle-list',
            loadChildren: './dashboard/vehicle-list/vehicle-list.module#VehicleListPageModule'
          },
          {
            path: 'history',
            children: [
              {
                path: '',
                loadChildren: './dashboard/history/history.module#HistoryPageModule'
              },
              {
                path: ':dev_id/:key',
                loadChildren: './dashboard/history/history.module#HistoryPageModule'
              }
            ]
          },
          {
            path: 'geo-fence',
            loadChildren: './dashboard/geo-fence/geo-fence.module#GeoFencePageModule'
          },
          {
            path: 'common/:Device_ID/:key',
            loadChildren: './dashboard/common/common.module#CommonPageModule'
          }
        ]
      },
      {
        path: 'dealer',
        children: [
          {
            path: '',
            loadChildren: './dealer/dealer.module#DealerPageModule'
          },
          {
            path: 'add-dealer',
            loadChildren: './dealer/add-dealer/add-dealer.module#AddDealerPageModule'
          },
          {
            path: 'edit-dealer/:dealerId',
            loadChildren: './dealer/edit-dealer/edit-dealer.module#EditDealerPageModule'
          }
        ]
      },
      {
        path: 'customer',
        children: [
          {
            path: '',
            loadChildren: './customer/customer.module#CustomerPageModule',
            // resolve: {
            //   someKey: TabsResolverService
            // }
          },
          {
            path: 'edit-cust/:custId',
            loadChildren: './customer/edit-customer/edit-customer.module#EditCustomerPageModule'
          }
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: './profile/profile.module#ProfilePageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/maintabs/tabs/dashboard',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/maintabs/tabs/dashboard',
    pathMatch: 'full'
  },
  { path: 'expired', loadChildren: './dashboard/expired/expired.module#ExpiredPageModule' },

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  providers: [TabsResolverService]
})

export class TabsRoutingModule {

}

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
// import { Plugins } from '@capacitor/core';
// import { Router } from '@angular/router';
import { Events, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  userData: any = {};
  isCustomer: boolean = false;
  isDealer: boolean = false;

  constructor(
    private authService: AuthService,
    private event: Events,
    private menuCtrl: MenuController
  ) { }

  ngOnInit() {
    this.getToken();
    this.checkIfDealerOrCustomer();
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  getToken() {
    this.authService.getTokenData().subscribe(data => {
      this.userData = data;
      console.log("tabs data=> ", this.userData);
      if (this.userData.isDealer === false && this.userData.isOperator === false && this.userData.isOrganisation === false && this.userData.isSuperAdmin === false) {
        this.isCustomer = true;
        this.event.publish('Auth:Role', {
          customer: this.isCustomer,
          dealer: this.isDealer
        });
      }
    })
  }

  checkIfDealerOrCustomer() {
    var dealStat = localStorage.getItem('dealer_status');
    var custStat = localStorage.getItem('custumer_status');
    if (dealStat !== null && dealStat === 'ON' && custStat === 'OFF') {
      this.isDealer = true;
      this.event.publish('Auth:Role', {
        customer: this.isCustomer,
        dealer: this.isDealer
      });
    } else if(custStat !== null && custStat === 'ON' && dealStat === 'OFF'){
      this.isCustomer = true;
      this.event.publish('Auth:Role', {
        customer: this.isCustomer,
        dealer: this.isDealer
      });
    }
  }

}

import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-expense-detail',
  templateUrl: './expense-detail.page.html',
  styleUrls: ['./expense-detail.page.scss'],
})
export class ExpenseDetailPage implements OnInit {
  userData: any;
  expId: string;
  // expName: string;

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private authService: AuthService,
    private elementRef: ElementRef
  ) {
    this.getToken();
  }

  setStyle(value: string): void {
    this.elementRef.nativeElement.style.setProperty('--my-var', value);
  }

  ngOnInit() {

    this.route.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('expId')) {
        this.navCtrl.navigateBack('/expense');
        return;
      }
      this.expId = paramMap.get('expId');
      console.log("expense id: ", this.expId)
      if (this.expId === 'tools') {
        this.setStyle('#ee5555');
      } else if (this.expId === 'service') {
        this.setStyle('#fe8a49');
      } else if (this.expId === 'fuel') {
        this.setStyle('#5b8016');
      } else if (this.expId === 'labour') {
        this.setStyle('#334861');
      } else if (this.expId === 'salary') {
        this.setStyle('#b8996a');
      } else if (this.expId === 'toll') {
        this.setStyle('#061c52');
      } else {

      }

      // if (this.report.key === 'summ' || this.report.key === 'dist') {
      //   this.datetimeFrom = moment({ hours: 0 }).subtract(1, 'days').format(); // yesterday date with 12:00 am
      //   this.datetimeTo = moment({ hours: 0 }).format(); // today date and time with 12:00am
      // } else {
      //   this.datetimeFrom = moment({ hours: 0 }).format();
      //   this.datetimeTo = moment().format();//new Date(a).toISOString();
      // }
    });
  }

  getToken() {
    this.authService.getTokenData().subscribe(data => {
      this.userData = data;
      // if (this.report.key === 'daily') {
      //   this.msgString = "Loading daily report(s)";
      //   this.getDailyReportData();
      // } else if (this.report.key === 'summ') {
      //   this.msgString = "Loading summary report(s)";
      //   this.getGlobalreportData();
      // } else if (this.report.key === 'ospeed') {
      //   this.msgString = "Loading overspeed report(s)";
      //   this.getGlobalreportData();
      // }
    })
  }

}

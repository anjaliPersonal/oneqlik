import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { AuthService } from '../auth/auth.service';
import { URLs } from '../app.model';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-expense',
  templateUrl: './expense.page.html',
  styleUrls: [
    './styles/expense.page.scss',
    './styles/expense.shell.scss',
    './styles/expense.responsive.scss'
  ]
})
export class ExpensePage implements OnInit {
  userData: any;
  totalExpense: number;
  datetimeFrom: string;
  datetimeTo: string;
  vehObj: any;
  expensesData: any[];

  constructor(
    private authService: AuthService,
    private constUrl: URLs,
    private loadingController: LoadingController
  ) {
    this.datetimeFrom = moment({ hours: 0 }).format();
    this.datetimeTo = moment().format();//new Date(a).toISOString();
    this.getToken();
  }

  ngOnInit() {
  }

  onAddExpence() {

  }

  getToken() {
    this.authService.getTokenData().subscribe(data => {
      this.userData = data;
      console.log("dashboard data=> ", this.userData);
      this.getExpenses();
    })
  }

  getVehObj($event) {
    this.vehObj = $event;
    this.getExpenses();
  }

  getExpenses() {
    let _bUrl;
    this.totalExpense = 0;
    if (this.vehObj != undefined) {
      _bUrl = this.constUrl.mainUrl + "expense/expensebycateogry?user=" + this.userData._id + "&fdate=" + new Date(this.datetimeFrom).toISOString() + "&tdate=" + new Date(this.datetimeTo).toISOString() + "&vehicle=" + this.vehObj._id;
    } else {
      _bUrl = this.constUrl.mainUrl + "expense/expensebycateogry?user=" + this.userData._id + "&fdate=" + new Date(this.datetimeFrom).toISOString() + "&tdate=" + new Date(this.datetimeTo).toISOString();
    }
    this.loadingController.create({
      message: 'Loading expenses...'
    }).then(loadEl => {
      loadEl.present();
      this.authService.getMethod(_bUrl)
      .subscribe(respData => {
        loadEl.dismiss();
        this.expensesData = [];
        var data = JSON.parse(JSON.stringify(respData));
        this.expensesData = data.expenseobj;
        for (var j = 0; j < data.expenseobj.length; j++) {
          this.totalExpense += data.expenseobj[j].total;
        }
        console.log("expense type=> " + data);
      },
        err => {
          loadEl.dismiss();
          console.log(err)
        })
    })
    
  }


}

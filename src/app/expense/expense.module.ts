import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ExpensePage } from './expense.page';
import { ComponentsModule } from 'src/app/tabs/components/components.module';
const routes: Routes = [
  {
    path: '',
    component: ExpensePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
  ],
  declarations: [ExpensePage]
})
export class ExpensePageModule {}

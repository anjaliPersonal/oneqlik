import { Component, OnInit, OnDestroy } from '@angular/core';

import { Platform, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './auth/auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: [
    './side-menu/styles/side-menu.scss',
    './side-menu/styles/side-menu.shell.scss',
    './side-menu/styles/side-menu.responsive.scss'
  ]
  // styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  appPages = [
    {
      title: 'Dashboard',
      url: '/maintabs/tabs/dashboard',
      icon: './assets/sample-icons/side-menu/categories.svg'
    },
    {
      title: 'Dealers',
      url: '/maintabs/tabs/dealer',
      icon: './assets/sample-icons/side-menu/profile.svg'
    },
    {
      title: 'Customers',
      url: '/maintabs/tabs/customer',
      icon: './assets/sample-icons/side-menu/profile.svg'
    },
    // {
    //   title: 'Profile',
    //   url: '/app/user',
    //   icon: './assets/sample-icons/side-menu/profile.svg'
    // },

    {
      title: 'Notifications',
      url: '/notification',
      icon: './assets/sample-icons/side-menu/notifications.svg'
    }
  ];
  accountPages = [
    // {
    //   title: 'Log In',
    //   url: '/auth/login',
    //   icon: './assets/sample-icons/side-menu/login.svg'
    // },
    // {
    //   title: 'Sign Up',
    //   url: '/auth/signup',
    //   icon: './assets/sample-icons/side-menu/signup.svg'
    // },
    {
      title: 'Contact Card',
      url: '/contact-card',
      icon: './assets/sample-icons/side-menu/contact-card.svg'
    },
    {
      title: 'Tutorial',
      url: '/walkthrough',
      icon: './assets/sample-icons/side-menu/tutorial.svg'
    },
    // {
    //   title: 'Getting Started',
    //   url: '/getting-started',
    //   icon: './assets/sample-icons/side-menu/getting-started.svg'
    // },
    // {
    //   title: '404 page',
    //   url: '/page-not-found',
    //   icon: './assets/sample-icons/side-menu/warning.svg'
    // }
  ];

  textDir = 'ltr';

  private authSub: Subscription;
  private previousAuthState = false;
  userdata: any = {};

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private router: Router,
    public translate: TranslateService,
    private event: Events
  ) {
    this.initializeApp();
    this.setLanguage();
    this.eventSubscription();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  setLanguage() {
    // this language will be used as a fallback when a translation isn't found in the current language
    this.translate.setDefaultLang('en');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this.translate.use('en');

    // this is to determine the text direction depending on the selected language
    // for the purpose of this example we determine that only arabic and hebrew are RTL.
    // this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
    //   this.textDir = (event.lang === 'ar' || event.lang === 'iw') ? 'rtl' : 'ltr';
    // });
  }

  isCustomer: boolean = false;
  isDealer: boolean = false;

  eventSubscription() {
    this.event.subscribe('Auth:Role', (data) => {
      // debugger
      console.log("check subscribed event data: => ", data);
      if (data.customer) {
        this.isCustomer = true;
        this.appPages.splice(1, 2);
        console.log(this.appPages)
      } else if (!data.dealer && !data.customer) {
        this.isDealer = true;
        this.appPages[1].title = 'Dealers';
        this.appPages[1].url = '/maintabs/tabs/dealer';
        this.appPages[1].icon = './assets/sample-icons/side-menu/profile.svg';

        this.appPages[2].title = 'Customers';
        this.appPages[2].url = '/maintabs/tabs/customer';
        this.appPages[2].icon = './assets/sample-icons/side-menu/profile.svg';

      } else if(data.dealer || !data.customer) {
        // this.appPages.splice(1, 2);
        this.appPages[1].title = 'Admin';
        this.appPages[1].url = '/maintabs/tabs/dashboard';
        this.appPages[1].icon = './assets/sample-icons/side-menu/profile.svg';

        this.appPages[2].title = 'Customers';
        this.appPages[2].url = '/maintabs/tabs/customer';
        this.appPages[2].icon = './assets/sample-icons/side-menu/profile.svg';
      }
    });

    this.event.subscribe('Auth:Data', (data) => {
      console.log("subscribed user data: ", data);
      this.userdata = data;
    })
  }

  adminCall() {
    console.log("admin call")
  }

  ngOnInit() {
    this.authSub = this.authService.userIsAuthenticated.subscribe(isAuth => {
      if (!isAuth && this.previousAuthState !== isAuth) {
        this.router.navigateByUrl('/auth');
      }
      this.previousAuthState = isAuth;
    });
  }

  onLogout() {
    this.authService.logout();
  }

  ngOnDestroy() {
    if (this.authSub) {
      this.authSub.unsubscribe();
    }
  }
}

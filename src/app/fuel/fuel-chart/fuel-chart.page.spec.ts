import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuelChartPage } from './fuel-chart.page';

describe('FuelChartPage', () => {
  let component: FuelChartPage;
  let fixture: ComponentFixture<FuelChartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuelChartPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuelChartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

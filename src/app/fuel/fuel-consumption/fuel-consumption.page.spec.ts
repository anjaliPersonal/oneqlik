import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuelConsumptionPage } from './fuel-consumption.page';

describe('FuelConsumptionPage', () => {
  let component: FuelConsumptionPage;
  let fixture: ComponentFixture<FuelConsumptionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuelConsumptionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuelConsumptionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

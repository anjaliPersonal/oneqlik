import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FuelConsumptionPage } from './fuel-consumption.page';

const routes: Routes = [
  {
    path: '',
    component: FuelConsumptionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FuelConsumptionPage]
})
export class FuelConsumptionPageModule {}

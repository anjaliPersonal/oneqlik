import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'maintabs', pathMatch: 'full' },
  {
    path: 'maintabs',
    loadChildren: './tabs/tabs.module#TabsPageModule',
    canLoad: [AuthGuard]
  },
  { path: 'auth', loadChildren: './auth/auth.module#AuthPageModule' },
  {
    path: 'fuel',
    children: [
      {
        path: '',
        loadChildren: './fuel/fuel.module#FuelPageModule'
      },
      {
        path: 'fuel-chart',
        loadChildren: './fuel/fuel-chart/fuel-chart.module#FuelChartPageModule'
      },
      {
        path: 'fuel-entry',
        loadChildren: './fuel/fuel-entry/fuel-entry.module#FuelEntryPageModule'
      },
      {
        path: 'fuel-consumption',
        loadChildren: './fuel/fuel-consumption/fuel-consumption.module#FuelConsumptionPageModule'
      },
      {
        path: '',
        redirectTo: 'maintabs',
        pathMatch: 'full'
      },
    ],
    canLoad: [AuthGuard]
  },
  {
    path: 'report',
    children: [
      {
        path: '',
        loadChildren: './report/report.module#ReportPageModule'
      },
      {
        path: ':reportId',
        loadChildren: './report/report-detail/report-detail.module#ReportDetailPageModule'
      }
    ],
    canLoad: [AuthGuard]
  },
  {
    path: 'notification',
    children: [
      {
        path: '',
        loadChildren: './notification/notification.module#NotificationPageModule'
      },
      {
        path: ':notifId',
        loadChildren: './notification/notif-detail/notif-detail.module#NotifDetailPageModule'
      }
    ],
    canLoad: [AuthGuard]
  },
  {
    path: 'expense',
    children: [
      {
        path: '',
        loadChildren: './expense/expense.module#ExpensePageModule'
      },
      {
        path: ':expId',
        loadChildren: './expense/expense-detail/expense-detail.module#ExpenseDetailPageModule'
      }
    ]
  },
  { path: 'user-trips', loadChildren: './user-trips/user-trips.module#UserTripsPageModule' },
  { path: 'poi', loadChildren: './poi/poi.module#PoiPageModule' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

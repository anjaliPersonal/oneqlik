export class IVehicleList {
    Device_ID: string;
    Device_Name: string;
    iconType: string;
    total_odo: number;
    user: string;
    _id: string;
}
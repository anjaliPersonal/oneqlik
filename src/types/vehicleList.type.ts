import { IVehicleList } from './vehicleList.interface';

export class VehicleList implements IVehicleList {
    Device_ID: string;
    Device_Name: string;
    iconType: string;
    total_odo: number;
    user: string;
    _id: string;
  
    constructor(veh: IVehicleList) {
      this.Device_ID = veh.Device_ID;
      this.Device_Name = veh.Device_Name;
      this.iconType = veh.iconType;
      this.total_odo = veh.total_odo;
      this.user = veh.user;
      this._id = veh._id;
    }
  }
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dealer-dealer-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dealer/add-points/add-points.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dealer/add-points/add-points.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class=\"divStyle\">\n    <ion-card style=\"--background: white;\">\n        <ion-card-header>\n            <ion-card-title text-center>Add Points</ion-card-title>\n        </ion-card-header>\n\n        <ion-card-content>\n            <ion-list>\n                <ion-item>\n                    <ion-label position=\"floating\">Points</ion-label>\n                    <ion-input type=\"number\"></ion-input>\n                </ion-item>\n                <p style=\"font-size: 0.8em;\">Please type No. of points to be shared.</p>\n\n                <ion-item>\n                    <ion-label position=\"floating\">Price/Unit</ion-label>\n                    <ion-input type=\"number\"></ion-input>\n                </ion-item>\n                <p style=\"font-size: 0.8em;\">Please type per unit price of points.</p>\n\n                <ion-item>\n                    <ion-label position=\"floating\">Payment status</ion-label>\n                    <ion-select style=\"min-width: 100% !important;\">\n                        <ion-select-option>Paid</ion-select-option>\n                        <ion-select-option>Credit</ion-select-option>\n                        <ion-select-option>Send payment link</ion-select-option>\n                    </ion-select>\n                </ion-item>\n            </ion-list>\n            <ion-list lines=\"none\">\n                <ion-item>\n                    <ion-label>Total Allocated Points</ion-label>\n                    <ion-badge slot=\"end\">51</ion-badge>\n                </ion-item>\n                <ion-item>\n                    <ion-label>Total Available Points</ion-label>\n                    <ion-badge slot=\"end\">51</ion-badge>\n                </ion-item>\n                <ion-item>\n                    <ion-label>Total Amount</ion-label>\n                    <ion-badge slot=\"end\">0</ion-badge>\n                </ion-item>\n            </ion-list>\n            <ion-row>\n                <ion-col col-6>\n                    <ion-button expand=\"block\" fill=\"outline\" (click)=\"onCancel()\">Cancel</ion-button>\n                </ion-col>\n                <ion-col col-6>\n                    <ion-button expand=\"block\">Proceed</ion-button>\n                </ion-col>\n            </ion-row>\n        </ion-card-content>\n    </ion-card>\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dealer/dealer.page.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dealer/dealer.page.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"secondary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Dealer List</ion-title>\n    <div *ngIf=\"platformKey == 'md'\" slot=\"primary\">\n      <ion-icon name=\"search\" *ngIf=\"!searchBar\" (click)=\"searchBar = !searchBar\" style=\"    font-size: 2em;\n    padding-right: 16px;\"></ion-icon>\n      <ion-searchbar animated showCancelButton=\"focus\" placeholder=\"Find customers..\" (ionCancel)=\"onCancel()\"\n        *ngIf=\"searchBar\" (ionInput)=\"callSearch($event)\"></ion-searchbar>\n    </div>\n  </ion-toolbar>\n  <ion-searchbar animated placeholder=\"Find customers..\" (ionCancel)=\"onCancel()\" *ngIf=\"platformKey === 'ios'\"\n    (ionInput)=\"callSearch($event)\"></ion-searchbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-card *ngFor=\"let item of dealersList;\" style=\"margin: 5px;\">\n          <ion-card-header style=\"padding-bottom: 0px;\">\n            <ion-row no-padding>\n              <ion-col size=\"3\" no-padding (click)=\"onDealerClicked(item)\">\n                <ion-img style=\"width: 45px;\" src=\"assets/imgs/images.png\"></ion-img>\n              </ion-col>\n              <ion-col size=\"9\" no-padding>\n                <ion-row>\n                  <ion-col size=\"10\" no-padding>\n                    <ion-card-title style=\"font-size: 0.9em;\n                    font-weight: 500;\">{{item.first_name}} {{item.last_name}}</ion-card-title>\n                    <ion-card-subtitle style=\"letter-spacing: 0.1px;\n                    font-size: 0.65em;\">{{item.created_on | date:\"mediumDate\"}}, {{item.created_on | date:\"shortTime\"}}\n                    </ion-card-subtitle>\n                  </ion-col>\n                  <ion-col size=\"2\" no-padding style=\"text-align: right;\" (click)=\"addPoints(item)\">\n                    <ion-icon style=\"font-size: 1.2em;\" src=\"assets/svg/money.svg\"></ion-icon>\n                  </ion-col>\n                </ion-row>\n                <ion-card-subtitle>\n                  <ion-button size=\"small\" fill=\"outline\" style=\"font-size: 0.6em;\" color=\"medium\"\n                    (click)=\"addDealer('edit', item)\">Edit</ion-button>\n                  <ion-button size=\"small\" fill=\"outline\" style=\"font-size: 0.6em;\" color=\"medium\"\n                    (click)=\"onDeleteClicked(item)\">Delete\n                  </ion-button>\n                  <ion-button fill=\"outline\" size=\"small\" style=\"font-size: 0.6em;\" color=\"success\"\n                    (click)=\"onCustStatus(item)\" *ngIf=\"item.status\">Active\n                  </ion-button>\n                  <ion-button fill=\"outline\" size=\"small\" style=\"font-size: 0.6em;\" color=\"secondary\"\n                    (click)=\"onCustStatus(item)\" *ngIf=\"!item.status\">InActive\n                  </ion-button>\n                  <ion-button fill=\"outline\" size=\"small\" style=\"font-size: 0.6em;\" color=\"medium\">Chat</ion-button>\n                </ion-card-subtitle>\n              </ion-col>\n            </ion-row>\n          </ion-card-header>\n          <ion-card-content style=\"padding: 0px 16px 16px 16px;\">\n            <ion-row style=\"border-bottom: 0.5px solid #ebe6e6;\">\n              <ion-col size=\"4\" no-padding>\n                <p class=\"pClass\">Password</p>\n              </ion-col>\n              <ion-col size=\"1\" no-padding>\n                <p class=\"pClass\">:</p>\n              </ion-col>\n              <ion-col size=\"7\" no-padding>\n                <p style=\"color: black\" class=\"pClass\">{{item.pass ? item.pass : 'Not Saved'}}</p>\n              </ion-col>\n            </ion-row>\n            <ion-row style=\"border-bottom: 0.5px solid #ebe6e6;\">\n              <ion-col size=\"4\" no-padding>\n                <p class=\"pClass\">E-mail Id</p>\n              </ion-col>\n              <ion-col size=\"1\" no-padding>\n                <p class=\"pClass\">:</p>\n              </ion-col>\n              <ion-col size=\"7\" no-padding>\n                <p style=\"min-height: 13px; margin: 0px; color: blueviolet; white-space: nowrap;\n                    text-overflow: ellipsis;\n                    overflow: hidden;\" class=\"pClass\">{{item.email ? item.email : 'N/A'}}</p>\n              </ion-col>\n            </ion-row>\n            <ion-row style=\"border-bottom: 0.5px solid #ebe6e6;\">\n              <ion-col size=\"4\" no-padding>\n                <p class=\"pClass\">Mobile Num.</p>\n              </ion-col>\n              <ion-col size=\"1\" no-padding>\n                <p class=\"pClass\">:</p>\n              </ion-col>\n              <ion-col size=\"7\" no-padding>\n                <p style=\"color: blueviolet\" class=\"pClass\">{{item.phone ? item.phone : 'N/A'}}</p>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col size=\"4\" no-padding>\n                <p class=\"pClass\">Vehicle Count</p>\n              </ion-col>\n              <ion-col size=\"1\" no-padding>\n                <p class=\"pClass\">:</p>\n              </ion-col>\n              <ion-col size=\"7\" no-padding>\n                <p style=\"color: black\" class=\"pClass\">{{item.total_vehicle}}</p>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n        <!-- <ion-card *ngFor=\"let item of dealersList;\">\n          <ion-card-header style=\"padding-bottom: 0px;\">\n\n            <ion-row no-padding>\n              <ion-col size=\"4\" no-padding (click)=\"onDealerClicked(item)\">\n                <ion-img style=\"width: 60px;\" src=\"assets/imgs/images.png\"></ion-img>\n              </ion-col>\n              <ion-col size=\"8\" no-padding>\n                <ion-card-title style=\"font-size: 15px;\n                font-weight: 500;\">{{item.first_name}} {{item.last_name}}</ion-card-title>\n                <ion-card-subtitle style=\"letter-spacing: 0.1px;\n                font-size: 11px;\">{{item.created_on | date:\"mediumDate\"}}, {{item.created_on | date:\"shortTime\"}}\n                </ion-card-subtitle>\n                <ion-card-subtitle>\n                  <ion-button size=\"small\" fill=\"outline\" style=\"font-size: 0.7em;\" color=\"medium\">Edit</ion-button>\n                  <ion-button size=\"small\" fill=\"outline\" style=\"font-size: 0.7em;\" color=\"medium\">Delete</ion-button>\n                  <ion-button fill=\"outline\" size=\"small\" style=\"font-size: 0.7em;\" color=\"medium\">Active</ion-button>\n                </ion-card-subtitle>\n              </ion-col>\n            </ion-row>\n\n          </ion-card-header>\n          <ion-card-content style=\"padding: 0px 16px 16px 16px;\">\n            <ion-row>\n              <ion-col size=\"4\">\n                <p>Password</p>\n              </ion-col>\n              <ion-col size=\"8\">\n                <p style=\"color: black\">{{item.pass ? item.pass : 'Not Saved'}}</p>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col size=\"4\">\n                <p>E-mail Id</p>\n              </ion-col>\n              <ion-col size=\"8\">\n                <p style=\"min-height: 13px; margin: 0px; color: blueviolet; white-space: nowrap;\n                text-overflow: ellipsis;\n                overflow: hidden;\">{{item.email}}</p>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col size=\"4\">\n                <p>Mobile Num.</p>\n              </ion-col>\n              <ion-col size=\"8\">\n                <p style=\"color: blueviolet\">{{item.phone}}</p>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col size=\"4\">\n                <p>Vehicle Count</p>\n              </ion-col>\n              <ion-col size=\"8\">\n                <p style=\"color: black\">{{item.total_vehicle}}</p>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card> -->\n        <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event)\">\n          <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more dealers...\">\n          </ion-infinite-scroll-content>\n        </ion-infinite-scroll>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button (click)=\"addDealer('add')\">\n      <ion-icon name=\"add\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dealer/add-points/add-points.scss":
/*!********************************************************!*\
  !*** ./src/app/tabs/dealer/add-points/add-points.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".divStyle {\n  --background: rgba(0, 0, 0, 0.5);\n  height: 100%;\n  width: 100%;\n  padding: 35px 16px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL2lvbmljNC9vbmVxbGlrL3NyYy9hcHAvdGFicy9kZWFsZXIvYWRkLXBvaW50cy9hZGQtcG9pbnRzLnNjc3MiLCJzcmMvYXBwL3RhYnMvZGVhbGVyL2FkZC1wb2ludHMvYWRkLXBvaW50cy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0NBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC90YWJzL2RlYWxlci9hZGQtcG9pbnRzL2FkZC1wb2ludHMuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kaXZTdHlsZSB7XG4gIC0tYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjUpO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAzNXB4IDE2cHg7XG59XG4iLCIuZGl2U3R5bGUge1xuICAtLWJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC41KTtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMzVweCAxNnB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/tabs/dealer/add-points/add-points.ts":
/*!******************************************************!*\
  !*** ./src/app/tabs/dealer/add-points/add-points.ts ***!
  \******************************************************/
/*! exports provided: AddPointsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddPointsComponent", function() { return AddPointsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var AddPointsComponent = /** @class */ (function () {
    function AddPointsComponent(modalCtrl) {
        this.modalCtrl = modalCtrl;
    }
    AddPointsComponent.prototype.ngOnInit = function () { };
    AddPointsComponent.prototype.onCancel = function () {
        this.modalCtrl.dismiss();
    };
    AddPointsComponent.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    AddPointsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-points',
            template: __webpack_require__(/*! raw-loader!./add-points.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dealer/add-points/add-points.html"),
            styles: [__webpack_require__(/*! ./add-points.scss */ "./src/app/tabs/dealer/add-points/add-points.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], AddPointsComponent);
    return AddPointsComponent;
}());



/***/ }),

/***/ "./src/app/tabs/dealer/dealer.module.ts":
/*!**********************************************!*\
  !*** ./src/app/tabs/dealer/dealer.module.ts ***!
  \**********************************************/
/*! exports provided: DealerPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DealerPageModule", function() { return DealerPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _dealer_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dealer.page */ "./src/app/tabs/dealer/dealer.page.ts");
/* harmony import */ var _add_points_add_points__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./add-points/add-points */ "./src/app/tabs/dealer/add-points/add-points.ts");
/* harmony import */ var _add_dealer_add_dealer_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./add-dealer/add-dealer.page */ "./src/app/tabs/dealer/add-dealer/add-dealer.page.ts");









var routes = [
    {
        path: '',
        component: _dealer_page__WEBPACK_IMPORTED_MODULE_6__["DealerPage"]
    }
];
var DealerPageModule = /** @class */ (function () {
    function DealerPageModule() {
    }
    DealerPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            entryComponents: [_add_points_add_points__WEBPACK_IMPORTED_MODULE_7__["AddPointsComponent"], _add_dealer_add_dealer_page__WEBPACK_IMPORTED_MODULE_8__["AddDealerPage"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_dealer_page__WEBPACK_IMPORTED_MODULE_6__["DealerPage"], _add_points_add_points__WEBPACK_IMPORTED_MODULE_7__["AddPointsComponent"], _add_dealer_add_dealer_page__WEBPACK_IMPORTED_MODULE_8__["AddDealerPage"]]
        })
    ], DealerPageModule);
    return DealerPageModule;
}());



/***/ }),

/***/ "./src/app/tabs/dealer/dealer.page.scss":
/*!**********************************************!*\
  !*** ./src/app/tabs/dealer/dealer.page.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".my-custom-modal-css {\n  --background: transparent !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL2lvbmljNC9vbmVxbGlrL3NyYy9hcHAvdGFicy9kZWFsZXIvZGVhbGVyLnBhZ2Uuc2NzcyIsInNyYy9hcHAvdGFicy9kZWFsZXIvZGVhbGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9DQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC90YWJzL2RlYWxlci9kZWFsZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm15LWN1c3RvbS1tb2RhbC1jc3Mge1xuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbn0iLCIubXktY3VzdG9tLW1vZGFsLWNzcyB7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/tabs/dealer/dealer.page.ts":
/*!********************************************!*\
  !*** ./src/app/tabs/dealer/dealer.page.ts ***!
  \********************************************/
/*! exports provided: DealerPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DealerPage", function() { return DealerPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var src_app_app_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/app.model */ "./src/app/app.model.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _add_points_add_points__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./add-points/add-points */ "./src/app/tabs/dealer/add-points/add-points.ts");
/* harmony import */ var _add_dealer_add_dealer_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./add-dealer/add-dealer.page */ "./src/app/tabs/dealer/add-dealer/add-dealer.page.ts");









var DealerPage = /** @class */ (function () {
    function DealerPage(authService, constURL, loadingController, route, modalCtrl, platform, toastController, alertController) {
        this.authService = authService;
        this.constURL = constURL;
        this.loadingController = loadingController;
        this.route = route;
        this.modalCtrl = modalCtrl;
        this.platform = platform;
        this.toastController = toastController;
        this.alertController = alertController;
        this.userData = {};
        this.pageNo = 1;
        this.limit = 6;
        this.dealersList = [];
        this.searchBar = false;
        this.getToken();
        if (this.platform.is('android')) {
            this.platformKey = 'md';
        }
        else if (this.platform.is('ios')) {
            this.platformKey = 'ios';
        }
    }
    DealerPage.prototype.ngOnInit = function () {
    };
    DealerPage.prototype.getToken = function () {
        var _this = this;
        this.authService.getTokenData().subscribe(function (data) {
            _this.userData = data;
            console.log("vehicle data=> ", _this.userData);
            _this.getDealersList();
        });
    };
    DealerPage.prototype.getDealersList = function () {
        var _this = this;
        if (this.scrollEv) {
            this.getData();
        }
        else {
            this.loadingController.create({
                message: "Loading dealers..."
            }).then(function (loadEl) {
                loadEl.present();
                _this.laodingEl = loadEl;
                _this.pageNo = 1;
                _this.dealersList = [];
                _this.getData();
            });
        }
    };
    DealerPage.prototype.addPoints = function (item) {
        this.modalCtrl.create({
            component: _add_points_add_points__WEBPACK_IMPORTED_MODULE_7__["AddPointsComponent"],
            componentProps: {
                'params': this.userData,
                'dealerData': item
            },
            cssClass: 'my-custom-modal-css'
        }).then(function (modalEl) {
            modalEl.present();
            return modalEl.onDidDismiss();
        }).then(function (resultData) {
            console.log(resultData);
        });
    };
    DealerPage.prototype.getData = function () {
        var _this = this;
        var bUrl = this.constURL.mainUrl + 'users/getDealers?supAdmin=' + this.userData._id + '&pageNo=' + this.pageNo + '&size=' + this.limit;
        this.authService.getMethod(bUrl)
            .subscribe(function (resData) {
            if (_this.laodingEl) {
                _this.laodingEl.dismiss();
            }
            var parsedData;
            if (_this.scrollEv) {
                _this.scrollEv.target.complete();
                _this.scrollEv.target.disable = true;
                parsedData = JSON.parse(JSON.stringify(resData));
                if (parsedData.length === 0) {
                    return;
                }
                for (var i = 0; i < parsedData.length; i++) {
                    _this.dealersList.push(parsedData[i]);
                }
            }
            else {
                if (_this.laodingEl) {
                    _this.laodingEl.dismiss();
                }
                parsedData = JSON.parse(JSON.stringify(resData));
                if (parsedData.length === 0) {
                    return;
                }
                _this.dealersList = parsedData;
            }
            // this.dealersList = JSON.parse(JSON.stringify(resData));
        }, function (err) {
            if (_this.laodingEl) {
                _this.laodingEl.dismiss();
            }
            console.log("getting error from server=> ", err);
        });
    };
    DealerPage.prototype.loadData = function (event) {
        this.scrollEv = undefined;
        this.laodingEl = undefined;
        this.scrollEv = event;
        this.pageNo = this.pageNo + 1;
        this.getDealersList();
    };
    DealerPage.prototype.onDealerClicked = function (dealerObj) {
        var _this = this;
        _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["Plugins"].Storage.get({ key: 'authData' }).then(function (storedToken) {
            if (!storedToken || !storedToken.value) {
                return;
            }
            _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["Plugins"].Storage.set({ key: 'superAdminToken', value: storedToken.value });
            localStorage.setItem('custumer_status', 'OFF');
            localStorage.setItem('dealer_status', 'ON');
            var url = _this.constURL.mainUrl + "users/getCustumerDetail?uid=" + dealerObj._id;
            _this.authService.getMethod(url)
                .subscribe(function (respData) {
                debugger;
                var data = JSON.parse(JSON.stringify(respData));
                var tokenS = JSON.stringify(data.custumer_token);
                _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["Plugins"].Storage.set({ key: 'authData', value: tokenS });
                _this.route.navigateByUrl('/maintabs/tabs');
            });
        });
    };
    DealerPage.prototype.addDealer = function (key, dealerData) {
        if (key === 'add') {
            this.modalCtrl.create({
                component: _add_dealer_add_dealer_page__WEBPACK_IMPORTED_MODULE_8__["AddDealerPage"],
                componentProps: {
                    'params': this.userData,
                    'key': key
                }
            }).then(function (modalEl) {
                modalEl.present();
                return modalEl.onDidDismiss();
            }).then(function (resultData) {
                console.log(resultData);
            });
        }
        else if (key === 'edit') {
            this.modalCtrl.create({
                component: _add_dealer_add_dealer_page__WEBPACK_IMPORTED_MODULE_8__["AddDealerPage"],
                componentProps: {
                    'params': this.userData,
                    'dealerData': dealerData,
                    'key': key
                }
            }).then(function (modalEl) {
                modalEl.present();
                return modalEl.onDidDismiss();
            }).then(function (resultData) {
                console.log(resultData);
            });
        }
    };
    DealerPage.prototype.onCancel = function () {
        this.searchBar = false;
        this.getDealersList();
    };
    DealerPage.prototype.callSearch = function (ev) {
        var _this = this;
        this.pageNo = 1;
        var searchKey = ev.target.value;
        var url = this.constURL.mainUrl + 'users/getDealers?supAdmin=' + this.userData._id + '&pageNo=' + this.pageNo + '&size=' + this.limit + '&search=' + searchKey;
        this.authService.getMethod(url)
            .subscribe(function (respData) {
            if (respData) {
                var res = JSON.parse(JSON.stringify(respData));
                if (res.length > 0) {
                    _this.dealersList = res;
                }
            }
        }, function (err) {
            console.log("err", err);
        });
    };
    DealerPage.prototype.onCustStatus = function (custData) {
        var _this = this;
        var msg;
        if (custData.status) {
            msg = 'Are you sure, you want to InActivate this dealer?';
        }
        else {
            msg = 'Are you sure, you want to Activate this dealer?';
        }
        this.alertController.create({
            message: msg,
            buttons: [{
                    text: 'Proceed',
                    handler: function () {
                        _this.doAction(custData);
                    }
                },
                {
                    text: 'Back',
                    handler: function () {
                        // this.getcustomer();
                    }
                }]
        }).then(function (alertEl) {
            alertEl.present();
        });
    };
    DealerPage.prototype.showToast = function (msgString) {
        this.toastController.create({
            message: msgString,
            duration: 2000,
            position: 'middle'
        }).then(function (toastEl) {
            toastEl.present();
        });
    };
    DealerPage.prototype.doAction = function (data) {
        var _this = this;
        var stat;
        if (data.status) {
            stat = false;
        }
        else {
            stat = true;
        }
        var payloadData = {
            "uId": data._id,
            "loggedIn_id": this.userData._id,
            "status": stat
        };
        var url = this.constURL.mainUrl + 'users/user_status';
        this.loadingController.create({
            message: "Please wait... we are updating dealer's status.."
        }).then(function (loadEl) {
            loadEl.present();
            _this.authService.postMethod(url, payloadData)
                .subscribe(function (respData) {
                loadEl.dismiss();
                if (respData) {
                    // var res = JSON.parse(JSON.stringify(respData));
                    _this.showToast("Dealer's status updated successfully.");
                    // this.pageNo = 1;
                    // this.getCustomersList();
                    _this.dealersList = [];
                    _this.getData();
                }
            }, function (err) {
                loadEl.dismiss();
                console.log(err);
                _this.showToast("Dealer's status updated successfully.");
                // this.pageNo = 1;
                // this.getCustomersList();
                _this.dealersList = [];
                _this.getData();
            });
        });
    };
    DealerPage.prototype.onDeleteClicked = function (custData) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Alert',
                            message: 'Are you sure? You want to delete this dealer?',
                            buttons: [
                                {
                                    text: 'Proceed',
                                    handler: function () {
                                        console.log('Favorite clicked');
                                        _this.continueToDeleteCust(custData);
                                    }
                                }, {
                                    text: 'Back',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    DealerPage.prototype.continueToDeleteCust = function (custData) {
        var _this = this;
        var url = this.constURL.mainUrl + "users/deleteUser";
        var payLoadData = {
            "userId": custData._id,
            'deleteuser': true
        };
        this.authService.postMethod(url, payLoadData).
            subscribe(function (respData) {
            if (respData) {
                var res = JSON.parse(JSON.stringify(respData));
                if (res) {
                    _this.alertController.create({
                        message: 'Dealer deleted successfully. Reload dealer list to reflect changes?',
                        buttons: [
                            {
                                text: 'Proceed',
                                handler: function () {
                                    console.log('Favorite clicked');
                                    _this.getDealersList();
                                }
                            }, {
                                text: 'Back',
                                role: 'cancel',
                                handler: function () {
                                    console.log('Cancel clicked');
                                }
                            }
                        ]
                    }).then(function (alertEl) {
                        alertEl.present();
                    });
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    DealerPage.ctorParameters = function () { return [
        { type: src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
        { type: src_app_app_model__WEBPACK_IMPORTED_MODULE_5__["URLs"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ToastController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"], {
            static: true
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"])
    ], DealerPage.prototype, "infiniteScroll", void 0);
    DealerPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-dealer',
            template: __webpack_require__(/*! raw-loader!./dealer.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dealer/dealer.page.html"),
            styles: [__webpack_require__(/*! ./dealer.page.scss */ "./src/app/tabs/dealer/dealer.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            src_app_app_model__WEBPACK_IMPORTED_MODULE_5__["URLs"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"]])
    ], DealerPage);
    return DealerPage;
}());



/***/ })

}]);
//# sourceMappingURL=dealer-dealer-module-es5.js.map
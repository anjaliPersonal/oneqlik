(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-trips-user-trips-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/user-trips/user-trips.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/user-trips/user-trips.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>userTrips</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/user-trips/user-trips.module.ts":
/*!*************************************************!*\
  !*** ./src/app/user-trips/user-trips.module.ts ***!
  \*************************************************/
/*! exports provided: UserTripsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserTripsPageModule", function() { return UserTripsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _user_trips_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-trips.page */ "./src/app/user-trips/user-trips.page.ts");







const routes = [
    {
        path: '',
        component: _user_trips_page__WEBPACK_IMPORTED_MODULE_6__["UserTripsPage"]
    }
];
let UserTripsPageModule = class UserTripsPageModule {
};
UserTripsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_user_trips_page__WEBPACK_IMPORTED_MODULE_6__["UserTripsPage"]]
    })
], UserTripsPageModule);



/***/ }),

/***/ "./src/app/user-trips/user-trips.page.scss":
/*!*************************************************!*\
  !*** ./src/app/user-trips/user-trips.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXItdHJpcHMvdXNlci10cmlwcy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/user-trips/user-trips.page.ts":
/*!***********************************************!*\
  !*** ./src/app/user-trips/user-trips.page.ts ***!
  \***********************************************/
/*! exports provided: UserTripsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserTripsPage", function() { return UserTripsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let UserTripsPage = class UserTripsPage {
    constructor() { }
    ngOnInit() {
    }
};
UserTripsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-user-trips',
        template: __webpack_require__(/*! raw-loader!./user-trips.page.html */ "./node_modules/raw-loader/index.js!./src/app/user-trips/user-trips.page.html"),
        styles: [__webpack_require__(/*! ./user-trips.page.scss */ "./src/app/user-trips/user-trips.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], UserTripsPage);



/***/ })

}]);
//# sourceMappingURL=user-trips-user-trips-module-es2015.js.map
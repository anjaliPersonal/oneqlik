(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["customer-customer-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/customer/add-customer/add-customer.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/customer/add-customer/add-customer.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>{{titleString}}</ion-title>\n    <ion-buttons slot=\"primary\">\n      <ion-button (click)=\"onCancle()\">\n        <ion-icon name=\"close\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col style=\"padding: 0px 5px;\">\n        <ion-list>\n          <ion-item>\n            <ion-label position=\"floating\">Enter User ID *</ion-label>\n            <ion-input type=\"text\" [(ngModel)]=\"userId\" autocomplete autocorrect></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Enter First Name *</ion-label>\n            <ion-input type=\"text\" [(ngModel)]=\"fName\" autocomplete autocorrect></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Enter Last Name *</ion-label>\n            <ion-input type=\"text\" [(ngModel)]=\"lName\" autocomplete autocorrect></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Enter Email ID</ion-label>\n            <ion-input type=\"email\" [(ngModel)]=\"eMail\"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Enter Password *</ion-label>\n            <ion-input *ngIf=\"!show\" type=\"password\" [(ngModel)]=\"pass\"></ion-input>\n            <ion-input *ngIf=\"show\" type=\"text\" [(ngModel)]=\"pass\"></ion-input>\n            <ion-icon *ngIf=\"!show\" (click)=\"show = !show\" style=\"margin: auto;\" slot=\"end\" name=\"eye-off\"></ion-icon>\n            <ion-icon *ngIf=\"show\" (click)=\"show = !show\" style=\"margin: auto;\" slot=\"end\" name=\"eye\"></ion-icon>\n          </ion-item>\n          <ion-item *ngIf=\"screenKey === 'add'\">\n            <ion-label position=\"floating\">Confirm Password *</ion-label>\n            <ion-input *ngIf=\"!show1\" type=\"password\" [(ngModel)]=\"cpass\"></ion-input>\n            <ion-input *ngIf=\"show1\" type=\"text\" [(ngModel)]=\"cpass\"></ion-input>\n            <ion-icon *ngIf=\"!show1\" (click)=\"show1 = !show1\" style=\"margin: auto;\" slot=\"end\" name=\"eye-off\"></ion-icon>\n            <ion-icon *ngIf=\"show1\" (click)=\"show1 = !show1\" style=\"margin: auto;\" slot=\"end\" name=\"eye\"></ion-icon>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Enter Mobile Number</ion-label>\n            <ion-input type=\"number\" [(ngModel)]=\"mobNumber\"></ion-input>\n          </ion-item>\n          <ion-item *ngIf=\"screenKey === 'add'\">\n            <ion-label position=\"floating\">Select Dealer</ion-label>\n            <!-- <ion-input type=\"text\" [(ngModel)]=\"userId\" autocomplete autocorrect></ion-input> -->\n            <ion-select interface=\"popover\" [(ngModel)]=\"selectedDealer\">\n              <ion-select-option *ngFor=\"let dealer of dealersData\" [value]=\"dealer.dealer_id\">\n                {{dealer.dealer_firstname | titlecase}} {{dealer.dealer_lastname | titlecase}}</ion-select-option>\n            </ion-select>\n          </ion-item>\n          <p style=\"color: #fc9d03; font-size: 0.65em;padding-left: 16px;\">{{msgString}}</p>\n          <ion-item *ngIf=\"screenKey !== 'add'\">\n            <ion-label position=\"floating\">Expiry Date</ion-label>\n            <ion-input type=\"text\" [(ngModel)]=\"expDate\" *ngIf=\"!dateShow\"></ion-input>\n            <ion-icon name=\"close\" slot=\"end\" *ngIf=\"!dateShow\" (click)=\"dateShow = !dateShow\"></ion-icon>\n            <ion-input type=\"date\" [(ngModel)]=\"exDate\" *ngIf=\"dateShow\"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Enter Address</ion-label>\n            <ion-textarea rows=\"2\" [(ngModel)]=\"address\"></ion-textarea>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf=\"screenKey === 'add'\">\n      <ion-col size=\"6\" style=\"padding: 0px 5px;\">\n        <ion-item>\n          <ion-label position=\"floating\">Doc Type</ion-label>\n          <ion-select interface=\"popover\" okText=\"Okay\" cancelText=\"Dismiss\" [(ngModel)]=\"docType\">\n            <ion-select-option value=\"adharCard\">Adhar Card</ion-select-option>\n            <ion-select-option value=\"voterId\">Voter Id</ion-select-option>\n            <ion-select-option value=\"PANCard\">PAN Card</ion-select-option>\n            <ion-select-option value=\"DriveLicence\">Driving Licence</ion-select-option>\n          </ion-select>\n          <!-- <ion-input type=\"text\"  autocomplete autocorrect></ion-input> -->\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"6\" style=\"padding: 0px 5px;\">\n        <ion-item>\n          <ion-label position=\"floating\">Doc Number</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"docNumber\" autocomplete autocorrect></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf=\"screenKey === 'add'\">\n      <ion-col size=\"6\" style=\"padding: 0px 5px;\">\n        <ion-item>\n          <ion-button fill=\"outline\" expand=\"block\">Choose File</ion-button>\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"6\" style=\"padding: 0px 5px;\">\n        <ion-item>\n          <ion-label>No File Choosen</ion-label>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <!-- <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item>\n          <ion-label position=\"floating\">Enter User ID *</ion-label>\n          <ion-input type=\"text\" autocomplete autocorrect></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item>\n          <ion-label position=\"floating\">Enter First Name *</ion-label>\n          <ion-input type=\"text\" autocomplete autocorrect></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item>\n          <ion-label position=\"floating\">Enter Last Name *</ion-label>\n          <ion-input type=\"text\" autocomplete autocorrect></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item>\n          <ion-label position=\"floating\">Enter Email ID *</ion-label>\n          <ion-input type=\"email\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item>\n          <ion-label position=\"floating\">Enter Password *</ion-label>\n          <ion-input type=\"password\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item>\n          <ion-label position=\"floating\">Confirm Password *</ion-label>\n          <ion-input type=\"password\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item>\n          <ion-label position=\"floating\">Enter Mobile Number</ion-label>\n          <ion-input type=\"number\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item>\n          <ion-label position=\"floating\">Select Dealer</ion-label>\n          <ion-input type=\"text\" autocomplete autocorrect></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item>\n          <ion-label position=\"floating\">Enter Address</ion-label>\n          <ion-textarea rows=\"2\"></ion-textarea>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size-sm=\"3\" offset-sm=\"3\">\n        <ion-item>\n          <ion-label position=\"floating\">Doc Type</ion-label>\n          <ion-input type=\"text\" autocomplete autocorrect></ion-input>\n        </ion-item>\n      </ion-col>\n      <ion-col>\n        <ion-item size-sm=\"3\">\n          <ion-label position=\"floating\">Doc Number</ion-label>\n          <ion-input type=\"text\" autocomplete autocorrect></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size-sm=\"3\" offset-sm=\"3\">\n        <ion-button fill=\"outline\" expand=\"block\">Choose File</ion-button>\n      </ion-col>\n      <ion-col size-sm=\"3\">\n        <p>No File Choosen</p>\n      </ion-col>\n    </ion-row>\n  </ion-grid> -->\n</ion-content>\n<ion-footer style=\"padding: 0px 16px 0px 16px;\">\n  <ion-toolbar>\n    <ion-button expand=\"block\" (click)=\"onSubmit()\">Submit</ion-button>\n  </ion-toolbar>\n</ion-footer>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/customer/customer.page.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/customer/customer.page.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"secondary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Customer List</ion-title>\n    <div *ngIf=\"platformKey == 'md'\" slot=\"primary\">\n      <ion-icon name=\"search\" *ngIf=\"!searchBar\" (click)=\"searchBar = !searchBar\" style=\"    font-size: 2em;\n    padding-right: 16px;\"></ion-icon>\n      <ion-searchbar animated showCancelButton=\"focus\" placeholder=\"Find customers..\" (ionCancel)=\"onCancel()\"\n        *ngIf=\"searchBar\" (ionInput)=\"callSearch($event)\"></ion-searchbar>\n    </div>\n  </ion-toolbar>\n  <ion-searchbar animated placeholder=\"Find customers..\" (ionCancel)=\"onCancel()\" *ngIf=\"platformKey === 'ios'\"\n    (ionInput)=\"callSearch($event)\"></ion-searchbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\" no-padding>\n        <ion-card *ngFor=\"let item of customerListData;\" style=\"margin: 5px;\">\n          <ion-card-header style=\"padding-bottom: 0px;\">\n            <ion-row no-padding>\n              <ion-col size=\"3\" no-padding (click)=\"onCustomerClicked(item)\">\n                <ion-img style=\"width: 45px;\" src=\"assets/imgs/images.png\"></ion-img>\n              </ion-col>\n              <ion-col size=\"9\" no-padding>\n                <ion-card-title style=\"font-size: 0.9em;\n                    font-weight: 500;\">{{item.first_name}} {{item.last_name}}</ion-card-title>\n                <ion-card-subtitle style=\"letter-spacing: 0.1px;\n                    font-size: 0.65em;\">{{item.created_on | date:\"mediumDate\"}}, {{item.created_on | date:\"shortTime\"}}\n                </ion-card-subtitle>\n                <ion-card-subtitle>\n                  <ion-button size=\"small\" fill=\"outline\" style=\"font-size: 0.6em;\" color=\"medium\"\n                    (click)=\"onAddCust('edit', item)\">Edit</ion-button>\n                  <ion-button size=\"small\" fill=\"outline\" style=\"font-size: 0.6em;\" color=\"medium\"\n                    (click)=\"onDeleteClicked(item)\">Delete\n                  </ion-button>\n                  <ion-button fill=\"outline\" size=\"small\" style=\"font-size: 0.6em;\" color=\"success\" (click)=\"onCustStatus(item)\" *ngIf=\"item.status\">Active\n                  </ion-button>\n                  <ion-button fill=\"outline\" size=\"small\" style=\"font-size: 0.6em;\" color=\"secondary\" (click)=\"onCustStatus(item)\" *ngIf=\"!item.status\">InActive\n                  </ion-button>\n                  <ion-button fill=\"outline\" size=\"small\" style=\"font-size: 0.6em;\" color=\"medium\">Chat</ion-button>\n                </ion-card-subtitle>\n              </ion-col>\n            </ion-row>\n          </ion-card-header>\n          <ion-card-content style=\"padding: 0px 16px 16px 16px;\">\n            <ion-row style=\"border-bottom: 0.5px solid #ebe6e6;\">\n              <ion-col size=\"4\" no-padding>\n                <p class=\"pClass\">Password</p>\n              </ion-col>\n              <ion-col size=\"1\" no-padding>\n                <p class=\"pClass\">:</p>\n              </ion-col>\n              <ion-col size=\"7\" no-padding>\n                <p style=\"color: black\" class=\"pClass\">{{item.pass ? item.pass : 'Not Saved'}}</p>\n              </ion-col>\n            </ion-row>\n            <ion-row style=\"border-bottom: 0.5px solid #ebe6e6;\">\n              <ion-col size=\"4\" no-padding>\n                <p class=\"pClass\">E-mail Id</p>\n              </ion-col>\n              <ion-col size=\"1\" no-padding>\n                <p class=\"pClass\">:</p>\n              </ion-col>\n              <ion-col size=\"7\" no-padding>\n                <p style=\"min-height: 13px; margin: 0px; color: blueviolet; white-space: nowrap;\n                    text-overflow: ellipsis;\n                    overflow: hidden;\" class=\"pClass\">{{item.email ? item.email : 'N/A'}}</p>\n              </ion-col>\n            </ion-row>\n            <ion-row style=\"border-bottom: 0.5px solid #ebe6e6;\">\n              <ion-col size=\"4\" no-padding>\n                <p class=\"pClass\">Mobile Num.</p>\n              </ion-col>\n              <ion-col size=\"1\" no-padding>\n                <p class=\"pClass\">:</p>\n              </ion-col>\n              <ion-col size=\"7\" no-padding>\n                <p style=\"color: blueviolet\" class=\"pClass\">{{item.phone ? item.phone : 'N/A'}}</p>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col size=\"4\" no-padding>\n                <p class=\"pClass\">Vehicle Count</p>\n              </ion-col>\n              <ion-col size=\"1\" no-padding>\n                <p class=\"pClass\">:</p>\n              </ion-col>\n              <ion-col size=\"7\" no-padding>\n                <p style=\"color: black\" class=\"pClass\">{{item.total_vehicle}}</p>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n        <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event)\">\n          <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more customers...\">\n          </ion-infinite-scroll-content>\n        </ion-infinite-scroll>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button (click)=\"onAddCust('add')\">\n      <ion-icon name=\"add\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/customer/add-customer/add-customer.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/tabs/customer/add-customer/add-customer.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvY3VzdG9tZXIvYWRkLWN1c3RvbWVyL2FkZC1jdXN0b21lci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/tabs/customer/add-customer/add-customer.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/tabs/customer/add-customer/add-customer.component.ts ***!
  \**********************************************************************/
/*! exports provided: AddCustomerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddCustomerComponent", function() { return AddCustomerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_app_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/app.model */ "./src/app/app.model.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/auth/auth.service */ "./src/app/auth/auth.service.ts");






let AddCustomerComponent = class AddCustomerComponent {
    constructor(modalCtrl, loadingController, consURL, navParams, toastController, authService) {
        this.modalCtrl = modalCtrl;
        this.loadingController = loadingController;
        this.consURL = consURL;
        this.navParams = navParams;
        this.toastController = toastController;
        this.authService = authService;
        this.msgString = "";
        this.dealersData = [];
        this.show = false;
        this.show1 = false;
        this.dateShow = false;
        this.custData = {};
    }
    ngOnInit() {
        this.userData = this.navParams.get('params');
        this.screenKey = this.navParams.get('key');
        if (this.screenKey === 'add') {
            this.titleString = "Add Customer";
            console.log('Navparams get: ', this.navParams.get('params'));
            this.getDealerList();
        }
        else if (this.screenKey === 'edit') {
            this.custData = this.navParams.get('custData');
            this.titleString = "Edit " + this.custData.first_name + "'s info";
            console.log("customer's data: ", this.custData);
            console.log(moment__WEBPACK_IMPORTED_MODULE_4__(new Date(this.custData.expiration_date), 'DD-MM-YYYY').format('MM/DD/YYYY'));
            this.fillUpForm(this.custData);
        }
    }
    fillUpForm(data) {
        this.userId = (data.userid ? data.userid : null);
        this.fName = (data.first_name ? data.first_name : null);
        this.lName = (data.last_name ? data.last_name : null);
        this.pass = (data.pass ? data.pass : null);
        // this.cpass = (data.pass ? data.pass : null);
        this.mobNumber = (data.phone ? data.phone : null);
        this.eMail = (data.email ? data.email : null);
        // this.docType = ((data.docObject.length > 0) ? data.docObject[0].doctype : null);
        // this.docNumber = ((data.docObject.length > 0) ? data.docObject[0].phone : null);
        this.address = (data.address ? data.address : null);
        this.expDate = (data.expiration_date ? moment__WEBPACK_IMPORTED_MODULE_4__(new Date(data.expiration_date), 'DD-MM-YYYY').format('DD/MM/YYYY hh:mm a') : moment__WEBPACK_IMPORTED_MODULE_4__(new Date(this.getExpDate()), 'DD-MM-YYYY').format('DD/MM/YYYY hh:mm a'));
    }
    onCancle() {
        this.modalCtrl.dismiss();
    }
    onSubmit() {
        debugger;
        if (this.screenKey === 'add') {
            this.loadingController.create({
                message: "Please wait... we are adding customer...",
            }).then((loadEl) => {
                loadEl.present();
                this.addCustomer(loadEl);
            });
        }
        else if (this.screenKey === 'edit') {
            this.editCustomer();
        }
    }
    editCustomer() {
        let payLoad = {};
        let url = this.consURL.mainUrl + "users/editUserDetails";
        if (this.exDate == undefined) {
            if (this.custData.expiration_date) {
                this.exDate = this.custData.expiration_date;
            }
        }
        payLoad = {
            "contactid": this.custData._id,
            "address": this.address,
            "expire_date": new Date(this.exDate).toISOString(),
            "first_name": this.fName,
            "last_name": this.lName,
            "status": this.custData.status,
            "user_id": this.userId,
            "email": this.eMail,
            "phone": this.mobNumber
        };
        this.loadingController.create({
            message: "Please wait... we are updating customer...",
        }).then((loadEl) => {
            loadEl.present();
            this.authService.postMethod(url, payLoad)
                .subscribe(respData => {
                loadEl.dismiss();
                if (respData) {
                    var res = JSON.parse(JSON.stringify(respData));
                    this.showToast('Customer updated successfully');
                    this.onCancle();
                }
            }, err => {
                loadEl.dismiss();
                console.log(err);
            });
        });
    }
    dealerOnChnage(dealerData) {
        console.log(dealerData);
    }
    showToast(msgString) {
        this.toastController.create({
            message: msgString,
            duration: 2000,
            position: 'middle'
        }).then((toastEl) => {
            toastEl.present();
        });
    }
    getDealerList() {
        let url = this.consURL.mainUrl + "users/getAllDealerVehicles?supAdmin=" + this.userData._id;
        this.msgString = "Please wait we are loading dealer's list...";
        this.authService.getMethod(url)
            .subscribe(respData => {
            this.msgString = "";
            if (respData) {
                var res = JSON.parse(JSON.stringify(respData));
                // console.log("dealers list: ", res);
                this.dealersData = res;
            }
        }, err => {
            this.msgString = "";
            console.log(err);
        });
    }
    getExpDate() {
        var currentDate = new Date();
        var futureMonth = moment__WEBPACK_IMPORTED_MODULE_4__(currentDate).add('years', 1).format('L');
        // console.log("futureMonth: " + new Date(futureMonth).toISOString());
        return new Date(futureMonth).toISOString();
    }
    addCustomer(loadEl) {
        let payLoad = {};
        if (this.pass !== this.cpass) {
            loadEl.dismiss();
            this.showToast('Password mismatched!!!');
            return;
        }
        payLoad = {
            "first_name": this.fName,
            "last_name": this.lName,
            "email": this.eMail,
            "password": this.pass,
            "phone": this.mobNumber,
            "expdate": this.getExpDate(),
            "custumer": true,
            "user_id": this.userId,
            "address": this.address,
            "std_code": {
                "countryCode": "in",
                "dialcode": "91"
            },
            "timezone": "Asia/Kolkata",
            "imageDoc": [
                {
                    "doctype": this.docType,
                    "image": "",
                    "phone": this.docNumber
                }
            ]
        };
        if (this.userData.isSuperAdmin) {
            payLoad.supAdmin = this.userData._id;
            payLoad.isDealer = false;
            if (this.selectedDealer !== undefined) {
                payLoad.Dealer = this.selectedDealer;
            }
            else {
                payLoad.Dealer = this.userData._id;
            }
        }
        else if (this.userData.isDealer) {
            payLoad.supAdmin = this.userData.supAdmin;
            payLoad.isDealer = this.userData.isDealer;
            if (this.selectedDealer !== undefined) {
                payLoad.Dealer = this.selectedDealer;
            }
            else {
                payLoad.Dealer = this.userData._id;
            }
        }
        let url = this.consURL.mainUrl + "users/signUp";
        this.authService.postMethod(url, payLoad)
            .subscribe(respData => {
            loadEl.dismiss();
            if (respData) {
                var res = JSON.parse(JSON.stringify(respData));
                console.log("added customer check: ", res);
                if (res.message) {
                    this.showToast(res.message);
                    if (res.message === 'Registered') {
                        this.onCancle();
                    }
                }
            }
        }, err => {
            console.log(err);
            loadEl.dismiss();
        });
    }
};
AddCustomerComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: src_app_app_model__WEBPACK_IMPORTED_MODULE_3__["URLs"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] }
];
AddCustomerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-customer',
        template: __webpack_require__(/*! raw-loader!./add-customer.component.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/customer/add-customer/add-customer.component.html"),
        styles: [__webpack_require__(/*! ./add-customer.component.scss */ "./src/app/tabs/customer/add-customer/add-customer.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        src_app_app_model__WEBPACK_IMPORTED_MODULE_3__["URLs"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]])
], AddCustomerComponent);



/***/ }),

/***/ "./src/app/tabs/customer/customer.module.ts":
/*!**************************************************!*\
  !*** ./src/app/tabs/customer/customer.module.ts ***!
  \**************************************************/
/*! exports provided: CustomerPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerPageModule", function() { return CustomerPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_tabs_customer_add_customer_add_customer_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/tabs/customer/add-customer/add-customer.component */ "./src/app/tabs/customer/add-customer/add-customer.component.ts");
/* harmony import */ var _customer_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./customer.page */ "./src/app/tabs/customer/customer.page.ts");
/* harmony import */ var _tabs_resolver_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../tabs-resolver.service */ "./src/app/tabs/tabs-resolver.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");










const routes = [
    {
        path: '',
        component: _customer_page__WEBPACK_IMPORTED_MODULE_7__["CustomerPage"],
        resolve: {
            someKey: _tabs_resolver_service__WEBPACK_IMPORTED_MODULE_8__["TabsResolverService"]
        }
    }
];
let CustomerPageModule = class CustomerPageModule {
};
CustomerPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        entryComponents: [src_app_tabs_customer_add_customer_add_customer_component__WEBPACK_IMPORTED_MODULE_6__["AddCustomerComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_customer_page__WEBPACK_IMPORTED_MODULE_7__["CustomerPage"], src_app_tabs_customer_add_customer_add_customer_component__WEBPACK_IMPORTED_MODULE_6__["AddCustomerComponent"]],
        providers: [_tabs_resolver_service__WEBPACK_IMPORTED_MODULE_8__["TabsResolverService"]]
    })
], CustomerPageModule);



/***/ }),

/***/ "./src/app/tabs/customer/customer.page.scss":
/*!**************************************************!*\
  !*** ./src/app/tabs/customer/customer.page.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-card-header ion-card-title {\n  font-size: 16px;\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  overflow: hidden;\n}\n\n.pClass {\n  font-size: 0.8em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL2lvbmljNC9vbmVxbGlrL3NyYy9hcHAvdGFicy9jdXN0b21lci9jdXN0b21lci5wYWdlLnNjc3MiLCJzcmMvYXBwL3RhYnMvY3VzdG9tZXIvY3VzdG9tZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtBQ0FKOztBREdBO0VBQ0UsZ0JBQUE7QUNBRiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvY3VzdG9tZXIvY3VzdG9tZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNhcmQtaGVhZGVyIHtcbiAgaW9uLWNhcmQtdGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gIH1cbn1cbi5wQ2xhc3Mge1xuICBmb250LXNpemU6IDAuOGVtO1xufSIsImlvbi1jYXJkLWhlYWRlciBpb24tY2FyZC10aXRsZSB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5wQ2xhc3Mge1xuICBmb250LXNpemU6IDAuOGVtO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/tabs/customer/customer.page.ts":
/*!************************************************!*\
  !*** ./src/app/tabs/customer/customer.page.ts ***!
  \************************************************/
/*! exports provided: CustomerPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerPage", function() { return CustomerPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_customer_add_customer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-customer/add-customer.component */ "./src/app/tabs/customer/add-customer/add-customer.component.ts");
/* harmony import */ var src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var src_app_app_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/app.model */ "./src/app/app.model.ts");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");


// import { ActivatedRoute } from '@angular/router';






let CustomerPage = class CustomerPage {
    constructor(modalCtrl, route, authService, constURL, loadingController, platform, toastController, alertController) {
        this.modalCtrl = modalCtrl;
        this.route = route;
        this.authService = authService;
        this.constURL = constURL;
        this.loadingController = loadingController;
        this.platform = platform;
        this.toastController = toastController;
        this.alertController = alertController;
        this.userData = {};
        this.pageNo = 1;
        this.limit = 6;
        this.searchBar = false;
        this.getToken();
        if (this.platform.is('android')) {
            this.platformKey = 'md';
        }
        else if (this.platform.is('ios')) {
            this.platformKey = 'ios';
        }
    }
    getToken() {
        this.authService.getTokenData().subscribe(data => {
            this.userData = data;
            console.log("customer data=> ", this.userData);
            this.getCustomersList();
        });
    }
    onCancel() {
        this.searchBar = false;
        this.getCustomersList();
    }
    callSearch(ev) {
        this.pageNo = 1;
        let searchKey = ev.target.value;
        let url = this.constURL.mainUrl + 'users/getCust?uid=' + this.userData._id + '&pageNo=' + this.pageNo + '&size=' + this.limit + '&search=' + searchKey;
        this.authService.getMethod(url)
            .subscribe(respData => {
            if (respData) {
                var res = JSON.parse(JSON.stringify(respData));
                if (res.length > 0) {
                    this.customerListData = res;
                }
            }
        }, err => {
            console.log("err", err);
        });
    }
    onDeleteClicked(custData) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Alert',
                message: 'Are you sure? You want to delete this customer?',
                // position: 'middle',
                buttons: [
                    {
                        // side: 'start',
                        // icon: 'trash',
                        text: 'Proceed',
                        handler: () => {
                            console.log('Favorite clicked');
                            this.continueToDeleteCust(custData);
                        }
                    }, {
                        text: 'Back',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    }
                ]
            });
            alert.present();
        });
    }
    continueToDeleteCust(custData) {
        let url = this.constURL.mainUrl + "users/deleteUser";
        let payLoadData = {
            "userId": custData._id,
            'deleteuser': true
        };
        this.authService.postMethod(url, payLoadData).
            subscribe(respData => {
            if (respData) {
                var res = JSON.parse(JSON.stringify(respData));
                if (res) {
                    this.toastController.create({
                        // header: 'Alert',
                        message: 'Customer deleted successfully. Reload customer list to reflect changes?',
                        position: 'middle',
                        buttons: [
                            {
                                // side: 'start',
                                icon: 'reload',
                                text: 'Proceed',
                                handler: () => {
                                    console.log('Favorite clicked');
                                    this.getCustomersList();
                                }
                            }, {
                                text: 'Back',
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                }
                            }
                        ]
                    }).then((toastEl) => {
                        toastEl.present();
                    });
                }
            }
        }, err => {
            console.log(err);
        });
    }
    // ngOnInit() {
    //   // debugger
    //   console.log('Progressive Shell Resovlers - ngOnInit()');
    //   if (this.route && this.route.data) {
    //     // We resolved a promise for the data Observable
    //     const promiseObservable = this.route.data;
    //     console.log('Progressive Shell Resovlers - Route Resolve Observable => promiseObservable: ', promiseObservable);
    //     if (promiseObservable) {
    //       promiseObservable.subscribe(promiseValue => {
    //         const dataObservable = promiseValue['someKey'];
    //         console.log('Progressive Shell Resovlers - Subscribe to promiseObservable => dataObservable: ', dataObservable);
    //         if (dataObservable) {
    //           dataObservable.subscribe(observableValue => {
    //             if (observableValue.length !== undefined) {
    //               const somArr = {
    //                 isShell: false,
    //                 something: observableValue
    //               };
    //               const pageData: CustShellListingModel = somArr;
    //               if (pageData) {
    //                 this.routeResolveData = pageData;
    //               }
    //               // tslint:disable-next-line:max-line-length
    //               console.log('Progressive Shell Resovlers - Subscribe to dataObservable (can emmit multiple values) => PageData (' + ((pageData && pageData.isShell) ? 'SHELL' : 'REAL') + '): ', pageData);
    //             } else {
    //               const pageData: CustShellListingModel = observableValue;
    //               if (pageData) {
    //                 this.routeResolveData = pageData;
    //               }
    //               // As we are implementing an App Shell architecture, pageData will be firstly an empty shell model,
    //               // and the real remote data once it gets fetched
    //               // tslint:disable-next-line:max-line-length
    //               console.log('Progressive Shell Resovlers - Subscribe to dataObservable (can emmit multiple values) => PageData (' + ((pageData && pageData.isShell) ? 'SHELL' : 'REAL') + '): ', pageData);
    //             }
    //           });
    //         } else {
    //           console.warn('No dataObservable coming from Route Resolver promiseObservable');
    //         }
    //       });
    //     } else {
    //       console.warn('No promiseObservable coming from Route Resolver data');
    //     }
    //   } else {
    //     console.warn('No data coming from Route Resolver');
    //   }
    // }
    ngOnInit() {
    }
    getCustomersList() {
        if (this.scrollEv) {
            this.getData();
        }
        else {
            this.loadingController.create({
                message: 'Loading customers...'
            }).then(loadEl => {
                loadEl.present();
                this.laodingEl = loadEl;
                this.pageNo = 1;
                this.customerListData = [];
                this.getData();
            });
        }
    }
    getData() {
        var baseURLp = this.constURL.mainUrl + 'users/getCust?uid=' + this.userData._id + '&pageNo=' + this.pageNo + '&size=' + this.limit;
        this.authService.getMethod(baseURLp)
            .subscribe(resData => {
            if (this.laodingEl) {
                this.laodingEl.dismiss();
            }
            let parsedData;
            if (this.scrollEv) {
                this.scrollEv.target.complete();
                this.scrollEv.target.disable = true;
                parsedData = JSON.parse(JSON.stringify(resData));
                if (parsedData.length === 0) {
                    return;
                }
                for (let i = 0; i < parsedData.length; i++) {
                    this.customerListData.push(parsedData[i]);
                }
            }
            else {
                if (this.laodingEl) {
                    this.laodingEl.dismiss();
                }
                parsedData = JSON.parse(JSON.stringify(resData));
                if (parsedData.length === 0) {
                    return;
                }
                this.customerListData = parsedData;
            }
        }, err => {
            console.log("error=> ", err);
            if (this.laodingEl) {
                this.laodingEl.dismiss();
            }
            if (this.scrollEv) {
                this.scrollEv.target.complete();
                this.scrollEv.target.disable = true;
            }
        });
    }
    onAddCust(key, custData) {
        if (key === 'add') {
            this.modalCtrl.create({
                component: _add_customer_add_customer_component__WEBPACK_IMPORTED_MODULE_3__["AddCustomerComponent"],
                componentProps: {
                    'params': this.userData,
                    'key': key
                }
            }).then((modalEl) => {
                modalEl.present();
                return modalEl.onDidDismiss();
            }).then((resultData) => {
                console.log(resultData);
            });
        }
        else if (key === 'edit') {
            this.modalCtrl.create({
                component: _add_customer_add_customer_component__WEBPACK_IMPORTED_MODULE_3__["AddCustomerComponent"],
                componentProps: {
                    'params': this.userData,
                    'custData': custData,
                    'key': key
                }
            }).then((modalEl) => {
                modalEl.present();
                return modalEl.onDidDismiss();
            }).then((resultData) => {
                console.log(resultData);
            });
        }
    }
    onCustStatus(custData) {
        var msg;
        if (custData.status) {
            msg = 'Are you sure, you want to InActivate this customer?';
        }
        else {
            msg = 'Are you sure, you want to Activate this customer?';
        }
        this.alertController.create({
            message: msg,
            buttons: [{
                    text: 'Proceed',
                    handler: () => {
                        this.doAction(custData);
                    }
                },
                {
                    text: 'Back',
                    handler: () => {
                        // this.getcustomer();
                    }
                }]
        }).then((alertEl) => {
            alertEl.present();
        });
    }
    showToast(msgString) {
        this.toastController.create({
            message: msgString,
            duration: 2000,
            position: 'middle'
        }).then((toastEl) => {
            toastEl.present();
        });
    }
    doAction(data) {
        let stat;
        if (data.status) {
            stat = false;
        }
        else {
            stat = true;
        }
        let payloadData = {
            "uId": data._id,
            "loggedIn_id": this.userData._id,
            "status": stat
        };
        let url = this.constURL.mainUrl + 'users/user_status';
        this.loadingController.create({
            message: "Please wait... we are updating customer's status.."
        }).then((loadEl) => {
            loadEl.present();
            this.authService.postMethod(url, payloadData)
                .subscribe(respData => {
                loadEl.dismiss();
                if (respData) {
                    // var res = JSON.parse(JSON.stringify(respData));
                    this.showToast("Customer's status updated successfully.");
                    // this.pageNo = 1;
                    // this.getCustomersList();
                    this.customerListData = [];
                    this.getData();
                }
            }, err => {
                loadEl.dismiss();
                console.log(err);
                this.showToast("Customer's status updated successfully.");
                // this.pageNo = 1;
                // this.getCustomersList();
                this.customerListData = [];
                this.getData();
            });
        });
    }
    loadData(event) {
        this.scrollEv = undefined;
        this.scrollEv = event;
        this.pageNo = this.pageNo + 1;
        this.getCustomersList();
    }
    onCustomerClicked(custObj) {
        _capacitor_core__WEBPACK_IMPORTED_MODULE_6__["Plugins"].Storage.get({ key: 'authData' }).then(storedToken => {
            if (!storedToken || !storedToken.value) {
                return;
            }
            _capacitor_core__WEBPACK_IMPORTED_MODULE_6__["Plugins"].Storage.set({ key: 'dealer', value: storedToken.value });
            localStorage.setItem('custumer_status', 'ON');
            localStorage.setItem('dealer_status', 'OFF');
            var url = this.constURL.mainUrl + "users/getCustumerDetail?uid=" + custObj._id;
            this.authService.getMethod(url)
                .subscribe(respData => {
                var data = JSON.parse(JSON.stringify(respData));
                const tokenS = JSON.stringify(data.custumer_token);
                _capacitor_core__WEBPACK_IMPORTED_MODULE_6__["Plugins"].Storage.set({ key: 'authData', value: tokenS });
                this.route.navigateByUrl('/maintabs/tabs');
            });
        });
    }
};
CustomerPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: src_app_app_model__WEBPACK_IMPORTED_MODULE_5__["URLs"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
];
CustomerPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-customer',
        template: __webpack_require__(/*! raw-loader!./customer.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/customer/customer.page.html"),
        styles: [__webpack_require__(/*! ./customer.page.scss */ "./src/app/tabs/customer/customer.page.scss"), __webpack_require__(/*! ./shell-elements.scss */ "./src/app/tabs/customer/shell-elements.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
        src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
        src_app_app_model__WEBPACK_IMPORTED_MODULE_5__["URLs"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
], CustomerPage);



/***/ }),

/***/ "./src/app/tabs/customer/shell-elements.scss":
/*!***************************************************!*\
  !*** ./src/app/tabs/customer/shell-elements.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".image-shell {\n  padding-bottom: 100%;\n  height: 0px;\n  position: relative;\n}\n.image-shell::before {\n  content: \"\";\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background: -webkit-gradient(linear, left top, right top, color-stop(8%, #eee), color-stop(18%, #ddd), color-stop(33%, #eee));\n  background: linear-gradient(to right, #eee 8%, #ddd 18%, #eee 33%);\n  background-size: 800px 104px;\n  -webkit-animation: animateBackground 2s ease-in-out infinite;\n          animation: animateBackground 2s ease-in-out infinite;\n}\n.image-shell > img {\n  position: absolute;\n  top: 0px;\n  left: 0px;\n  right: 0px;\n  bottom: 0px;\n}\n.image-shell > img[src=\"\"], .image-shell > img[src=null] {\n  display: none;\n}\n.text-shell {\n  position: relative;\n}\n.text-shell::before {\n  content: \"\";\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background: -webkit-gradient(linear, left top, right top, color-stop(8%, #eee), color-stop(18%, #ddd), color-stop(33%, #eee));\n  background: linear-gradient(to right, #eee 8%, #ddd 18%, #eee 33%);\n  background-size: 800px 104px;\n  -webkit-animation: animateBackground 2s ease-in-out infinite;\n          animation: animateBackground 2s ease-in-out infinite;\n}\n.text-shell.text-loaded::before, .text-shell.text-loaded::after {\n  background: none !important;\n  -webkit-animation: 0 !important;\n          animation: 0 !important;\n}\nh3.text-shell::after {\n  content: \"\";\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background-repeat: no-repeat;\n  background-image: -webkit-gradient(linear, left top, right top, color-stop(95%, transparent), color-stop(95%, #fff));\n  background-image: linear-gradient(to right, transparent 95%, #fff 95%);\n  background-size: 100% 22px;\n  background-position: 0 0px;\n}\np.text-shell::after {\n  content: \"\";\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background-repeat: no-repeat;\n  background-image: -webkit-gradient(linear, left top, right top, color-stop(95%, transparent), color-stop(95%, #fff)), -webkit-gradient(linear, left top, right top, color-stop(100%, #fff), to(#fff)), -webkit-gradient(linear, left top, right top, color-stop(65%, transparent), color-stop(65%, #fff));\n  background-image: linear-gradient(to right, transparent 95%, #fff 95%), linear-gradient(to right, #fff 100%, #fff 100%), linear-gradient(to right, transparent 65%, #fff 65%);\n  background-size: 100% 16px, 100% 3px, 100% 16px;\n  background-position: 0 0px, 0 16px, 0 19px;\n}\n@-webkit-keyframes animateBackground {\n  0% {\n    background-position: -468px 0;\n  }\n  100% {\n    background-position: 468px 0;\n  }\n}\n@keyframes animateBackground {\n  0% {\n    background-position: -468px 0;\n  }\n  100% {\n    background-position: 468px 0;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL2lvbmljNC9vbmVxbGlrL3NyYy9hcHAvdGFicy9jdXN0b21lci9zaGVsbC1lbGVtZW50cy5zY3NzIiwic3JjL2FwcC90YWJzL2N1c3RvbWVyL3NoZWxsLWVsZW1lbnRzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxvQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQ0NGO0FERUU7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsNkhBQUE7RUFBQSxrRUFBQTtFQUNBLDRCQUFBO0VBQ0EsNERBQUE7VUFBQSxvREFBQTtBQ0FKO0FER0U7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QUNESjtBREdJO0VBRUUsYUFBQTtBQ0ZOO0FET0E7RUFDRSxrQkFBQTtBQ0pGO0FETUU7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsNkhBQUE7RUFBQSxrRUFBQTtFQUNBLDRCQUFBO0VBQ0EsNERBQUE7VUFBQSxvREFBQTtBQ0pKO0FEUUk7RUFFRSwyQkFBQTtFQUNBLCtCQUFBO1VBQUEsdUJBQUE7QUNQTjtBRGNFO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLDRCQUFBO0VBQ0Esb0hBQ2tEO0VBRGxELHNFQUNrRDtFQU1sRCwwQkFDNEM7RUFFNUMsMEJBQzhDO0FDcEJsRDtBRDBCRTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSw0QkFBQTtFQUNBLHlTQUNrRDtFQURsRCw2S0FDa0Q7RUFVbEQsK0NBQzRDO0VBSTVDLDBDQUM4QztBQ3RDbEQ7QUQ4Q0E7RUFDRTtJQUNFLDZCQUFBO0VDM0NGO0VEOENBO0lBQ0UsNEJBQUE7RUM1Q0Y7QUFDRjtBRHFDQTtFQUNFO0lBQ0UsNkJBQUE7RUMzQ0Y7RUQ4Q0E7SUFDRSw0QkFBQTtFQzVDRjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvdGFicy9jdXN0b21lci9zaGVsbC1lbGVtZW50cy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmltYWdlLXNoZWxsIHtcbiAgcGFkZGluZy1ib3R0b206IDEwMCU7XG4gIGhlaWdodDogMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgLy8gVGhlIGFuaW1hdGlvbiB0aGF0IGdvZXMgYmVuZWF0aCB0aGUgbWFza3NcbiAgJjo6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICBib3R0b206IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZWVlIDglLCAjZGRkIDE4JSwgI2VlZSAzMyUpO1xuICAgIGJhY2tncm91bmQtc2l6ZTogODAwcHggMTA0cHg7XG4gICAgYW5pbWF0aW9uOiBhbmltYXRlQmFja2dyb3VuZCAycyBlYXNlLWluLW91dCBpbmZpbml0ZTtcbiAgfVxuXG4gICYgPiBpbWcge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDBweDtcbiAgICBsZWZ0OiAwcHg7XG4gICAgcmlnaHQ6IDBweDtcbiAgICBib3R0b206IDBweDtcblxuICAgICZbc3JjPVwiXCJdLFxuICAgICZbc3JjPVwibnVsbFwiXSB7XG4gICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbiAgfVxufVxuXG4udGV4dC1zaGVsbCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgLy8gVGhlIGFuaW1hdGlvbiB0aGF0IGdvZXMgYmVuZWF0aCB0aGUgbWFza3NcbiAgJjo6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICBib3R0b206IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZWVlIDglLCAjZGRkIDE4JSwgI2VlZSAzMyUpO1xuICAgIGJhY2tncm91bmQtc2l6ZTogODAwcHggMTA0cHg7XG4gICAgYW5pbWF0aW9uOiBhbmltYXRlQmFja2dyb3VuZCAycyBlYXNlLWluLW91dCBpbmZpbml0ZTtcbiAgfVxuXG4gICYudGV4dC1sb2FkZWQge1xuICAgICY6OmJlZm9yZSxcbiAgICAmOjphZnRlciB7XG4gICAgICBiYWNrZ3JvdW5kOiBub25lICFpbXBvcnRhbnQ7XG4gICAgICBhbmltYXRpb246IDAgIWltcG9ydGFudDtcbiAgICB9XG4gIH1cbn1cblxuaDMudGV4dC1zaGVsbCB7XG4gIC8vIFRoZSBtYXNrc1xuICAmOjphZnRlciB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIHJpZ2h0OiAwO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1pbWFnZTpcbiAgICAgIC8qIEZpcnN0IGxpbmU6IDk1JSB3aWR0aCBncmV5LCA1JSB3aGl0ZSBtYXNrICovIGxpbmVhci1ncmFkaWVudChcbiAgICAgIHRvIHJpZ2h0LFxuICAgICAgdHJhbnNwYXJlbnQgOTUlLFxuICAgICAgI2ZmZiA5NSVcbiAgICApO1xuXG4gICAgYmFja2dyb3VuZC1zaXplOlxuICAgICAgLyogRmlyc3QgbGluZTogMTAwJSB3aWR0aCwgMTZweCBoZWlnaHQgKi8gMTAwJSAyMnB4O1xuXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjpcbiAgICAgIC8qIEZpcnN0IGxpbmU6IGJlZ2lucyBhdCBsZWZ0OiAwLCB0b3A6IDAgKi8gMCAwcHg7XG4gIH1cbn1cblxucC50ZXh0LXNoZWxsIHtcbiAgLy8gVGhlIG1hc2tzXG4gICY6OmFmdGVyIHtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICBib3R0b206IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOlxuICAgICAgLyogRmlyc3QgbGluZTogOTUlIHdpZHRoIGdyZXksIDUlIHdoaXRlIG1hc2sgKi8gbGluZWFyLWdyYWRpZW50KFxuICAgICAgICB0byByaWdodCxcbiAgICAgICAgdHJhbnNwYXJlbnQgOTUlLFxuICAgICAgICAjZmZmIDk1JVxuICAgICAgKSxcbiAgICAgIC8qIFNlcGFyYXRpb24gYmV0d2VlbiBsaW5lcyAoYSBmdWxsIHdpZHRoIHdoaXRlIGxpbmUgbWFzaykgKi9cbiAgICAgICAgbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZmZmIDEwMCUsICNmZmYgMTAwJSksXG4gICAgICAvKiBTZWNvbmQgbGluZTogNjUlIHdpZHRoIGdyZXksIDM1JSB3aGl0ZSBtYXNrICovXG4gICAgICAgIGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgdHJhbnNwYXJlbnQgNjUlLCAjZmZmIDY1JSk7XG5cbiAgICBiYWNrZ3JvdW5kLXNpemU6XG4gICAgICAvKiBGaXJzdCBsaW5lOiAxMDAlIHdpZHRoLCAxNnB4IGhlaWdodCAqLyAxMDAlIDE2cHgsXG4gICAgICAvKiBTZXBhcmF0aW9uIGJldHdlZW4gbGluZXM6IGEgZnVsbCB3aWR0aCwgM3B4IGhlaWdodCBsaW5lICovIDEwMCUgM3B4LFxuICAgICAgLyogU2Vjb25kIGxpbmU6IDEwMCUgd2lkdGgsIDE2cHggaGVpZ2h0ICovIDEwMCUgMTZweDtcblxuICAgIGJhY2tncm91bmQtcG9zaXRpb246XG4gICAgICAvKiBGaXJzdCBsaW5lOiBiZWdpbnMgYXQgbGVmdDogMCwgdG9wOiAwICovIDAgMHB4LFxuICAgICAgLyogU2VwYXJhdGlvbiBiZXR3ZWVuIGxpbmVzOiBiZWdpbnMgYXQgbGVmdDogMCwgdG9wOiAxNnB4IChyaWdodCBiZWxvdyB0aGUgZmlyc3QgbGluZSkgKi9cbiAgICAgICAgMCAxNnB4LFxuICAgICAgLyogU2Vjb25kIGxpbmU6IGJlZ2lucyBhdCBsZWZ0OiAwLCB0b3A6ICgxNnB4ICsgM3B4KSAocmlnaHQgYmVsb3cgdGhlIHNlcGFyYXRpb24gYmV0d2VlbiBsaW5lcykgKi9cbiAgICAgICAgMCAxOXB4O1xuICB9XG59XG5cbkBrZXlmcmFtZXMgYW5pbWF0ZUJhY2tncm91bmQge1xuICAwJSB7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogLTQ2OHB4IDA7XG4gIH1cblxuICAxMDAlIHtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA0NjhweCAwO1xuICB9XG59XG4iLCIuaW1hZ2Utc2hlbGwge1xuICBwYWRkaW5nLWJvdHRvbTogMTAwJTtcbiAgaGVpZ2h0OiAwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5pbWFnZS1zaGVsbDo6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGJvdHRvbTogMDtcbiAgcmlnaHQ6IDA7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2VlZSA4JSwgI2RkZCAxOCUsICNlZWUgMzMlKTtcbiAgYmFja2dyb3VuZC1zaXplOiA4MDBweCAxMDRweDtcbiAgYW5pbWF0aW9uOiBhbmltYXRlQmFja2dyb3VuZCAycyBlYXNlLWluLW91dCBpbmZpbml0ZTtcbn1cbi5pbWFnZS1zaGVsbCA+IGltZyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwcHg7XG4gIGxlZnQ6IDBweDtcbiAgcmlnaHQ6IDBweDtcbiAgYm90dG9tOiAwcHg7XG59XG4uaW1hZ2Utc2hlbGwgPiBpbWdbc3JjPVwiXCJdLCAuaW1hZ2Utc2hlbGwgPiBpbWdbc3JjPW51bGxdIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLnRleHQtc2hlbGwge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4udGV4dC1zaGVsbDo6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGJvdHRvbTogMDtcbiAgcmlnaHQ6IDA7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2VlZSA4JSwgI2RkZCAxOCUsICNlZWUgMzMlKTtcbiAgYmFja2dyb3VuZC1zaXplOiA4MDBweCAxMDRweDtcbiAgYW5pbWF0aW9uOiBhbmltYXRlQmFja2dyb3VuZCAycyBlYXNlLWluLW91dCBpbmZpbml0ZTtcbn1cbi50ZXh0LXNoZWxsLnRleHQtbG9hZGVkOjpiZWZvcmUsIC50ZXh0LXNoZWxsLnRleHQtbG9hZGVkOjphZnRlciB7XG4gIGJhY2tncm91bmQ6IG5vbmUgIWltcG9ydGFudDtcbiAgYW5pbWF0aW9uOiAwICFpbXBvcnRhbnQ7XG59XG5cbmgzLnRleHQtc2hlbGw6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGJvdHRvbTogMDtcbiAgcmlnaHQ6IDA7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgdHJhbnNwYXJlbnQgOTUlLCAjZmZmIDk1JSk7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAyMnB4O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwIDBweDtcbn1cblxucC50ZXh0LXNoZWxsOjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiAwO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsIHRyYW5zcGFyZW50IDk1JSwgI2ZmZiA5NSUpLCBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNmZmYgMTAwJSwgI2ZmZiAxMDAlKSwgbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCB0cmFuc3BhcmVudCA2NSUsICNmZmYgNjUlKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDE2cHgsIDEwMCUgM3B4LCAxMDAlIDE2cHg7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IDAgMHB4LCAwIDE2cHgsIDAgMTlweDtcbn1cblxuQGtleWZyYW1lcyBhbmltYXRlQmFja2dyb3VuZCB7XG4gIDAlIHtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAtNDY4cHggMDtcbiAgfVxuICAxMDAlIHtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA0NjhweCAwO1xuICB9XG59Il19 */"

/***/ })

}]);
//# sourceMappingURL=customer-customer-module-es2015.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["report-report-detail-report-detail-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/report/report-detail/report-detail.page.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/report/report-detail/report-detail.page.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/report\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>{{ report.name }} Report</ion-title>\n    <ion-buttons slot=\"primary\" *ngIf=\"report.key === 'daily'\">\n      <ion-button slot=\"icon-only\">\n        <ion-icon name=\"clock\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <!-- <ion-buttons slot=\"end\" *ngIf=\"report.key === 'value' \">\n      <ion-datetime class=\"dateStyle\" displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YY hh:mm a\" (toDateOutput)=\"exampleMethodParent1($event)\"\n      (fromDateOutput)=\"exampleMethodParent($event)\" (ionChange)=\"getDailyReportData()\"></ion-datetime>\n    </ion-buttons> -->\n  </ion-toolbar>\n\n    <!-- <app-select-vehicle [receivedParentMessage]=\"messageToSendP\" (selectedVehObj)=\"getVehObj($event)\">\n    </app-select-vehicle> -->\n  \n</ion-header>\n\n<ion-content>\n  <ion-grid\n    *ngIf=\"report.key === 'summ' || report.key === 'ospeed' || report.key === 'dist' || report.key === 'ac' || report.key === 'routevio' || report.key === 'sos' || report.key === 'trip' || report.key === 'stop' || report.key === 'ign' || report.key === 'daywise' || report.key === 'idle' || report.key === 'value' \"\n    no-padding>\n    <ion-row no-padding>\n      <ion-col size-sm=\"6\" offset-sm=\"3\" no-padding>\n        <ion-row no-padding>\n          <ion-col size=\"12\" no-padding>\n            <app-from-to (fromDateOutput)=\"exampleMethodParent($event)\" (toDateOutput)=\"exampleMethodParent1($event)\"\n              [fromDate]=\"datetimeFrom\" [toDate]=\"datetimeTo\">\n            </app-from-to>\n          </ion-col>\n          <ion-col size=\"12\" style=\"background: #cd2a00; padding: 5px;\">\n            <app-select-vehicle [receivedParentMessage]=\"messageToSendP\" (selectedVehObj)=\"getVehObj($event)\">\n            </app-select-vehicle>\n          </ion-col>\n          <ion-item style=\"background-color: #fafafa;\" *ngIf=\"report.key === 'poi'\">\n            <ion-label>Min Idle Time(Min)</ion-label>\n            <ion-input type=\"number\" [(ngModel)]=\"minTime\"></ion-input >\n          </ion-item>\n          <ion-col size=\"12\">\n            <ion-row style=\"background-color: #fafafa;\">\n              <ion-col style=\"text-align: center;\">\n                <ion-button class=\"ionBtn\" shape=\"round\" size=\"small\" (click)=\"changeDate('today')\">Today\n                </ion-button>\n                <ion-button class=\"ionBtn\" shape=\"round\" size=\"small\" (click)=\"changeDate('yest')\">\n                  Yesterday</ion-button>\n                <ion-button class=\"ionBtn\" shape=\"round\" size=\"small\" (click)=\"changeDate('week')\">Week\n                </ion-button>\n                <ion-button class=\"ionBtn\" shape=\"round\" size=\"small\" (click)=\"changeDate('month')\">Month\n                </ion-button>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-grid no-padding>\n    <ion-row no-padding>\n      <ion-col size-sm=\"6\" offset-sm=\"3\" no-padding>\n        <div *ngIf=\"report.key === 'daily'\">\n          <ion-card *ngFor=\"let item of reportData\">\n            <ion-card-header style=\"padding: 5px\" color=\"medium\">\n\n              <ion-card-subtitle>\n                <ion-icon style=\"font-size: 1.2em;\" name=\"logo-model-s\"></ion-icon>\n                &nbsp;&nbsp;&nbsp;&nbsp;{{item.Device_Name}}\n              </ion-card-subtitle>\n\n            </ion-card-header>\n            <ion-card-content style=\"padding: 0px\">\n              <ion-row no-padding>\n                <ion-col size=\"6\">\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#454344;\">Today Distance(km)</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.today_odo}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#454344;\">Fuel Consumption(L)</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.mileage}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#454344;\">Max Speed(km/hr)</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.maxSpeed}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#454344;\">Today Trips</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.today_trips}}</ion-col>\n                  </ion-row>\n                </ion-col>\n                <ion-col size=\"6\">\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#11a46e;\">Running (Hr:Min)</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.today_running}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#ff3f32;\">Stopped (Hr:Min)</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.today_stopped}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#1bb6d4;\">Out of reach (Hr:Min)</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.t_ofr}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#ffc13c;\">Idle (Hr:Min)</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.t_idling}}</ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </ion-card-content>\n          </ion-card>\n        </div>\n        <div *ngIf=\"report.key === 'summ'\">\n          <ion-card *ngFor=\"let item of reportData\">\n            <ion-card-header style=\"padding: 5px\" color=\"medium\" no-padding>\n              <ion-row>\n                <ion-col size=\"7\">\n                  <ion-card-subtitle style=\"color: white;\" no-padding>\n                    <ion-icon style=\"font-size: 1.2em;\" name=\"logo-model-s\"></ion-icon>\n                    &nbsp;&nbsp;&nbsp;&nbsp;{{item.Device_Name}}\n                  </ion-card-subtitle>\n                </ion-col>\n                <ion-col size=\"5\" text-right>\n                  <ion-card-subtitle style=\"color: white;\" no-padding>\n                    Trips - {{item.tripCount}}\n                  </ion-card-subtitle>\n                </ion-col>\n              </ion-row>\n            </ion-card-header>\n            <ion-card-content style=\"padding: 0px\">\n              <ion-row no-padding>\n                <ion-col size=\"6\">\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#454344;\">Distance (km)</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.distance}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#454344;\">Fuel Consumption (L)</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.mileage}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#454344;\">Overspeeding (Hr:Min)</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.overspeeds}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#454344;\">Route Violation</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.routeViolations}}</ion-col>\n                  </ion-row>\n                </ion-col>\n                <ion-col size=\"6\">\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#11a46e;\">Running (Hr:Min)</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.ignOn}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#ff3f32;\">Stopped (Hr:Min)</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.ignOff}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#1bb6d4;\">Out of reach (Hr:Min)</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.t_ofr}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#ffc13c;\">Idle (Hr:Min)</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item.t_idling}}</ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n              <ion-row *ngIf=\"item.StartLocation !== 'N/A'\">\n                <ion-col size=\"1\" style=\"padding: 7px 0px; text-align: center;\">\n                  <ion-icon name=\"pin\" style=\"font-size: 1em;margin: auto;\"></ion-icon>\n                </ion-col>\n                <ion-col size=\"11\">\n                  <p> {{item.StartLocation}}</p>\n                </ion-col>\n              </ion-row>\n              <ion-row *ngIf=\"item.EndLocation !== 'N/A'\">\n                <ion-col size=\"1\" style=\"padding: 7px 0px; text-align: center;\">\n                  <ion-icon name=\"pin\" style=\"font-size: 1em;margin: auto;\"></ion-icon>\n                </ion-col>\n                <ion-col size=\"11\">\n                  <p> {{item.EndLocation}}</p>\n                </ion-col>\n              </ion-row>\n            </ion-card-content>\n          </ion-card>\n        </div>\n        <div *ngIf=\"report.key === 'ospeed'\">\n          <ion-card *ngFor=\"let item of reportData\">\n            <ion-card-header style=\"padding: 5px\" color=\"medium\" no-padding>\n              <ion-card-subtitle>\n                <ion-icon style=\"font-size: 1.2em;\" name=\"logo-model-s\"></ion-icon>\n                &nbsp;&nbsp;&nbsp;&nbsp;{{item.Device_Name}}\n              </ion-card-subtitle>\n\n            </ion-card-header>\n            <ion-card-content style=\"padding: 0px\">\n              <ion-row no-padding>\n                <ion-col size=\"12\">\n                  <ion-row>\n                    <ion-col size=\"5\">\n                      <p style=\"color:#11a46e;\">Datetime</p>\n                    </ion-col>\n                    <ion-col size=\"7\">{{item.timestamp | date:'mediumDate'}}, {{item.timestamp | date:'shortTime'}}\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"5\">\n                      <p style=\"color:#ff3f32;\">Overspeed</p>\n                    </ion-col>\n                    <ion-col size=\"7\">{{item.overSpeed}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"1\">\n                      <ion-icon style=\"font-size: 1.8em;\" name=\"pin\"></ion-icon>\n                    </ion-col>\n                    <ion-col size=\"11\">\n                      <p>{{item.StartLocation}}</p>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </ion-card-content>\n          </ion-card>\n        </div>\n        <div *ngIf=\"report.key === 'dist'\">\n          <ion-card *ngFor=\"let item of reportData\">\n            <ion-item>\n              <ion-icon name=\"car\" slot=\"start\"></ion-icon>\n              <ion-label>{{item.Device_Name}}</ion-label>\n              <ion-button fill=\"outline\" slot=\"end\">{{item.distance | number : '1.0-2'}} kms</ion-button>\n            </ion-item>\n\n            <ion-card-content>\n              <ion-row>\n                <ion-col size=\"1\" style=\"padding: 7px 0px;\">\n                  <ion-icon name=\"pin\" style=\"font-size: 1.8em;margin: auto; color: #419441;\"></ion-icon>\n                </ion-col>\n                <ion-col size=\"11\">\n                  <p> {{item.StartLocation}}</p>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col size=\"1\" class=\"dottedBorder\"></ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col size=\"1\" style=\"padding: 7px 0px;\">\n                  <ion-icon name=\"pin\" style=\"font-size: 1.8em;margin: auto; color: #f7573e;\"></ion-icon>\n                </ion-col>\n                <ion-col size=\"11\">\n                  <p> {{item.EndLocation}}</p>\n                </ion-col>\n              </ion-row>\n            </ion-card-content>\n          </ion-card>\n          <!-- <ion-card>\n            <ion-item>\n                <ion-icon name=\"car\" slot=\"start\"></ion-icon>\n              <ion-label>ion-item in</ion-label>\n              <ion-button fill=\"outline\" slot=\"end\">100 kms</ion-button>\n            </ion-item>\n          \n            <ion-card-content>\n              <ion-row>\n                <ion-col size=\"1\" style=\"padding: 7px 0px;\"><ion-icon name=\"pin\" style=\"font-size: 1.8em;margin: auto;\"></ion-icon></ion-col>\n                <ion-col size=\"11\">\n                    <p>  This is content, without any paragraph or header tags,\n                        within an ion-card-content element.</p>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                  <ion-col size=\"1\" style=\"padding: 7px 0px;\"><ion-icon name=\"pin\" style=\"font-size: 1.8em;margin: auto;\"></ion-icon></ion-col>\n                  <ion-col size=\"11\">\n                      <p>  This is content, without any paragraph or header tags,\n                          within an ion-card-content element.</p>\n                  </ion-col>\n                </ion-row>\n            </ion-card-content>\n          </ion-card> -->\n        </div>\n        <div *ngIf=\"report.key === 'trip'\">\n          <ion-card *ngFor=\"let item of reportData\">\n            <ion-item>\n              <ion-icon name=\"car\" slot=\"start\"></ion-icon>\n              <ion-label>{{item.Device_Name}}</ion-label>\n              <ion-button fill=\"outline\" slot=\"end\">{{item.distance | number : '1.0-2'}} kms</ion-button>\n            </ion-item>\n\n            <ion-card-content>\n              <ion-row>\n                <ion-col size=\"1\" style=\"padding: 7px 0px;\">\n                  <ion-icon name=\"pin\" style=\"font-size: 1.8em;margin: auto; color: #419441;\"></ion-icon>\n                </ion-col>\n                <ion-col size=\"11\">\n                  <p> {{item.StartLocation}}</p>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col size=\"1\" class=\"dottedBorder\"></ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col size=\"1\" style=\"padding: 7px 0px;\">\n                  <ion-icon name=\"pin\" style=\"font-size: 1.8em;margin: auto; color: #f7573e;\"></ion-icon>\n                </ion-col>\n                <ion-col size=\"11\">\n                  <p> {{item.EndLocation}}</p>\n                </ion-col>\n              </ion-row>\n            </ion-card-content>\n            <ion-row style=\"margin-top: -15px; padding-left: 16px;\">\n              <ion-col size=\"1\">\n                <ion-icon style=\"font-weight: bold; font-size: 1.5em;\" name=\"calendar\"></ion-icon>\n              </ion-col>\n              <ion-col size=\"4\">\n                <p style=\"font-size: 1em; padding-top: 3px; color: darkslateblue;\">{{item.Startdate}}</p>\n              </ion-col>\n              <ion-col size=\"1\">\n                <ion-icon style=\"font-weight: bold; font-size: 1.5em;\" name=\"time\"></ion-icon>\n              </ion-col>\n              <ion-col size=\"6\">\n                <p style=\"font-size: 1em; padding-top: 3px; color: darkslateblue;\">DURATION {{item.duration}}</p>\n              </ion-col>\n            </ion-row>\n          </ion-card>\n        </div>\n        <div *ngIf=\"report.key === 'ign'\">\n          <ion-card *ngFor=\"let item of reportData\">\n            <ion-item>\n              <ion-icon name=\"car\" slot=\"start\"></ion-icon>\n              <ion-label>{{item.Device_Name}}</ion-label>\n              <ion-button slot=\"end\" fill=\"clear\">\n                <ion-icon *ngIf=\"item.switch === 'OFF'\" slot=\"icon-only\" name=\"key\"\n                  style=\"font-size: 1.8em; color: #f7573e;\"></ion-icon>\n                <ion-icon *ngIf=\"item.switch !== 'OFF'\" slot=\"icon-only\" name=\"key\"\n                  style=\"font-size: 1.8em; color: #419441;\"></ion-icon>\n              </ion-button>\n            </ion-item>\n            <ion-card-content style=\"padding: 0px\">\n              <ion-row style=\"padding-left: 13px;\">\n                <ion-col size=\"1\">\n                  <ion-icon name=\"calendar\" style=\"font-size: 1.5em;\"></ion-icon>\n                </ion-col>\n                <ion-col size=\"11\">{{item.timestamp | date: \"medium\"}}, {{item.timestamp | date:'shortTime'}}\n                </ion-col>\n              </ion-row>\n              <ion-row style=\"padding-left: 11px;\">\n                <ion-col size=\"1\">\n                  <ion-icon style=\"font-size: 1.8em; color: #419441;\" name=\"pin\"></ion-icon>\n                </ion-col>\n                <ion-col size=\"11\">\n                  <p>{{item.StartLocation}}</p>\n                </ion-col>\n              </ion-row>\n            </ion-card-content>\n          </ion-card>\n        </div>\n        <div *ngIf=\"report.key === 'sos'\">\n          <ion-card *ngFor=\"let item of reportData\">\n            <ion-item>\n              <ion-icon name=\"car\" slot=\"start\"></ion-icon>\n              <ion-label>{{item.vehicleName}}</ion-label>\n              <!-- <ion-button slot=\"end\" fill=\"clear\">\n                <ion-icon *ngIf=\"item.switch === 'OFF'\" slot=\"icon-only\" name=\"key\"\n                  style=\"font-size: 1.8em; color: #f7573e;\"></ion-icon>\n                <ion-icon *ngIf=\"item.switch !== 'OFF'\" slot=\"icon-only\" name=\"key\"\n                  style=\"font-size: 1.8em; color: #419441;\"></ion-icon>\n              </ion-button> -->\n            </ion-item>\n            <ion-card-content style=\"padding: 0px\">\n              <ion-row style=\"padding-left: 13px;\">\n                <ion-col size=\"1\">\n                  <ion-icon name=\"calendar\" style=\"font-size: 1.5em;\"></ion-icon>\n                </ion-col>\n                <ion-col size=\"11\">{{item.timestamp | date: \"medium\"}}, {{item.timestamp | date:'shortTime'}}\n                </ion-col>\n              </ion-row>\n              <!-- <ion-row style=\"padding-left: 11px;\">\n                <ion-col size=\"1\">\n                  <ion-icon style=\"font-size: 1.8em; color: #419441;\" name=\"pin\"></ion-icon>\n                </ion-col>\n                <ion-col size=\"11\">\n                  <p>{{item.address ? item.address : 'N/A'}}</p>\n                  <p>Hivara-Suleman Deola Road, Beed, 414202, Hivara, India, Maharashtra</p>\n                </ion-col>\n              </ion-row> -->\n            </ion-card-content>\n          </ion-card>\n        </div>\n        <div *ngIf=\"report.key === 'daywise'\">\n            <ion-card *ngFor=\"let item of reportData\">\n            <ion-card-header style=\"padding: 5px\" color=\"medium\" no-padding>\n              <ion-row>\n                <ion-col size=\"7\">\n                  <ion-card-subtitle style=\"color: white;\" no-padding>\n                    <ion-icon style=\"font-size: 1.2em;\" name=\"logo-model-s\"></ion-icon>\n                    &nbsp;&nbsp;&nbsp;&nbsp;{{ item.VRN }}\n                  </ion-card-subtitle>\n                </ion-col>\n                <ion-col size=\"5\" text-right>\n                  <ion-card-subtitle style=\"color: white;\" no-padding>\n                    {{item.Date}}\n                  </ion-card-subtitle>\n                </ion-col>\n              </ion-row>\n            </ion-card-header>\n            <ion-card-content style=\"padding: 0px\">\n              <ion-row no-padding>\n                <ion-col size=\"6\">\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#53ab53;\">Moving</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item['Moving Time']}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:#e6c917;\">Idle</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item['Idle Time']}}</ion-col>\n                  </ion-row>\n                </ion-col>\n                <ion-col size=\"6\">\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:red;\">Stopped</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item['Stoppage Time']}}</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col size=\"8\">\n                      <p style=\"color:gray;\">Distance</p>\n                    </ion-col>\n                    <ion-col size=\"4\">{{item['Distance(Kms)']}}</ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col size=\"1\" style=\"padding: 7px 0px; text-align: center;\">\n                  <ion-icon name=\"pin\" style=\"font-size: 1em;margin: auto;\"></ion-icon>\n                </ion-col>\n                <ion-col size=\"11\">\n                  <p> {{item.StartLocation}} </p>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col size=\"1\" style=\"padding: 7px 0px; text-align: center;\">\n                  <ion-icon name=\"pin\" style=\"font-size: 1em;margin: auto;\"></ion-icon>\n                </ion-col>\n                <ion-col size=\"11\">\n                  <p> {{item.EndLocation}}</p>\n                </ion-col>\n              </ion-row>\n            </ion-card-content>\n          </ion-card>\n        </div>\n\n        <div *ngIf=\"report.key === 'stop'\">\n          <ion-card *ngFor=\"let item of reportData\">\n          <ion-card-header style=\"padding: 5px\" color=\"medium\" no-padding>\n            <ion-row>\n              <ion-col size=\"7\">\n                <ion-card-subtitle style=\"color: white;\" no-padding>\n                  <ion-icon style=\"font-size: 1.2em;\" name=\"logo-model-s\"></ion-icon>\n                  &nbsp;&nbsp;&nbsp;&nbsp;{{item.device}}\n                </ion-card-subtitle>\n              </ion-col>\n              <ion-col size=\"5\" text-right>\n                <ion-card-subtitle style=\"color: white;\" no-padding>\n                  {{item.Durations}}\n                </ion-card-subtitle>\n              </ion-col>\n            </ion-row>\n          </ion-card-header>\n          <ion-card-content style=\"padding: 0px\">\n            <ion-row style=\"padding-top:5px\">\n              <ion-col col-6 style=\"color:gray;font-size:11px;font-weight: 400;\">\n\n                <ion-icon ios=\"ios-time\" md=\"md-time\" style=\"color:#5edb82; font-size: 15px;\"></ion-icon> &nbsp;\n                {{item.arrival_time | date: 'short'}}\n      \n              </ion-col>\n              <ion-col col-6 style=\"color:gray;font-size:11px;font-weight: 400;\">\n      \n                <ion-icon ios=\"ios-time\" md=\"md-time\" style=\"color:#ee7272; font-size: 15px;\"></ion-icon>&nbsp;\n                {{item.departure_time | date: 'short'}}\n      \n              </ion-col>\n            </ion-row>  \n            <ion-row>\n              <ion-col size=\"1\" style=\"padding: 7px 0px; text-align: center;\">\n                <ion-icon name=\"pin\" style=\"font-size: 1em;margin: auto;\"></ion-icon>\n              </ion-col>\n              <ion-col size=\"11\">\n                <p> {{item.EndLocation}} </p>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </div>\n\n      <div *ngIf=\"report.key === 'idle'\">\n        <ion-card *ngFor=\"let item of reportData\">\n          <ion-card-header style=\"padding: 5px\" color=\"medium\" no-padding>\n            <ion-row>\n              <ion-col size=\"7\">\n                <ion-card-subtitle style=\"color: white;\" no-padding>\n                  <ion-icon style=\"font-size: 1.2em;\" name=\"logo-model-s\"></ion-icon>\n                  &nbsp;&nbsp;&nbsp;&nbsp;{{ item.Device_Name }}\n                </ion-card-subtitle>\n              </ion-col>\n              <ion-col size=\"5\" text-right>\n                <ion-card-subtitle style=\"color: white;\" no-padding>\n                  AC Status - {{ item.ac_status}}\n                </ion-card-subtitle>\n              </ion-col>\n            </ion-row>\n          </ion-card-header>\n          <ion-card-content style=\"padding: 0px\">\n            <ion-row no-padding>\n              <ion-col size=\"12\">\n                <ion-row>\n                  <ion-col size=\"4\" style=\"padding: 8px;\">\n                    <p style=\"color:#53ab53;\">Start Time</p>\n                  </ion-col>\n                  <ion-col size=\"8\">{{item.start_time | date: 'mediumDate'}}, {{item.start_time | date:'shortTime'}}</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col size=\"4\" style=\"padding: 8px;\">\n                    <p style=\"color:red;\">End Time</p>\n                  </ion-col>\n                  <ion-col size=\"8\">{{item.end_time | date:'mediumDate'}}, {{item.end_time | date:'shortTime'}}</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col size=\"4\" style=\"padding: 8px;\">\n                    <p style=\"color:#009688;\">Duration</p>\n                  </ion-col>\n                  <ion-col size=\"8\">{{item.duration}}</ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col size=\"1\" style=\"padding: 7px 0px; text-align: center;\">\n                <ion-icon name=\"pin\" style=\"font-size: 1em;margin: auto;\"></ion-icon>\n              </ion-col>\n              <ion-col size=\"11\">\n                <p> {{item.StartLocation}} </p>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </div>  \n      <div *ngIf=\"report.key ==='ac'\">\n        <ion-card *ngFor=\"let item of reportData\">\n          <ion-card-header style=\"padding: 5px\" color=\"medium\" no-padding>\n            <ion-row>\n              <ion-col size=\"7\">\n                <ion-card-subtitle style=\"color: white;\" no-padding>\n                  <ion-icon style=\"font-size: 1.2em;\" name=\"logo-model-s\"></ion-icon>\n                  &nbsp;&nbsp;&nbsp;&nbsp;{{item.Device_Name}}\n                </ion-card-subtitle>\n              </ion-col>\n            </ion-row>\n          </ion-card-header>\n          <ion-card-content style=\"padding: 0px\">\n            <ion-row no-padding>\n              <ion-col size=\"12\">\n                <ion-row>\n                  <ion-col size=\"4\" style=\"padding: 8px;\">\n                    <p style=\"color:#53ab53;\">{{item.Action_First}}&nbsp;Time</p>\n                    \n                    <!-- <span></span> -->\n                  </ion-col>\n                  <ion-col size=\"8\">{{item.Time_First}}</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col size=\"4\" style=\"padding: 8px;\">\n                    <p style=\"color:red;\">{{item.Action_Second}}&nbsp;Time</p>\n                  </ion-col>\n                  <ion-col size=\"8\">{{item.Time_Second}}</ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </div>  \n\n      <div *ngIf=\"report.key === 'value'\">\n        <ion-grid *ngFor=\"let item of reportData\">\n          <ion-row>\n            <ion-col col-12 offset-sm=\"3\">\n              <ion-card style=\"background-image: linear-gradient(to right top, #ff5177, #ff6c88, #ff849a, #fc9aab, #f9afbb);padding: 5px\">\n                <p style=\"color: white;\">Total Distance</p>\n                <ion-row>\n                  <ion-col col-12>\n                    <p>\n                      <span style=\"font-size: xx-large; color: white;\" *ngIf=\"item.today_odo\">{{\n                            item.today_odo | number: \"1.0-2\"\n                          }}</span>\n                      <span *ngIf=\"!item.today_odo\">00.00</span>\n                      <span style=\"color: white;\">&nbsp;KM</span>\n                      <ion-buttons style=\"float: right;\n                          font-size: xx-large; color: white;\"><img src=\"assets/imgs/distan.png\" style=\"width: 50px;\">\n                      </ion-buttons>\n                    </p>\n                  </ion-col>\n                </ion-row>\n                <p style=\"color: white;\">Total distance travelled</p>\n              </ion-card>\n      \n              <ion-card\n                style=\"background-image: linear-gradient(to right top, #1ef015, #40f144, #57f260, #6bf278, #7ef28d); padding: 5px;\">\n                <p style=\"color: white;\">Running(Hr:Min)</p>\n                <ion-row>\n                  <ion-col col-12>\n                    <p>\n                      <span style=\"font-size: xx-large; color: white;\"\n                        *ngIf=\"item.today_running\">{{ item.today_running }}</span>\n                      <span *ngIf=\"!item.today_running\">00.00</span>\n                      <span style=\"color: white;\"></span>\n                      <ion-buttons style=\"float: right;\n                          font-size: xx-large; color: white;\"><img src=\"assets/imgs/playbtn.png\" style=\"width: 50px;\">\n                      </ion-buttons>\n                    </p>\n                  </ion-col>\n                </ion-row>\n                <p style=\"color: white;\">Total running time</p>\n              </ion-card>\n      \n              <ion-card\n                style=\"background-image: linear-gradient(to right top, #f0151f, #ee2932, #ea3842, #e64550, #e0515d); padding: 5px;\">\n                <p style=\"color: white;\">Stop(Hr:Min)</p>\n                <ion-row>\n                  <ion-col col-12>\n                    <p>\n                      <span style=\"font-size: xx-large; color: white;\"\n                        *ngIf=\"item.today_stopped\">{{ item.today_stopped }}</span>\n                      <span *ngIf=\"!item.today_stopped\">00.00</span>\n                      <span style=\"color: white;\"></span>\n                      <ion-buttons style=\"float: right;\n                              font-size: xx-large; color: white;\"><img src=\"assets/imgs/stopping.png\" style=\"width: 50px;\">\n                      </ion-buttons>\n                    </p>\n                  </ion-col>\n                </ion-row>\n                <p style=\"color: white;\">Total stopped time</p>\n              </ion-card>\n      \n              <ion-card\n                style=\"background-image: linear-gradient(to right top, #cbd500, #cada26, #cadf3a, #cae44a, #cae959); padding: 5px;\">\n                <p style=\"color: white;\">Idle</p>\n                <ion-row>\n                  <ion-col col-12>\n                    <p>\n                      <span style=\"font-size: xx-large; color: white;\" *ngIf=\"item.t_idling\">{{ item.t_idling }}</span>\n                      <span *ngIf=\"!item.t_idling\">00.00</span>\n                      <span style=\"color: white;\"></span>\n                      <ion-buttons style=\"float: right;\n                              font-size: xx-large; color: white;\"><img src=\"assets/imgs/steering-wheel.png\"\n                          style=\"width: 50px;\"></ion-buttons>\n                    </p>\n                  </ion-col>\n                </ion-row>\n                <p style=\"color: white;\">Total idle time</p>\n              </ion-card>\n      \n              <ion-card\n                style=\"background-image: linear-gradient(to right top, #72bee7, #6cbce7, #66bbe8, #60b9e8, #59b7e9); padding: 5px;\">\n                <p style=\"color: white;\">Out of reach</p>\n                <ion-row>\n                  <ion-col col-12>\n                    <p>\n                      <span style=\"font-size: xx-large; color: white;\" *ngIf=\"item.t_ofr\">{{ item.t_ofr }}</span>\n                      <span *ngIf=\"!item.t_ofr\">00.00</span>\n                      <span style=\"color: white;\"></span>\n                      <ion-buttons style=\"float: right;\n                              font-size: xx-large; color: white;\"><img src=\"assets/imgs/no-wifi.png\" style=\"width: 50px;\">\n                      </ion-buttons>\n                    </p>\n                  </ion-col>\n                </ion-row>\n                <p style=\"color: white;\">Total Out of reach</p>\n              </ion-card>\n      \n              <ion-card\n                style=\"background-image: linear-gradient(to right top, #5ce0e0, #5ee1cf, #6ae0bc, #7bdfa8, #8fdc93); padding: 5px;\">\n                <p style=\"color: white;\">Fuel Con(Litre)</p>\n                <ion-row>\n                  <ion-col col-12>\n                    <p>\n                      <span style=\"font-size: xx-large; color: white;\">{{ item.mileage }}</span>\n                      <span style=\"color: white;\"></span>\n                      <ion-buttons style=\"float: right;\n                          font-size: xx-large; color: white;\"><img src=\"assets/imgs/fuelcon.png\" style=\"width: 50px;\">\n                      </ion-buttons>\n                    </p>\n                  </ion-col>\n                </ion-row>\n                <p style=\"color: white;\">Total fuel consumption</p>\n              </ion-card>\n      \n              <ion-card\n                style=\"background-image: linear-gradient(to right top, #29e081, #7fdb7e, #a7d586, #c0d097, #cdcdae); padding: 5px;\">\n                <p style=\"color: white;\">Max Speed</p>\n                <ion-row>\n                  <ion-col col-12>\n                    <p>\n                      <span style=\"font-size: xx-large; color: white;\" *ngIf=\"item.maxSpeed\">{{ item.maxSpeed }}</span>\n                      <span *ngIf=\"!item.maxSpeed\">00.00</span>\n                      <span style=\"color: white;\">Km/Hr</span>\n                      <ion-buttons style=\"float: right;\n                                  font-size: xx-large; color: white;\"><img src=\"assets/imgs/max.png\" style=\"width: 50px;\">\n                      </ion-buttons>\n                    </p>\n                  </ion-col>\n                </ion-row>\n                <p style=\"color: white;\">Max speed</p>\n              </ion-card>\n      \n              <ion-card\n                style=\"background-image: linear-gradient(to right top, #9f94c0, #88a7d9, #66bce6, #49cee4, #57ded2); padding: 5px;\">\n                <p style=\"color: white;\">Trips</p>\n                <ion-row>\n                  <ion-col col-12>\n                    <p>\n                      <span style=\"font-size: xx-large; color: white;\" *ngIf=\"item.today_trips\">{{ item.today_trips }}</span>\n                      <span style=\"font-size: xx-large; color: white;\" *ngIf=\"!item.today_trips\">00.00</span>\n                      <span style=\"color: white;\"></span>\n                      <ion-buttons style=\"float: right;\n                                  font-size: xx-large; color: white;\"><img src=\"assets/imgs/trip.png\" style=\"width: 50px;\">\n                      </ion-buttons>\n                    </p>\n                  </ion-col>\n                </ion-row>\n                <p style=\"color: white;\">Total trips</p>\n              </ion-card>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>  \n        <ion-row *ngIf=\"showString !== undefined\" class=\"ion-padding\">\n          <ion-col style=\"text-align: center;\">\n            <p style=\"color: #454344;\">{{showString}}</p>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/report/report-detail/report-detail.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/report/report-detail/report-detail.module.ts ***!
  \**************************************************************/
/*! exports provided: ReportDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportDetailPageModule", function() { return ReportDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _report_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./report-detail.page */ "./src/app/report/report-detail/report-detail.page.ts");
/* harmony import */ var src_app_tabs_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/tabs/components/components.module */ "./src/app/tabs/components/components.module.ts");








var routes = [
    {
        path: '',
        component: _report_detail_page__WEBPACK_IMPORTED_MODULE_6__["ReportDetailPage"]
    }
];
var ReportDetailPageModule = /** @class */ (function () {
    function ReportDetailPageModule() {
    }
    ReportDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                src_app_tabs_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
            ],
            declarations: [_report_detail_page__WEBPACK_IMPORTED_MODULE_6__["ReportDetailPage"]]
        })
    ], ReportDetailPageModule);
    return ReportDetailPageModule;
}());



/***/ }),

/***/ "./src/app/report/report-detail/report-detail.page.scss":
/*!**************************************************************!*\
  !*** ./src/app/report/report-detail/report-detail.page.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "p {\n  padding: 0px;\n  margin: 0px;\n  font-size: 0.8em;\n  font-weight: 500;\n}\n\n.dottedBorder {\n  margin-left: 11px;\n  margin-top: -12px;\n  margin-bottom: -5px;\n  border-left-style: dashed;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL2lvbmljNC9vbmVxbGlrL3NyYy9hcHAvcmVwb3J0L3JlcG9ydC1kZXRhaWwvcmVwb3J0LWRldGFpbC5wYWdlLnNjc3MiLCJzcmMvYXBwL3JlcG9ydC9yZXBvcnQtZGV0YWlsL3JlcG9ydC1kZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDQ0Y7O0FEWUE7RUFDRSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtBQ1RGIiwiZmlsZSI6InNyYy9hcHAvcmVwb3J0L3JlcG9ydC1kZXRhaWwvcmVwb3J0LWRldGFpbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJwIHtcbiAgcGFkZGluZzogMHB4O1xuICBtYXJnaW46IDBweDtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLy8gLmlvbkJ0biB7XG4vLyAgIC8vIC0tYmFja2dyb3VuZDogI2YwODEwZjtcbi8vICAgZm9udC1zaXplOiAwLjdlbTtcbi8vICAgLy8gcGFkZGluZzogMTBweDtcbi8vICAgd2lkdGg6IDIzJTtcbi8vICAgLy8gYmFja2dyb3VuZC1jb2xvcjogI2YwODEwZjtcbi8vICAgLS1iYWNrZ3JvdW5kOiAjZjA4MTBmO1xuLy8gICAtLWNvbG9yOiBibGFjaztcbi8vIH1cblxuLmRvdHRlZEJvcmRlciB7XG4gIG1hcmdpbi1sZWZ0OiAxMXB4O1xuICBtYXJnaW4tdG9wOiAtMTJweDtcbiAgbWFyZ2luLWJvdHRvbTogLTVweDtcbiAgYm9yZGVyLWxlZnQtc3R5bGU6IGRhc2hlZDtcbn1cbiIsInAge1xuICBwYWRkaW5nOiAwcHg7XG4gIG1hcmdpbjogMHB4O1xuICBmb250LXNpemU6IDAuOGVtO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4uZG90dGVkQm9yZGVyIHtcbiAgbWFyZ2luLWxlZnQ6IDExcHg7XG4gIG1hcmdpbi10b3A6IC0xMnB4O1xuICBtYXJnaW4tYm90dG9tOiAtNXB4O1xuICBib3JkZXItbGVmdC1zdHlsZTogZGFzaGVkO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/report/report-detail/report-detail.page.ts":
/*!************************************************************!*\
  !*** ./src/app/report/report-detail/report-detail.page.ts ***!
  \************************************************************/
/*! exports provided: ReportDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportDetailPage", function() { return ReportDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var src_app_app_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/app.model */ "./src/app/app.model.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);








var ReportDetailPage = /** @class */ (function () {
    function ReportDetailPage(route, navCtrl, appService, authService, constUrl, loadingController, toastCtrl) {
        this.route = route;
        this.navCtrl = navCtrl;
        this.appService = appService;
        this.authService = authService;
        this.constUrl = constUrl;
        this.loadingController = loadingController;
        this.toastCtrl = toastCtrl;
        this.userData = {};
        this.pageNo = 0;
        this.limit = 10;
        this.reportData = [];
        this.vehObj = [];
        this.minTime = 5;
        console.log("vehonj", this.vehObj);
        this.getToken();
        this.from = moment__WEBPACK_IMPORTED_MODULE_7__({ hours: 0 }).format();
        this.to = moment__WEBPACK_IMPORTED_MODULE_7__().format(); //new Date(a).toISOString();
    }
    ReportDetailPage.prototype.ngOnInit = function () {
        var _this = this;
        console.log("vehonj", this.vehObj);
        this.route.paramMap.subscribe(function (paramMap) {
            if (!paramMap.has('reportId')) {
                _this.navCtrl.navigateBack('/report');
                return;
            }
            _this.report = _this.appService.getReportTypes(paramMap.get('reportId'));
            _this.messageToSendP = _this.report.key;
            if (_this.report.key === 'summ' || _this.report.key === 'dist') {
                _this.datetimeFrom = moment__WEBPACK_IMPORTED_MODULE_7__({ hours: 0 }).subtract(1, 'days').format(); // yesterday date with 12:00 am
                _this.datetimeTo = moment__WEBPACK_IMPORTED_MODULE_7__({ hours: 0 }).format(); // today date and time with 12:00am
            }
            else {
                _this.datetimeFrom = moment__WEBPACK_IMPORTED_MODULE_7__({ hours: 0 }).format();
                _this.datetimeTo = moment__WEBPACK_IMPORTED_MODULE_7__().format(); //new Date(a).toISOString();
            }
        });
    };
    ReportDetailPage.prototype.exampleMethodParent = function ($event) {
        this.datetimeFrom = $event;
        //this.getGlobalreportData();
    };
    ReportDetailPage.prototype.exampleMethodParent1 = function ($event) {
        this.datetimeTo = $event;
        //this.getGlobalreportData();
    };
    ReportDetailPage.prototype.getVehObj = function ($event) {
        debugger;
        this.vehObj = $event;
        console.log("vecobj", this.vehObj);
        this.getGlobalreportData();
        // this.getIdleReportData();
        this.getDailyReportData();
        //console.log("vehobj", this.vehObj);
    };
    ReportDetailPage.prototype.getToken = function () {
        var _this = this;
        this.authService.getTokenData().subscribe(function (data) {
            _this.userData = data;
            if (_this.report.key === 'daily') {
                _this.msgString = "Loading daily report...";
                _this.getDailyReportData();
            }
            else if (_this.report.key === 'value') {
                _this.msgString = "Loading value screen...";
                _this.getDailyReportData();
            }
            else if (_this.report.key === 'idle') {
                _this.msgString === 'Loading Idle Report...',
                    _this.getIdleReportData();
            }
            else if (_this.report.key === 'summ') {
                _this.msgString = "Loading summary report...";
                _this.getGlobalreportData();
            }
            else if (_this.report.key === 'ospeed') {
                _this.msgString = "Loading overspeed report...";
                _this.getGlobalreportData();
            }
            else if (_this.report.key === 'daywise') {
                _this.msgString = 'Loading Overspeed Report...';
                //this.getGlobalreportData();
            }
            else if (_this.report.key === 'dist') {
                _this.msgString = "Loading distance report..";
                _this.getGlobalreportData();
            }
            else if (_this.report.key === 'trip') {
                _this.msgString = "Loading trip report..";
                _this.getGlobalreportData();
            }
            else if (_this.report.key === 'ign') {
                _this.msgString = "Loading ignition report..";
                _this.getGlobalreportData();
            }
            else if (_this.report.key === 'sos') {
                _this.msgString = "Loading sos report..";
                _this.getGlobalreportData();
            }
            else if (_this.report.key === 'stop') {
                _this.msgString = 'Loading stoppage report';
                _this.getGlobalreportData();
            }
            else if (_this.report.key === 'poi') {
                _this.msgString = 'Loading Poi Report';
                _this.getGlobalreportData();
            }
            else if (_this.report.key === 'ac') {
                _this.msgString = 'Loading AC Report';
                _this.getGlobalreportData();
            }
        });
    };
    ReportDetailPage.prototype.changeDate = function (key) {
        this.datetimeFrom = undefined;
        if (key === 'today') {
            this.datetimeFrom = moment__WEBPACK_IMPORTED_MODULE_7__({ hours: 0 }).format();
        }
        else if (key === 'yest') {
            this.datetimeFrom = moment__WEBPACK_IMPORTED_MODULE_7__().subtract(1, 'days').format();
        }
        else if (key === 'week') {
            this.datetimeFrom = moment__WEBPACK_IMPORTED_MODULE_7__().subtract(1, 'weeks').endOf('isoWeek').format();
        }
        else if (key === 'month') {
            this.datetimeFrom = moment__WEBPACK_IMPORTED_MODULE_7__().startOf('month').format();
        }
    };
    ReportDetailPage.prototype.getIdleReportData = function () {
        var _this = this;
        console.log("entered");
        debugger;
        var vehId = [];
        if (this.report.key === 'idle') {
            for (var t = 0; t < this.vehObj.length; t++) {
                vehId.push(this.vehObj[t]._id);
            }
        }
        console.log("id", vehId);
        // if (vehId.length === 0) {
        //   this.toastCtrl.create({
        //     message: 'Please select vehicle and try again..',
        //     duration: 1500,
        //     position: 'bottom'
        //   })
        //   return;
        // }
        // console.log("id", vehId);
        var burl;
        burl = this.constUrl.mainUrl + 'stoppage/idleReportdatatable';
        var payload = {
            "draw": 3,
            "columns": [
                {
                    "data": "_id"
                },
                {
                    "data": "device"
                },
                {
                    "data": "device.Device_Name"
                },
                {
                    "data": "start_time"
                },
                {
                    "data": "end_time"
                },
                {
                    "data": "lat"
                },
                {
                    "data": "long"
                },
                {
                    "data": "ac_status"
                },
                {
                    "data": "idle_time"
                },
                {
                    "data": "address"
                },
                {
                    "data": null,
                    "defaultContent": ""
                }
            ],
            "order": [
                {
                    "column": 0,
                    "dir": "asc"
                }
            ],
            "start": 0,
            "length": 10,
            "search": {
                "value": "",
                "regex": false
            },
            "op": {},
            "select": [],
            "find": {
                "device": {
                    "$in": vehId
                },
                "start_time": {
                    "$gte": new Date(this.datetimeFrom).toISOString()
                },
                "end_time": {
                    "$lte": new Date(this.datetimeTo).toISOString()
                },
                "idle_time": {
                    "$gte": (this.minTime * 60000)
                }
            }
        };
        this.reportData = [];
        this.loadingController
            .create({
            message: this.msgString
        })
            .then(function (loadEl) {
            loadEl.present();
            _this.authService.postMethod(burl, payload).subscribe(function (respData) {
                loadEl.dismiss();
                console.log("idleReportData", respData);
                debugger;
                var data = JSON.parse(JSON.stringify(respData));
                var outerthis = _this;
                var i = 0, howManyTimes = data.length;
                function f() {
                    for (var i = 0; i < data.data.length; i++) {
                        outerthis.reportData.push({
                            'Device_Name': data.data[i].device.Device_Name,
                            'start_location': {
                                'lat': data.data[i].lat,
                                'long': data.data[i].long
                            },
                            'ac_status': (data.data[i].ac_status ? data.data[i].ac_status : 'NA'),
                            'end_time': data.data[i].end_time,
                            'duration': outerthis.parseMillisecondsIntoReadableTime(data.data[i].idle_time),
                            'start_time': data.data[i].start_time
                        });
                        outerthis.start_address(outerthis.reportData[i], i);
                    }
                    i++;
                    if (i < howManyTimes) {
                        setTimeout(f, 100);
                    }
                }
                f();
                console.log("Final idleReportData", outerthis.reportData);
            });
        });
    };
    ReportDetailPage.prototype.parseMillisecondsIntoReadableTime = function (milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;
        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
        // return h + ':' + m;
        return h + ':' + m + ':' + s;
    };
    ReportDetailPage.prototype.getDailyReportData = function () {
        var _this = this;
        console.log("entered");
        var vehId = [];
        if (this.vehObj.length === 0) {
            vehId = [];
        }
        else {
            if (this.report.key === 'value') {
                for (var t = 0; t < this.vehObj.length; t++) {
                    vehId.push(this.vehObj[t].Device_ID);
                }
            }
        }
        this.pageNo = 0;
        var baseUrl;
        baseUrl = this.constUrl.mainUrl + "devices/daily_report";
        var that = this;
        var currDay = new Date().getDay();
        var currMonth = new Date().getMonth();
        var currYear = new Date().getFullYear();
        var selectedDay = new Date(that.to).getDay();
        var selectedMonth = new Date(that.to).getMonth();
        var selectedYear = new Date(that.to).getFullYear();
        var devname, devid, today_odo, today_running, today_stopped, t_idling, t_ofr, today_trips, maxSpeed, mileage;
        if ((currDay == selectedDay) && (currMonth == selectedMonth) && (currYear == selectedYear)) {
            devname = "Device_Name";
            devid = "Device_ID";
            today_odo = "today_odo";
            today_running = "today_running";
            today_stopped = "today_stopped";
            t_idling = "t_idling";
            t_ofr = "t_ofr";
            today_trips = "today_trips";
            maxSpeed = "maxSpeed";
            mileage = "Mileage";
        }
        else {
            console.log("else block called");
            devid = "imei";
            devname = "ID.Device_Name";
            today_odo = "today_odo";
            today_running = "today_running";
            today_stopped = "today_stopped";
            t_idling = "t_idling";
            t_ofr = "t_ofr";
            today_trips = "today_trips";
            maxSpeed = "ID.maxSpeed";
            mileage = "Mileage";
        }
        // debugger
        var payload = {};
        debugger;
        if (this.vehObj == undefined) {
            payload = {
                "draw": 2,
                "columns": [
                    {
                        "data": devname
                    },
                    {
                        "data": devid
                    },
                    {
                        "data": today_odo
                    },
                    {
                        "data": today_running
                    },
                    {
                        "data": today_stopped
                    },
                    {
                        "data": t_idling
                    },
                    {
                        "data": t_ofr
                    },
                    {
                        "data": today_trips
                    },
                    {
                        "data": maxSpeed
                    },
                    {
                        "data": mileage
                    },
                    { "data": "t_running" },
                    { "data": "t_stopped" },
                    { "data": "t_idling" },
                    { "data": "t_ofr" },
                    { "data": "t_noGps" },
                    {
                        "data": null,
                        "defaultContent": ""
                    }
                ],
                "order": [
                    {
                        "column": 0,
                        "dir": "asc"
                    }
                ],
                "start": 0,
                "length": this.limit,
                "search": {
                    "value": "",
                    "regex": false
                },
                "op": {},
                "select": [],
                "find": {
                    "user_id": this.userData._id,
                    "date": new Date(this.datetimeTo).toISOString()
                }
            };
        }
        else {
            payload = {
                "draw": 2,
                "columns": [
                    {
                        "data": devname
                    },
                    {
                        "data": devid
                    },
                    {
                        "data": today_odo
                    },
                    {
                        "data": today_running
                    },
                    {
                        "data": today_stopped
                    },
                    {
                        "data": t_idling
                    },
                    {
                        "data": t_ofr
                    },
                    {
                        "data": today_trips
                    },
                    {
                        "data": mileage
                    },
                    {
                        "data": maxSpeed
                    },
                    { "data": "t_running" },
                    { "data": "t_stopped" },
                    { "data": "t_idling" },
                    { "data": "t_ofr" },
                    { "data": "t_noGps" },
                    {
                        "data": null,
                        "defaultContent": ""
                    }
                ],
                "order": [
                    {
                        "column": 0,
                        "dir": "asc"
                    }
                ],
                "start": 0,
                "length": this.limit,
                "search": {
                    "value": "",
                    "regex": false
                },
                "op": {},
                "select": [],
                "find": {
                    "user_id": this.userData._id,
                    "devId": vehId,
                    "date": new Date(this.datetimeTo).toISOString()
                }
            };
        }
        this.reportData = [];
        this.loadingController.create({
            message: this.msgString
        }).then(function (loadEl) {
            loadEl.present();
            _this.authService.postMethod(baseUrl, payload)
                .subscribe(function (respData) {
                loadEl.dismiss();
                var data = JSON.parse(JSON.stringify(respData));
                for (var i = 0; i < data.data.length; i++) {
                    // var ignOff = 86400000 - parseInt(data.data[i].today_running);
                    _this.reportData.push({
                        _id: data.data[i]._id,
                        Device_ID: data.data[i].Device_ID ? data.data[i].Device_ID : data.data[i].imei,
                        Device_Name: data.data[i].Device_Name ? data.data[i].Device_Name : (data.data[i].ID ? data.data[i].ID.Device_Name : 'N/A'),
                        maxSpeed: data.data[i].maxSpeed ? data.data[i].maxSpeed : (data.data[i].ID ? data.data[i].ID.maxSpeed : '0'),
                        today_odo: (data.data[i].today_odo).toFixed(2),
                        today_running: _this.millisToMinutesAndSeconds(data.data[i].today_running),
                        today_stopped: _this.millisToMinutesAndSeconds(data.data[i].today_stopped),
                        t_idling: _this.millisToMinutesAndSeconds(data.data[i].t_idling),
                        t_ofr: _this.millisToMinutesAndSeconds(data.data[i].t_ofr),
                        today_trips: data.data[i].today_trips,
                        mileage: data.data[i].Mileage ? ((data.data[i].today_odo) / Number(data.data[i].Mileage)).toFixed(2) : 'N/A'
                        // avgSpeed: this.calcAvgSpeed(odo1[0], data.data[i].today_running)
                    });
                }
                console.log("daily report data: ", respData);
            }, function (error) {
                loadEl.dismiss();
                console.log("error in service=> " + error);
            });
        });
    };
    ReportDetailPage.prototype.millisToMinutesAndSeconds = function (millis) {
        var ms = millis;
        ms = 1000 * Math.round(ms / 1000); // round to nearest second
        var d = new Date(ms);
        // debugger
        var min1;
        var min = d.getUTCMinutes();
        if ((min).toString().length == 1) {
            min1 = '0' + (d.getUTCMinutes()).toString();
        }
        else {
            min1 = min;
        }
        return d.getUTCHours() + ':' + min1;
    };
    ReportDetailPage.prototype.getGlobalreportData = function () {
        var _this = this;
        debugger;
        this.reportData = [];
        var vehId = [];
        console.log("vehid", vehId);
        console.log("vehobj", this.vehObj);
        if (this.vehObj.length === 0) {
            vehId = [];
        }
        else {
            // debugger
            if (this.report.key === 'dist') {
                vehId = this.vehObj._id;
            }
            else {
                if (this.report.key === 'sos') {
                    vehId = this.vehObj.Device_ID;
                }
                else {
                    if (this.report.key === 'stop') {
                        for (var t = 0; t < this.vehObj.length; t++) {
                            vehId.push(this.vehObj[t]._id);
                        }
                    }
                    else {
                        if (this.report.key === 'ospeed') {
                            for (var t = 0; t < this.vehObj.length; t++) {
                                vehId.push(this.vehObj[t].Device_ID);
                            }
                        }
                        else {
                            if (this.report.key === 'ign') {
                                for (var t = 0; t < this.vehObj.length; t++) {
                                    vehId.push(this.vehObj[t].Device_ID);
                                }
                            }
                            else {
                                if (this.report.key === 'ac') {
                                    for (var t = 0; t < this.vehObj.length; t++) {
                                        vehId.push(this.vehObj[t].Device_ID);
                                    }
                                }
                                else {
                                    if (this.report.key === 'daywise') {
                                        for (var t = 0; t < this.vehObj.length; t++) {
                                            vehId.push(this.vehObj[t].Device_ID);
                                        }
                                    }
                                    else {
                                        for (var t = 0; t < this.vehObj.length; t++) {
                                            vehId.push(this.vehObj[t]._id);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        this.loadingController.create({
            message: this.msgString
        }).then(function (loadEl) {
            loadEl.present();
            var url;
            if (_this.report.key === 'summ') {
                url = _this.constUrl.mainUrl + "summary/summaryReport?from=" +
                    new Date(_this.datetimeFrom).toISOString() + '&to=' + new Date(_this.datetimeTo).toISOString() + '&user=' + _this.userData._id + '&device=' + vehId;
            }
            else if (_this.report.key === 'ospeed') {
                url = _this.constUrl.mainUrl + "notifs/overSpeedReport?from_date=" +
                    new Date(_this.datetimeFrom).toISOString() + '&to_date=' + new Date(_this.datetimeTo).toISOString() + '&_u=' + _this.userData._id + '&device=' + vehId;
            }
            else if (_this.report.key === 'daywise') {
                url = _this.constUrl.mainUrl + 'summary/getDayWiseReport?from=' +
                    new Date(_this.datetimeFrom).toString() + '&to=' + new Date(_this.datetimeTo).toString() + '&user=' + _this.userData._id + '&device=' + vehId;
            }
            else if (_this.report.key === 'dist') {
                url = _this.constUrl.mainUrl + "summary/distance?from=" + new Date(_this.datetimeFrom).toISOString() + '&to=' + new Date(_this.datetimeTo).toISOString() + '&user=' + _this.userData._id + '&device=' + vehId;
            }
            else if (_this.report.key === 'trip') {
                url = _this.constUrl.mainUrl + "user_trip/trip_detail?from_date=" + new Date(_this.datetimeFrom).toISOString() + "&to_date=" + new Date(_this.datetimeTo).toISOString() + "&uId=" + _this.userData._id + "&device=" + vehId;
            }
            else if (_this.report.key === 'ign') {
                url = _this.constUrl.mainUrl + "notifs/ignitionReport?from_date=" + new Date(_this.datetimeFrom).toISOString() + "&to_date=" + new Date(_this.datetimeTo).toISOString() + "&_u=" + _this.userData._id + "&device=" + vehId;
            }
            else if (_this.report.key === 'sos') {
                url = _this.constUrl.mainUrl + "notifs/statusReport";
            }
            else if (_this.report.key === 'stop') {
                url = _this.constUrl.mainUrl + 'stoppage/stoppageReport?from_date=' +
                    new Date(_this.datetimeFrom).toISOString() + '&to_date=' + new Date(_this.datetimeTo).toISOString() +
                    '&_u=' + _this.userData._id + '&vname=' + vehId;
            }
            else if (_this.report.key === 'ac') {
                url = _this.constUrl.mainUrl + 'notifs/ACSwitchReport?from_date=' +
                    new Date(_this.datetimeFrom).toISOString() + '&to_date=' + new Date(_this.datetimeTo).toISOString() +
                    '&user=' + _this.userData._id + '&device=' + vehId;
            }
            if (_this.report.key !== 'sos') {
                _this.func1(loadEl, url);
            }
            else {
                _this.func2(loadEl, url, vehId);
            }
        });
    };
    ReportDetailPage.prototype.func1 = function (loadEl, url) {
        var _this = this;
        this.authService.getMethod(url)
            .subscribe(function (respData) {
            loadEl.dismiss();
            console.log(" report data: ", respData);
            var data = JSON.parse(JSON.stringify(respData));
            if (data.length > 0) {
                if (_this.showString) {
                    _this.showString = undefined;
                }
                // debugger
                if (_this.report.key === 'summ') {
                    _this.summ(data);
                }
                else if (_this.report.key === 'ospeed') {
                    _this.ovspeed(data);
                }
                else if (_this.report.key === 'daywise') {
                    _this.daywise(data);
                }
                else if (_this.report.key === 'dist') {
                    _this.distanceRep(data);
                }
                else if (_this.report.key === 'trip') {
                    _this.tripRep(data);
                }
                else if (_this.report.key === 'ign') {
                    _this.ignRep(data);
                }
                else if (_this.report.key === 'sos') {
                    _this.sosRep(data);
                }
                else if (_this.report.key === 'stop') {
                    _this.stoppage(data);
                }
                else if (_this.report.key == 'ac') {
                    _this.acRep(data);
                }
            }
            else {
                _this.showString = "Oops.. report data not found..!";
            }
        }, function (err) {
            loadEl.dismiss();
        });
    };
    ReportDetailPage.prototype.func2 = function (loadEl, url, vehId) {
        var _this = this;
        // debugger
        var payload = {};
        if (this.vehObj.length === 0) {
            payload = {
                "draw": 3,
                "columns": [
                    {
                        "data": "_id"
                    },
                    {
                        "data": "device"
                    },
                    {
                        "data": "vehicleName"
                    },
                    {
                        "data": "timestamp"
                    },
                    {
                        "data": "address"
                    }
                ],
                "order": [
                    {
                        "column": 0,
                        "dir": "asc"
                    }
                ],
                "start": this.pageNo,
                "length": 10,
                "search": {
                    "value": "",
                    "regex": false
                },
                "find": {
                    "user": this.userData._id,
                    "type": "SOS",
                    "timestamp": {
                        "$gte": new Date(this.datetimeFrom).toISOString(),
                        "$lte": new Date(this.datetimeTo).toISOString()
                    }
                }
            };
        }
        else {
            payload = {
                "draw": 3,
                "columns": [
                    {
                        "data": "_id"
                    },
                    {
                        "data": "device"
                    },
                    {
                        "data": "vehicleName"
                    },
                    {
                        "data": "timestamp"
                    },
                    {
                        "data": "address"
                    }
                ],
                "order": [
                    {
                        "column": 0,
                        "dir": "asc"
                    }
                ],
                "start": this.pageNo,
                "length": 10,
                "search": {
                    "value": "",
                    "regex": false
                },
                "find": {
                    "user": this.userData._id,
                    "type": "SOS",
                    "device": vehId,
                    "timestamp": {
                        "$gte": new Date(this.datetimeFrom).toISOString(),
                        "$lte": new Date(this.datetimeTo).toISOString()
                    }
                }
            };
        }
        this.authService.postMethod(url, payload)
            .subscribe(function (respData) {
            loadEl.dismiss();
            console.log(" report data: ", respData);
            var data = JSON.parse(JSON.stringify(respData));
            if (data.data.length > 0) {
                if (_this.showString) {
                    _this.showString = undefined;
                }
                // debugger
                if (_this.report.key === 'sos') {
                    _this.sosRep(data.data);
                }
            }
            else {
                _this.showString = "Oops.. report data not found..!";
            }
        }, function (err) {
            loadEl.dismiss();
        });
    };
    ReportDetailPage.prototype.acRep = function (data) {
        console.log("AcReportData", data);
        var that = this;
        var i = 0, howManyTimes = data.length;
        function f() {
            that.reportData.push({
                'Device_Name': data[i].VehicleName,
                'Time_First': that.timeConvert(data[i]['1stTime']),
                'Time_Second': that.timeConvert(data[i]['2stTime']),
                'Action_First': data[i]['1stAction'],
                'Action_Second': data[i]['2stAction']
            });
            console.log("see push data", that.reportData);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    ReportDetailPage.prototype.timeConvert = function (min) {
        var num = min;
        var hours = (num / 60);
        var rhours = Math.floor(hours);
        var minutes = (hours - rhours) * 60;
        var rminutes = Math.round(minutes);
        return rhours + " hour " + rminutes + " minute";
    };
    ReportDetailPage.prototype.ovspeed = function (data) {
        var that = this;
        var i = 0, howManyTimes = data.length;
        function f() {
            that.reportData.push({
                'Device_Name': data[i].vehicleName,
                'overSpeed': data[i].overSpeed,
                'start_location': {
                    'lat': data[i].lat,
                    'long': data[i].long
                },
                'timestamp': data[i].timestamp
            });
            that.start_address(that.reportData[i], i);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    ReportDetailPage.prototype.daywise = function (data) {
        var outerthis = this;
        var i = 0, howManyTimes = data.length;
        function f() {
            outerthis.reportData.push({
                // 'Date': new Date(daywiseReport[i].Date),
                'Date': moment__WEBPACK_IMPORTED_MODULE_7__(data[i].Date).format('ddd MMM DD YYYY'),
                'VRN': data[i].VRN,
                'end_location': data[i].end_location,
                'start_location': data[i].start_location,
                'Distance(Kms)': data[i]["Distance(Kms)"],
                'Moving Time': data[i]['Moving Time'],
                'Stoppage Time': data[i]['Stoppage Time'],
                'Idle Time': data[i]['Idle Time']
            });
            outerthis.start_address(outerthis.reportData[i], i);
            outerthis.end_address(outerthis.reportData[i], i);
            console.log("see data here: ", outerthis.reportData);
            console.log("address", outerthis.reportData.start_location);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    ReportDetailPage.prototype.stoppage = function (data) {
        var outerthis = this;
        outerthis.locationEndAddress = undefined;
        var i = 0, howManyTimes = data.length;
        function f() {
            var fd = new Date(data[i].arrival_time).getTime();
            var td = new Date(data[i].departure_time).getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60;
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            var Durations = rhours + 'hrs : ' + rminutes + 'mins';
            outerthis.reportData.push({
                'arrival_time': data[i].arrival_time,
                'departure_time': data[i].departure_time,
                'Durations': Durations,
                'device': data[i].device ? data[i].device.Device_Name : 'N/A',
                'end_location': {
                    'lat': data[i].lat,
                    'long': data[i].long
                }
            });
            outerthis.end_address(outerthis.reportData[i], i);
            console.log("see StopageData here: ", outerthis.reportData);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    ReportDetailPage.prototype.summ = function (data) {
        var that = this;
        var i = 0, howManyTimes = data.length;
        function f() {
            that.reportData.push({
                'Device_Name': data[i].devObj[0].Device_Name,
                'routeViolations': data[i].today_routeViolations,
                'overspeeds': that.millisToMinutesAndSeconds(data[i].today_overspeeds),
                'ignOn': that.millisToMinutesAndSeconds(data[i].today_running),
                'ignOff': that.millisToMinutesAndSeconds(data[i].today_stopped),
                'distance': (data[i].today_odo).toFixed(2),
                'tripCount': data[i].today_trips,
                'mileage': ((data[i].devObj[0].Mileage == null) || (data[i].devObj[0].Mileage == undefined)) ? "NA" : (data[i].today_odo / parseFloat(data[i].devObj[0].Mileage)).toFixed(2),
                'end_location': data[i].end_location,
                'start_location': data[i].start_location,
                't_ofr': that.millisToMinutesAndSeconds(data[i].t_ofr),
                't_idling': that.millisToMinutesAndSeconds(data[i].t_idling)
            });
            that.start_address(that.reportData[i], i);
            that.end_address(that.reportData[i], i);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    ReportDetailPage.prototype.distanceRep = function (data) {
        var outerthis = this;
        var i = 0, howManyTimes = data.length;
        function f() {
            outerthis.reportData.push({
                'distance': data[i].distance,
                'Device_Name': data[i].device.Device_Name,
                'end_location': {
                    'lat': data[i].endLat,
                    'long': data[i].endLng
                },
                'start_location': {
                    'lat': data[i].startLat,
                    'long': data[i].startLng
                }
            });
            outerthis.start_address(outerthis.reportData[i], i);
            outerthis.end_address(outerthis.reportData[i], i);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    ReportDetailPage.prototype.tripRep = function (data) {
        var that = this;
        var i = 0, howManyTimes = data.length;
        function f() {
            // var deviceId = data[i]._id;
            var distanceBt = data[i].distance / 1000;
            var gmtDateTime = moment__WEBPACK_IMPORTED_MODULE_7__["utc"](JSON.stringify(data[i].start_time).split('T')[1].split('.')[0], "HH:mm:ss");
            var gmtDate = moment__WEBPACK_IMPORTED_MODULE_7__["utc"](JSON.stringify(data[i].start_time).slice(0, -1).split('T'), "YYYY-MM-DD");
            var Startetime = gmtDateTime.local().format(' h:mm a');
            var Startdate = gmtDate.format('ll');
            var gmtDateTime1 = moment__WEBPACK_IMPORTED_MODULE_7__["utc"](JSON.stringify(data[i].end_time).split('T')[1].split('.')[0], "HH:mm:ss");
            var gmtDate1 = moment__WEBPACK_IMPORTED_MODULE_7__["utc"](JSON.stringify(data[i].end_time).slice(0, -1).split('T'), "YYYY-MM-DD");
            var Endtime = gmtDateTime1.local().format(' h:mm a');
            var Enddate = gmtDate1.format('ll');
            var startDate = new Date(data[i].start_time).toLocaleString();
            var endDate = new Date(data[i].end_time).toLocaleString();
            var fd = new Date(startDate).getTime();
            var td = new Date(endDate).getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60;
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            var Durations = rhours + 'hrs' + ' ' + rminutes + 'mins';
            that.reportData.push({
                'Device_Name': data[i].device.Device_Name,
                'Device_ID': data[i].device.Device_ID,
                'Startetime': Startetime,
                'Startdate': Startdate,
                'Endtime': Endtime,
                'Enddate': Enddate,
                'distance': distanceBt,
                '_id': data[i]._id,
                'start_time': data[i].start_time,
                'end_time': data[i].end_time,
                'duration': Durations,
                'end_location': {
                    'lat': data[i].end_lat,
                    'long': data[i].end_long
                },
                'start_location': {
                    'lat': data[i].start_lat,
                    'long': data[i].start_long
                }
            });
            that.start_address(that.reportData[i], i);
            that.end_address(that.reportData[i], i);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    ReportDetailPage.prototype.ignRep = function (data) {
        var outerthis = this;
        var i = 0, howManyTimes = data.length;
        function f() {
            outerthis.reportData.push({
                'Device_Name': data[i].vehicleName,
                'switch': data[i].switch,
                'timestamp': data[i].timestamp,
                'start_location': {
                    'lat': data[i].lat,
                    'long': data[i].long
                }
            });
            outerthis.start_address(outerthis.reportData[i], i);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    ReportDetailPage.prototype.sosRep = function (data) {
        for (var i = 0; i < data.length; i++) {
            this.reportData.push(data[i]);
        }
        // this.reportData.push(data)
    };
    ReportDetailPage.prototype.start_address = function (item, index) {
        // debugger
        var that = this;
        if (!item.start_location) {
            that.reportData[index].StartLocation = "N/A";
            return;
        }
        var tempcord = {
            "lat": item.start_location.lat,
            "long": item.start_location.long
        };
        this.appService.getAddress(tempcord)
            .subscribe(function (resp) {
            // console.log("test");
            // console.log("result", resp);
            // if(resp !== undefined) {
            // }
            var res = JSON.parse(JSON.stringify(resp));
            console.log("result " + res.address);
            // that.allDevices[index].address = res.address;
            if (res.message == "Address not found in databse") {
                that.reportData[index].StartLocation = 'N/A';
                // this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.long)
                //   .then(res => {
                //     var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                //     that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
                //     that.distanceReportData[index].StartLocation = str;
                //     // console.log("inside", that.address);
                //   })
            }
            else {
                if (res.address !== undefined) {
                    that.reportData[index].StartLocation = res.address;
                }
                else {
                    that.reportData[index].StartLocation = 'N/A';
                }
                // that.reportData[index].StartLocation = res.address;
            }
        });
    };
    ReportDetailPage.prototype.end_address = function (item, index) {
        var that = this;
        if (!item.end_location) {
            that.reportData[index].EndLocation = "N/A";
            return;
        }
        var tempcord = {
            "lat": item.end_location.lat,
            "long": item.end_location.long
        };
        this.appService.getAddress(tempcord)
            .subscribe(function (resp) {
            // console.log("test");
            var res = JSON.parse(JSON.stringify(resp));
            console.log("endlocation result", res.address);
            // console.log(res.address);
            // that.allDevices[index].address = res.address;
            if (res.message == "Address not found in databse") {
                that.reportData[index].EndLocation = 'N/A';
                // this.geocoderApi.reverseGeocode(item.end_location.lat, item.end_location.long)
                //   .then(res => {
                //     var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                //     that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
                //     that.distanceReportData[index].EndLocation = str;
                //     // console.log("inside", that.address);
                //   })
            }
            else {
                if (res.address !== undefined) {
                    that.reportData[index].EndLocation = res.address;
                }
                else {
                    that.reportData[index].EndLocation = 'N/A';
                }
            }
        });
    };
    ReportDetailPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
        { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"] },
        { type: src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
        { type: src_app_app_model__WEBPACK_IMPORTED_MODULE_6__["URLs"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] }
    ]; };
    ReportDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-report-detail',
            template: __webpack_require__(/*! raw-loader!./report-detail.page.html */ "./node_modules/raw-loader/index.js!./src/app/report/report-detail/report-detail.page.html"),
            styles: [__webpack_require__(/*! ./report-detail.page.scss */ "./src/app/report/report-detail/report-detail.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
            src_app_app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"],
            src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
            src_app_app_model__WEBPACK_IMPORTED_MODULE_6__["URLs"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]])
    ], ReportDetailPage);
    return ReportDetailPage;
}());



/***/ })

}]);
//# sourceMappingURL=report-report-detail-report-detail-module-es5.js.map
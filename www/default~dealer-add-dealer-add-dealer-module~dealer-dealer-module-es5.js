(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~dealer-add-dealer-add-dealer-module~dealer-dealer-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dealer/add-dealer/add-dealer.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dealer/add-dealer/add-dealer.page.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>{{titleString}}</ion-title>\n    <ion-buttons slot=\"primary\">\n      <ion-button (click)=\"onCancle()\">\n        <ion-icon name=\"close\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col style=\"padding: 0px 5px;\">\n        <ion-list>\n          <ion-item>\n            <ion-label position=\"floating\">Enter User ID *</ion-label>\n            <ion-input type=\"text\" [(ngModel)]=\"userId\" autocomplete autocorrect></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Enter First Name *</ion-label>\n            <ion-input type=\"text\" [(ngModel)]=\"fName\" autocomplete autocorrect></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Enter Last Name *</ion-label>\n            <ion-input type=\"text\" [(ngModel)]=\"lName\" autocomplete autocorrect></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Enter Email ID</ion-label>\n            <ion-input type=\"email\" [(ngModel)]=\"eMail\"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Enter Password *</ion-label>\n            <ion-input *ngIf=\"!show\" type=\"password\" [(ngModel)]=\"pass\"></ion-input>\n            <ion-input *ngIf=\"show\" type=\"text\" [(ngModel)]=\"pass\"></ion-input>\n            <ion-icon *ngIf=\"!show\" (click)=\"show = !show\" style=\"margin: auto;\" slot=\"end\" name=\"eye-off\"></ion-icon>\n            <ion-icon *ngIf=\"show\" (click)=\"show = !show\" style=\"margin: auto;\" slot=\"end\" name=\"eye\"></ion-icon>\n          </ion-item>\n          <ion-item *ngIf=\"screenKey === 'add'\">\n            <ion-label position=\"floating\">Confirm Password *</ion-label>\n            <ion-input *ngIf=\"!show1\" type=\"password\" [(ngModel)]=\"cpass\"></ion-input>\n            <ion-input *ngIf=\"show1\" type=\"text\" [(ngModel)]=\"cpass\"></ion-input>\n            <ion-icon *ngIf=\"!show1\" (click)=\"show1 = !show1\" style=\"margin: auto;\" slot=\"end\" name=\"eye-off\">\n            </ion-icon>\n            <ion-icon *ngIf=\"show1\" (click)=\"show1 = !show1\" style=\"margin: auto;\" slot=\"end\" name=\"eye\"></ion-icon>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Enter Mobile Number</ion-label>\n            <ion-input type=\"number\" [(ngModel)]=\"mobNumber\"></ion-input>\n          </ion-item>\n          <ion-item *ngIf=\"screenKey === 'add'\">\n            <ion-label position=\"floating\">Select Dealer</ion-label>\n            <!-- <ion-input type=\"text\" [(ngModel)]=\"userId\" autocomplete autocorrect></ion-input> -->\n            <ion-select interface=\"popover\" [(ngModel)]=\"selectedDealer\">\n              <ion-select-option *ngFor=\"let dealer of dealersData\" [value]=\"dealer.dealer_id\">\n                {{dealer.dealer_firstname | titlecase}} {{dealer.dealer_lastname | titlecase}}</ion-select-option>\n            </ion-select>\n          </ion-item>\n          <p style=\"color: #fc9d03; font-size: 0.65em;padding-left: 16px;\">{{msgString}}</p>\n          <ion-item *ngIf=\"screenKey !== 'add'\">\n            <ion-label position=\"floating\">Expiry Date</ion-label>\n            <ion-input type=\"text\" [(ngModel)]=\"expDate\" *ngIf=\"!dateShow\"></ion-input>\n            <ion-icon name=\"close\" slot=\"end\" *ngIf=\"!dateShow\" (click)=\"dateShow = !dateShow\"></ion-icon>\n            <ion-input type=\"date\" [(ngModel)]=\"exDate\" *ngIf=\"dateShow\"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Enter Address</ion-label>\n            <ion-textarea rows=\"2\" [(ngModel)]=\"address\"></ion-textarea>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf=\"screenKey === 'add'\">\n      <ion-col size=\"6\" style=\"padding: 0px 5px;\">\n        <ion-item>\n          <ion-label position=\"floating\">Doc Type</ion-label>\n          <ion-select interface=\"popover\" okText=\"Okay\" cancelText=\"Dismiss\" [(ngModel)]=\"docType\">\n            <ion-select-option value=\"adharCard\">Adhar Card</ion-select-option>\n            <ion-select-option value=\"voterId\">Voter Id</ion-select-option>\n            <ion-select-option value=\"PANCard\">PAN Card</ion-select-option>\n            <ion-select-option value=\"DriveLicence\">Driving Licence</ion-select-option>\n          </ion-select>\n          <!-- <ion-input type=\"text\"  autocomplete autocorrect></ion-input> -->\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"6\" style=\"padding: 0px 5px;\">\n        <ion-item>\n          <ion-label position=\"floating\">Doc Number</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"docNumber\" autocomplete autocorrect></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf=\"screenKey === 'add'\">\n      <ion-col size=\"6\" style=\"padding: 0px 5px;\">\n        <ion-item>\n          <ion-button fill=\"outline\" expand=\"block\">Choose File</ion-button>\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"6\" style=\"padding: 0px 5px;\">\n        <ion-item>\n          <ion-label>No File Choosen</ion-label>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n<ion-footer style=\"padding: 0px 16px 0px 16px;\">\n  <ion-toolbar>\n    <ion-button expand=\"block\" (click)=\"onSubmit()\">Submit</ion-button>\n  </ion-toolbar>\n</ion-footer>"

/***/ }),

/***/ "./src/app/tabs/dealer/add-dealer/add-dealer.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/tabs/dealer/add-dealer/add-dealer.page.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvZGVhbGVyL2FkZC1kZWFsZXIvYWRkLWRlYWxlci5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/tabs/dealer/add-dealer/add-dealer.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/tabs/dealer/add-dealer/add-dealer.page.ts ***!
  \***********************************************************/
/*! exports provided: AddDealerPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDealerPage", function() { return AddDealerPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_app_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/app.model */ "./src/app/app.model.ts");
/* harmony import */ var src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);






var AddDealerPage = /** @class */ (function () {
    function AddDealerPage(modalCtrl, loadingController, consURL, navParams, toastController, authService) {
        this.modalCtrl = modalCtrl;
        this.loadingController = loadingController;
        this.consURL = consURL;
        this.navParams = navParams;
        this.toastController = toastController;
        this.authService = authService;
        this.msgString = "";
        this.dealersData = [];
        this.show = false;
        this.show1 = false;
        this.dateShow = false;
        this.dealerData = {};
    }
    AddDealerPage.prototype.ngOnInit = function () {
        this.userData = this.navParams.get('params');
        this.screenKey = this.navParams.get('key');
        if (this.screenKey === 'add') {
            this.titleString = "Add Dealer";
            console.log('Navparams get: ', this.navParams.get('params'));
            // this.getDealerList();
        }
        else if (this.screenKey === 'edit') {
            this.dealerData = this.navParams.get('dealerData');
            this.titleString = "Edit " + this.dealerData.first_name + "'s info";
            console.log("dealer's data: ", this.dealerData);
            console.log(moment__WEBPACK_IMPORTED_MODULE_5__(new Date(this.dealerData.expiration_date), 'DD-MM-YYYY').format('MM/DD/YYYY'));
            this.fillUpForm(this.dealerData);
        }
    };
    AddDealerPage.prototype.fillUpForm = function (data) {
        this.userId = (data.user_id ? data.user_id : null);
        this.fName = (data.first_name ? data.first_name : null);
        this.lName = (data.last_name ? data.last_name : null);
        this.pass = (data.pass ? data.pass : null);
        // this.cpass = (data.pass ? data.pass : null);
        this.mobNumber = (data.phone ? data.phone : null);
        this.eMail = (data.email ? data.email : null);
        // this.docType = ((data.docObject.length > 0) ? data.docObject[0].doctype : null);
        // this.docNumber = ((data.docObject.length > 0) ? data.docObject[0].phone : null);
        this.address = (data.address ? data.address : null);
        this.expDate = (data.expire_date ? moment__WEBPACK_IMPORTED_MODULE_5__(new Date(data.expire_date), 'DD-MM-YYYY').format('DD/MM/YYYY hh:mm a') : moment__WEBPACK_IMPORTED_MODULE_5__(new Date(this.getExpDate()), 'DD-MM-YYYY').format('DD/MM/YYYY hh:mm a'));
    };
    AddDealerPage.prototype.onCancle = function () {
        this.modalCtrl.dismiss();
    };
    AddDealerPage.prototype.onSubmit = function () {
        var _this = this;
        if (this.screenKey === 'add') {
            this.loadingController.create({
                message: "Please wait... we are adding dealer...",
            }).then(function (loadEl) {
                loadEl.present();
                _this.addDealer(loadEl);
            });
        }
        else if (this.screenKey === 'edit') {
            this.editDealer();
        }
    };
    AddDealerPage.prototype.editDealer = function () {
        var _this = this;
        debugger;
        var payLoad = {};
        var url = this.consURL.mainUrl + "users/editUserDetails";
        if (this.exDate == undefined) {
            if (this.dealerData.expire_date) {
                this.exDate = this.dealerData.expire_date;
            }
        }
        else {
            this.exDate = new Date(this.exDate).toISOString();
        }
        payLoad = {
            "contactid": this.dealerData._id,
            "address": this.address,
            "expire_date": this.exDate,
            "first_name": this.fName,
            "last_name": this.lName,
            "status": this.dealerData.status,
            "user_id": this.userId,
            "email": this.eMail,
            "phone": this.mobNumber
        };
        this.loadingController.create({
            message: "Please wait... we are updating dealer...",
        }).then(function (loadEl) {
            loadEl.present();
            _this.authService.postMethod(url, payLoad)
                .subscribe(function (respData) {
                loadEl.dismiss();
                if (respData) {
                    var res = JSON.parse(JSON.stringify(respData));
                    _this.showToast('Dealer updated successfully');
                    _this.onCancle();
                }
            }, function (err) {
                loadEl.dismiss();
                console.log(err);
            });
        });
    };
    AddDealerPage.prototype.dealerOnChnage = function (dealerData) {
        console.log(dealerData);
    };
    AddDealerPage.prototype.showToast = function (msgString) {
        this.toastController.create({
            message: msgString,
            duration: 2000,
            position: 'middle'
        }).then(function (toastEl) {
            toastEl.present();
        });
    };
    AddDealerPage.prototype.getExpDate = function () {
        var currentDate = new Date();
        var futureMonth = moment__WEBPACK_IMPORTED_MODULE_5__(currentDate).add('years', 1).format('L');
        // console.log("futureMonth: " + new Date(futureMonth).toISOString());
        return new Date(futureMonth).toISOString();
    };
    AddDealerPage.prototype.addDealer = function (loadEl) {
        var _this = this;
        var payLoad = {};
        if (this.pass !== this.cpass) {
            loadEl.dismiss();
            this.showToast('Password mismatched!!!');
            return;
        }
        payLoad = {
            "first_name": this.fName,
            "last_name": this.lName,
            "email": this.eMail,
            "password": this.pass,
            "phone": this.mobNumber,
            "expdate": this.getExpDate(),
            "custumer": false,
            "isDealer": true,
            "sysadmin": this.userData.isSuperAdmin,
            "user_id": this.userId,
            "address": this.address,
            "std_code": {
                "countryCode": "in",
                "dialcode": "91"
            },
            "timezone": "Asia/Kolkata",
            "imageDoc": [
                {
                    "doctype": this.docType,
                    "image": "",
                    "phone": this.docNumber
                }
            ]
        };
        if (this.userData.isSuperAdmin) {
            payLoad.supAdmin = this.userData._id;
        }
        else if (this.userData.isDealer) {
            payLoad.supAdmin = this.userData.supAdmin;
        }
        var url = this.consURL.mainUrl + "users/signUp";
        this.authService.postMethod(url, payLoad)
            .subscribe(function (respData) {
            loadEl.dismiss();
            if (respData) {
                var res = JSON.parse(JSON.stringify(respData));
                console.log("added dealer check: ", res);
                if (res.message) {
                    _this.showToast(res.message);
                    if (res.message === 'Registered') {
                        _this.onCancle();
                    }
                }
            }
        }, function (err) {
            console.log(err);
            loadEl.dismiss();
        });
    };
    AddDealerPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: src_app_app_model__WEBPACK_IMPORTED_MODULE_3__["URLs"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
        { type: src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] }
    ]; };
    AddDealerPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-dealer',
            template: __webpack_require__(/*! raw-loader!./add-dealer.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dealer/add-dealer/add-dealer.page.html"),
            styles: [__webpack_require__(/*! ./add-dealer.page.scss */ "./src/app/tabs/dealer/add-dealer/add-dealer.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            src_app_app_model__WEBPACK_IMPORTED_MODULE_3__["URLs"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
            src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], AddDealerPage);
    return AddDealerPage;
}());



/***/ })

}]);
//# sourceMappingURL=default~dealer-add-dealer-add-dealer-module~dealer-dealer-module-es5.js.map
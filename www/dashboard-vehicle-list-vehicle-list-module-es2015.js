(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-vehicle-list-vehicle-list-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/maintabs/tabs/dashboard\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Vehicle List</ion-title>\n  </ion-toolbar>\n  <ion-segment scrollable color=\"secondary\" (ionChange)=\"segmentChanged($event)\" [value]=\"checkIfStat\"\n    style=\"background: floralwhite;\">\n    <ion-segment-button class=\"segClass\" value=\"RUNNING\">\n      <ion-label>RUNNING</ion-label>\n    </ion-segment-button>\n    <ion-segment-button class=\"segClass\" value=\"STOPPED\">\n      <ion-label>STOPPED</ion-label>\n    </ion-segment-button>\n    <ion-segment-button class=\"segClass\" value=\"IDLING\">\n      <ion-label>IDLING</ion-label>\n    </ion-segment-button>\n    <ion-segment-button class=\"segClass\" value=\"OUT OF REACH\">\n      <ion-label>OUT OF REACH</ion-label>\n    </ion-segment-button>\n    <!-- <ion-segment-button class=\"segClass\" value=\"Maintainance\">\n      <ion-label>MAINTAINANCE</ion-label>\n    </ion-segment-button> -->\n    <ion-segment-button class=\"segClass\" value=\"NO GPS FIX\">\n      <ion-label>NO GPS</ion-label>\n    </ion-segment-button>\n    <ion-segment-button class=\"segClass\" value=\"NO DATA\">\n      <ion-label>NO DATA</ion-label>\n    </ion-segment-button>\n    <ion-segment-button class=\"segClass\" value=\"Expired\">\n      <ion-label>EXPIRED</ion-label>\n    </ion-segment-button>\n  </ion-segment>\n</ion-header>\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\" no-padding>\n        <ion-card *ngIf=\"vehicleListData.length === 0\">\n          <ion-card-content>\n            <p>Oops.. Vehicle List not found. :(</p>\n          </ion-card-content>\n        </ion-card>\n        <ion-card *ngFor=\"let d of vehicleListData;\" style=\"padding: 0px; margin: 2px;\">\n          <ion-card-header style=\"padding: 5px;\">\n            <ion-row>\n              <ion-col size=\"3\" *ngIf=\"(d.vehicleType != null || d.vehicleType != undefined) && d.iconType !== null\">\n                <!-- vehicle type car -->\n                <ion-img *ngIf=\"((d.iconType == 'car')&&(d.status=='STOPPED'))\" src=\"assets/imgs/icons/redcar.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf='((d.iconType == \"car\")&&(d.status==\"RUNNING\"))' src=\"assets/imgs/icons/greencar.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'car')&&(d.status=='OUT OF REACH'))\" src=\"assets/imgs/icons/bluecar.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'car')&&(d.status=='IDLING'))\" src=\"assets/imgs/icons/yellowcar.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'car')&&(d.status=='NO GPS FIX'))\" src=\"assets/imgs/icons/graycar.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'car')&&(d.status=='NO DATA'))\" src=\"assets/imgs/icons/graycar.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n                <!-- vehicle type bus -->\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bus')&&(d.status=='STOPPED'))\"\n                  src=\"assets/imgs/icons/redbus.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bus')&&(d.status=='RUNNING'))\"\n                  src=\"assets/imgs/icons/greenbus.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bus')&&(d.status=='OUT OF REACH'))\"\n                  src=\"assets/imgs/icons/bluebus.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bus')&&(d.status=='IDLING'))\"\n                  src=\"assets/imgs/icons/yellowbus.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bus')&&(d.status=='NO GPS FIX'))\"\n                  src=\"assets/imgs/icons/graybus.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bus')&&(d.status=='NO DATA'))\"\n                  src=\"assets/imgs/icons/graybus.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n                <!-- vehicle type truck -->\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'truck')&&(d.status=='STOPPED'))\"\n                  src=\"assets/imgs/icons/redtruck.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'truck')&&(d.status=='RUNNING'))\"\n                  src=\"assets/imgs/icons/greentruck.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'truck')&&(d.status=='OUT OF REACH'))\"\n                  src=\"assets/imgs/icons/bluetruck.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'truck')&&(d.status=='IDLING'))\"\n                  src=\"assets/imgs/icons/yellowtruck.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'truck')&&(d.status=='NO GPS FIX'))\"\n                  src=\"assets/imgs/icons/graytruck.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'truck')&&(d.status=='NO DATA'))\"\n                  src=\"assets/imgs/icons/graytruck.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n                <!-- vehicle type bike -->\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bike')&&(d.status=='STOPPED'))\"\n                  src=\"assets/imgs/icons/redbike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bike')&&(d.status=='RUNNING'))\"\n                  src=\"assets/imgs/icons/greenbike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bike')&&(d.status=='OUT OF REACH'))\"\n                  src=\"assets/imgs/icons/bluebike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bike')&&(d.status=='IDLING'))\"\n                  src=\"assets/imgs/icons/yellowbike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bike')&&(d.status=='NO GPS FIX'))\"\n                  src=\"assets/imgs/icons/graybike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bike')&&(d.status=='NO DATA'))\"\n                  src=\"assets/imgs/icons/graybike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n                <!-- vehicle type tractor -->\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'tractor')&&(d.status=='STOPPED'))\"\n                  src=\"assets/imgs/icons/redtractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'tractor')&&(d.status=='RUNNING'))\"\n                  src=\"assets/imgs/icons/greentractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'tractor')&&(d.status=='OUT OF REACH'))\"\n                  src=\"assets/imgs/icons/bluetractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'tractor')&&(d.status=='IDLING'))\"\n                  src=\"assets/imgs/icons/yellowbike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'tractor')&&(d.status=='NO GPS FIX'))\"\n                  src=\"assets/imgs/icons/graytractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'tractor')&&(d.status=='NO DATA'))\"\n                  src=\"assets/imgs/icons/graytractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n                <!-- vehicle type jcb -->\n                <ion-img *ngIf=\"((d.vehicleType.iconType === 'jcb')&&(d.status === 'STOPPED'))\"\n                  src=\"assets/imgs/icons/redjcb.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType === 'jcb')&&(d.status === 'RUNNING'))\"\n                  src=\"assets/imgs/icons/greenjcb.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType === 'jcb')&&(d.status === 'OUT OF REACH'))\"\n                  src=\"assets/imgs/icons/bluejcb.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType === 'jcb')&&(d.status === 'IDLING'))\"\n                  src=\"assets/imgs/icons/yellowjcb.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType === 'jcb')&&(d.status === 'NO GPS FIX'))\"\n                  src=\"assets/imgs/icons/grayjcb.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType === 'jcb')&&(d.status === 'NO DATA'))\"\n                  src=\"assets/imgs/icons/grayjcb.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n              </ion-col>\n              <ion-col size=\"3\" *ngIf=\"d.vehicleType !== null && d.iconType === null\">\n                <!-- vehicle type car -->\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'car')&&(d.status=='STOPPED'))\"\n                  src=\"assets/imgs/icons/redcar.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'car')&&(d.status=='RUNNING'))\"\n                  src=\"assets/imgs/icons/greencar.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'car')&&(d.status=='OUT OF REACH'))\"\n                  src=\"assets/imgs/icons/bluecar.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'car')&&(d.status=='IDLING'))\"\n                  src=\"assets/imgs/icons/yellowcar.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'car')&&(d.status=='NO GPS FIX'))\"\n                  src=\"assets/imgs/icons/graycar.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'car')&&(d.status=='NO DATA'))\"\n                  src=\"assets/imgs/icons/graycar.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n                <!-- vehicle type bus -->\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bus')&&(d.status=='STOPPED'))\"\n                  src=\"assets/imgs/icons/redbus.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bus')&&(d.status=='RUNNING'))\"\n                  src=\"assets/imgs/icons/greenbus.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bus')&&(d.status=='OUT OF REACH'))\"\n                  src=\"assets/imgs/icons/bluebus.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bus')&&(d.status=='IDLING'))\"\n                  src=\"assets/imgs/icons/yellowbus.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bus')&&(d.status=='NO GPS FIX'))\"\n                  src=\"assets/imgs/icons/graybus.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bus')&&(d.status=='NO DATA'))\"\n                  src=\"assets/imgs/icons/graybus.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n                <!-- vehicle type truck -->\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'truck')&&(d.status=='STOPPED'))\"\n                  src=\"assets/imgs/icons/redtruck.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'truck')&&(d.status=='RUNNING'))\"\n                  src=\"assets/imgs/icons/greentruck.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'truck')&&(d.status=='OUT OF REACH'))\"\n                  src=\"assets/imgs/icons/bluetruck.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'truck')&&(d.status=='IDLING'))\"\n                  src=\"assets/imgs/icons/yellowtruck.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'truck')&&(d.status=='NO GPS FIX'))\"\n                  src=\"assets/imgs/icons/graytruck.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'truck')&&(d.status=='NO DATA'))\"\n                  src=\"assets/imgs/icons/graytruck.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n\n                <!-- vehicle type bike -->\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bike')&&(d.status=='STOPPED'))\"\n                  src=\"assets/imgs/icons/redbike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bike')&&(d.status=='RUNNING'))\"\n                  src=\"assets/imgs/icons/greenbike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bike')&&(d.status=='OUT OF REACH'))\"\n                  src=\"assets/imgs/icons/bluebike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bike')&&(d.status=='IDLING'))\"\n                  src=\"assets/imgs/icons/yellowbike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bike')&&(d.status=='NO GPS FIX'))\"\n                  src=\"assets/imgs/icons/graybike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'bike')&&(d.status=='NO DATA'))\"\n                  src=\"assets/imgs/icons/graybike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n                <!-- vehicle type tractor -->\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'tractor')&&(d.status=='STOPPED'))\"\n                  src=\"assets/imgs/icons/redtractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'tractor')&&(d.status=='RUNNING'))\"\n                  src=\"assets/imgs/icons/greentractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'tractor')&&(d.status=='OUT OF REACH'))\"\n                  src=\"assets/imgs/icons/bluetractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'tractor')&&(d.status=='IDLING'))\"\n                  src=\"assets/imgs/icons/yellowbike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'tractor')&&(d.status=='NO GPS FIX'))\"\n                  src=\"assets/imgs/icons/graytractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'tractor')&&(d.status=='NO DATA'))\"\n                  src=\"assets/imgs/icons/graytractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n                <!-- vehicle type jcb -->\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'jcb')&&(d.status=='STOPPED'))\"\n                  src=\"assets/imgs/icons/redjcb.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'jcb')&&(d.status=='RUNNING'))\"\n                  src=\"assets/imgs/icons/greenjcb.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'jcb')&&(d.status=='OUT OF REACH'))\"\n                  src=\"assets/imgs/icons/bluejcb.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'jcb')&&(d.status=='IDLING'))\"\n                  src=\"assets/imgs/icons/yellowjcb.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'jcb')&&(d.status=='NO GPS FIX'))\"\n                  src=\"assets/imgs/icons/grayjcb.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.vehicleType.iconType == 'jcb')&&(d.status=='NO DATA'))\"\n                  src=\"assets/imgs/icons/grayjcb.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n              </ion-col>\n              <ion-col size=\"3\" *ngIf=\"d.vehicleType === null && d.iconType !== null\">\n                <!-- vehicle type car -->\n                <ion-img *ngIf=\"((d.iconType == 'car')&&(d.status=='STOPPED'))\" src=\"assets/imgs/icons/redcar.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'car')&&(d.status=='RUNNING'))\" src=\"assets/imgs/icons/greencar.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'car')&&(d.status=='OUT OF REACH'))\" src=\"assets/imgs/icons/bluecar.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'car')&&(d.status=='IDLING'))\" src=\"assets/imgs/icons/yellowcar.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'car')&&(d.status=='NO GPS FIX'))\" src=\"assets/imgs/icons/graycar.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'car')&&(d.status=='NO DATA'))\" src=\"assets/imgs/icons/graycar.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n                <!-- vehicle type bus -->\n                <ion-img *ngIf=\"((d.iconType == 'bus')&&(d.status=='STOPPED'))\" src=\"assets/imgs/icons/redbus.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'bus')&&(d.status=='RUNNING'))\" src=\"assets/imgs/icons/greenbus.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'bus')&&(d.status=='OUT OF REACH'))\" src=\"assets/imgs/icons/bluebus.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'bus')&&(d.status=='IDLING'))\" src=\"assets/imgs/icons/yellowbus.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'bus')&&(d.status=='NO GPS FIX'))\" src=\"assets/imgs/icons/graybus.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'bus')&&(d.status=='NO DATA'))\" src=\"assets/imgs/icons/graybus.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n                <!-- vehicle type truck -->\n                <ion-img *ngIf=\"((d.iconType == 'truck')&&(d.status=='STOPPED'))\" src=\"assets/imgs/icons/redtruck.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'truck')&&(d.status=='RUNNING'))\" src=\"assets/imgs/icons/greentruck.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'truck')&&(d.status=='OUT OF REACH'))\"\n                  src=\"assets/imgs/icons/bluetruck.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'truck')&&(d.status=='IDLING'))\" src=\"assets/imgs/icons/yellowtruck.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'truck')&&(d.status=='NO GPS FIX'))\"\n                  src=\"assets/imgs/icons/graytruck.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'truck')&&(d.status=='NO DATA'))\" src=\"assets/imgs/icons/graytruck.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n                <!-- vehicle type bike -->\n                <ion-img *ngIf=\"((d.iconType == 'bike')&&(d.status=='STOPPED'))\" src=\"assets/imgs/icons/redbike.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'bike')&&(d.status=='RUNNING'))\" src=\"assets/imgs/icons/greenbike.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'bike')&&(d.status=='OUT OF REACH'))\"\n                  src=\"assets/imgs/icons/bluebike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'bike')&&(d.status=='IDLING'))\" src=\"assets/imgs/icons/yellowbike.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'bike')&&(d.status=='NO GPS FIX'))\" src=\"assets/imgs/icons/graybike.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'bike')&&(d.status=='NO DATA'))\" src=\"assets/imgs/icons/graybike.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n                <!-- vehicle type tractor -->\n                <ion-img *ngIf=\"((d.iconType == 'tractor')&&(d.status=='STOPPED'))\"\n                  src=\"assets/imgs/icons/redtractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'tractor')&&(d.status=='RUNNING'))\"\n                  src=\"assets/imgs/icons/greentractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'tractor')&&(d.status=='OUT OF REACH'))\"\n                  src=\"assets/imgs/icons/bluetractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'tractor')&&(d.status=='IDLING'))\"\n                  src=\"assets/imgs/icons/yellowbike.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'tractor')&&(d.status=='NO GPS FIX'))\"\n                  src=\"assets/imgs/icons/graytractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'tractor')&&(d.status=='NO DATA'))\"\n                  src=\"assets/imgs/icons/graytractor.png\" style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n                <!-- vehicle type jcb -->\n                <ion-img *ngIf=\"((d.iconType == 'jcb')&&(d.status=='STOPPED'))\" src=\"assets/imgs/icons/redjcb.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'jcb')&&(d.status=='RUNNING'))\" src=\"assets/imgs/icons/greenjcb.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'jcb')&&(d.status=='OUT OF REACH'))\" src=\"assets/imgs/icons/bluejcb.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'jcb')&&(d.status=='IDLING'))\" src=\"assets/imgs/icons/yellowjcb.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'jcb')&&(d.status=='NO GPS FIX'))\" src=\"assets/imgs/icons/grayjcb.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n                <ion-img *ngIf=\"((d.iconType == 'jcb')&&(d.status=='NO DATA'))\" src=\"assets/imgs/icons/grayjcb.png\"\n                  style=\"width: 55px; margin-top: -10px;\"></ion-img>\n\n              </ion-col>\n              <ion-col size=\"9\">\n                <ion-card-title style=\"font-size: 1.25em;\" class=\"ion-no-padding\">{{d.Device_Name}}<ion-badge\n                    float-right *ngIf=\"d.last_speed\">\n                    {{d.last_speed}} km/h</ion-badge>\n                </ion-card-title>\n                <ion-card-subtitle style=\"font-size: 0.7em;\" class=\"ion-no-padding\">{{ d.status }} since&nbsp;{{\n                    d.status_updated_at | date: \"mediumDate\"\n                  }}, {{\n                    d.status_updated_at | date: \"shortTime\"\n                  }}</ion-card-subtitle>\n              </ion-col>\n            </ion-row>\n          </ion-card-header>\n          <ion-card-content no-padding>\n            <ion-row no-padding style=\"padding-top: 5px; background: #f2f2f2; padding-bottom: 5px;\">\n              <ion-col size=\"1\" class=\"ion-no-padding\">\n                <ion-row (click)=\"btnClicked(d, 'live')\">\n                  <ion-col size=\"12\" text-center no-padding>\n                    <ion-icon name=\"pin\" style=\"font-size: 1em; color: mediumseagreen;\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"12\" text-center class=\"ion-no-padding\">\n                    <p style=\"margin: 0px; font-size: 0.7em;\">Live</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col size=\"2\" no-padding>\n                <ion-row (click)=\"btnClicked(d, 'history')\">\n                  <ion-col size=\"12\" text-center no-padding>\n                    <ion-icon name=\"time\" style=\"font-size: 1em; color: tomato;\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"12\" text-center class=\"ion-no-padding\">\n                    <p style=\"margin: 0px; font-size: 0.7em;\">History</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col size=\"1\" no-padding>\n                <ion-row>\n                  <ion-col size=\"12\" text-center no-padding>\n                    <ion-icon name=\"key\" style=\"font-size: 1em;\" color=\"danger\" *ngIf=\"d.last_ACC == '0'\"></ion-icon>\n                    <ion-icon name=\"key\" style=\"font-size: 1em;\" color=\"secondary\" *ngIf=\"d.last_ACC == '1'\"></ion-icon>\n                    <ion-icon name=\"key\" style=\"font-size: 1em;\" *ngIf=\"d.last_ACC == null\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"12\" text-center class=\"ion-no-padding\">\n                    <p style=\"margin: 0px; font-size: 0.7em;\">Ign</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col size=\"2\" no-padding (click)=\"btnClicked(d, 'lock')\">\n                <ion-row>\n                  <ion-col size=\"12\" text-center no-padding>\n                    <ion-icon name=\"lock\" style=\"font-size: 1em;\" color=\"danger\" *ngIf=\"d.ignitionLock == '1'\">\n                    </ion-icon>\n                    <ion-icon name=\"lock\" style=\"font-size: 1em;\" color=\"secondary\" *ngIf=\"d.ignitionLock == '0'\">\n                    </ion-icon>\n                  </ion-col>\n                  <ion-col size=\"12\" text-center class=\"ion-no-padding\">\n                    <p style=\"margin: 0px; font-size: 0.7em;\">Lock</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col size=\"1\" no-padding (click)=\"btnClicked(d, 'tow')\">\n                <ion-row>\n                  <ion-col size=\"12\" text-center no-padding>\n                    <ion-icon name=\"alert\" style=\"font-size: 1em;\" color=\"danger\" *ngIf=\"d.towAlert\"></ion-icon>\n                    <ion-icon name=\"alert\" style=\"font-size: 1em;\" color=\"secondary\" *ngIf=\"!d.towAlert\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"12\" text-center class=\"ion-no-padding\">\n                    <p style=\"margin: 0px; font-size: 0.7em;\">Tow</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col size=\"2\" no-padding>\n                <ion-row>\n                  <ion-col size=\"12\" text-center no-padding>\n                    <ion-icon name=\"power\" style=\"font-size: 1em;\" color=\"danger\" *ngIf=\"d.power != '1'\"></ion-icon>\n                    <ion-icon name=\"power\" style=\"font-size: 1em;\" color=\"secondary\" *ngIf=\"d.power == '1'\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"12\" text-center class=\"ion-no-padding\">\n                    <p style=\"margin: 0px; font-size: 0.7em;\">Power</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col size=\"1\" no-padding (click)=\"btnClicked(d, 'theft')\">\n                <ion-row>\n                  <ion-col size=\"12\" text-center style=\"padding: 0px 0px 5px 0px;\">\n                    <ion-img style=\"width: 15px; margin: auto;\" src=\"assets/imgs/theft.png\" *ngIf=\"d.theftAlert\">\n                    </ion-img>\n                    <ion-img style=\"width: 15px; margin: auto;\" src=\"assets/imgs/antitheft.png\" *ngIf=\"!d.theftAlert\">\n                    </ion-img>\n                  </ion-col>\n                  <ion-col size=\"12\" text-center class=\"ion-no-padding\">\n                    <p style=\"margin: 0px; font-size: 0.7em;\">Theft</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col size=\"2\" no-padding (click)=\"btnClicked(d, 'driver')\">\n                <ion-row>\n                  <ion-col size=\"12\" text-center no-padding>\n                    <ion-icon name=\"call\" style=\"font-size: 1em;\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"12\" text-center class=\"ion-no-padding\">\n                    <p style=\"margin: 0px; font-size: 0.7em;\">Driver</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col size=\"7\" class=\"ion-no-padding\">\n                <ion-row style=\"background: lightgrey; color: black;\">\n                  <ion-col size=\"2\" text-center style=\"color: rgb(8, 187, 88); margin-top: 1px;\">\n                    <ion-icon name=\"pin\" style=\"font-size: 1.8em;\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"10\">\n                    <p style=\"font-size: 0.8em;\">Near new sai temple vinobha nagar tumsar 441912.</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col size=\"5\" class=\"ion-no-padding\" style=\"background: #cd2a00; color: white;\">\n                <ion-row>\n                  <ion-col size=\"12\" text-center>\n                    <p style=\"font-size: 0.8em; font-weight: 500;\">Today's Distance</p>\n                  </ion-col>\n                  <ion-col size=\"12\" no-padding text-center style=\"margin-top: -7px;\">\n                    <p style=\"font-weight: 600;\">{{d.today_odo | number: \"1.0-2\" }} Km(s)</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n            <ion-row style=\"background-color: #000;\n            color: white;\n            font-weight: bold;font-size: 0.6em;\">\n              <ion-col size=\"6\">Licence Purchased On - {{d.created_on | date:'mediumDate'}}</ion-col>\n              <ion-col size=\"6\" style=\"text-align: right;\">Licence Expired On -\n                {{d.expiration_date | date:'mediumDate'}}\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n        <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event)\">\n          <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more vehicles...\">\n          </ion-infinite-scroll-content>\n        </ion-infinite-scroll>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/vehicle-list.module.ts ***!
  \********************************************************************/
/*! exports provided: VehicleListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehicleListPageModule", function() { return VehicleListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _vehicle_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./vehicle-list.page */ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.ts");







const routes = [
    {
        path: '',
        component: _vehicle_list_page__WEBPACK_IMPORTED_MODULE_6__["VehicleListPage"]
    }
];
let VehicleListPageModule = class VehicleListPageModule {
};
VehicleListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_vehicle_list_page__WEBPACK_IMPORTED_MODULE_6__["VehicleListPage"]]
    })
], VehicleListPageModule);



/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".ios ion-segment-button {\n  padding: 7px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL2lvbmljNC9vbmVxbGlrL3NyYy9hcHAvdGFicy9kYXNoYm9hcmQvdmVoaWNsZS1saXN0L3ZlaGljbGUtbGlzdC5wYWdlLnNjc3MiLCJzcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3ZlaGljbGUtbGlzdC92ZWhpY2xlLWxpc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksWUFBQTtBQ0FSIiwiZmlsZSI6InNyYy9hcHAvdGFicy9kYXNoYm9hcmQvdmVoaWNsZS1saXN0L3ZlaGljbGUtbGlzdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW9zIHtcbiAgICBpb24tc2VnbWVudC1idXR0b257IFxuICAgICAgICBwYWRkaW5nOiA3cHg7XG4gICAgfVxufSIsIi5pb3MgaW9uLXNlZ21lbnQtYnV0dG9uIHtcbiAgcGFkZGluZzogN3B4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.ts ***!
  \******************************************************************/
/*! exports provided: VehicleListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehicleListPage", function() { return VehicleListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var src_app_app_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/app.model */ "./src/app/app.model.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");







let VehicleListPage = class VehicleListPage {
    constructor(route, router, authService, constURL, loadingController) {
        this.route = route;
        this.router = router;
        this.authService = authService;
        this.constURL = constURL;
        this.loadingController = loadingController;
        this.userData = {};
        this.checkIfStat = 'RUNNING';
        this.pageNo = 0;
        this.limit = 6;
        this.vehicleListData = [];
        this.getToken();
        // debugger
        this.route.queryParams.subscribe(params => {
            if (params && params.special) {
                var data = JSON.parse(params.special);
                this.checkIfStat = data.label;
                console.log(data);
            }
        });
    }
    ngOnInit() { }
    segmentChanged(ev) {
        console.log('Segment changed', ev.detail.value);
        this.checkIfStat = ev.detail.value;
        this.scrollEv = undefined;
        this.laodingEl = undefined;
        this.pageNo = 0;
        this.vehicleListData = [];
        this.getVehicleList();
    }
    getToken() {
        this.authService.getTokenData().subscribe(data => {
            this.userData = data;
            console.log("vehicle data=> ", this.userData);
            this.getVehicleList();
        });
    }
    ionViewWillLoad() {
    }
    getVehicleList() {
        if (this.scrollEv) {
            this.getData();
        }
        else {
            this.loadingController.create({
                message: "Loading vehciles..."
            }).then(loadEl => {
                loadEl.present();
                this.laodingEl = loadEl;
                this.pageNo = 0;
                this.vehicleListData = [];
                this.getData();
            });
        }
    }
    getData() {
        var baseURLp;
        if (this.checkIfStat) {
            baseURLp = this.constURL.mainUrl + 'devices/getDeviceByUser?id=' + this.userData._id + '&email=' + this.userData.email + '&statuss=' + this.checkIfStat + '&skip=' + this.pageNo + '&limit=' + this.limit;
        }
        else {
            baseURLp = this.constURL.mainUrl + 'devices/getDeviceByUser?id=' + this.userData._id + '&email=' + this.userData.email + '&skip=' + this.pageNo + '&limit=' + this.limit;
        }
        if (this.userData.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.userData._id;
        }
        else {
            if (this.userData.isDealer == true) {
                baseURLp += '&dealer=' + this.userData._id;
            }
        }
        this.authService.getMethod(baseURLp)
            .subscribe(resData => {
            if (this.laodingEl) {
                this.laodingEl.dismiss();
            }
            let parsedData;
            if (this.scrollEv) {
                this.scrollEv.target.complete();
                this.scrollEv.target.disable = true;
                parsedData = JSON.parse(JSON.stringify(resData));
                if (parsedData.devices.length === 0) {
                    return;
                }
                for (let i = 0; i < parsedData.devices.length; i++) {
                    this.vehicleListData.push(parsedData.devices[i]);
                }
            }
            else {
                if (this.laodingEl) {
                    this.laodingEl.dismiss();
                }
                parsedData = JSON.parse(JSON.stringify(resData));
                if (parsedData.devices.length === 0) {
                    return;
                }
                this.vehicleListData = parsedData.devices;
            }
        }, err => {
            console.log("error=> ", err);
            if (this.laodingEl) {
                this.laodingEl.dismiss();
            }
            if (this.scrollEv) {
                this.scrollEv.target.complete();
                this.scrollEv.target.disable = true;
            }
        });
    }
    loadData(event) {
        this.scrollEv = undefined;
        this.laodingEl = undefined;
        this.scrollEv = event;
        this.pageNo = this.pageNo + 1;
        this.getVehicleList();
    }
    btnClicked(d, key) {
        debugger;
        if (key === 'live') {
            this.router.navigateByUrl('/maintabs/tabs/dashboard/common/' + d.Device_ID + '/' + key);
        }
        else if (key === 'history') {
            // this.router.navigateByUrl('/maintabs/tabs/dashboard/common/' + d.Device_ID + '/' + key);
            this.router.navigateByUrl('/maintabs/tabs/dashboard/history/' + d.Device_ID + '/' + key);
        }
        else if (key === 'lock') {
        }
        else if (key === 'theft') {
        }
        else if (key === 'tow') {
        }
        else if (key === 'driver') {
        }
    }
};
VehicleListPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: src_app_app_model__WEBPACK_IMPORTED_MODULE_4__["URLs"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"], {
        static: true
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"])
], VehicleListPage.prototype, "infiniteScroll", void 0);
VehicleListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-vehicle-list',
        template: __webpack_require__(/*! raw-loader!./vehicle-list.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.html"),
        styles: [__webpack_require__(/*! ./vehicle-list.page.scss */ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
        _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
        src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
        src_app_app_model__WEBPACK_IMPORTED_MODULE_4__["URLs"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
], VehicleListPage);



/***/ })

}]);
//# sourceMappingURL=dashboard-vehicle-list-vehicle-list-module-es2015.js.map
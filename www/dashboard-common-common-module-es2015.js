(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-common-common-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/common/common.page.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/common/common.page.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/maintabs/tabs/dashboard/vehicle-list\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      Common\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content fullscreen>\n  <app-google-map *ngIf=\"deviceObj\" [passedObj]=\"deviceObj\" [key]=\"screenKey\"></app-google-map>\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/common/common.module.ts":
/*!********************************************************!*\
  !*** ./src/app/tabs/dashboard/common/common.module.ts ***!
  \********************************************************/
/*! exports provided: CommonPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonPageModule", function() { return CommonPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _common_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./common.page */ "./src/app/tabs/dashboard/common/common.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/tabs/components/components.module.ts");








const routes = [
    {
        path: '',
        component: _common_page__WEBPACK_IMPORTED_MODULE_6__["CommonPage"]
    }
];
let CommonPageModule = class CommonPageModule {
};
CommonPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_common_page__WEBPACK_IMPORTED_MODULE_6__["CommonPage"]]
    })
], CommonPageModule);



/***/ }),

/***/ "./src/app/tabs/dashboard/common/common.page.scss":
/*!********************************************************!*\
  !*** ./src/app/tabs/dashboard/common/common.page.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-toolbar {\n  --background-color: transparent;\n  --ion-color-base: transparent !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL2lvbmljNC9vbmVxbGlrL3NyYy9hcHAvdGFicy9kYXNoYm9hcmQvY29tbW9uL2NvbW1vbi5wYWdlLnNjc3MiLCJzcmMvYXBwL3RhYnMvZGFzaGJvYXJkL2NvbW1vbi9jb21tb24ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsK0JBQUE7RUFDQSx3Q0FBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvdGFicy9kYXNoYm9hcmQvY29tbW9uL2NvbW1vbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIC0taW9uLWNvbG9yLWJhc2U6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG59XG4iLCJpb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIC0taW9uLWNvbG9yLWJhc2U6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/tabs/dashboard/common/common.page.ts":
/*!******************************************************!*\
  !*** ./src/app/tabs/dashboard/common/common.page.ts ***!
  \******************************************************/
/*! exports provided: CommonPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonPage", function() { return CommonPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var src_app_app_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/app.model */ "./src/app/app.model.ts");






let CommonPage = class CommonPage {
    constructor(route, navCtrl, authService, constUrl, loadingController) {
        this.route = route;
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.constUrl = constUrl;
        this.loadingController = loadingController;
        this.getData();
    }
    ngOnInit() {
    }
    getData() {
        this.route.paramMap.subscribe((paramMap) => {
            if (!paramMap.has('Device_ID') && !paramMap.has('key')) {
                this.navCtrl.navigateBack('/maintabs/tabs/dashboard/vehicle-list');
                return;
            }
            let url = this.constUrl.mainUrl + "devices/getDevicebyId?deviceId=" + paramMap.get("Device_ID");
            this.loadingController.create({
                spinner: "crescent"
            }).then((loadEl) => {
                loadEl.present();
                this.authService.getMethod(url)
                    .subscribe((respData) => {
                    loadEl.dismiss();
                    if (respData) {
                        var res = JSON.parse(JSON.stringify(respData));
                        this.deviceObj = res;
                        this.screenKey = paramMap.get('key');
                    }
                }, err => {
                    loadEl.dismiss();
                    console.log(err);
                });
            });
        });
    }
};
CommonPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: src_app_app_model__WEBPACK_IMPORTED_MODULE_5__["URLs"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] }
];
CommonPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-common',
        template: __webpack_require__(/*! raw-loader!./common.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/common/common.page.html"),
        styles: [__webpack_require__(/*! ./common.page.scss */ "./src/app/tabs/dashboard/common/common.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
        src_app_app_model__WEBPACK_IMPORTED_MODULE_5__["URLs"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]])
], CommonPage);



/***/ })

}]);
//# sourceMappingURL=dashboard-common-common-module-es2015.js.map
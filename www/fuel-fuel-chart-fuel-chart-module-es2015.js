(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["fuel-fuel-chart-fuel-chart-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/fuel/fuel-chart/fuel-chart.page.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/fuel/fuel-chart/fuel-chart.page.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/fuel\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Fuel Chart</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/fuel/fuel-chart/fuel-chart.module.ts":
/*!******************************************************!*\
  !*** ./src/app/fuel/fuel-chart/fuel-chart.module.ts ***!
  \******************************************************/
/*! exports provided: FuelChartPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelChartPageModule", function() { return FuelChartPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _fuel_chart_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fuel-chart.page */ "./src/app/fuel/fuel-chart/fuel-chart.page.ts");







const routes = [
    {
        path: '',
        component: _fuel_chart_page__WEBPACK_IMPORTED_MODULE_6__["FuelChartPage"]
    }
];
let FuelChartPageModule = class FuelChartPageModule {
};
FuelChartPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_fuel_chart_page__WEBPACK_IMPORTED_MODULE_6__["FuelChartPage"]]
    })
], FuelChartPageModule);



/***/ }),

/***/ "./src/app/fuel/fuel-chart/fuel-chart.page.scss":
/*!******************************************************!*\
  !*** ./src/app/fuel/fuel-chart/fuel-chart.page.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Z1ZWwvZnVlbC1jaGFydC9mdWVsLWNoYXJ0LnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/fuel/fuel-chart/fuel-chart.page.ts":
/*!****************************************************!*\
  !*** ./src/app/fuel/fuel-chart/fuel-chart.page.ts ***!
  \****************************************************/
/*! exports provided: FuelChartPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelChartPage", function() { return FuelChartPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FuelChartPage = class FuelChartPage {
    constructor() { }
    ngOnInit() {
    }
};
FuelChartPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fuel-chart',
        template: __webpack_require__(/*! raw-loader!./fuel-chart.page.html */ "./node_modules/raw-loader/index.js!./src/app/fuel/fuel-chart/fuel-chart.page.html"),
        styles: [__webpack_require__(/*! ./fuel-chart.page.scss */ "./src/app/fuel/fuel-chart/fuel-chart.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], FuelChartPage);



/***/ })

}]);
//# sourceMappingURL=fuel-fuel-chart-fuel-chart-module-es2015.js.map
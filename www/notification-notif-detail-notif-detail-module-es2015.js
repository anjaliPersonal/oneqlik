(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["notification-notif-detail-notif-detail-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/notification/notif-detail/notif-detail.page.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/notification/notif-detail/notif-detail.page.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"'/notification'\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Notification Detail</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/notification/notif-detail/notif-detail.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/notification/notif-detail/notif-detail.module.ts ***!
  \******************************************************************/
/*! exports provided: NotifDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotifDetailPageModule", function() { return NotifDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _notif_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notif-detail.page */ "./src/app/notification/notif-detail/notif-detail.page.ts");







const routes = [
    {
        path: '',
        component: _notif_detail_page__WEBPACK_IMPORTED_MODULE_6__["NotifDetailPage"]
    }
];
let NotifDetailPageModule = class NotifDetailPageModule {
};
NotifDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_notif_detail_page__WEBPACK_IMPORTED_MODULE_6__["NotifDetailPage"]]
    })
], NotifDetailPageModule);



/***/ }),

/***/ "./src/app/notification/notif-detail/notif-detail.page.scss":
/*!******************************************************************!*\
  !*** ./src/app/notification/notif-detail/notif-detail.page.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25vdGlmaWNhdGlvbi9ub3RpZi1kZXRhaWwvbm90aWYtZGV0YWlsLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/notification/notif-detail/notif-detail.page.ts":
/*!****************************************************************!*\
  !*** ./src/app/notification/notif-detail/notif-detail.page.ts ***!
  \****************************************************************/
/*! exports provided: NotifDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotifDetailPage", function() { return NotifDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




let NotifDetailPage = class NotifDetailPage {
    constructor(route, navCtrl) {
        this.route = route;
        this.navCtrl = navCtrl;
    }
    ngOnInit() {
        this.route.paramMap.subscribe((paramMap) => {
            if (!paramMap.has('notifId')) {
                this.navCtrl.navigateBack('/notification');
                return;
            }
            this.notifType = paramMap.get('notifId');
            console.log('notifType: ' + this.notifType);
        });
    }
};
NotifDetailPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] }
];
NotifDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-notif-detail',
        template: __webpack_require__(/*! raw-loader!./notif-detail.page.html */ "./node_modules/raw-loader/index.js!./src/app/notification/notif-detail/notif-detail.page.html"),
        styles: [__webpack_require__(/*! ./notif-detail.page.scss */ "./src/app/notification/notif-detail/notif-detail.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])
], NotifDetailPage);



/***/ })

}]);
//# sourceMappingURL=notification-notif-detail-notif-detail-module-es2015.js.map
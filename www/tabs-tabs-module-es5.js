(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tabs-tabs-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/tabs.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/tabs.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-tabs>\n  <ion-tab-bar clot=\"bottom\">\n    <ion-tab-button tab=\"dashboard\" color=\"secondary\">\n      <ion-icon name=\"home\"></ion-icon>\n      <ion-label>Dashboard</ion-label>\n    </ion-tab-button>\n    <!-- <ion-tab-button *ngIf=\"isDealer\" tab=\"dashboard\" (click)=\"onAdminClicked()\">\n      <ion-icon name=\"people\"></ion-icon>\n      <ion-label>Admin</ion-label>\n    </ion-tab-button> -->\n    <ion-tab-button tab=\"dealer\" *ngIf=\"!isCustomer && !isDealer\">\n      <ion-icon name=\"people\"></ion-icon>\n      <ion-label>Dealer</ion-label>\n    </ion-tab-button>\n    <ion-tab-button tab=\"customer\" *ngIf=\"!isCustomer\">\n      <ion-icon name=\"person\"></ion-icon>\n      <ion-label>Customer</ion-label>\n    </ion-tab-button>\n    <ion-tab-button tab=\"profile\">\n      <ion-icon name=\"card\"></ion-icon>\n      <ion-label>Profile</ion-label>\n    </ion-tab-button>\n  </ion-tab-bar>\n</ion-tabs>"

/***/ }),

/***/ "./src/app/tabs/tabs-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/tabs/tabs-routing.module.ts ***!
  \*********************************************/
/*! exports provided: TabsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsRoutingModule", function() { return TabsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tabs.page */ "./src/app/tabs/tabs.page.ts");
/* harmony import */ var _tabs_resolver_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./tabs-resolver.service */ "./src/app/tabs/tabs-resolver.service.ts");





var routes = [
    {
        path: 'tabs',
        component: _tabs_page__WEBPACK_IMPORTED_MODULE_3__["TabsPage"],
        children: [
            {
                path: 'dashboard',
                children: [
                    {
                        path: '',
                        loadChildren: './dashboard/dashboard.module#DashboardPageModule'
                    },
                    {
                        path: 'live-tracking',
                        loadChildren: './dashboard/live-tracking/live-tracking.module#LiveTrackingPageModule'
                    },
                    {
                        path: 'vehicle-list',
                        loadChildren: './dashboard/vehicle-list/vehicle-list.module#VehicleListPageModule'
                    },
                    {
                        path: 'history',
                        children: [
                            {
                                path: '',
                                loadChildren: './dashboard/history/history.module#HistoryPageModule'
                            },
                            {
                                path: ':dev_id/:key',
                                loadChildren: './dashboard/history/history.module#HistoryPageModule'
                            }
                        ]
                    },
                    {
                        path: 'geo-fence',
                        loadChildren: './dashboard/geo-fence/geo-fence.module#GeoFencePageModule'
                    },
                    {
                        path: 'common/:Device_ID/:key',
                        loadChildren: './dashboard/common/common.module#CommonPageModule'
                    }
                ]
            },
            {
                path: 'dealer',
                children: [
                    {
                        path: '',
                        loadChildren: './dealer/dealer.module#DealerPageModule'
                    },
                    {
                        path: 'add-dealer',
                        loadChildren: './dealer/add-dealer/add-dealer.module#AddDealerPageModule'
                    },
                    {
                        path: 'edit-dealer/:dealerId',
                        loadChildren: './dealer/edit-dealer/edit-dealer.module#EditDealerPageModule'
                    }
                ]
            },
            {
                path: 'customer',
                children: [
                    {
                        path: '',
                        loadChildren: './customer/customer.module#CustomerPageModule',
                    },
                    {
                        path: 'edit-cust/:custId',
                        loadChildren: './customer/edit-customer/edit-customer.module#EditCustomerPageModule'
                    }
                ]
            },
            {
                path: 'profile',
                children: [
                    {
                        path: '',
                        loadChildren: './profile/profile.module#ProfilePageModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/maintabs/tabs/dashboard',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/maintabs/tabs/dashboard',
        pathMatch: 'full'
    },
    { path: 'expired', loadChildren: './dashboard/expired/expired.module#ExpiredPageModule' },
];
var TabsRoutingModule = /** @class */ (function () {
    function TabsRoutingModule() {
    }
    TabsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            providers: [_tabs_resolver_service__WEBPACK_IMPORTED_MODULE_4__["TabsResolverService"]]
        })
    ], TabsRoutingModule);
    return TabsRoutingModule;
}());



/***/ }),

/***/ "./src/app/tabs/tabs.module.ts":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.module.ts ***!
  \*************************************/
/*! exports provided: TabsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tabs.page */ "./src/app/tabs/tabs.page.ts");
/* harmony import */ var _tabs_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tabs-routing.module */ "./src/app/tabs/tabs-routing.module.ts");







var TabsPageModule = /** @class */ (function () {
    function TabsPageModule() {
    }
    TabsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _tabs_routing_module__WEBPACK_IMPORTED_MODULE_6__["TabsRoutingModule"]
            ],
            declarations: [_tabs_page__WEBPACK_IMPORTED_MODULE_5__["TabsPage"]]
        })
    ], TabsPageModule);
    return TabsPageModule;
}());



/***/ }),

/***/ "./src/app/tabs/tabs.page.scss":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvdGFicy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/tabs/tabs.page.ts":
/*!***********************************!*\
  !*** ./src/app/tabs/tabs.page.ts ***!
  \***********************************/
/*! exports provided: TabsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPage", function() { return TabsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



// import { Plugins } from '@capacitor/core';
// import { Router } from '@angular/router';

var TabsPage = /** @class */ (function () {
    function TabsPage(authService, event, menuCtrl) {
        this.authService = authService;
        this.event = event;
        this.menuCtrl = menuCtrl;
        this.userData = {};
        this.isCustomer = false;
        this.isDealer = false;
    }
    TabsPage.prototype.ngOnInit = function () {
        this.getToken();
        this.checkIfDealerOrCustomer();
    };
    TabsPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
    };
    TabsPage.prototype.getToken = function () {
        var _this = this;
        this.authService.getTokenData().subscribe(function (data) {
            _this.userData = data;
            console.log("tabs data=> ", _this.userData);
            if (_this.userData.isDealer === false && _this.userData.isOperator === false && _this.userData.isOrganisation === false && _this.userData.isSuperAdmin === false) {
                _this.isCustomer = true;
                _this.event.publish('Auth:Role', {
                    customer: _this.isCustomer,
                    dealer: _this.isDealer
                });
            }
        });
    };
    TabsPage.prototype.checkIfDealerOrCustomer = function () {
        var dealStat = localStorage.getItem('dealer_status');
        var custStat = localStorage.getItem('custumer_status');
        if (dealStat !== null && dealStat === 'ON' && custStat === 'OFF') {
            this.isDealer = true;
            this.event.publish('Auth:Role', {
                customer: this.isCustomer,
                dealer: this.isDealer
            });
        }
        else if (custStat !== null && custStat === 'ON' && dealStat === 'OFF') {
            this.isCustomer = true;
            this.event.publish('Auth:Role', {
                customer: this.isCustomer,
                dealer: this.isDealer
            });
        }
    };
    TabsPage.ctorParameters = function () { return [
        { type: _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Events"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] }
    ]; };
    TabsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tabs',
            template: __webpack_require__(/*! raw-loader!./tabs.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/tabs.page.html"),
            styles: [__webpack_require__(/*! ./tabs.page.scss */ "./src/app/tabs/tabs.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Events"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]])
    ], TabsPage);
    return TabsPage;
}());



/***/ })

}]);
//# sourceMappingURL=tabs-tabs-module-es5.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["expense-expense-detail-expense-detail-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/expense/expense-detail/expense-detail.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/expense/expense-detail/expense-detail.page.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar style='--background:var(--my-var); color: white;'>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/expense\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>{{expId | titlecase }} Expense</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/expense/expense-detail/expense-detail.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/expense/expense-detail/expense-detail.module.ts ***!
  \*****************************************************************/
/*! exports provided: ExpenseDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpenseDetailPageModule", function() { return ExpenseDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _expense_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./expense-detail.page */ "./src/app/expense/expense-detail/expense-detail.page.ts");







var routes = [
    {
        path: '',
        component: _expense_detail_page__WEBPACK_IMPORTED_MODULE_6__["ExpenseDetailPage"]
    }
];
var ExpenseDetailPageModule = /** @class */ (function () {
    function ExpenseDetailPageModule() {
    }
    ExpenseDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_expense_detail_page__WEBPACK_IMPORTED_MODULE_6__["ExpenseDetailPage"]]
        })
    ], ExpenseDetailPageModule);
    return ExpenseDetailPageModule;
}());



/***/ }),

/***/ "./src/app/expense/expense-detail/expense-detail.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/expense/expense-detail/expense-detail.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V4cGVuc2UvZXhwZW5zZS1kZXRhaWwvZXhwZW5zZS1kZXRhaWwucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/expense/expense-detail/expense-detail.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/expense/expense-detail/expense-detail.page.ts ***!
  \***************************************************************/
/*! exports provided: ExpenseDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpenseDetailPage", function() { return ExpenseDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/auth/auth.service */ "./src/app/auth/auth.service.ts");





var ExpenseDetailPage = /** @class */ (function () {
    // expName: string;
    function ExpenseDetailPage(route, navCtrl, authService, elementRef) {
        this.route = route;
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.elementRef = elementRef;
        this.getToken();
    }
    ExpenseDetailPage.prototype.setStyle = function (value) {
        this.elementRef.nativeElement.style.setProperty('--my-var', value);
    };
    ExpenseDetailPage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.paramMap.subscribe(function (paramMap) {
            if (!paramMap.has('expId')) {
                _this.navCtrl.navigateBack('/expense');
                return;
            }
            _this.expId = paramMap.get('expId');
            console.log("expense id: ", _this.expId);
            if (_this.expId === 'tools') {
                _this.setStyle('#ee5555');
            }
            else if (_this.expId === 'service') {
                _this.setStyle('#fe8a49');
            }
            else if (_this.expId === 'fuel') {
                _this.setStyle('#5b8016');
            }
            else if (_this.expId === 'labour') {
                _this.setStyle('#334861');
            }
            else if (_this.expId === 'salary') {
                _this.setStyle('#b8996a');
            }
            else if (_this.expId === 'toll') {
                _this.setStyle('#061c52');
            }
            else {
            }
            // if (this.report.key === 'summ' || this.report.key === 'dist') {
            //   this.datetimeFrom = moment({ hours: 0 }).subtract(1, 'days').format(); // yesterday date with 12:00 am
            //   this.datetimeTo = moment({ hours: 0 }).format(); // today date and time with 12:00am
            // } else {
            //   this.datetimeFrom = moment({ hours: 0 }).format();
            //   this.datetimeTo = moment().format();//new Date(a).toISOString();
            // }
        });
    };
    ExpenseDetailPage.prototype.getToken = function () {
        var _this = this;
        this.authService.getTokenData().subscribe(function (data) {
            _this.userData = data;
            // if (this.report.key === 'daily') {
            //   this.msgString = "Loading daily report(s)";
            //   this.getDailyReportData();
            // } else if (this.report.key === 'summ') {
            //   this.msgString = "Loading summary report(s)";
            //   this.getGlobalreportData();
            // } else if (this.report.key === 'ospeed') {
            //   this.msgString = "Loading overspeed report(s)";
            //   this.getGlobalreportData();
            // }
        });
    };
    ExpenseDetailPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
        { type: src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    ExpenseDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-expense-detail',
            template: __webpack_require__(/*! raw-loader!./expense-detail.page.html */ "./node_modules/raw-loader/index.js!./src/app/expense/expense-detail/expense-detail.page.html"),
            styles: [__webpack_require__(/*! ./expense-detail.page.scss */ "./src/app/expense/expense-detail/expense-detail.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
            src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], ExpenseDetailPage);
    return ExpenseDetailPage;
}());



/***/ })

}]);
//# sourceMappingURL=expense-expense-detail-expense-detail-module-es5.js.map
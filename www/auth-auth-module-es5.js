(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["auth-auth-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/auth/auth.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/auth/auth.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding class=\"background-image\">\n\n  <form #f=\"ngForm\" (ngSubmit)=\"onSubmit(f)\">\n    <ion-grid>\n      <ion-row>\n        <ion-col size-sm=\"6\" offset-sm=\"3\" no-padding>\n          <ion-row>\n            <ion-col size=\"3\"></ion-col>\n            <ion-col size=\"6\">\n              <ion-img src=\"assets/icon/icon.png\"></ion-img>\n            </ion-col>\n            <ion-col size=\"3\"></ion-col>\n          </ion-row>\n          <ion-list *ngIf=\"isLogin\" style=\"background: transparent;\">\n            <ion-item class=\"bg-item\">\n              <ion-label position=\"floating\" style=\"font-size: 1.2em; color: white;\">E-Mail / Mobile Number / User Id\n              </ion-label>\n              <ion-input type=\"text\" ngModel name=\"email\" required #emailCtrl=\"ngModel\"></ion-input>\n            </ion-item>\n            <ion-item *ngIf=\"!emailCtrl.valid && emailCtrl.touched\" lines=\"none\" class=\"ion-no-padding bg-item\">\n              <ion-icon style=\"font-size: 15px;font-weight:bolder;padding-left: 16px; padding-right: 5px;\" color=\"light\"\n                name=\"information-circle-outline\"></ion-icon>\n              <ion-label color=\"light\" style=\"font-size: 0.8em; font-weight: 400;\">\n                email-id / mobile number is required!\n              </ion-label>\n            </ion-item>\n            <ion-item class=\"bg-item\">\n              <ion-label position=\"floating\" style=\"font-size: 1.2em; color: white;\">Password</ion-label>\n              <ion-input type=\"password\" ngModel name=\"password\" required #passwordCtrl=\"ngModel\"></ion-input>\n            </ion-item>\n            <ion-item *ngIf=\"!passwordCtrl.valid && passwordCtrl.touched\" lines=\"none\" class=\"ion-no-padding bg-item\">\n              <ion-icon style=\"font-size: 15px;font-weight:bolder;padding-left: 16px; padding-right: 5px;\" color=\"light\"\n                name=\"information-circle-outline\"></ion-icon>\n              <ion-label color=\"light\" style=\"font-size: 0.8em; font-weight: 400;\">\n                password is required!\n              </ion-label>\n            </ion-item>\n          </ion-list>\n          <ion-list *ngIf=\"!isLogin\" style=\"background: transparent;\">\n            <ion-item class=\"bg-item\">\n              <ion-label position=\"floating\">User Id</ion-label>\n              <ion-input type=\"text\" ngModel name=\"userid\" required #userIdCtrl=\"ngModel\"></ion-input>\n            </ion-item>\n            <ion-item *ngIf=\"!userIdCtrl.valid && userIdCtrl.touched\" lines=\"none\" class=\"ion-no-padding bg-item\"\n              style=\"color: #fff;\">\n              <ion-icon style=\"font-size: 15px; font-weight:bolder; padding-left: 16px; padding-right: 5px;\"\n                name=\"information-circle-outline\" color=\"light\"></ion-icon>&nbsp;\n              <ion-label style=\"font-size: 0.8em; font-weight: 400;\">user id is required!</ion-label>\n            </ion-item>\n            <ion-item class=\"bg-item\">\n              <ion-label position=\"floating\">Full Name</ion-label>\n              <ion-input type=\"text\" ngModel name=\"fname\" required #nameCtrl=\"ngModel\"></ion-input>\n            </ion-item>\n            <ion-item *ngIf=\"!nameCtrl.valid && nameCtrl.touched\" lines=\"none\" class=\"ion-no-padding bg-item\"\n              style=\"color: #fff;\">\n              <ion-icon style=\"font-size: 15px; font-weight:bolder;padding-left: 16px; padding-right: 5px;\"\n                name=\"information-circle-outline\" color=\"light\"></ion-icon>&nbsp;\n              <ion-label style=\"font-size: 0.8em; font-weight: 400;\">name is required!</ion-label>\n            </ion-item>\n            <ion-item class=\"bg-item\">\n              <ion-label position=\"floating\">Mobile Number</ion-label>\n              <ion-input type=\"tel\" ngModel name=\"mobno\" required minlength=\"10\" maxlength=\"10\" #mobnoCtrl=\"ngModel\">\n              </ion-input>\n            </ion-item>\n            <ion-item *ngIf=\"!mobnoCtrl.valid && mobnoCtrl.touched\" lines=\"none\" class=\"ion-no-padding bg-item\"\n              style=\"color: #fff;\">\n              <ion-icon style=\"font-size: 15px; font-weight:bolder;padding-left: 16px; padding-right: 5px;\"\n                name=\"information-circle-outline\" color=\"light\"></ion-icon>&nbsp;\n              <ion-label style=\"font-size: 0.8em; font-weight: 400;\">Should atleast be 10 digits long.</ion-label>\n            </ion-item>\n            <ion-item class=\"bg-item\">\n              <ion-label position=\"floating\">Email Id</ion-label>\n              <ion-input type=\"email\" ngModel name=\"email\" required email #emailCtrl=\"ngModel\"></ion-input>\n            </ion-item>\n            <ion-item *ngIf=\"!emailCtrl.valid && emailCtrl.touched\" lines=\"none\" class=\"ion-no-padding bg-item\"\n              style=\"color: #fff;\">\n              <ion-icon style=\"font-size: 15px; font-weight:bolder;padding-left: 16px; padding-right: 5px;\"\n                name=\"information-circle-outline\" color=\"light\"></ion-icon>&nbsp;\n              <ion-label style=\"font-size: 0.8em; font-weight: 400;\">Should be a valid email address.</ion-label>\n            </ion-item>\n            <ion-item class=\"bg-item\">\n              <ion-label position=\"floating\">Password</ion-label>\n              <ion-input type=\"password\" ngModel name=\"password\" required minlength=\"6\" #passCtrl=\"ngModel\"></ion-input>\n            </ion-item>\n            <ion-item *ngIf=\"!passCtrl.valid && passCtrl.touched\" lines=\"none\" class=\"ion-no-padding bg-item\"\n              style=\"color: #fff;\">\n              <ion-icon style=\"font-size: 15px; font-weight:bolder;padding-left: 16px; padding-right: 5px;\"\n                name=\"information-circle-outline\" color=\"light\"></ion-icon>&nbsp;\n              <ion-label style=\"font-size: 0.8em; font-weight: 400;\">Should atleast be 6 characters long.</ion-label>\n            </ion-item>\n          </ion-list>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size-sm=\"6\" offset-sm=\"3\">\n          <ion-button color=\"secondary\" type=\"button\" fill=\"clear\" expand=\"block\" (click)=\"onSwitchAuthMode()\">Switch To\n            {{ isLogin ? \"Signup\" : \"Login\" }}</ion-button>\n          <ion-button color=\"secondary\" type=\"submit\" expand=\"block\" [disabled]=\"!f.valid\">\n            {{ isLogin ? \"Login\" : \"Signup\" }}\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/auth/verify-otp/verify-otp.component.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/auth/verify-otp/verify-otp.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mainDiv\">\n  <div class=\"secondDiv\">\n    <form #f=\"ngForm\" (ngSubmit)=\"onSubmit(f)\">\n      <ion-grid>\n        <ion-row>\n          <ion-col size-sm=\"6\" offset-sm=\"3\">\n            <ion-card>\n              <ion-card-content>\n                <ion-item>\n                  <ion-input placeholder=\"Enter OTP here\" type=\"tel\" minlength=\"4\" maxlength=\"4\" ngModel name=\"otp\"\n                    required></ion-input>\n                </ion-item>\n              </ion-card-content>\n              <ion-button [disabled]=\"!f.valid\" type=\"submit\">Verify OTP</ion-button>\n            </ion-card>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </form>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/auth/auth.module.ts":
/*!*************************************!*\
  !*** ./src/app/auth/auth.module.ts ***!
  \*************************************/
/*! exports provided: AuthPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthPageModule", function() { return AuthPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _auth_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth.page */ "./src/app/auth/auth.page.ts");
/* harmony import */ var _verify_otp_verify_otp_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./verify-otp/verify-otp.component */ "./src/app/auth/verify-otp/verify-otp.component.ts");








var routes = [
    {
        path: '',
        component: _auth_page__WEBPACK_IMPORTED_MODULE_6__["AuthPage"]
    }
];
var AuthPageModule = /** @class */ (function () {
    function AuthPageModule() {
    }
    AuthPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_auth_page__WEBPACK_IMPORTED_MODULE_6__["AuthPage"], _verify_otp_verify_otp_component__WEBPACK_IMPORTED_MODULE_7__["VerifyOTPComponent"]],
            entryComponents: [_verify_otp_verify_otp_component__WEBPACK_IMPORTED_MODULE_7__["VerifyOTPComponent"]]
        })
    ], AuthPageModule);
    return AuthPageModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.page.scss":
/*!*************************************!*\
  !*** ./src/app/auth/auth.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".background-image {\n  --background: url('spl.jpg') 0 0/100% 100% no-repeat;\n  /* Add the blur effect */\n}\n\n.bg-item {\n  --background: rgba(0, 0, 0, 0);\n  --color: white;\n  --border-color: rgb(66, 64, 64);\n}\n\n.secondary {\n  color: #cd2a00;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL2lvbmljNC9vbmVxbGlrL3NyYy9hcHAvYXV0aC9hdXRoLnBhZ2Uuc2NzcyIsInNyYy9hcHAvYXV0aC9hdXRoLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG9EQUFBO0VBQ0Esd0JBQUE7QUNDRjs7QURJQTtFQUNFLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLCtCQUFBO0FDREY7O0FEYUE7RUFDRSxjQUFBO0FDVkYiLCJmaWxlIjoic3JjL2FwcC9hdXRoL2F1dGgucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJhY2tncm91bmQtaW1hZ2Uge1xuICAtLWJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1ncy9zcGwuanBnKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcbiAgLyogQWRkIHRoZSBibHVyIGVmZmVjdCAqL1xuICAvLyAgIGZpbHRlcjogYmx1cig4cHgpO1xuICAvLyAgIC13ZWJraXQtZmlsdGVyOiBibHVyKDhweCk7XG59XG5cbi5iZy1pdGVtIHtcbiAgLS1iYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDApO1xuICAtLWNvbG9yOiB3aGl0ZTtcbiAgLS1ib3JkZXItY29sb3I6IHJnYig2NiwgNjQsIDY0KTtcbi8vICAgLS1jb2xvci1hY3RpdmF0ZWQ6IHdoaXRlO1xuLy8gICAtLWNvbG9yLWZvY3VzZWRcdDogd2hpdGU7XG59XG4vLyA6cm9vdCB7XG4vLyAgIC0taW9uLWNvbG9yLWRhbmdlcjogI2YwNDE0MTsgIFxuLy8gfVxuLy8gaW9uLWl0ZW0gLml0ZW0taGFzLWZvY3VzIGlvbi1sYWJlbC5pb24tY29sb3IgeyAgXG4vLyAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuLy8gICAvLyBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhbmdlcikgIWltcG9ydGFudDtcbi8vIH1cblxuLnNlY29uZGFyeSB7XG4gIGNvbG9yOiAjY2QyYTAwO1xufVxuIiwiLmJhY2tncm91bmQtaW1hZ2Uge1xuICAtLWJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1ncy9zcGwuanBnKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcbiAgLyogQWRkIHRoZSBibHVyIGVmZmVjdCAqL1xufVxuXG4uYmctaXRlbSB7XG4gIC0tYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwKTtcbiAgLS1jb2xvcjogd2hpdGU7XG4gIC0tYm9yZGVyLWNvbG9yOiByZ2IoNjYsIDY0LCA2NCk7XG59XG5cbi5zZWNvbmRhcnkge1xuICBjb2xvcjogI2NkMmEwMDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/auth/auth.page.ts":
/*!***********************************!*\
  !*** ./src/app/auth/auth.page.ts ***!
  \***********************************/
/*! exports provided: AuthPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthPage", function() { return AuthPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _verify_otp_verify_otp_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./verify-otp/verify-otp.component */ "./src/app/auth/verify-otp/verify-otp.component.ts");






var AuthPage = /** @class */ (function () {
    function AuthPage(router, loadingCtrl, authService, toastController, modalController, alertController, menuCtrl) {
        this.router = router;
        this.loadingCtrl = loadingCtrl;
        this.authService = authService;
        this.toastController = toastController;
        this.modalController = modalController;
        this.alertController = alertController;
        this.menuCtrl = menuCtrl;
        this.isLogin = true;
        this.isLoading = false;
    }
    AuthPage.prototype.ngOnInit = function () {
    };
    AuthPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(false);
    };
    AuthPage.prototype.onSubmit = function (form) {
        if (!form.valid) {
            return;
        }
        var email = form.value.email;
        var pass = form.value.password;
        var fname = form.value.fname;
        var mobno = form.value.mobno;
        var userid = form.value.userid;
        console.log(email, pass);
        this.authenticate(fname, mobno, email, pass, userid);
        form.reset();
    };
    AuthPage.prototype.showTostMsg = function (msg) {
        this.toastController.create({
            message: msg,
            duration: 1800,
            position: 'bottom'
        }).then(function (toastEl) {
            toastEl.present();
        });
    };
    AuthPage.prototype.onSwitchAuthMode = function () {
        this.isLogin = !this.isLogin;
    };
    AuthPage.prototype.authenticate = function (fname, mobno, email, pass, userid) {
        var _this = this;
        this.isLoading = true;
        ////////
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
        var isEmail = validateEmail(email);
        var isName = isNaN(email);
        var strNum;
        if (isName == false) {
            strNum = email.trim();
        }
        var data;
        if (isEmail == false && isName == false && strNum.length == 10) {
            data = {
                "psd": pass,
                "ph_num": email
            };
        }
        else if (isEmail) {
            data = {
                "psd": pass,
                "emailid": email
            };
        }
        else {
            data = {
                "psd": pass,
                "user_id": email
            };
        }
        ////////
        this.loadingCtrl.create({
            keyboardClose: true, message: 'Loging in...'
        }).then(function (loadingEl) {
            loadingEl.present();
            var authObs;
            if (_this.isLogin) {
                authObs = _this.authService.login(data);
            }
            else {
                authObs = _this.authService.signup(fname, mobno, email, pass, userid);
            }
            authObs.subscribe(function (respData) {
                loadingEl.dismiss();
                _this.isLoading = false;
                if (respData.message === 'Email ID or Mobile Number already exists') {
                    _this.showTostMsg(respData.message);
                    return;
                }
                if (respData.message === 'OTP sent successfully') {
                    _this.modalController.create({
                        component: _verify_otp_verify_otp_component__WEBPACK_IMPORTED_MODULE_5__["VerifyOTPComponent"],
                        componentProps: {
                            phonenumber: mobno
                        }
                    }).then(function (modelEl) {
                        modelEl.present();
                    });
                    return;
                }
                _this.showTostMsg('Welcome, you have logged in successfully!');
                _this.router.navigateByUrl('/maintabs/tabs/dashboard');
            }, function (error) {
                loadingEl.dismiss();
                console.log(error.error.message);
                var msg = "Could not sign you up. Please try again.";
                if (error.error.message) {
                    msg = error.error.message;
                }
                _this.showAlert(msg);
            });
        });
    };
    AuthPage.prototype.showAlert = function (message) {
        this.alertController.create({
            header: 'Authentication failed',
            message: message,
            buttons: [{
                    text: 'Okay',
                    cssClass: 'secondary',
                    handler: function () {
                        console.log('Confirm Okay');
                    }
                }]
        }).then(function (alertEl) { return alertEl.present(); });
    };
    AuthPage.prototype.handleFirstNameValue = function (event) {
        var firstName = event.target.value;
    };
    AuthPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
        { type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] }
    ]; };
    AuthPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-auth',
            template: __webpack_require__(/*! raw-loader!./auth.page.html */ "./node_modules/raw-loader/index.js!./src/app/auth/auth.page.html"),
            styles: [__webpack_require__(/*! ./auth.page.scss */ "./src/app/auth/auth.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]])
    ], AuthPage);
    return AuthPage;
}());



/***/ }),

/***/ "./src/app/auth/verify-otp/verify-otp.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/auth/verify-otp/verify-otp.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mainDiv {\n  padding: 50px;\n  height: 100%;\n  background-color: rgba(0, 0, 0, 0.7);\n}\n\n.secondDiv {\n  padding-top: 20px;\n  border-radius: 18px;\n  border: 2px solid black;\n  background: white;\n}\n\nion-card, .card-ios, .card-md {\n  border: 0 !important;\n  box-shadow: none !important;\n  border: none !important;\n  border-style: none !important;\n  position: relative;\n  text-align: center;\n}\n\n.card {\n  border: 0 !important;\n  box-shadow: 0 !important;\n  border: none !important;\n  border-style: none !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL2lvbmljNC9vbmVxbGlrL3NyYy9hcHAvYXV0aC92ZXJpZnktb3RwL3ZlcmlmeS1vdHAuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2F1dGgvdmVyaWZ5LW90cC92ZXJpZnktb3RwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtFQUNBLFlBQUE7RUFDQSxvQ0FBQTtBQ0NGOztBREVBO0VBQ0UsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7QUNDRjs7QURFQTtFQUNJLG9CQUFBO0VBQ0EsMkJBQUE7RUFDQSx1QkFBQTtFQUNBLDZCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ0NKOztBRENFO0VBQ0Usb0JBQUE7RUFDQSx3QkFBQTtFQUNBLHVCQUFBO0VBQ0EsNkJBQUE7QUNFSiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvdmVyaWZ5LW90cC92ZXJpZnktb3RwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1haW5EaXYge1xuICBwYWRkaW5nOiA1MHB4O1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC43KTtcbn1cblxuLnNlY29uZERpdiB7XG4gIHBhZGRpbmctdG9wOiAyMHB4O1xuICBib3JkZXItcmFkaXVzOiAxOHB4O1xuICBib3JkZXI6IDJweCBzb2xpZCBibGFjaztcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbmlvbi1jYXJkLCAuY2FyZC1pb3MsIC5jYXJkLW1kIHtcbiAgICBib3JkZXI6IDAgIWltcG9ydGFudDtcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLXN0eWxlOiBub25lICFpbXBvcnRhbnQ7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICAuY2FyZHtcbiAgICBib3JkZXI6IDAgIWltcG9ydGFudDtcbiAgICBib3gtc2hhZG93OiAwICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLXN0eWxlOiBub25lICFpbXBvcnRhbnQ7XG4gIH1cbiIsIi5tYWluRGl2IHtcbiAgcGFkZGluZzogNTBweDtcbiAgaGVpZ2h0OiAxMDAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNyk7XG59XG5cbi5zZWNvbmREaXYge1xuICBwYWRkaW5nLXRvcDogMjBweDtcbiAgYm9yZGVyLXJhZGl1czogMThweDtcbiAgYm9yZGVyOiAycHggc29saWQgYmxhY2s7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuXG5pb24tY2FyZCwgLmNhcmQtaW9zLCAuY2FyZC1tZCB7XG4gIGJvcmRlcjogMCAhaW1wb3J0YW50O1xuICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xuICBib3JkZXItc3R5bGU6IG5vbmUgIWltcG9ydGFudDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5jYXJkIHtcbiAgYm9yZGVyOiAwICFpbXBvcnRhbnQ7XG4gIGJveC1zaGFkb3c6IDAgIWltcG9ydGFudDtcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1zdHlsZTogbm9uZSAhaW1wb3J0YW50O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/auth/verify-otp/verify-otp.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/auth/verify-otp/verify-otp.component.ts ***!
  \*********************************************************/
/*! exports provided: VerifyOTPComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifyOTPComponent", function() { return VerifyOTPComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var VerifyOTPComponent = /** @class */ (function () {
    function VerifyOTPComponent(navParams, authService, toastController, router, modalController) {
        this.navParams = navParams;
        this.authService = authService;
        this.toastController = toastController;
        this.router = router;
        this.modalController = modalController;
        console.log(navParams.get('phonenumber'));
        this.phoneNumber = navParams.get('phonenumber');
    }
    VerifyOTPComponent.prototype.ngOnInit = function () { };
    VerifyOTPComponent.prototype.onSubmit = function (form) {
        var _this = this;
        if (!form.valid) {
            return;
        }
        var otp = form.value.otp;
        this.authService.sendOtp(this.phoneNumber, otp).subscribe(function (resData) {
            console.log(resData);
            if (resData.message === "Didn't match otp") {
                _this.showTostMsg(resData.message);
                return;
            }
            _this.router.navigateByUrl('auth');
        }, function (err) {
            console.log(err);
            _this.showTostMsg(err.message);
            _this.modalController.dismiss();
        });
    };
    VerifyOTPComponent.prototype.showTostMsg = function (msg) {
        this.toastController.create({
            message: msg,
            duration: 1800,
            position: 'bottom'
        }).then(function (toastEl) {
            toastEl.present();
        });
    };
    VerifyOTPComponent.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"] },
        { type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    VerifyOTPComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-verify-otp',
            template: __webpack_require__(/*! raw-loader!./verify-otp.component.html */ "./node_modules/raw-loader/index.js!./src/app/auth/verify-otp/verify-otp.component.html"),
            styles: [__webpack_require__(/*! ./verify-otp.component.scss */ "./src/app/auth/verify-otp/verify-otp.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"],
            _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], VerifyOTPComponent);
    return VerifyOTPComponent;
}());



/***/ })

}]);
//# sourceMappingURL=auth-auth-module-es5.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-history-history-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/history/history.page.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/history/history.page.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"./tabs/dashbaord\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>History</ion-title>\n    <ion-buttons *ngIf=\"dataArrayCoords.length > 1\" slot=\"primary\"\n      style=\"border-radius: 13px; background: white; color: black;\">\n      <ion-button slot=\"icon-only\" fill=\"clear\">\n        <ion-icon style=\"font-size: 2em;\" name=\"arrow-dropleft\"></ion-icon>\n      </ion-button>\n      <ion-button *ngIf=\"!playPause\" slot=\"icon-only\" fill=\"clear\" color=\"secondary\" (click)=\"animateHistory()\">\n        <ion-icon style=\"font-size: 2em;\" name=\"play-circle\"></ion-icon>\n      </ion-button>\n      <ion-button *ngIf=\"playPause\" slot=\"icon-only\" fill=\"clear\" color=\"secondary\" (click)=\"animateHistory()\">\n        <ion-icon style=\"font-size: 2em;\" name=\"pause\"></ion-icon>\n      </ion-button>\n      <ion-button slot=\"icon-only\" fill=\"clear\">\n        <ion-icon style=\"font-size: 2em;\" name=\"arrow-dropright\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\" class=\"ion-no-padding\" style=\"background: white;\">\n        <ion-row class=\"ion-no-padding\">\n          <ion-col size=\"12\" no-padding>\n            <app-from-to (fromDateOutput)=\"exampleMethodParent($event)\" (toDateOutput)=\"exampleMethodParent1($event)\"\n              [fromDate]=\"datetimeFrom\" [toDate]=\"datetimeTo\">\n            </app-from-to>\n          </ion-col>\n          <ion-col size=\"12\" *ngIf=\"!isSingle\"  style=\"background: #cd2a00; padding: 5px;\">\n            <app-select-vehicle [receivedParentMessage]=\"messageToSendP\" (selectedVehObj)=\"getVehObj($event)\">\n            </app-select-vehicle>\n          </ion-col>\n          <ion-col size=\"12\">\n            <!-- <ion-row style=\"background-color: #fafafa;\"> -->\n            <ion-row style=\"background-color: white;\">\n              <ion-col style=\"text-align: center;\">\n                <ion-button class=\"ionBtn\" shape=\"round\" size=\"small\" (click)=\"changeDate('today')\">Today\n                </ion-button>\n                <ion-button class=\"ionBtn\" shape=\"round\" size=\"small\" (click)=\"changeDate('yest')\">\n                  Yesterday</ion-button>\n                <ion-button class=\"ionBtn\" shape=\"round\" size=\"small\" (click)=\"changeDate('week')\">Week\n                </ion-button>\n                <ion-button class=\"ionBtn\" shape=\"round\" size=\"small\" (click)=\"changeDate('month')\">Month\n                </ion-button>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <div *ngIf=\"deviceObj\">\n    <app-google-map [passedObj]=\"deviceObj\" [key]=\"messageToSendP\"></app-google-map>\n  </div>\n\n  <div id=\"map_canvas\" *ngIf=\"!isSingle\">\n\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/history/history.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/tabs/dashboard/history/history.module.ts ***!
  \**********************************************************/
/*! exports provided: HistoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPageModule", function() { return HistoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _history_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./history.page */ "./src/app/tabs/dashboard/history/history.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/tabs/components/components.module.ts");








const routes = [
    {
        path: '',
        component: _history_page__WEBPACK_IMPORTED_MODULE_6__["HistoryPage"]
    }
];
let HistoryPageModule = class HistoryPageModule {
};
HistoryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_history_page__WEBPACK_IMPORTED_MODULE_6__["HistoryPage"]]
    })
], HistoryPageModule);



/***/ }),

/***/ "./src/app/tabs/dashboard/history/history.page.scss":
/*!**********************************************************!*\
  !*** ./src/app/tabs/dashboard/history/history.page.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#map_canvas {\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL2lvbmljNC9vbmVxbGlrL3NyYy9hcHAvdGFicy9kYXNoYm9hcmQvaGlzdG9yeS9oaXN0b3J5LnBhZ2Uuc2NzcyIsInNyYy9hcHAvdGFicy9kYXNoYm9hcmQvaGlzdG9yeS9oaXN0b3J5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC90YWJzL2Rhc2hib2FyZC9oaXN0b3J5L2hpc3RvcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI21hcF9jYW52YXMge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuLy8gICBib3JkZXI6IDFweCBzb2xpZCByZWQ7XG59XG4iLCIjbWFwX2NhbnZhcyB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59Il19 */"

/***/ }),

/***/ "./src/app/tabs/dashboard/history/history.page.ts":
/*!********************************************************!*\
  !*** ./src/app/tabs/dashboard/history/history.page.ts ***!
  \********************************************************/
/*! exports provided: HistoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPage", function() { return HistoryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var src_app_app_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/app.model */ "./src/app/app.model.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");








// import { AppService } from 'src/app/app.service';
let HistoryPage = class HistoryPage {
    constructor(route, navCtrl, constUrl, toastCtrl, plt, loadingController, authService) {
        this.route = route;
        this.navCtrl = navCtrl;
        this.constUrl = constUrl;
        this.toastCtrl = toastCtrl;
        this.plt = plt;
        this.loadingController = loadingController;
        this.authService = authService;
        this.messageToSendP = 'history';
        this.vehObj = [];
        this.dataArrayCoords = [];
        this.mapData = [];
        this.isSingle = true;
        this.target = 0;
        this.speed = 0;
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.getData();
    }
    ngOnInit() {
        // this.route.paramMap.subscribe((paramMap) => {
        //   if(!paramMap.has('dev_id')){
        //     this.navCtrl.navigateBack('/maintabs/tabs/dashboard/vehicle-list');
        //     return;
        //   }
        // console.log("history data: ", paramMap.get('dev_id'), " key: ", paramMap.get('key'))
        // })
        localStorage.removeItem("markerTarget");
        this.datetimeFrom = moment__WEBPACK_IMPORTED_MODULE_2__({ hours: 0 }).format();
        this.datetimeTo = moment__WEBPACK_IMPORTED_MODULE_2__().format(); //new Date(a).toISOString();
        // this.plt.ready().then(() => {
        //   this.loadMap();
        // });
    }
    getData() {
        this.route.paramMap.subscribe((paramMap) => {
            if (!paramMap.has('dev_id') && !paramMap.has('key')) {
                this.getToken();
                this.isSingle = false;
                // this.navCtrl.navigateBack('/maintabs/tabs/dashboard/vehicle-list');
                // return;
                this.plt.ready().then(() => {
                    this.loadMap();
                });
            }
            else {
                let url = this.constUrl.mainUrl + "devices/getDevicebyId?deviceId=" + paramMap.get("dev_id");
                this.loadingController.create({
                    spinner: "crescent"
                }).then((loadEl) => {
                    loadEl.present();
                    this.authService.getMethod(url)
                        .subscribe((respData) => {
                        loadEl.dismiss();
                        if (respData) {
                            var res = JSON.parse(JSON.stringify(respData));
                            this.deviceObj = res;
                            this.messageToSendP = paramMap.get('key');
                        }
                    }, err => {
                        loadEl.dismiss();
                        console.log(err);
                    });
                });
            }
        });
    }
    getToken() {
        this.authService.getTokenData().subscribe(data => {
            this.userData = data;
        });
    }
    changeDate(key) {
        this.checkIfMapExist(); // check if map is already defined, if yes then first remove it
        this.datetimeFrom = undefined;
        if (key === 'today') {
            this.datetimeFrom = moment__WEBPACK_IMPORTED_MODULE_2__({ hours: 0 }).format();
        }
        else if (key === 'yest') {
            this.datetimeFrom = moment__WEBPACK_IMPORTED_MODULE_2__().subtract(1, 'days').format();
        }
        else if (key === 'week') {
            this.datetimeFrom = moment__WEBPACK_IMPORTED_MODULE_2__().subtract(1, 'weeks').endOf('isoWeek').format();
        }
        else if (key === 'month') {
            this.datetimeFrom = moment__WEBPACK_IMPORTED_MODULE_2__().startOf('month').format();
        }
    }
    checkIfMapExist() {
        if (this.map != undefined) {
            this.map.remove();
            this.loadMap();
        }
    }
    exampleMethodParent($event) {
        this.datetimeFrom = $event;
        this.getHistoryOfSelectedVehicle();
    }
    exampleMethodParent1($event) {
        this.datetimeTo = $event;
        this.getHistoryOfSelectedVehicle();
    }
    getVehObj($event) {
        this.checkIfMapExist();
        this.vehObj = $event;
        // this.vehObj = {
        //   key: 'history',
        //   deviceObj: $event
        // };
        this.getHistoryOfSelectedVehicle();
    }
    loadMap() {
        _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["LocationService"].getMyLocation().then((myLocation) => {
            let options = {
                camera: {
                    target: myLocation.latLng
                },
                preferences: {
                    zoom: {
                        minZoom: 5,
                        maxZoom: 50
                    },
                    padding: {
                        left: 10,
                        top: 10,
                        bottom: 10,
                        right: 10
                    },
                    building: true
                }
            };
            this.map = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMaps"].create('map_canvas', options);
        });
    }
    getHistoryOfSelectedVehicle() {
        if (new Date(this.datetimeTo).toISOString() <= new Date(this.datetimeFrom).toISOString()) {
            this.toastCtrl.create({
                message: 'Please select valid time and try again..(to time always greater than from Time).',
                duration: 3000,
                position: 'bottom'
            }).then((toastEl) => {
                toastEl.present();
            });
            return;
        }
        this.loadingController.create({
            message: 'Loading history data...',
        }).then((loadEl) => {
            loadEl.present();
            this.getHistoryData(loadEl);
        });
    }
    getDistanceAndSpeed(loadEl) {
        var url = this.constUrl.mainUrl + "gps/getDistanceSpeed?imei=" + this.vehObj.Device_ID + "&from=" + new Date(this.datetimeFrom).toISOString() + "&to=" + new Date(this.datetimeTo).toISOString();
        this.authService.getMethod(url)
            .subscribe(respData => {
            loadEl.dismiss();
            if (respData != {}) {
                var res = JSON.parse(JSON.stringify(respData));
                console.log("check history: " + res.Distance);
            }
        }, err => {
            console.log(err);
            loadEl.dismiss();
        });
    }
    getHistoryData(loadEl) {
        var url1 = this.constUrl.mainUrl + "gps/v2?id=" + this.vehObj.Device_ID + "&from=" + new Date(this.datetimeFrom).toISOString() + "&to=" + new Date(this.datetimeTo).toISOString();
        this.authService.getMethod(url1)
            .subscribe(respData => {
            loadEl.dismiss();
            if (respData != undefined) {
                var res = JSON.parse(JSON.stringify(respData));
                console.log("res gps data: ", res);
                if (res.length > 1) {
                    this.plotHstory(res.reverse());
                }
            }
        }, err => {
            loadEl.dismiss();
            console.log(err);
        });
    }
    plotHstory(data) {
        let that = this;
        that.dataArrayCoords = [];
        for (var i = 0; i < data.length; i++) {
            if (data[i].lat && data[i].lng) {
                var arr = [];
                var cumulativeDistance = 0;
                var startdatetime = new Date(data[i].insertionTime);
                arr.push(data[i].lat);
                arr.push(data[i].lng);
                arr.push({ "time": startdatetime.toLocaleString() });
                arr.push({ "speed": data[i].speed });
                arr.push({ "imei": data[i].imei });
                if (data[i].isPastData != true) {
                    if (i === 0) {
                        cumulativeDistance += 0;
                    }
                    else {
                        cumulativeDistance += data[i].distanceFromPrevious ? parseFloat(data[i].distanceFromPrevious) : 0;
                    }
                    data[i]['cummulative_distance'] = (cumulativeDistance).toFixed(2);
                    arr.push({ "cumu_dist": data[i]['cummulative_distance'] });
                }
                else {
                    data[i]['cummulative_distance'] = (cumulativeDistance).toFixed(2);
                    arr.push({ "cumu_dist": data[i]['cummulative_distance'] });
                }
                that.dataArrayCoords.push(arr);
            }
        }
        that.mapData = [];
        that.mapData = data.map(function (d) {
            return { lat: d.lat, lng: d.lng };
        });
        that.mapData.reverse();
        this.drawOnMap();
    }
    drawOnMap() {
        let that = this;
        let bounds = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["LatLngBounds"](that.mapData);
        that.map.animateCamera({
            target: bounds
        });
        that.map.addMarker({
            title: 'D',
            position: that.mapData[0],
            icon: 'red',
            styles: {
                'text-align': 'center',
                'font-style': 'italic',
                'font-weight': 'bold',
                'color': 'red'
            },
        }).then((marker) => {
            marker.showInfoWindow();
            that.map.addMarker({
                title: 'S',
                position: that.mapData[that.mapData.length - 1],
                icon: 'green',
                styles: {
                    'text-align': 'center',
                    'font-style': 'italic',
                    'font-weight': 'bold',
                    'color': 'green'
                },
            }).then((marker) => {
                marker.showInfoWindow();
            });
        });
        that.map.addPolyline({
            points: that.mapData,
            color: '#635400',
            width: 3,
            geodesic: true
        });
        // let mapOptions = {
        //   gestures: {
        //     rotate: false,
        //     tilt: true
        //   }
        // }
    }
    animateHistory() {
        let that = this;
        // that.showZoom = true;
        if (localStorage.getItem("markerTarget") != null) {
            that.target = JSON.parse(localStorage.getItem("markerTarget"));
        }
        that.playPause = !that.playPause; // This would alternate the state each time
        var coord = that.dataArrayCoords[that.target];
        // that.coordreplaydata = coord;
        var lat = coord[0];
        var lng = coord[1];
        var startPos = [lat, lng];
        that.speed = 200; // km/h
        if (that.playPause) {
            that.map.setCameraTarget({ lat: lat, lng: lng });
            if (that.mark == undefined) {
                debugger;
                var icicon;
                // let icon: MarkerIcon = {
                //   url: 'http://icons.iconarchive.com/icons/iconarchive/red-orb-alphabet/24/Number-1-icon.png',
                //   // url: './assets/imgs/running' + that.vehObj.iconType + '.png',
                //   size: {
                //     width: 50,
                //     height: 50
                //   }
                // };
                // console.log("icon object: ", icon)
                if (that.plt.is('ios')) {
                    icicon = 'www/assets/imgs/running' + that.vehObj.iconType + '.png';
                }
                else if (that.plt.is('android')) {
                    icicon = './assets/imgs/running' + that.vehObj.iconType + '.png';
                }
                that.map.addMarker({
                    icon: {
                        url: icicon,
                        size: {
                            height: 40,
                            width: 40
                        }
                    },
                    // icon: icon,
                    // styles: {
                    //   'text-align': 'center',
                    //   'font-style': 'italic',
                    //   'font-weight': 'bold',
                    //   'color': 'green'
                    // },
                    position: new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["LatLng"](startPos[0], startPos[1]),
                }).then((marker) => {
                    that.mark = marker;
                    that.liveTrack(that.map, that.mark, that.dataArrayCoords, that.target, startPos, that.speed, 100);
                });
            }
            else {
                // debugger
                // that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
                that.liveTrack(that.map, that.mark, that.dataArrayCoords, that.target, startPos, that.speed, 100);
            }
        }
        else {
            that.mark.setPosition(new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["LatLng"](startPos[0], startPos[1]));
        }
    }
    liveTrack(map, mark, coords, target, startPos, speed, delay) {
        let that = this;
        // that.events.subscribe("SpeedValue:Updated", (sdata) => {
        //   speed = sdata;
        // })
        var target = target;
        clearTimeout(that.ongoingGoToPoint[coords[target][4].imei]);
        clearTimeout(that.ongoingMoveMarker[coords[target][4].imei]);
        console.log("check coord imei: ", coords[target][4].imei);
        if (!startPos.length)
            coords.push([startPos[0], startPos[1]]);
        function _gotoPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000;
            if (coords[target] == undefined)
                return;
            var dest = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["LatLng"](coords[target][0], coords[target][1]);
            var distance = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["Spherical"].computeDistanceBetween(dest, mark.getPosition()); //in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target][0] - lat) / numStep;
            var deltaLng = (coords[target][1] - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["Spherical"].computeHeading(mark.getPosition(), new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["LatLng"](lat, lng));
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["LatLng"](lat, lng));
                    map.setCameraTarget(new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["LatLng"](lat, lng));
                    that.ongoingMoveMarker[coords[target][4].imei] = setTimeout(_moveMarker, delay);
                }
                else {
                    head = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["Spherical"].computeHeading(mark.getPosition(), dest);
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(dest);
                    map.setCameraTarget(dest);
                    target++;
                    that.ongoingGoToPoint[coords[target][4].imei] = setTimeout(_gotoPoint, delay);
                }
            }
            a++;
            if (a > coords.length) {
            }
            else {
                // that.speedMarker = coords[target][3].speed;
                // that.updatetimedate = coords[target][2].time;
                // that.cumu_distance = coords[target][5].cumu_dist;
                if (that.playPause) {
                    _moveMarker();
                    target = target;
                    localStorage.setItem("markerTarget", target);
                }
                else { }
                // km_h = km_h;
            }
        }
        var a = 0;
        _gotoPoint();
    }
};
HistoryPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: src_app_app_model__WEBPACK_IMPORTED_MODULE_6__["URLs"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] }
];
HistoryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-history',
        template: __webpack_require__(/*! raw-loader!./history.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/history/history.page.html"),
        styles: [__webpack_require__(/*! ./history.page.scss */ "./src/app/tabs/dashboard/history/history.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
        src_app_app_model__WEBPACK_IMPORTED_MODULE_6__["URLs"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
        src_app_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]])
], HistoryPage);



/***/ })

}]);
//# sourceMappingURL=dashboard-history-history-module-es2015.js.map
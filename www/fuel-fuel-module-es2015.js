(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["fuel-fuel-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/fuel/fuel.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/fuel/fuel.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"primary\">\n        <ion-buttons slot=\"start\">\n            <ion-menu-button></ion-menu-button>\n        </ion-buttons>\n        <ion-title>Fuel</ion-title>\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n    <ion-grid>\n        <ion-row>\n            <ion-col size-sm=\"6\" offset-sm=\"3\">\n                <ion-list>\n                    <ion-item lines=\"full\" detail\n                        [routerLink]=\"['/', 'fuel', 'fuel-chart']\">\n                        <ion-icon src=\"assets/imgs/icons/ecology.svg\" slot=\"start\"></ion-icon>\n                        <ion-label>Fuel Chart</ion-label>\n                    </ion-item>\n                    <ion-item lines=\"full\" detail\n                        [routerLink]=\"['/', 'fuel', 'fuel-consumption']\">\n                        <ion-icon src=\"assets/imgs/icons/ecology.svg\" slot=\"start\"></ion-icon>\n                        <ion-label>Fuel Consumption</ion-label>\n                    </ion-item>\n                    <ion-item lines=\"full\" detail\n                        [routerLink]=\"['/', 'fuel', 'fuel-entry']\">\n                        <ion-icon src=\"assets/imgs/icons/ecology.svg\" slot=\"start\"></ion-icon>\n                        <ion-label>Fuel Entry</ion-label>\n                    </ion-item>\n                </ion-list>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/fuel/fuel.module.ts":
/*!*************************************!*\
  !*** ./src/app/fuel/fuel.module.ts ***!
  \*************************************/
/*! exports provided: FuelPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelPageModule", function() { return FuelPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _fuel_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fuel.page */ "./src/app/fuel/fuel.page.ts");





// import { IonicSelectableModule } from 'ionic-selectable';


const routes = [
    {
        path: '',
        component: _fuel_page__WEBPACK_IMPORTED_MODULE_6__["FuelPage"]
    }
];
let FuelPageModule = class FuelPageModule {
};
FuelPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
        ],
        declarations: [_fuel_page__WEBPACK_IMPORTED_MODULE_6__["FuelPage"]]
    })
], FuelPageModule);



/***/ }),

/***/ "./src/app/fuel/fuel.page.ts":
/*!***********************************!*\
  !*** ./src/app/fuel/fuel.page.ts ***!
  \***********************************/
/*! exports provided: FuelPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelPage", function() { return FuelPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FuelPage = class FuelPage {
    constructor() {
    }
};
FuelPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        template: __webpack_require__(/*! raw-loader!./fuel.page.html */ "./node_modules/raw-loader/index.js!./src/app/fuel/fuel.page.html")
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], FuelPage);



/***/ })

}]);
//# sourceMappingURL=fuel-fuel-module-es2015.js.map
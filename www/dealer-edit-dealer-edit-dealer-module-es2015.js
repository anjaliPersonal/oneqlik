(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dealer-edit-dealer-edit-dealer-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dealer/edit-dealer/edit-dealer.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dealer/edit-dealer/edit-dealer.page.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>editDealer</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/tabs/dealer/edit-dealer/edit-dealer.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/tabs/dealer/edit-dealer/edit-dealer.module.ts ***!
  \***************************************************************/
/*! exports provided: EditDealerPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditDealerPageModule", function() { return EditDealerPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _edit_dealer_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./edit-dealer.page */ "./src/app/tabs/dealer/edit-dealer/edit-dealer.page.ts");







const routes = [
    {
        path: '',
        component: _edit_dealer_page__WEBPACK_IMPORTED_MODULE_6__["EditDealerPage"]
    }
];
let EditDealerPageModule = class EditDealerPageModule {
};
EditDealerPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_edit_dealer_page__WEBPACK_IMPORTED_MODULE_6__["EditDealerPage"]]
    })
], EditDealerPageModule);



/***/ }),

/***/ "./src/app/tabs/dealer/edit-dealer/edit-dealer.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/tabs/dealer/edit-dealer/edit-dealer.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvZGVhbGVyL2VkaXQtZGVhbGVyL2VkaXQtZGVhbGVyLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/tabs/dealer/edit-dealer/edit-dealer.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/tabs/dealer/edit-dealer/edit-dealer.page.ts ***!
  \*************************************************************/
/*! exports provided: EditDealerPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditDealerPage", function() { return EditDealerPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let EditDealerPage = class EditDealerPage {
    constructor() { }
    ngOnInit() {
    }
};
EditDealerPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit-dealer',
        template: __webpack_require__(/*! raw-loader!./edit-dealer.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dealer/edit-dealer/edit-dealer.page.html"),
        styles: [__webpack_require__(/*! ./edit-dealer.page.scss */ "./src/app/tabs/dealer/edit-dealer/edit-dealer.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EditDealerPage);



/***/ })

}]);
//# sourceMappingURL=dealer-edit-dealer-edit-dealer-module-es2015.js.map
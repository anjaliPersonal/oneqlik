(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["fuel-fuel-consumption-fuel-consumption-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/fuel/fuel-consumption/fuel-consumption.page.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/fuel/fuel-consumption/fuel-consumption.page.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/fuel\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Fuel Consumption</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/fuel/fuel-consumption/fuel-consumption.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/fuel/fuel-consumption/fuel-consumption.module.ts ***!
  \******************************************************************/
/*! exports provided: FuelConsumptionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelConsumptionPageModule", function() { return FuelConsumptionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _fuel_consumption_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fuel-consumption.page */ "./src/app/fuel/fuel-consumption/fuel-consumption.page.ts");







const routes = [
    {
        path: '',
        component: _fuel_consumption_page__WEBPACK_IMPORTED_MODULE_6__["FuelConsumptionPage"]
    }
];
let FuelConsumptionPageModule = class FuelConsumptionPageModule {
};
FuelConsumptionPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_fuel_consumption_page__WEBPACK_IMPORTED_MODULE_6__["FuelConsumptionPage"]]
    })
], FuelConsumptionPageModule);



/***/ }),

/***/ "./src/app/fuel/fuel-consumption/fuel-consumption.page.scss":
/*!******************************************************************!*\
  !*** ./src/app/fuel/fuel-consumption/fuel-consumption.page.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Z1ZWwvZnVlbC1jb25zdW1wdGlvbi9mdWVsLWNvbnN1bXB0aW9uLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/fuel/fuel-consumption/fuel-consumption.page.ts":
/*!****************************************************************!*\
  !*** ./src/app/fuel/fuel-consumption/fuel-consumption.page.ts ***!
  \****************************************************************/
/*! exports provided: FuelConsumptionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelConsumptionPage", function() { return FuelConsumptionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FuelConsumptionPage = class FuelConsumptionPage {
    constructor() { }
    ngOnInit() {
    }
};
FuelConsumptionPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fuel-consumption',
        template: __webpack_require__(/*! raw-loader!./fuel-consumption.page.html */ "./node_modules/raw-loader/index.js!./src/app/fuel/fuel-consumption/fuel-consumption.page.html"),
        styles: [__webpack_require__(/*! ./fuel-consumption.page.scss */ "./src/app/fuel/fuel-consumption/fuel-consumption.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], FuelConsumptionPage);



/***/ })

}]);
//# sourceMappingURL=fuel-fuel-consumption-fuel-consumption-module-es2015.js.map